# Summary

Date : 2022-11-09 13:49:43

Directory /var/www/html

Total : 1 files,  8 codes, 0 comments, 1 blanks, all 9 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| jsonc | 1 | 8 | 0 | 1 | 9 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 1 | 8 | 0 | 1 | 9 |
| .expo | 1 | 8 | 0 | 1 | 9 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)