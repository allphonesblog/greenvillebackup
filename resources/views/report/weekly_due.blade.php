@extends('layouts.master')
@section('title')Weekly Due
@endsection
@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">

               Weekly Due
            </h3>
 
          
        </div>

        <div class="box-body table-responsive">

            <br>
            <table id="data-table" class="table table-bordered table-striped table-condensed table-hover">
                <thead>
                    <tr>
                        <td>
                            <?php if(isset($_GET['status'])):?>
                            <a href="#" onclick="c_all()">Toggle check</a>
<?php else:?>
                            -
                            <?php endif;?>

                        
                        </td>
                    <th>{{trans_choice('general.borrower',1)}}</th>

                    <th>#</th>
                    <th>Loan Officer</th>
                    <th>Branch</th>
                    <th>{{trans_choice('general.principal',1)}}</th>
                    <th>{{trans_choice('general.released',1)}}</th>
                    <th>{{trans_choice('general.interest',1)}}%</th>
                    <th>{{trans_choice('general.due',1)}}</th>
                    <th>{{trans_choice('general.paid',1)}}</th>
                        <th>{{trans_choice('general.balance',1)}}</th>
                        <th>{{trans_choice('general.status',1)}}</th>
                        <th>{{ trans_choice('general.action',1) }}</th>

                    </tr>
                </thead>
                
                
                <tfoot>
                    <tr>
<td>-</td>
                        <th>{{trans_choice('general.borrower',1)}}</th>

                        <th>#</th>
                        <th>Loan Officer</th>
                        <th>Branch</th>
                        <th>{{trans_choice('general.principal',1)}}</th>
                        <th>{{trans_choice('general.released',1)}}</th>
                        <th>{{trans_choice('general.interest',1)}}%</th>
                        <th>{{trans_choice('general.due',1)}}</th>
                        <th>{{trans_choice('general.paid',1)}}</th>
                        <th>{{trans_choice('general.balance',1)}}</th>
                        <th>{{trans_choice('general.status',1)}}</th>
                        <th>{{ trans_choice('general.action',1) }}</th>

                    </tr>
                </tfoot>

            </table>
        </div>
        <!-- /.box-body -->

</div>
<!-- /.box -->
@endsection

@section('footer-scripts')
<script src="{{ asset('assets/plugins/datatable/media/js/jquery.dataTables.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/media/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.colVis.min.js')}}"></script>
<script src="https://cdn.datatables.net/scroller/2.0.3/js/dataTables.scroller.min.js"></script>

 


<script>
    function disburse(){

        $("#disburseLoan").modal("show")


    }
</script>
<script>
    $(document).ready(function () {
      
            var url = "/weekly_data?status="+"{{$status}}"
          
         $('#data-table').DataTable({
             "order": [[ 3, "desc" ]],
             "processing": true,
             "serverSide": true,
             searchDelay: 2000,
             "ajax":{
                 "url": url,
                 "dataType": "json",
                 "type": "POST",
                 "data":{ _token: "{{csrf_token()}}"}
             },

             "columns": [
                 { "data": "select" },
                 { "data": "borrowers" },
                 { "data": "id" },
                 { "data": "loan_officer" },
                 { "data": "branch" },
                 { "data": "principal" },
                 { "data": "released" },
                 { "data": "interest" },
                 { "data": "due" },
                 { "data": "paid" },

                 { "data": "balance" },
                 { "data": "status" },
                 { "data": "action" }
             ]	 ,
             aoColumnDefs: [
                 {
                     bSortable: false,
                     aTargets: [ 0 ]
                 }
             ]
        });
    });
  
</script>
@endsection
