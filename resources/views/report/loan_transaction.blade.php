@extends('layouts.master')
@section('title')
    Loan Repayment
@endsection
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
               Loan Repayment
                @if(!empty($start_date))
                    for period: <b>{{$start_date}} to {{$end_date}}</b>
                @endif
            </h3>

            <div class="box-tools pull-right">
                <button class="btn btn-sm btn-info hidden-print" onclick="window.print()">Print</button>
            </div>
        </div>
        <div class="box-body hidden-print">
            <h4 class="">{{trans_choice('general.date',1)}} {{trans_choice('general.range',1)}}</h4>
         {!! Form::open(array('url' => Request::url(), 'method' => 'post','class'=>'form-horizontal filterx', 'name' => 'form')) !!}
            <div class="row">
                <div class="col-xs-4">
                    {!! Form::text('start_date',null, array('class' => 'form-control date-picker start_date', 'placeholder'=>"From Date",'required'=>'required')) !!}
                </div>
                <div class="col-xs-4">
                    {!! Form::text('end_date',null, array('class' => 'form-control date-picker end_date', 'placeholder'=>"To Date",'required'=>'required')) !!}
                </div>

               <div class="col-xs-4">
                    <label>Loan Officer</label>
                    <select class="form-control" name="loan_officer" id="loan_officer">
                        <option value="">select loan officer</option>
                        @foreach($users as $u)
                        <option value="{{$u->id}}">{{$u->first_name}} {{$u->last_name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-xs-4" style="display:none" id="sgroup">
                    <label>Select Group</label>
                    <select class="form-control" name="loan_officer" id="group">
                        <option value="">select group</option>

                    </select>
                </div>

                <div class="col-xs-4">
                    <label>Search Type</label>
                    <select class="form-control" name="loan_officer" id="loan_type" >
                        <option value="">select loan type</option>
                        <option value="individual">Individual Loan</option>
                        <option value="group">Group Loan</option>
                    </select>
                </div>


                @if(session("branch_id") == 1)
                <div class="col-xs-4">
                    <label>Select Branch</label>
                    <select class="form-control" name="loan_officer" id="branch" >
                        <option value="">select Branch</option>
                        @foreach($branch as $b)
                        <option value="{{$b->id}}">{{$b->name}}</option>
                        @endforeach
                    </select>
                </div>
                @endif

             <!--   <div class="col-xs-2 " style="    margin-top: 21px;">
                    <label>Select Unpaid</label>
                    <br>
                    <input type="checkbox" class="su">

                </div>//-->


            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-2">
                        <span class="input-group-btn">
                            <button type="submit" class="btn bg-olive btn-flat">{{trans_choice('general.search',1)}}!
                            </button>
                        </span>
                        <span class="input-group-btn">
                            <a href="{{Request::url()}}"
                               class="btn bg-purple  btn-flat pull-right">{{trans_choice('general.reset',1)}}!</a>
                        </span>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}


        </div>
        <!-- /.box-body -->

</div>
<!-- /.box -->
<div class="row">

    <div class="col-md-2">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Principal</h3>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="principal">

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>



    <div class="col-md-2">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Interest</h3>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="interest">

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>

    
    <div class="col-md-2">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Balance</h3>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="balance">

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>

    <div class="col-md-2">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Total Borrower</h3>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="totalb">

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>


</div>

<div class="box box-info">
    <div class="box-body table-responsive no-padding">

        <table id="view-repayments"
               class="table table-bordered table-condensed table-hover dataTable no-footer">
            <thead>
                <tr style="background-color: #D1F9FF">
                    <th>{{trans_choice('general.branch',1)}}</th>

                    <th>{{trans_choice('general.borrower',1)}}</th>

                    <th>{{trans_choice('general.principal',1)}}</th>

                    <th>{{trans_choice('general.interest',1)}}</th>

 
 
                    <th>{{trans_choice('general.balance',1)}}</th>

                    
                    <th>Total borrowers</th>
                    



                </tr>
                </thead>


            </table>


        </div>
    </div>
@endsection

@section('footer-scripts')
<script src="{{ asset('assets/plugins/datatable/media/js/jquery.dataTables.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/media/js/dataTables.bootstrap.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.colVis.min.js')}}"></script>

<script>
    $(".filterx").submit(function(){
        event.preventDefault()
        $("#totalam").html("loading...")
        $("#principal").html("loading..")
        $("#balance").html("loading..")
        $("#due").html("loading..")
        $("#totalb").html("loading..")
        $("#paid").html("loading..")
        $("#interest").html("loading..")
        $("#view-repayments").dataTable().fnDestroy()
        const start_date = $(".start_date").val()
        const end_date = $(".end_date").val()

        const loan_officer = $("#loan_officer").find(":selected").val()
        const loan_type = $("#loan_type").find(":selected").val()
        const group = $("#group").find(":selected").val()
        @if(session("branch_id") == 1)
            const branch = $("#branch").find(":selected").val()
            @else

                const branch = {{session("branch_id")}}
                @endif
        const su = "";
//         var su = $('.su').iCheck('update')[0].checked
       /*  if(su == true){
            su = "on";
        }else{
            su = "";
        } */
       var table =  $('#view-repayments').DataTable({
           "ajax":{
               "url": "/report/apiloan_transaction",
               "dataType": "json",
               "type": "POST",
               "data":{ _token: "{{csrf_token()}}",su:su,start_date:start_date,end_date:end_date,loan_officer:loan_officer,loan_type:loan_type,group:group,branch:branch}
           },
    "drawCallback": function (settings) { 
        // Here the response
        var response = settings.json;
        if(response != undefined){

            $("#balance").html(`{{\App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value}} ${response.balance}`)
            $("#principal").html(`{{\App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value}} ${response.principal}`)
            $("#interest").html(`{{\App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value}} ${response.interest}`)
            $("#totalb").html(` ${response.totalb}`)

        }
        console.log(response);
    }
       });

    })
    $("#loan_type").change(function(){
        if($(this).find(":selected").val() == "group"){
            $("#sgroup").show()
        }else{
            $("#sgroup").hide()
        }
    })
    $("#branch").change(function(){
        $.post('/loanofficer',{
            id:$(this).find(":selected").val(),
            "_token":"{{csrf_token()}}"
        }).then(function(res){
            $(`#loan_officer`).html('  <option value="">select loan officer</option>')
            res.users.map(function(r){
                $("#loan_officer").append(`  <option value="${r.id}">${r.first_name} ${r.last_name}</option>`)
            })
        })
    })
    $("#loan_officer").change(function(){
        $.post('/searchgrp',{
            id:$(this).find(":selected").val(),
            "_token":"{{csrf_token()}}"
        }).then(function(res){
            $(`#group`).html('  <option value="">select group</option>')
            res.data.map(function(r){
                $("#group").append(`  <option value="${r.id}">${r.name}</option>`)
            })
        })
    })
</script>
@endsection
