@extends('layouts.master')
@section('title')
My Complain
@endsection
@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">My Complain</h3>


    </div>
    <div class="box-body ">
        <div class="table-responsive">
            <table id="data-table" class="table table-bordered table-condensed table-hover">
                <thead>
                    <tr style="background-color: #D1F9FF">

                       <th>Title</th>
                        <th>Complain</th>
                        <th>Status</th>
                        <th>Created At</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $key)
                    <tr>
                         <td>{{$key->title}}</td>
                        <td>{{$key->text}}</td>
                        <td>{{$key->status == null ? 'Pending' : 'Fixed'}}</td>
                        <td>{{$key->created_at}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
@endsection
@section('footer-scripts')
<script src="{{ asset('assets/plugins/datatable/media/js/jquery.dataTables.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/media/js/dataTables.bootstrap.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.colVis.min.js')}}"></script>
<script>



    $('#data-table').DataTable();
</script>
@endsection