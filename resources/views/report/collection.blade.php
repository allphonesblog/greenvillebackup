@extends('layouts.master')
@section('title'){{trans_choice('general.collection',1)}} {{trans_choice('general.report',1)}}
@endsection
@section('content')

    <p>Please note that <b>Total Loans</b> may not equal <b>Principal Loans</b> + <b>Interest Loans</b> + <b>Penalty
            Loans</b> + <b>Fees Loans</b> if you have overriden the total due amount of any loan.</p>
    <div class="box box-primary">
        <div class="box-header with-border">


            <div class="box-tools pull-right">
                <button class="btn btn-sm btn-info hidden-print"
                        onclick="window.print()">{{trans_choice('general.print',1)}}</button>
            </div>
        </div>
        <div class="box-body hidden-print">
            <h4 class="">{{trans_choice('general.date',1)}} {{trans_choice('general.range',1)}}</h4>
            {!! Form::open(array('url' => Request::url(), 'method' => 'post','class'=>'form-horizontal filterx', 'name' => 'form')) !!}
            <div class="row">
                <div class="col-xs-5">
                    {!! Form::text('start_date',null, array('class' => 'form-control date-picker from', 'placeholder'=>"From Date",'required'=>'required')) !!}
                </div>
                <div class="col-xs-1  text-center" style="padding-top: 5px;">
                    to
                </div>
                <div class="col-xs-5">
                    {!! Form::text('end_date',null, array('class' => 'form-control date-picker to', 'placeholder'=>"To Date",'required'=>'required')) !!}
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-2">
                        <span class="input-group-btn">
                          <button type="submit" class="btn bg-olive btn-flat">{{trans_choice('general.search',1)}}!
                          </button>
                        </span>
                        <span class="input-group-btn">
                          <a href="{{Request::url()}}"
                             class="btn bg-purple  btn-flat pull-right">{{trans_choice('general.reset',1)}}!</a>
                        </span>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
        <!-- /.box-body -->

    </div>
    <!-- /.box -->
    <div class="row" id="num1">

    </div>
    <div class="row" id="num2"> 

    </div>
    <h4>Monthly </h4>
    <div class="row">
        <div class="col-md-12">
            <!-- AREA CHART -->
            <!-- LINE CHART -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><span
                                style="color: #ff0000">{{trans_choice('general.due',1)}} {{trans_choice('general.amount',1)}}</span>
                        /
                        <span style="color: #00a65a">{{trans_choice('general.paid',1)}} {{trans_choice('general.amount',1)}}</span>
                    </h3>

                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body" style="display: block;">
                    <div id="operatingProfit" style="height: 350px;">

                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

@endsection
@section('footer-scripts')
    <script src="{{ asset('assets/plugins/amcharts/amcharts.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/amcharts/serial.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/amcharts/pie.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/amcharts/themes/light.js') }}"
            type="text/javascript"></script>
    <script>

        $(document).ready(function(){
            $.get("/apicollection_report").then(function(res){
$("#num1").html(res.num1)
                $("#num2").html(res.num2)
                AmCharts.makeChart("operatingProfit", {
                    "type": "serial",
                    "theme": "light",
                    "autoMargins": true,
                    "marginLeft": 30,
                    "marginRight": 8,
                    "marginTop": 10,
                    "marginBottom": 26,
                    "fontFamily": 'Open Sans',
                    "color": '#888',

                    "dataProvider": res.monthly_collections,
                    "valueAxes": [{
                        "axisAlpha": 0,

                    }],
                    "startDuration": 1,
                    "graphs": [{
                        "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b> [[value]]</b> [[additional]]</span>",
                        "lineAlpha": 0,
                        "fillColors": "#ff0000",
                        "fillAlphas": 1,
                        "title": "{{trans_choice('general.due',1)}} {{trans_choice('general.amount',1)}}",
                        "type": "column",
                        "valueField": "due"
                    }, {
                        "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b> [[value]]</b> [[additional]]</span>",
                        "lineAlpha": 0,
                        "fillColors": "#00a65a",
                        "fillAlphas": 1,
                        "title": "{{trans_choice('general.paid',1)}} {{trans_choice('general.amount',1)}}",
                        "type": "column",
                        "valueField": "payments"
                    }],
                    "categoryField": "month",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "axisAlpha": 0,
                        "tickLength": 0,
                        "labelRotation": 30,

                    }


                }).addLegend(new AmCharts.AmLegend());

            })
        })
        $(".filterx").submit(function(){
            $("#num1").html("<b>Loading..</b>")
            $("#num2").html("<b>Loading..</b>")
            event.preventDefault();
            $.post("/apicollection_report",{
                start_date: $(".from").val(),
                end_date:$(".to").val(),
                "_token":"{{csrf_token()}}"
            }).then(function(res){
                $("#num1").html(res.num1)
                $("#num2").html(res.num2)
                AmCharts.makeChart("operatingProfit", {
                    "type": "serial",
                    "theme": "light",
                    "autoMargins": true,
                    "marginLeft": 30,
                    "marginRight": 8,
                    "marginTop": 10,
                    "marginBottom": 26,
                    "fontFamily": 'Open Sans',
                    "color": '#888',

                    "dataProvider": res.monthly_collections,
                    "valueAxes": [{
                        "axisAlpha": 0,

                    }],
                    "startDuration": 1,
                    "graphs": [{
                        "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b> [[value]]</b> [[additional]]</span>",
                        "lineAlpha": 0,
                        "fillColors": "#ff0000",
                        "fillAlphas": 1,
                        "title": "{{trans_choice('general.due',1)}} {{trans_choice('general.amount',1)}}",
                        "type": "column",
                        "valueField": "due"
                    }, {
                        "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b> [[value]]</b> [[additional]]</span>",
                        "lineAlpha": 0,
                        "fillColors": "#00a65a",
                        "fillAlphas": 1,
                        "title": "{{trans_choice('general.paid',1)}} {{trans_choice('general.amount',1)}}",
                        "type": "column",
                        "valueField": "payments"
                    }],
                    "categoryField": "month",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "axisAlpha": 0,
                        "tickLength": 0,
                        "labelRotation": 30,

                    }


                }).addLegend(new AmCharts.AmLegend());

            })
        })
    </script>
@endsection
