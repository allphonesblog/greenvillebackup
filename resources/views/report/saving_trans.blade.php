@extends('layouts.master')
@section('title')
Savings Transactions
@endsection
@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">
            {{trans_choice('general.loan',1)}} {{trans_choice('general.transaction',2)}}
            @if(!empty($start_date))
            for period: <b>{{$start_date}} to {{$end_date}}</b>
            @endif
        </h3>

        <div class="box-tools pull-right">
            <button class="btn btn-sm btn-info hidden-print" onclick="window.print()">Print</button>
        </div>
    </div>
    <div class="box-body hidden-print">
        <h4 class="">{{trans_choice('general.date',1)}} {{trans_choice('general.range',1)}}</h4>
        {!! Form::open(array('url' => Request::url(), 'method' => 'post','class'=>'form-horizontal filterx', 'name' => 'form')) !!}
        <div class="row">
            <div class="col-xs-4">
                {!! Form::text('start_date',null, array('class' => 'form-control date-picker start_date', 'placeholder'=>"From Date",'required'=>'required')) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::text('end_date',null, array('class' => 'form-control date-picker end_date', 'placeholder'=>"To Date",'required'=>'required')) !!}
            </div>


        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-2">
                    <span class="input-group-btn">
                        <button type="submit" class="btn bg-olive btn-flat">{{trans_choice('general.search',1)}}!
                        </button>
                    </span>
                    <span class="input-group-btn">
                        <a href="{{Request::url()}}"
                           class="btn bg-purple  btn-flat pull-right">{{trans_choice('general.reset',1)}}!</a>
                    </span>
                </div>
            </div>
        </div>
        {!! Form::close() !!}


    </div>
    <!-- /.box-body -->

</div>
<!-- /.box -->
<div class="row">

    <div class="col-md-2">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Balance</h3>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="balance">

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>




</div>

<div class="box box-info">
    <div class="box-body table-responsive no-padding">

        <table id="view-repayments"
               class="table table-bordered table-condensed table-hover dataTable no-footer">
            <thead>
                <tr style="background-color: #D1F9FF">
                    <th>{{trans_choice('general.borrower',1)}}</th>
                    <th>{{trans_choice('general.amount',1)}}</th>
                  




                </tr>
            </thead>


        </table>


    </div>
</div>
@endsection

@section('footer-scripts')
<script src="{{ asset('assets/plugins/datatable/media/js/jquery.dataTables.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/media/js/dataTables.bootstrap.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.colVis.min.js')}}"></script>

<script>
    $(".filterx").submit(function(){
        event.preventDefault()

        $("#balance").html("loading..")

        $("#view-repayments").dataTable().fnDestroy()
        const start_date = $(".start_date").val()
        const end_date = $(".end_date").val()

        var table =  $('#view-repayments').DataTable({
            "ajax":{
                "url": "/report/apisaving_transactions",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}",start_date:start_date,end_date:end_date}
            }   

        });

        table.on( 'xhr', function () {
            var response = table.ajax.json();

            $("#balance").html(`{{\App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value}} ${response.balance}`)

        } );

    })

</script>
@endsection