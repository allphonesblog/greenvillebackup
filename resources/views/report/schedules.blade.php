@extends('layouts.master')
@section('title')
   Expected Revenue
@endsection
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
               Loan Repayment
                @if(!empty($start_date))
                    for period: <b>{{$start_date}} to {{$end_date}}</b>
                @endif
            </h3>

            <div class="box-tools pull-right">
                <button class="btn btn-sm btn-info hidden-print" onclick="window.print()">Print</button>
            </div>
        </div>
        <div class="box-body hidden-print">
        <h4 class="date_type"></h4>
       <div class="form-horizontal filterx">

         
            <div class="row">
                <div id="rangee" style="display:none">

                <div class="col-xs-4">
                    {!! Form::text('start_date',null, array('class' => 'form-control date-picker start_date', 'placeholder'=>"From Date",'required'=>'required')) !!}
                </div>
                <div class="col-xs-4">
                    {!! Form::text('end_date',null, array('class' => 'form-control date-picker end_date', 'placeholder'=>"To Date",'required'=>'required')) !!}
                </div>
</div>

<div id="rangee2" style="display:none">

<div class="col-xs-4">
    {!! Form::text('start_date',null, array('class' => 'form-control date-picker ndate', 'placeholder'=>"From Date",'required'=>'required')) !!}
</div>
 
</div>

<div class="col-xs-4"  id="">
                    <label>Select Query Type</label>
                    <select class="form-control" name="loan_officer" id="query_type">
                        <option value="">select type</option>
                        <option value="notabove">Due</option>
                        <option value="above">Overdue</option>
                        <option value="expected">Expected Revenue</option>
                     
                    

                    </select>
                </div>



               <div class="col-xs-4">
                    <label>Loan Officer</label>
                    <select class="form-control" name="loan_officer" id="loan_officer">
                        <option value="">select loan officer</option>
                        @foreach($users as $u)
                        <option value="{{$u->id}}">{{$u->first_name}} {{$u->last_name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-xs-4" style="display:none" id="sgroup">
                    <label>Select Group</label>
                    <select class="form-control" name="loan_officer" id="group">
                        <option value="">select group</option>

                    </select>
                </div>


              

                


                @if(session("branch_id") == 1)
                <div class="col-xs-4">
                    <label>Select Branch</label>
                    <select class="form-control" name="loan_officer" id="branch" >
                        <option value="">select Branch</option>
                        @foreach($branch as $b)
                        <option value="{{$b->id}}">{{$b->name}}</option>
                        @endforeach
                    </select>
                </div>
                @endif

             <!--   <div class="col-xs-2 " style="    margin-top: 21px;">
                    <label>Select Unpaid</label>
                    <br>
                    <input type="checkbox" class="su">

                </div>//-->


            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-2">
                        <span class="input-group-btn">
                            <button type="submit" id="filterx" class="btn bg-olive btn-flat">{{trans_choice('general.search',1)}}!
                            </button>
                        </span>
                        <span class="input-group-btn">
                            <a href="{{Request::url()}}"
                               class="btn bg-purple  btn-flat pull-right">{{trans_choice('general.reset',1)}}!</a>
                        </span>
                    </div>
                </div>
            </div>
</div>


        </div>
        <!-- /.box-body -->

</div>
<!-- /.box -->
<div class="row">

    <div class="col-md-2"  style="display:none" id="one">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Expected revenue</h3>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="expected_">

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>



    <div class="col-md-2"  style="display:none" id="two">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Overdue</h3>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="above_">

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>

    
    <div class="col-md-2" style="display:none" id="three">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Due</h3>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="not_above_">

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
 


</div>

<div class="box box-info">
    <div class="box-body table-responsive no-padding">

        <table id="view-repayments"
               class="table table-bordered table-condensed table-hover dataTable no-footer">
            <thead>
                <tr style="background-color: #D1F9FF">
                    <th>{{trans_choice('general.branch',1)}}</th>

                    <th>{{trans_choice('general.borrower',1)}}</th>

                    <th>Phone Number</th>

                    <th>Amount Due</th>

 
 
                    <!-- <th>{{trans_choice('general.balance',1)}}</th> -->
                    



                </tr>
                </thead>


            </table>


        </div>
    </div>
@endsection

@section('footer-scripts')
<script src="{{ asset('assets/plugins/datatable/media/js/jquery.dataTables.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/media/js/dataTables.bootstrap.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.colVis.min.js')}}"></script>

<script>
 
    $("#query_type").change(()=>{
        $("#rangee").show()
    $(".date_type").html("Date Range")
        var type = $("#query_type").find(":selected").val()
        if(type == "expected"){
                $("#expected").show()
            
                $("#notabove").hide() 
                $("#above").hide() 
       
        }else if(type == "above"){
            $("#expected").hide()
                // $("#rangee").hide()
            $("#notabove").hide()
                // $("#rangee2").show() 
                $("#above").show() 
                $(".date_type").html("Date")
        }else if(type == "notabove"){
            $("#expected").hide()
                // $("#rangee").hide()
                $("#above").hide()
                $("#notabove").show()
                // $("#rangee2").show() 
                $(".date_type").html("Date")
        }
    })
    $("#filterx").click(function(){
        $("#view-repayments").dataTable().fnDestroy()
        // event.preventDefault()
        $("#expected").html("loading...")
        $("#above").html("loading..")
        $("#not_above").html("loading..")
       
        const start_date = $(".start_date").val()
        const end_date = $(".end_date").val()

        const loan_officer = $("#loan_officer").find(":selected").val()
        const loan_type = $("#loan_type").find(":selected").val()  
  
            const branch = $("#branch").find(":selected").val()
          
const date = $(".ndate").val()
const query_type = $("#query_type").val()
             



$('#view-repayments').DataTable({
    "order": [[ 3, "desc" ]],
            "processing": true,
            "serverSide": true,
           "ajax":{
               "url": "/report/schedule_report",
               "dataType": "json",
               "type": "POST",
               "data":{
                   
                
        start_date,
        end_date,
        loan_officer,
        loan_type,
        branch,
        date,
        query_type,
        "_token":"{{csrf_token()}}"
       
               }
           },

           "columns": [
                { "data": "branch" },
                { "data": "borrower" },
                { "data": "phone" },
                { "data": "amount" }, 


            ],
    "drawCallback": function (settings) { 
        // Here the response
        var res = settings.json;
        if(res != undefined){

            if(query_type == "expected"){
               $("#one").show()
               $("#two").hide()
               $("#three").hide()
               $("#expected_").html(res.expected)
           }else if(query_type == "above"){
            $("#one").hide()
               $("#two").show()
               $("#three").hide()
            $("#above_").html(res.above)
           }else if(query_type == "notabove"){
            $("#one").hide()
               $("#two").hide()
               $("#three").show()
            $("#not_above_").html(res.notabove)
           }

        }
        // console.log(response);
    }
       });

      

    })
    $("#loan_type").change(function(){
        if($(this).find(":selected").val() == "group"){
            $("#sgroup").show()
        }else{
            $("#sgroup").hide()
        }
    })
    $("#branch").change(function(){
        $.post('/loanofficer',{
            id:$(this).find(":selected").val(),
            "_token":"{{csrf_token()}}"
        }).then(function(res){
            $(`#loan_officer`).html('  <option value="">select loan officer</option>')
            res.users.map(function(r){
                $("#loan_officer").append(`  <option value="${r.id}">${r.first_name} ${r.last_name}</option>`)
            })
        })
    })
    $("#loan_officer").change(function(){
        $.post('/searchgrp',{
            id:$(this).find(":selected").val(),
            "_token":"{{csrf_token()}}"
        }).then(function(res){
            $(`#group`).html('  <option value="">select group</option>')
            res.data.map(function(r){
                $("#group").append(`  <option value="${r.id}">${r.name}</option>`)
            })
        })
    })
</script>
@endsection
