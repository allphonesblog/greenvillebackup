@extends('layouts.master')
@section('title'){{trans_choice('general.repayment',2)}}
@endsection
@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">{{trans_choice('general.repayment',2)}}</h3>

        <div class="box-tools pull-right">; 

        </div>
        <br>
        <a class="float-right" href="#" onclick="$('#sort').toggle()">Advance Search</a>  |      <a href='#' onclick="$('form :input').val('');">Reset input field </a>
        <form class="form-inline" id="sort" style="display: none;">
                    <label>Date</label>
                    <input type="date" id="date"   class="form-control mb-2 mr-sm-2">

                    <label>Loan Officer</label>
                    <div class="input-group mb-2 mr-sm-2">

                       <select class="form-control" id="loan_officer">
                            <option value="">select loan officer</option>
                                @foreach($users as $u)
                           <option value="{{$u->id}}">{{$u->first_name}} {{$u->last_name}}</option>
                           @endforeach
                        </select>
                    </div>



                    <button type="submit" class="btn btn-primary mb-2">Submit</button>
        </form>
    </div>
    <div class="box-body">
        <div class="table-responsive">

            <table id="view-repayments"
                   class="table table-bordered table-condensed table-hover dataTable no-footer">
                <thead>
                    <tr style="background-color: #D1F9FF" role="row">
                        <th>
                            {{trans_choice('general.collection',1)}} {{trans_choice('general.date',1)}}
                        </th>
                        <th>
                            {{trans_choice('general.borrower',1)}}
                        </th>
                        <th>
                            {{trans_choice('general.collected_by',1)}}
                        </th>
                        <th>
                        {{trans_choice('general.method',1)}}
                    </th>
                    <th>
                        {{trans_choice('general.amount',1)}}
                    </th>
                    <th>
                        {{trans_choice('general.action',1)}}
                    </th>
                    <th>
                        {{trans_choice('general.receipt',1)}}
                    </th>
                </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th id="total" colspan="4"></th>
                        <td>Total : <b>NGN{{number_format($total,2)}}</b></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
@endsection
@section('footer-scripts')
    <script>
        $(document).ready(function () {
            $('.deletePayment').on('click', function (e) {
                e.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: '{{trans_choice('general.are_you_sure',1)}}',
                    text: 'this cant be undone',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '{{trans_choice('general.ok',1)}}',
                    cancelButtonText: '{{trans_choice('general.cancel',1)}}'
                }).then(function () {
                    window.location = href;
                })
            });
        });
    </script>
    <script src="{{ asset('assets/plugins/datatable/media/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/media/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.colVis.min.js')}}"></script>

    <script>
        $("form#sort").submit(function(){
            event.preventDefault()
            $("#view-repayments").dataTable().fnDestroy()
            const loan_officer = $("#loan_officer").find(":selected").val()
            const date = $("#date").val()
            $('#view-repayments').DataTable({
                "ajax":"/apirepayment?loan_officer="+loan_officer+"&date="+date,


            });

        })
        
        <?php if(isset($_GET['search'])):?>
        $('#view-repayments').DataTable( {
            "ajax":"/apirepayment?search="+<?php echo $_GET['search'];?>,
            "order": [[ 3, "desc" ]]
        });
        <?php else:?>
        $('#view-repayments').DataTable( {
            "ajax":"/apirepayment",
            "order": [[ 3, "desc" ]]
        });
        <?php endif;?>


    </script>
@endsection
