@extends('layouts.master')
@section('title')
{{trans_choice('general.borrower',2)}}
@endsection
@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Pending For Transfer Request</h3> | <select id="action">

        <option value="">Select Action</option>
        <option value="approve">Approve selected</option>
        <option value="disapprove">Disapprove selected</option>

        </select>


    </div>

    <div class="box-body ">

        <div class="table-responsive">

            <table id="data-table" class="table table-bordered table-condensed table-hover">

                <thead>
                    <tr>


                        <th>     <a href="#" onclick="c_all()">Toggle check</a></th>
                        <th>{{trans_choice('general.full_name',1)}}</th>
                        <th> From Branch </th>
                        <th> To Branch </th>
                        <th>Date</th>
                        <th>Action</th>

                    </tr>
                </thead>
                <tbody>
                @foreach($b as $bb)
                    <?php
                    $bfrom = DB::table("branches")->where(["id"=>$bb->from_branch])->first();
                    $bto = DB::table("branches")->where(["id"=>$bb->branch_id])->first();

                    ?>
                <tr>
                    <td><input type="checkbox" value="{{$bb->id}}"></td>
                    <td>{{$bb->bfname}} {{$bb->blname}}</td>
                    <td>{{$bfrom->name}}</td>
                    <td>{{$bto->name}}</td>
                    <td>{{$bb->created_at}}</td>
                    <td>
                    
                        <div class="btn-group">

                            <button type="button btn-sm" class="btn btn-info btn-flat dropdown-toggle"
                                    data-toggle="dropdown" aria-expanded="false">
                                {{ trans('general.choose') }} <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="javascript:;" onclick="confirm('/borrower/approvetransfer/{{$bb->id}}')"><i
                                                                                     class="fa fa-check"></i>Approve transfer
                                    </a></li>
                                
                                <li><a href="javascript:;" onclick="confirm('/borrower/disapprovetransfer/{{$bb->id}}')"><i
                                                                                     class="fa fa-times"></i>Disapprove transfer
                                    </a></li>

                            </ul>
                        </div>
                    </td>

                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>

                     <td>-</td>

                        <th>{{trans_choice('general.full_name',1)}}</th>
                        <th> From Branch </th>
                        <th> To Branch </th>

                        <th>Date</th>
                        <th>Action</th>


                    </tr>
                </tfoot>
            </table>

        </div>

    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->


@endsection
@section('footer-scripts')
<script src="{{ asset('assets/plugins/datatable/media/js/jquery.dataTables.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/media/js/dataTables.bootstrap.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.colVis.min.js')}}"></script>
<script>
    $("#data-table").DataTable({
        aoColumnDefs: [
            {
                bSortable: false,
                aTargets: [ 0 ]
            }]
    })


    function c_all(){
        $('input[type=checkbox]').iCheck('toggle'); 

    }
    
    
    function approve(){
        swal({
            title: '{{trans_choice('general.are_you_sure',1)}}',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '{{trans_choice('general.ok',1)}}',
            cancelButtonText: '{{trans_choice('general.cancel',1)}}'
        })
            .then((willDelete) => {
            if (willDelete) {
                var ids = []

                $("body").find("input[type='checkbox']:checked").map(function(){

                    ids.push($(this).val())

                })

                $.post('/borrower/bulkapprove',{

                    ids: ids ,
                    "_token":"{{csrf_token()}}"
                }).done(function(res){
                      swal("Success",res.message, "success")
                    location.reload()
                }).fail(function(res){
                    $("select").find('option').attr("selected","") ;
                    $(".sender").attr("disabled",false)
                    swal("Oops..",res.responseJSON.message, "error")
                })
            } else {
                $(':input').val('');
                return false;
            }
        });
    }
    
    function disapprove(){
        swal({
            title: '{{trans_choice('general.are_you_sure',1)}}',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '{{trans_choice('general.ok',1)}}',
            cancelButtonText: '{{trans_choice('general.cancel',1)}}'
        })
            .then((willDelete) => {
            if (willDelete) {
                var ids = []

                $("body").find("input[type='checkbox']:checked").map(function(){

                    ids.push($(this).val())

                })

                $.post('/borrower/disapprovebulk',{

                    ids: ids ,
                    "_token":"{{csrf_token()}}"
                }).done(function(res){

                    swal("Success",res.message, "success")
                    location.reload()
                }).fail(function(res){
                    $("select").find('option').attr("selected","") ;
                    $(".sender").attr("disabled",false)
                    swal("Oops..",res.responseJSON.message, "error")
                })
            } else {
                $(':input').val('');
                return false;
            }
        });
    }
    
    $("#action").change(function(){
        const action = $(this).find(":selected").val()
        if(action == "approve"){
            approve()
        }

        if(action == "disapprove"){
            disapprove()
        }


    })
</script>
<script>

    function confirm(url){
        swal({
            title: 'Are you sure?',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ok',
            cancelButtonText: 'Cancel'
        }).then(function () {
            window.location = url;
        })
    }
</script>
@endsection
