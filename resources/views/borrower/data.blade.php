@extends('layouts.master')
@section('title')
    {{trans_choice('general.borrower',2)}}
@endsection
@section('content')

<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/js/standalone/selectize.js" integrity="sha512-pF+DNRwavWMukUv/LyzDyDMn8U2uvqYQdJN0Zvilr6DDo/56xPDZdDoyPDYZRSL4aOKO/FGKXTpzDyQJ8je8Qw==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/css/selectize.bootstrap3.css" integrity="sha512-IvEBCcESwoUfopjiLqNEtnEO7l1Flm1T3tEPe0aywpd1B5xG/PedbsB0nKMgbvQOLc+D+FesXFhOp/x4dgwd2A==" crossorigin="anonymous" />

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans_choice('general.borrower',2)}}</h3>

            <div class="box-tools pull-right">
                @if(Sentinel::hasAccess('borrowers.create'))
                    <a href="{{ url('borrower/create') }}"
                       class="btn btn-info btn-sm">{{trans_choice('general.add',1)}} {{trans_choice('general.borrower',1)}}</a>
                @endif
            </div>

        </div>

        <div class="box-body ">

            <div class="table-responsive">

                <a class="float-right" href="#" onclick="$('#sort').toggle()">Advance Search</a> |
                <select id="action">

                    <option value="">Select Action</option>
                    <option value="delete">Delete selected</option>
                    <option value="decline">Decline selected</option>
                    <option value="blacklist">Blacklist selected</option>
                    <option value="unblacklistbulk">unBlacklist selected</option>
                    <option value="transfer">Transfer selected</option>
                </select>
                <form class="form-inline" id="sort" style="display: none;">
                    <label>Date</label>
                    <input type="date" id="date" value="{{$date}}" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2">

                    <label>Loan Officer</label>
                    <div class="input-group mb-2 mr-sm-2">

                       <select class="form-control" id="loan_officer">
                            <option value="">select loan officer</option>
                                @foreach($users as $u)
                           <option value="{{$u->id}}">{{$u->first_name}} {{$u->last_name}}</option>
                           @endforeach
                        </select>
                    </div>



                    <button type="submit" class="btn btn-primary mb-2">Submit</button>
                </form>

                <br>
                <table id="data-table" class="table table-bordered table-condensed table-hover">

                    <thead>
                        <tr>
                            <th>     <a href="#" onclick="c_all()">Toggle check</a></th>
                            <th>Loan Officer</th>
                            <th>Branch</th>

                            <th>{{trans_choice('general.full_name',1)}}</th>
                            <th>{{trans_choice('general.business',1)}}</th>
                            <th>{{trans_choice('general.unique',1)}}#</th>
                            <th>{{trans_choice('general.mobile',1)}}</th>
                            <th>{{trans_choice('general.email',1)}}</th>
                            <th>{{trans_choice('general.status',1)}}</th>
                            <th>Date</th>
                            <th>Action</th>

                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
<th>-</th>
                            <th>Loan Officer</th>
                            <th>Branch</th>

                            <th>{{trans_choice('general.full_name',1)}}</th>
                            <th>{{trans_choice('general.business',1)}}</th>
                            <th>{{trans_choice('general.unique',1)}}#</th>
                            <th>{{trans_choice('general.mobile',1)}}</th>
                            <th>{{trans_choice('general.email',1)}}</th>
                            <th>{{trans_choice('general.status',1)}}</th>
                            <th>Date</th>
                            <th>Action</th>


                        </tr>
                    </tfoot>
                </table>

            </div>

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

<div class="modal fade" id="adsearch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Advance Search</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">

<label>Select search filter</label>
              <select class="form-control">
                  <option></option>
                  <option>Search by date</option>
                  <option>Search by loan officer</option>
                </select>
                </div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-primary">Search</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer-scripts')
    <script src="{{ asset('assets/plugins/datatable/media/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/media/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.colVis.min.js')}}"></script>

<script>


    $('#data-table').DataTable({
        "ajax":"/borrowers",
        processing: true,

        aoColumnDefs: [
            {
                bSortable: false,
                aTargets: [ 0 ]
            }],
        "language": {
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        }

    });
    
    $("#sort").submit(function(){
        event.preventDefault()
        $("#data-table").dataTable().fnDestroy()
        const loan_officer = $("#loan_officer").find(":selected").val()
        const date = $("#date").val()
        $('#data-table').DataTable({
            "ajax":"/borrowers?loan_officer="+loan_officer+"&date="+date,
            aoColumnDefs: [
                {
                    bSortable: false,
                    aTargets: [ 0 ]
                }],

        });

    })
    
    function c_all(){
        $('input[type=checkbox]').trigger('click'); 
    }
    
    function deletecheck(){

        swal({
            title: '{{trans_choice('general.are_you_sure',1)}}',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '{{trans_choice('general.ok',1)}}',
            cancelButtonText: '{{trans_choice('general.cancel',1)}}'
        })
            .then((willDelete) => {
            if (willDelete) {
                var ids = []

                $("body").find("input[type='checkbox']:checked").map(function(){

                    ids.push($(this).val())

                })

                $.post('/borrower/deletecheck',{

                    ids: ids ,
                    "_token":"{{csrf_token()}}"
                }).done(function(res){
                    $(".sender").attr("disabled",false)
                    $("table").dataTable().fnDestroy()
                   $('#data-table').DataTable({
        "ajax":"/borrowers",
        processing: true,

        aoColumnDefs: [
            {
                bSortable: false,
                aTargets: [ 0 ]
            }],
        "language": {
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        }

                   });
                    $("select").find('option').attr("selected","") ;
                    swal("Success",res.message, "success")
                    location.reload()
                }).fail(function(res){
                    $("select").find('option').attr("selected","") ;
                    $(".sender").attr("disabled",false)
                    swal("Oops..",res.responseJSON.message, "error")
                })
            } else {
                $(':input').val('');
                return false;
            }
        });

    }
    
    
    
    function declinebulk(){

        swal({
            title: '{{trans_choice('general.are_you_sure',1)}}',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '{{trans_choice('general.ok',1)}}',
            cancelButtonText: '{{trans_choice('general.cancel',1)}}'
        })
            .then((willDelete) => {
            if (willDelete) {
                var ids = []

                $("body").find("input[type='checkbox']:checked").map(function(){

                    ids.push($(this).val())

                })

                $.post('/borrower/declinebulk',{

                    ids: ids ,
                    "_token":"{{csrf_token()}}"
                }).done(function(res){
                    $(".sender").attr("disabled",false)
                    $("table").dataTable().fnDestroy()
                    $('#data-table').DataTable({
                        "ajax":"/borrowers",
                        processing: true,

                        aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ 0 ]
                            }],
                        "language": {
                            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                        }

                    });
                    $("select").find('option').attr("selected","") ;
                    swal("Success",res.message, "success")
                    location.reload()
                }).fail(function(res){
                    $("select").find('option').attr("selected","") ;
                    $(".sender").attr("disabled",false)
                    swal("Oops..",res.responseJSON.message, "error")
                })
            } else {
                $(':input').val('');
                return false;
            }
        });

    }

    
    
    function blacklistbulk(){

        swal({
            title: '{{trans_choice('general.are_you_sure',1)}}',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '{{trans_choice('general.ok',1)}}',
            cancelButtonText: '{{trans_choice('general.cancel',1)}}'
        })
            .then((willDelete) => {
            if (willDelete) {
                var ids = []

                $("body").find("input[type='checkbox']:checked").map(function(){

                    ids.push($(this).val())

                })

                $.post('/borrower/blacklistbulk',{

                    ids: ids ,
                    "_token":"{{csrf_token()}}"
                }).done(function(res){
                    $(".sender").attr("disabled",false)
                    $("table").dataTable().fnDestroy()
                    $('#data-table').DataTable({
                        "ajax":"/borrowers",
                        processing: true,

                        aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ 0 ]
                            }],
                        "language": {
                            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                        }

                    });
                    $("select").find('option').attr("selected","") ;
                    swal("Success",res.message, "success")
                    location.reload()
                }).fail(function(res){
                    $("select").find('option').attr("selected","") ;
                    $(".sender").attr("disabled",false)
                    swal("Oops..",res.responseJSON.message, "error")
                })
            } else {
                $(':input').val('');
                return false;
            }
        });

    }

    
    function Transfer(){
        $("#btransfer").attr("disabled",true)
        swal({
            title: '{{trans_choice('general.are_you_sure',1)}}',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '{{trans_choice('general.ok',1)}}',
            cancelButtonText: '{{trans_choice('general.cancel',1)}}'
        })
            .then((willDelete) => {
            if (willDelete) {
                var ids = []

                $("body").find("input[type='checkbox']:checked").map(function(){

                    ids.push($(this).val())

                })

                $.post('/borrower/bulktransfer',{
                    branch:$("#branch").find(":selected").val(),
                    ids: ids ,
                    "_token":"{{csrf_token()}}"
                }).done(function(res){
                    $(".sender").attr("disabled",false)
                    $("table").dataTable().fnDestroy()
                    $('#data-table').DataTable({
                        "ajax":"/borrowers",
                        processing: true,

                        aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ 0 ]
                            }],
                        "language": {
                            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                        }

                    });
                    $("select").find('option').attr("selected","") ;
                    swal("Success",res.message, "success")
                    $("#btransfer").attr("disabled",false)
                    location.reload()
                }).fail(function(res){
                    $("select").find('option').attr("selected","") ;
                    $(".sender").attr("disabled",false)
                    swal("Oops..",res.responseJSON.message, "error")
                    $("#btransfer").attr("disabled",false)
                })
            } else {
                $(':input').val('');
                return false;
            }
        });
    }
    
    function unblacklistbulk(){

        swal({
            title: '{{trans_choice('general.are_you_sure',1)}}',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '{{trans_choice('general.ok',1)}}',
            cancelButtonText: '{{trans_choice('general.cancel',1)}}'
        })
            .then((willDelete) => {
            if (willDelete) {
                var ids = []

                $("body").find("input[type='checkbox']:checked").map(function(){

                    ids.push($(this).val())

                })

                $.post('/borrower/unblacklistbulk',{

                    ids: ids ,
                    "_token":"{{csrf_token()}}"
                }).done(function(res){
                    $(".sender").attr("disabled",false)
                    $("table").dataTable().fnDestroy()
                    $('#data-table').DataTable({
                        "ajax":"/borrowers",
                        processing: true,

                        aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ 0 ]
                            }],
                        "language": {
                            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                        }

                    });
                    $("select").find('option').attr("selected","") ;
                    swal("Success",res.message, "success")
                    location.reload()
                }).fail(function(res){
                    $("select").find('option').attr("selected","") ;
                    $(".sender").attr("disabled",false)
                    swal("Oops..",res.responseJSON.message, "error")
                })
            } else {
                $(':input').val('');
                return false;
            }
        });

    }

    $("#action").change(function(){
        const action = $(this).find(":selected").val()
        if(action == "delete"){
            deletecheck()
        }

        if(action == "decline"){
            declinebulk()
        }
        
        if(action == "blacklist"){
            blacklistbulk()
        }
        
        if(action == "unblacklistbulk"){
            unblacklistbulk()
        }
        if(action == "transfer"){
           $("#tborrower").modal("show")
        }
    })

    </script>
<div class="modal fade" id="tborrower" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="/borrower/transfer" method="POST">


            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Transfer Borrower</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>


                <div class="modal-body">
                    <div class="form-group">
                        <label>Select Branch</label>
                        <select id="branch" name="branch" class="form-control ">

                        </select>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-primary" onclick="Transfer()" id="btransfer">Save changes</button>
                </div>

            </div>
        </form>
    </div>
</div>
<script>
    $('select#branch').selectize({
        preload: true,
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',

        load: function(query, callback) {
            $.ajax({
                url: "/searchbranch" + "/?q=" + encodeURIComponent(query),
                type: 'GET',
                error: function() {
                    callback();
                },
                success: function(result) {
                    callback(result)
                }
            });
        }
    });
</script>
@endsection
