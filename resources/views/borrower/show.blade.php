@extends('layouts.master')
@section('title')
    {{trans_choice('general.borrower',1)}} {{trans_choice('general.detail',2)}}
@endsection
@section('content')

<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/js/standalone/selectize.js" integrity="sha512-pF+DNRwavWMukUv/LyzDyDMn8U2uvqYQdJN0Zvilr6DDo/56xPDZdDoyPDYZRSL4aOKO/FGKXTpzDyQJ8je8Qw==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/css/selectize.bootstrap3.css" integrity="sha512-IvEBCcESwoUfopjiLqNEtnEO7l1Flm1T3tEPe0aywpd1B5xG/PedbsB0nKMgbvQOLc+D+FesXFhOp/x4dgwd2A==" crossorigin="anonymous" />


@if(!DB::table("transfers")->where(["borrower_id"=>$borrower->id,"from_branch"=>$borrower->branch_id])->exists())

@else
<div class="alert alert-danger" role="alert">
    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Pending For Transfer
</div>
@endif
    <div class="box box-widget">
        <div class="box-header with-border">
            <div class="row">
                <div class="col-sm-3">
                    <div class="user-block">
                        @if(!empty($borrower->photo))
                            <a href="{{asset('uploads/'.$borrower->photo)}}" class="fancybox"> <img class="img-circle"
                                 src="{{asset('uploads/'.$borrower->photo)}}"
                                 alt="user image"/></a>
                        @else
                            <img class="img-circle"
                                 src="{{asset('assets/dist/img/user.png')}}"
                                 alt="user image"/>
                        @endif
                        <span class="username">
                                {{$borrower->title}}
                            . {{$borrower->first_name}} {{$borrower->last_name}}
                            </span>
                        <span class="description" style="font-size:13px; color:#000000">{{$borrower->unique_number}}
                            <br>
                                <a href="{{url('borrower/'.$borrower->id.'/edit')}}">{{trans_choice('general.edit',1)}}</a><br>
                            {{$borrower->business_name}}, {{$borrower->working_status}}
                            <br>{{$borrower->gender}}
                            , {{date("Y") - date('Y', strtotime($borrower->dob))}} {{trans_choice('general.year',2)}}
                            </span>
                    </div>
                    <!-- /.user-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-3">
                    <ul class="list-unstyled">
                        <li><b>{{trans_choice('general.address',1)}}:</b> {{$borrower->address}}</li>
                        <li><b>{{trans_choice('general.city',2)}}:</b> {{$borrower->city}}</li>
                        <li><b>{{trans_choice('general.state',2)}}:</b> {{$borrower->state}}</li>
                        <li><b>{{trans_choice('general.zip',2)}}:</b> {{$borrower->zip}}</li>
                        <li><b>{{trans_choice('general.blacklisted',1)}}:</b>
                            @if($borrower->blacklisted==1)
                                <span class="label label-danger">{{trans_choice('general.yes',1)}}</span>
                            @else
                                <span class="label label-success">{{trans_choice('general.no',1)}}</span>
                            @endif
                        </li>
                        <li><b>Borrower Branch:</b> {{$borrower->branch->name}}</li>
                       
                    </ul>
                </div>
                <div class="col-sm-3">
                    <ul class="list-unstyled">
                        <li><b>{{trans_choice('general.phone',1)}}:</b> {{$borrower->phone}}</li>
                        <li><b>{{trans_choice('general.email',1)}}:</b> <a
                                    onclick="javascript:window.open('mailto:{{$borrower->email}}', 'mail');event.preventDefault()"
                                    href="mailto:{{$borrower->email}}">{{$borrower->email}}</a>

                            <div class="btn-group-horizontal"><a type="button" class="btn-xs bg-red"
                                                                 href="{{url('communication/email/create?borrower_id='.$borrower->id)}}">{{trans_choice('general.send',1)}}
                                    {{trans_choice('general.email',1)}}</a></div>
                        </li>
                        <li><b>{{trans_choice('general.mobile',1)}}:</b> {{$borrower->mobile}}
                            <div class="btn-group-horizontal"><a type="button" class="btn-xs bg-red"
                                                                 href="{{url('communication/sms/create?borrower_id='.$borrower->id)}}">{{trans_choice('general.send',1)}}
                                    {{trans_choice('general.sms',1)}}</a></div>
                        </li>

                    </ul>
                </div>
                <div class="col-sm-3">
                    <ul class="list-unstyled">
                        <li><b>{{trans_choice('general.custom_field',2)}}</b></li>
                        @foreach($custom_fields as $key)
                            <li>
                                @if(!empty($key->custom_field))
                                    <strong>{{$key->custom_field->name}}:</strong>
                                @endif
                                {{$key->name}}
                            </li>
                        @endforeach

                    </ul>
                </div>
               
                <br>

                <div class="col-sm-4">
             
             <li style="list-style-type: none;"> <b>Loan Officer:</b>  @foreach(unserialize($borrower->loan_officers) as $key)
                        <?php $users = DB::table("users")->where(["id"=>$key])->first();?>
                             
                             {{$users->first_name}} {{$users->last_name}} ,
                        
                     @endforeach </li>

</div>
 
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-sm-9">
                    <div class="btn-group-horizontal row"><a type="button" class="btn bg-olive margin  btn-sm"
                                                         href="{{url('loan/create?borrower_id='.$borrower->id)}}">{{trans_choice('general.add',1)}}
                            {{trans_choice('general.loan',1)}}</a> 
                                          
                        <?php $id = \App\Models\Loan::where('borrower_id', $borrower->id)->where("status","!=","closed")->select("*")->first();?>
                         @if($id == null)
                                
                        <a type="button" title="No Active Loan" class="btn bg-red margin  btn-sm"
                                                         href="#" disabled>Make Loan Repayment</a>
                         @else
                                
                            <a type="button" class="btn bg-red margin  btn-sm"
                                                         href="{{url('/loan/'.$id->id.'/repayment/create')}}">Make Loan Repayment</a>
                         @endif
                                @if(DB::table("guarantors")->where(["userid"=>$borrower->id])->exists())    
                                <a type="button" data-toggle="modal" data-target="#g" class="btn bg-blue margin  btn-sm"
                                                         >Show Guarantor</a> 
                                                         @endif
                                                                        @if(Sentinel::hasAccess('savings.transactions.create'))
                            <a type="button" class="btn bg-olive  btn-sm"
                   href="{{url('saving/'.$savings->id.'/savings_transaction/create')}}">make savings payments</a>
            @endif              
                                @if(!DB::table("transfers")->where(["borrower_id"=>$borrower->id,"from_branch"=>$borrower->branch_id])->exists())
                            <a type="button"  data-toggle="modal" data-target="#tborrower" class="btn bg-primary  btn-sm"
                                   href="#">Transfer Borrower</a>
                                @else
                                <button disabled  class="btn bg-primary  btn-sm"
                                 >Pending For Transfer</button>
                                @endif
                        

                     </div>
                </div>
                <div class="col-sm-3">
                    <div class="pull-left">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-info dropdown-toggle margin" data-toggle="dropdown">
                                {{trans_choice('general.borrower',1)}} {{trans_choice('general.statement',1)}}
                                <span class="fa fa-caret-down"></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{url('loan/'.$borrower->id.'/borrower_statement/print')}}"
                                       target="_blank">{{trans_choice('general.print',1)}} {{trans_choice('general.statement',1)}}</a>
                                </li>
                                <li>
                                    <a href="{{url('loan/'.$borrower->id.'/borrower_statement/pdf')}}"
                                       target="_blank">{{trans_choice('general.download',1)}} {{trans_choice('general.in',1)}} {{trans_choice('general.pdf',1)}}</a>
                                </li>
                                <li>
                                    <a href="{{url('loan/'.$borrower->id.'/borrower_statement/email')}}">{{trans_choice('general.email',1)}}
                                        {{trans_choice('general.statement',1)}}</a></li>
                                <li>
                                    <a href="/saving/savings_transaction/data?borrower_id={{$borrower->id}}">Savings Transactions</a></li>
                            <!--<li>
                                    <a href="{{url('loan/'.$borrower->id.'/borrower_statement/excel')}}"
                                       target="_blank">{{trans_choice('general.download',1)}} {{trans_choice('general.in',1)}} {{trans_choice('general.excel',1)}}</a></li>

                                <li>
                                    <a href="{{url('loan/'.$borrower->id.'/borrower_statement/csv')}}"
                                       target="_blank">{{trans_choice('general.download',1)}} {{trans_choice('general.in',1)}} {{trans_choice('general.csv',1)}}</a></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
        <div class="modal fade" id="tborrower" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="/borrower/transfer" method="POST">


                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Transfer Borrower</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="id" value="{{$borrower->id}}">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Select Branch</label>
                            <select id="branch" name="branch" class="form-control ">

                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>

                </div>
                </form>
            </div>
        </div>
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans_choice('general.loan',2)}}</h3>

            <div class="box-tools pull-right">

            </div>
        </div>
        <div class="box-body table-responsive ">
            <table id="data-table" class="table table-bordered table-condensed table-hover">
                <thead>
                <tr style="background-color: #D1F9FF">
                    <th>#</th>
                    <th>{{trans_choice('general.principal',1)}}</th>
                    <th>{{trans_choice('general.released',1)}}</th>
                    <th>{{trans_choice('general.interest',1)}}%</th>
                    <th>{{trans_choice('general.due',1)}}</th>
                    <th>{{trans_choice('general.paid',1)}}</th>
                    <th>{{trans_choice('general.balance',1)}}</th>
                    <th>{{trans_choice('general.status',1)}}</th>
                    <th>Loan Type</th>
                    <th>{{ trans_choice('general.action',1) }}</th>
                    
                </tr>
                </thead>
                <tbody>
                @foreach($borrower->loans as $key)
                    <tr>

                        <td>{{$key->id}}</td>
                        <td>{{round($key->principal,2)}}</td>
                        <td>{{$key->release_date}}</td>
                        <td>
                            {{round($key->interest_rate,2)}}%/{{$key->interest_period}}
                        </td>
                        <td>{{round(\App\Helpers\GeneralHelper::loan_total_due_amount($key->id),2)}}</td>
                        <td>{{round(\App\Helpers\GeneralHelper::loan_total_paid($key->id),2)}}</td>
                        <td>{{round(\App\Helpers\GeneralHelper::loan_total_balance($key->id),2)}}</td>
                        <td>
                            @if($key->maturity_date<date("Y-m-d") && \App\Helpers\GeneralHelper::loan_total_balance($key->id)>0)
                                <span class="label label-danger">{{trans_choice('general.past_maturity',1)}}</span>
                            @else
                                @if($key->status=='pending')
                                    <span class="label label-warning">{{trans_choice('general.pending',1)}} {{trans_choice('general.approval',1)}}</span>
                                @endif
                                @if($key->status=='approved')
                                    <span class="label label-info">{{trans_choice('general.awaiting',1)}} {{trans_choice('general.disbursement',1)}}</span>
                                @endif
                                @if($key->status=='disbursed')
                                    <span class="label label-info">{{trans_choice('general.active',1)}}</span>
                                @endif
                                @if($key->status=='declined')
                                    <span class="label label-danger">{{trans_choice('general.declined',1)}}</span>
                                @endif
                                @if($key->status=='withdrawn')
                                    <span class="label label-danger">{{trans_choice('general.withdrawn',1)}}</span>
                                @endif
                                @if($key->status=='written_off')
                                    <span class="label label-danger">{{trans_choice('general.written_off',1)}}</span>
                                @endif
                                @if($key->status=='closed')
                                    <span class="label label-success">{{trans_choice('general.closed',1)}}</span>
                                @endif
                                @if($key->status=='pending_reschedule')
                                    <span class="label label-warning">{{trans_choice('general.pending',1)}} {{trans_choice('general.reschedule',1)}}</span>
                                @endif
                                @if($key->status=='rescheduled')
                                    <span class="label label-info">{{trans_choice('general.rescheduled',1)}}</span>
                                @endif
                            @endif
                        </td>
                        @if($key->group_id == NULL)
                        <td><b>Individual</b></td>
                        @else
                        <td><b>Group</b></td>
                        @endif
                        <td>
                            <div class="btn-group">
                                  @if($key->group_id != NULL)
                                  
                                  ---
                                  
                                  @else
                                <button type="button" class="btn btn-info btn-flat dropdown-toggle btn-sm"
                                        data-toggle="dropdown" aria-expanded="false">
                                    {{ trans('general.choose') }} <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                @endif
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('loan/'.$key->id.'/show') }}"><i
                                                    class="fa fa-search"></i> {{ trans_choice('general.detail',2) }}
                                        </a></li>
                                    <li><a href="{{ url('loan/'.$key->id.'/edit') }}"><i
                                                    class="fa fa-edit"></i> {{ trans('general.edit') }} </a></li>
                                    <li><a href="{{ url('loan/'.$key->id.'/delete') }}"
                                           data-toggle="confirmation"><i
                                                    class="fa fa-trash"></i> Delete </a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans_choice('general.repayment',2)}}</h3>

            <div class="box-tools pull-right">

            </div>
        </div>
        <div class="box-body table-responsive">
            <table id="view-repayments"
                   class="table table-bordered table-condensed table-hover dataTable no-footer">
                <thead>
                <tr style="background-color: #D1F9FF" role="row">
                    <th>
                        {{trans_choice('general.collection',1)}} {{trans_choice('general.date',1)}}
                    </th>
                    <th>
                        {{trans_choice('general.collected_by',1)}}
                    </th>
                    <th>
                        {{trans_choice('general.method',1)}}
                    </th>
                    <th>
                        {{trans_choice('general.amount',1)}}
                    </th>
                    <th>
                        {{trans_choice('general.action',1)}}
                    </th>
                    <th>
                        {{trans_choice('general.receipt',1)}}
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($borrower->payments as $key)


                    <tr>
                        <td>{{$key->collection_date}}</td>
                        <td>
                            @if(!empty($key->user))
                                {{$key->user->first_name}} {{$key->user->last_name}}
                            @endif
                        </td>
                        <td>
                            @if(!empty($key->loan_repayment_method))
                                {{$key->loan_repayment_method->name}}
                            @endif
                        </td>
                        <td>NGN{{round($key->amount,2)}}</td>
                        <td>
                            <div class="btn-group-horizontal">
                                <a type="button" class="btn bg-white btn-xs text-bold"
                                   href="{{url('loan/'.$key->loan_id.'/repayment/'.$key->id.'/edit')}}">{{trans_choice('general.edit',1)}}</a>
                                <a type="button"
                                   class="btn bg-white btn-xs text-bold deletePayment"
                                   href="{{url('loan/'.$key->loan_id.'/repayment/'.$key->id.'/delete')}}"
                                >{{trans_choice('general.delete',1)}}</a>
                            </div>
                        </td>
                        <td>
                            <a type="button" class="btn btn-default btn-xs"
                               href="{{url('loan/'.$key->loan_id.'/repayment/'.$key->id.'/print')}}"
                               target="_blank">
                                                                <span class="glyphicon glyphicon-print"
                                                                      aria-hidden="true"></span>
                            </a>
                            <a type="button" class="btn btn-default btn-xs"
                               href="{{url('loan/'.$key->loan_id.'/repayment/'.$key->id.'/pdf')}}"
                               target="_blank">
                                                                <span class="glyphicon glyphicon-file"
                                                                      aria-hidden="true"></span>
                            </a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>



<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Saving Transactions</h3>

        <div class="box-tools pull-right">

        </div>
    </div>
    <div class="box-body table-responsive">
        <div class="table-responsive">

                <br>
                <table id="savings" class="table table-bordered table-condensed table-hover" style="width:100%">
                    <thead>
                    <tr style="background-color: #D1F9FF">
                        <th>{{trans_choice('general.name',1)}}</th>
                        <th>{{trans_choice('general.account',1)}}</th>
                        <th>{{trans_choice('general.date',1)}}</th>
                        <th>{{trans_choice('general.transaction',1)}}</th>
                        <th>{{trans_choice('general.debit',1)}}</th>
                        <th>{{trans_choice('general.credit',1)}}</th>
                        <th>{{ trans_choice('general.action',1) }}</th>
                    </tr>
                    </thead>

                </table>
            </div>
    </div>
</div>
        <script>
            $('select#branch').selectize({
                preload: true,
                valueField: 'id',
                labelField: 'name',
                searchField: 'name',

                load: function(query, callback) {
                    $.ajax({
                        url: "/searchbranch" + "/?q=" + encodeURIComponent(query),
                        type: 'GET',
                        error: function() {
                            callback();
                        },
                        success: function(result) {
                            callback(result)
                        }
                    });
                }
            });
        </script>
@endsection
@section('footer-scripts')
    <script src="{{ asset('assets/plugins/datatable/media/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/media/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.colVis.min.js')}}"></script>
<script>
    $('#savings').DataTable({

        "order": [[ 3, "desc" ]],
        "processing": true,
        "serverSide": true,
        "ajax":{
            "url": "/apisavingaccount2?id="+{{$borrower->id}},
            "dataType": "json",
            "type": "POST",
            "data":{ _token: "{{csrf_token()}}"}
        },

       "columns": [
                { "data": "name" },
                { "data": "amount" },
                { "data": "date" },
                { "data": "status" },
                { "data": "debit" },
                { "data": "credit" },
                            { "data": "action" }

                            ]	

                            });


</script>
<script>
    $('#data-table').DataTable();
        $('#view-repayments').DataTable();


    </script>
    <script>
        $(document).ready(function () {
            $('.deletePayment').on('click', function (e) {
                e.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: '{{trans_choice('general.are_you_sure',1)}}',
                    text: 'If you revert a payment, a fully paid loan may change status to open.',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '{{trans_choice('general.ok',1)}}',
                    cancelButtonText: '{{trans_choice('general.cancel',1)}}'
                }).then(function () {
                    window.location = href;
                })
            });

        });
    </script>
    
               <div class="modal fade" id="g" tabindex="-1" role="dialog" aria-labelledby="g" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Guarantor</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                          @if(DB::table("guarantors")->where(["userid"=>$borrower->id])->exists())
                          <center>
                              @if($guarantors->photo != null)
                              <img src="/guarantors/{{$guarantors->photo}}" class="img img-responsive"/><br>
                              @endif
                          </center>
                        <div class="form-group">
                            <input type="text" class="form-control" value="{{$guarantors->firstname}}" placeholder="Guarantor Firstname" name="gfirstname" id="gfirstname" disabled>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" value="{{$guarantors->lastname}}" placeholder="Guarantor SurName" name="gsurname" id="gsurname" disabled>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" value="{{$guarantors->othername}}" placeholder="Guarantor Others Name" name="gothername" id="gothername" disabled>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" value="{{$guarantors->address}}" placeholder="Guarantor Address" name="gaddress" id="gaddress" disabled>
                        </div>
                          <div class="form-group">
                            <input type="text" class="form-control" value="{{$guarantors->phone}}" placeholder="Guarantor Mobile Number" name="gphone" id="gphone" disabled>
                        </div>
                          <div class="form-group">
                            <input type="text" class="form-control" value="{{$guarantors->biz_type}}" placeholder="Guarantor  Business" name="gbiz" id="gbiz" disabled>
                        </div>
                        @endif
                          
                        
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                         
                      </div>
                    </div>
                  </div>
                </div>
                </div>
@endsection