@extends('layouts.master')
@section('title') {{ trans_choice('general.borrower',1) }} {{ trans_choice('general.group',2) }}
@endsection
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans_choice('general.borrower',1) }} {{ trans_choice('general.group',2) }}</h3><br>
<span><code>Note: To Create A Group Loan Add Borrowers To Group </code></span>
            <div class="box-tools pull-right">
                <a href="{{ url('borrower/group/create') }}"
                   class="btn btn-info btn-sm">{{ trans_choice('general.add',1) }} {{ trans_choice('general.borrower',1) }} {{ trans_choice('general.group',1) }}</a>
            </div>
        </div>
        <div class="container">
        <a class="float-right" href="#" onclick="$('#sort').toggle()">Advance Search</a>
        <form class="form-inline" id="sort" style="display: none;">
            <label>Date</label>
            <input type="date" name="date" value="{{$date}}" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2">

            <label>Group Name</label>
            <input type="text" name="name" value="{{$text}}" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2">

            <label>Loan Officer</label>
            <div class="input-group mb-2 mr-sm-2">

                <select class="form-control" name="loan_officer">
                    <option value="">select loan officer</option>
                    @foreach($users as $u)
                    <option value="{{$u->id}}">{{$u->first_name}} {{$u->last_name}}</option>
                    @endforeach
                </select>
            </div>



            <button type="submit" class="btn btn-primary mb-2">Submit</button>
        </form>
        </div>
        <div class="box-body">
            <table id="data-table" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>{{ trans_choice('general.name',1) }}</th>
                    <th>{{ trans_choice('general.borrower',2) }}</th>
                    <th>{{ trans_choice('general.note',2) }}</th>
                    <th>{{ trans_choice('general.action',1) }}</th>
                    
                </tr>
                </thead>
                <tbody>
                @foreach($data as $key)
             
                    <tr>
                        <td>{{ $key->name }}</td>
                        <td>{{ \App\Models\BorrowerGroupMember::where('borrower_group_id',$key->id)->count() }}</td>
                        <td>{!!   $key->notes !!}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-info btn-flat dropdown-toggle"
                                        data-toggle="dropdown" aria-expanded="false">
                                    {{ trans('general.choose') }} <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('borrower/group/'.$key->id.'/show') }}"><i
                                                    class="fa fa-search"></i> {{ trans_choice('general.detail',2) }} </a></li>
                                    <li><a href="{{ url('borrower/group/'.$key->id.'/edit') }}"><i
                                                    class="fa fa-edit"></i> {{ trans('general.edit') }} </a></li>
                                                    <?php $check = DB::table("borrower_group_members")->where(["borrower_group_id"=>$key->id])->exists();?>
                                                         <?php $check2 = DB::table("grouploan")->where(["group_id"=>$key->id,"status"=>"approved"])->exists();?>
                                                          <?php $check3 = DB::table("grouploan")->where(["group_id"=>$key->id,"status"=>"pending"])->exists();?>
                                                    @if($check || $check2 || $check3)
                                                    <li><a href="/create/group/loan/{{$key->id}}"><i class="fa fa-list"></i>Create Group Loan</a></li>
                                                    @endif
                                    <li><a href="{{ url('borrower/group/'.$key->id.'/delete') }}"
                                           class="delete"><i
                                                    class="fa fa-trash"></i> {{ trans('general.delete') }} </a></li>
                                                 
                                </ul>
                            </div>
                        </td>
                        
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('footer-scripts')
<script src="{{ asset('assets/plugins/datatable/media/js/jquery.dataTables.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/media/js/dataTables.bootstrap.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.colVis.min.js')}}"></script>
<script>
    $('#data-table').DataTable();
</script>
@endsection
