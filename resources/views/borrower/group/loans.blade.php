@extends('layouts.master')
@section('title'){{trans_choice('general.loan',2)}}
@endsection
@section('content')

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">

            @if(isset($_REQUEST['status']))
            @if($_REQUEST['status']=='pending')
            {{trans_choice('general.loan',2)}}  {{trans_choice('general.pending',1)}} {{trans_choice('general.approval',1)}}
            @endif
            @if($_REQUEST['status']=='approved')
            {{trans_choice('general.loan',2)}}  {{trans_choice('general.awaiting',1)}} {{trans_choice('general.disbursement',1)}}
            @endif
            @if($_REQUEST['status']=='disbursed')
            {{trans_choice('general.loan',2)}}  {{trans_choice('general.disbursed',1)}}
            @endif
            @if($_REQUEST['status']=='declined')
            {{trans_choice('general.loan',2)}} {{trans_choice('general.declined',1)}}
            @endif
            @if($_REQUEST['status']=='withdrawn')
            {{trans_choice('general.loan',2)}} {{trans_choice('general.withdrawn',1)}}
            @endif
            @if($_REQUEST['status']=='written_off')
            {{trans_choice('general.loan',2)}} {{trans_choice('general.written_off',1)}}
            @endif
            @if($_REQUEST['status']=='closed')
            {{trans_choice('general.loan',2)}} {{trans_choice('general.closed',1)}}
            @endif
            @if($_REQUEST['status']=='pending_reschedule')
            {{trans_choice('general.loan',2)}} {{trans_choice('general.pending',1)}} {{trans_choice('general.reschedule',1)}}
            @endif
            @else
            {{trans_choice('general.all',2)}} {{trans_choice('general.loan',2)}}
            @endif
        </h3>
        <br>
        <a class="float-right" href="#" onclick="$('#sort').toggle()">Advance Search</a> | <a href='#' onclick="$('form :input').val('');">Reset input field </a> |
        <select id="changestatus">
            <option>Select Status type</option>
            <option value="approved">Approve Selected</option>
            <option value="disburse">Disburse Selected</option>
            <option value="undisburse">UnDisburse Selected</option>
        </select> |  <select id="cstatus">
        <option>Change Status</option>
        <option value="pending">Pending Group Loan</option>
        <option value="approved">Approved Group Loan</option>
        <option value="disbursed">Disbursed Group Loan</option>
        </select>
        

        <div id="sort" style="display: none;">


            <form class="form-inline" id="sort"  action="">
                <label>Date</label>
                <input type="date" name="date" id="date" value="{{$date}}" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2">

                <label>Loan Officer</label>
                <div class="input-group mb-2 mr-sm-2">

                    <select class="form-control" id="loan_officer">
                        <option value="">select loan officer</option>
                        @foreach($users as $u)
                        <option value="{{$u->id}}">{{$u->first_name}} {{$u->last_name}}</option>
                        @endforeach
                    </select>
                </div>



                <button type="submit" class="btn btn-primary mb-2">Submit</button>
            </form>


        </div>
        <div class="box-tools pull-right">
            @if($status == "approved")
            <button class="btn btn-primary btm-sm" onclick="disburse()">
                Disburse all
            </button>
            @endif

            @if($status == "pending")
            <button class="btn btn-primary btm-sm" onclick="approve()">
                Approve all
            </button>
            @endif

            @if($gr->status != "closed")
            <a href='{{ url("/loan/repayment/group/bulk/create?groupid=$gid") }}'
               class="btn btn-primary btn-sm">Loan Repayment</a>
            @endif

        </div>
    </div>

    <div class="box-body table-responsive">
        <div class="row">




            <div class="col-md-2">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Group Loan Amount</h3>
                        <!-- /.box-tools -->

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" >
                        <h3>   {{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value }}  {{number_format(DB::table("grouploan")->where(["sc"=>$gid])->sum("loan_amount"))}}</h3>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            
            
            <div class="col-md-2">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Approved  Amount</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <h3>  {{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value }}  {{number_format(DB::table("loans")->where(["sc"=>$gid])->sum("approved_amount"))}}</h3>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>


            <div class="col-md-2">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Loan Paid</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <h3>  {{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value }}  {{number_format($balance)}}</h3>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            
            <div class="col-md-2">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Loan Balance</h3>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <h3>  {{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value }}  {{number_format($balance2)}}</h3>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>



        </div>
        <br>

        <table id="data-table" class="table table-bordered table-striped table-condensed table-hover">

            <thead>
                <tr>
                    <td id="checkall">   <a href="#" onclick="c_all()">Toggle check</a></td>

                    <th>{{trans_choice('general.borrower',1)}}</th>

                    <th>#</th>
                    <th>Loan Officer</th>
                    <th>Branch</th>
                    <th>{{trans_choice('general.principal',1)}}</th>
                    <th>{{trans_choice('general.released',1)}}</th>
                    <th>{{trans_choice('general.interest',1)}}%</th>
                    <th>{{trans_choice('general.due',1)}}</th>
                    <th>{{trans_choice('general.paid',1)}}</th>
                    <th>{{trans_choice('general.balance',1)}}</th>
                    <th>{{trans_choice('general.status',1)}}</th>
                    <th>{{ trans_choice('general.action',1) }}</th>

                </tr>
            </thead>


            <tfoot>
                <tr>
<td>-</td>
                    <th>{{trans_choice('general.borrower',1)}}</th>

                    <th>#</th>
                    <th>Loan Officer</th>
                    <th>Branch</th>
                    <th>{{trans_choice('general.principal',1)}}</th>
                    <th>{{trans_choice('general.released',1)}}</th>
                    <th>{{trans_choice('general.interest',1)}}%</th>
                    <th>{{trans_choice('general.due',1)}}</th>
                    <th>{{trans_choice('general.paid',1)}}</th>
                    <th>{{trans_choice('general.balance',1)}}</th>
                    <th>{{trans_choice('general.status',1)}}</th>
                    <th>{{ trans_choice('general.action',1) }}</th>

                </tr>
            </tfoot>

        </table>
    </div>
    <!-- /.box-body -->

</div>
<!-- /.box -->
@endsection

@section('footer-scripts')
<script src="{{ asset('assets/plugins/datatable/media/js/jquery.dataTables.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/media/js/dataTables.bootstrap.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.colVis.min.js')}}"></script>
<script src="https://cdn.datatables.net/scroller/2.0.3/js/dataTables.scroller.min.js"></script>




<div class="modal fade" id="approvedloan">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">*</span></button>
                <h4 class="modal-title">{{trans_choice('general.approve',1)}} {{trans_choice('general.loan',1)}}</h4>
            </div>
            {!! Form::open(array('url' => url('loan/approvebulk'),'method'=>'post', 'class'=>'lapprove')) !!}
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-line">
                        {!!  Form::label('approved_date',null,array('class'=>' control-label')) !!}
                        {!! Form::text('approved_date',date("Y-m-d"),array('class'=>'form-control date-picker approved_date','required'=>'required')) !!}
                    </div>
                </div>

                 

                <div class="form-group">
                    <div class="form-line">
                        {!!  Form::label( 'Notes',null,array('class'=>' control-label')) !!}
                        {!! Form::textarea('approved_notes','',array('class'=>'form-control approved_notes','rows'=>'3')) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-info sender2">{{trans_choice('general.save',1)}}</button>
                <button type="button" class="btn default "
                        data-dismiss="modal">{{trans_choice('general.close',1)}}</button>
            </div>
            {!! Form::close() !!}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="disburseLoan">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">*</span></button>
                <h4 class="modal-title">{{trans_choice('general.disburse',1)}} {{trans_choice('general.loan',1)}}</h4>
            </div>
            <form id="disbursecheck">

            <div class="modal-body">
                <div class="form-group">
                    <div class="form-line">
                        {!!  Form::label('disbursed_date',null,array('class'=>' control-label')) !!}
                        {!! Form::text('disbursed_date',Date("Y-m-d"),array('class'=>'form-control date-picker gdisbursed_date','required'=>'required')) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-line">
                        {!!  Form::label('first_payment_date',null,array('class'=>' control-label')) !!}
                        {!! Form::text('first_payment_date',Date("Y-m-d"),array('class'=>'form-control date-picker gfirst_payment_date',''=>'')) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-line">
                        {!!  Form::label('loan_disbursed_by_id',"Disbursed By",array('class'=>' control-label')) !!}
                        {!! Form::select('loan_disbursed_by_id',$loan_disbursed_by,null,array('class'=>'form-control gloan_disbursed_by_id','required'=>'required')) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-line">
                        {!!  Form::label( 'Notes',null,array('class'=>' control-label')) !!}
                        {!! Form::textarea('disbursed_notes','',array('class'=>'form-control gnotes','rows'=>'3')) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-info sender">{{trans_choice('general.save',1)}}</button>
                <button type="button" class="btn default"
                        data-dismiss="modal">{{trans_choice('general.close',1)}}</button>
            </div>

            </form>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
    

    <!-- /.modal-dialog -->

<script>
    function disburse(){

        $("#disburseLoan").modal("show")


    }
    
    function approvedloan(){
        $("#approvedloan").modal("show")
    }
</script>
<script>
    $(document).ready(function () {
        @if($status != null)
            var url = "/borrower/group/{{$gid}}/loans?status="+"{{$status}}"
            @else
                var url = "/borrower/group/{{$gid}}/loans"
                @endif
                $('#data-table').DataTable({
                    "order": [[ 3, "desc" ]],
                    "processing": true,
                    "serverSide": true,
                    "ajax":{
                        "url": url,
                        "dataType": "json",
                        "type": "POST",
                        "data":{ _token: "{{csrf_token()}}"}
                    },
                    aoColumnDefs: [
                        {
                            bSortable: false,
                            aTargets: [ 0 ]
                        }
                    ],
                    "columns": [
                        { "data": "select" },
                        { "data": "borrowers" },
                        { "data": "id" },
                        { "data": "loan_officer" },
                        { "data": "branch" },
                        { "data": "principal" },
                        { "data": "released" },
                        { "data": "interest" },
                        { "data": "due" },
                        { "data": "paid" },

                        { "data": "balance" },
                        { "data": "status" },
                        { "data": "action" }
                    ]	
                });
        });
        function approve(){
            $("#approve").modal("show")
        }

        $("#sort").submit(function(){
            event.preventDefault()

            $("#data-table").dataTable().fnDestroy()
            const loan_officer = $("#loan_officer").find(":selected").val()
            const date = $("#date").val()
            @if($status != null)
                var url = "/borrower/group/{{$gid}}/loans?status="+"{{$status}}"
                @else
                    var url = "/borrower/group/{{$gid}}/loans?loan_officer="+loan_officer+"&date="+date
                    @endif

                    $('#data-table').DataTable({
                        "order": [[ 3, "desc" ]],
                        "processing": true,
                        "serverSide": true,
                        "ajax":{
                            "url": url,
                            "dataType": "json",
                            "type": "POST",
                            "data":{ _token: "{{csrf_token()}}"}
                        },
                        aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ 0 ]
                            }
                        ],
                        "columns": [
                            { "data": "select" },
                            { "data": "borrowers" },
                            { "data": "id" },
                            { "data": "loan_officer" },
                            { "data": "branch" },
                            { "data": "principal" },
                            { "data": "released" },
                            { "data": "interest" },
                            { "data": "due" },
                            { "data": "paid" },

                            { "data": "balance" },
                            { "data": "status" },
                            { "data": "action" }
                        ]	
                    });

        })
        
        
        function approved(){

            swal({
                title: '{{trans_choice('general.are_you_sure',1)}}',
                text: '',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{trans_choice('general.ok',1)}}',
                cancelButtonText: '{{trans_choice('general.cancel',1)}}'
            })
                .then((willDelete) => {
                if (willDelete) {
                    var ids = []

                    $("body").find("input[type='checkbox']:checked").map(function(){

                        ids.push($(this).val())

                    })
                    $(".sender2").attr("disabled",true)

                    $.post('/borrower/group/{{$gid}}/approvedcheck',{
                        approved_notes: $("#approved_notes").val(),
                        approved_date: $("#approved_date").val(),
                        approved_amount: $("#approved_amount").val(),
                        ids: ids ,
                        "_token":"{{csrf_token()}}"
                    }).done(function(res){
                        $(".sender").attr("disabled",false)
                        $("#data-table").dataTable().fnDestroy()
                        var url = "/borrower/group/{{$gid}}/loans"

                        $('#data-table').DataTable({
                            "order": [[ 3, "desc" ]],
                            "processing": true,
                            "serverSide": true,
                            "ajax":{
                                "url": url,
                                "dataType": "json",
                                "type": "POST",
                                "data":{ _token: "{{csrf_token()}}"}
                            },
                            aoColumnDefs: [
                                {
                                    bSortable: false,
                                    aTargets: [ 0 ]
                                }
                            ],
                            "columns": [
                                { "data": "select" },
                                { "data": "borrowers" },
                                { "data": "id" },
                                { "data": "loan_officer" },
                                { "data": "branch" },
                                { "data": "principal" },
                                { "data": "released" },
                                { "data": "interest" },
                                { "data": "due" },
                                { "data": "paid" },

                                { "data": "balance" },
                                { "data": "status" },
                                { "data": "action" }
                            ]	
                        });
                        $("select").find('option').attr("selected","") ;
                        swal("Success",res.message, "success")
                        $(".sender2").attr("disabled",false)
                        location.reload()
                    }).fail(function(res){
                        $("select").find('option').attr("selected","") ;
                        $(".sender2").attr("disabled",false)
                        swal("Oops..",res.responseJSON.message, "error")
                    })
                } else {
                    $(':input').val('');
                    return false;
                }
            });
        }

        function undisburse(){

            swal({
                title: '{{trans_choice('general.are_you_sure',1)}}',
                text: '',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{trans_choice('general.ok',1)}}',
                cancelButtonText: '{{trans_choice('general.cancel',1)}}'
            })
                .then((willDelete) => {
                if (willDelete) {
                    var ids = []

                    $("body").find("input[type='checkbox']:checked").map(function(){

                        ids.push($(this).val())

                    })

                    $.post('/borrower/group/{{$gid}}/undisbursecheck',{

                        ids: ids ,
                        "_token":"{{csrf_token()}}"
                    }).done(function(res){
                        $(".sender").attr("disabled",false)
                        $("#data-table").dataTable().fnDestroy()
                        var url = "/borrower/group/{{$gid}}/loans"

                        $('#data-table').DataTable({
                            "order": [[ 3, "desc" ]],
                            "processing": true,
                            "serverSide": true,
                            "ajax":{
                                "url": url,
                                "dataType": "json",
                                "type": "POST",
                                "data":{ _token: "{{csrf_token()}}"}
                            },
                            aoColumnDefs: [
                                {
                                    bSortable: false,
                                    aTargets: [ 0 ]
                                }
                            ],
                            "columns": [
                                { "data": "select" },
                                { "data": "borrowers" },
                                { "data": "id" },
                                { "data": "loan_officer" },
                                { "data": "branch" },
                                { "data": "principal" },
                                { "data": "released" },
                                { "data": "interest" },
                                { "data": "due" },
                                { "data": "paid" },

                                { "data": "balance" },
                                { "data": "status" },
                                { "data": "action" }
                            ]	
                        });
                        $("select").find('option').attr("selected","") ;
                        swal("Success",res.message, "success")
                        location.reload()
                    }).fail(function(res){
                        $("select").find('option').attr("selected","") ;
                        $(".sender").attr("disabled",false)
                        swal("Oops..",res.responseJSON.message, "error")
                    })
                } else {
                    $(':input').val('');
                    return false;
                }
            });

        }
        $(".lapprove").submit(function(){
            event.preventDefault();
            approved()
            
            })
        $("#disbursecheck").submit(function(){
            event.preventDefault();
            $(".sender").attr("disabled",true)
             var ids = []

            $("body").find("input[type='checkbox']:checked").map(function(){

                ids.push($(this).val())

            })

            const gdisbursed_date = $(".gdisbursed_date").val()
            const gfirst_payment_date = $(".gfirst_payment_date").val()
            const gloan_disbursed_by_id = $(".gloan_disbursed_by_id").val()
            const gnotes = $(".gnotes").val()
            $.post('/borrower/group/{{$gid}}/disbursecheck',{
                disbursed_date: gdisbursed_date,
                ids: ids,
                first_payment_date: gfirst_payment_date,
                loan_disbursed_by_id: gloan_disbursed_by_id,
                disbursed_notes: gnotes,
                "_token":"{{csrf_token()}}"
            }).done(function(res){
                $(".sender").attr("disabled",false)
                $("#data-table").dataTable().fnDestroy()
                var url = "/borrower/group/{{$gid}}/loans"

                $('#data-table').DataTable({
                    "order": [[ 3, "desc" ]],
                    "processing": true,
                    "serverSide": true,
                    "ajax":{
                        "url": url,
                        "dataType": "json",
                        "type": "POST",
                        "data":{ _token: "{{csrf_token()}}"}
                    },
                    aoColumnDefs: [
                        {
                            bSortable: false,
                            aTargets: [ 0 ]
                        }
                    ],
                    "columns": [
                        { "data": "select" },
                        { "data": "borrowers" },
                        { "data": "id" },
                        { "data": "loan_officer" },
                        { "data": "branch" },
                        { "data": "principal" },
                        { "data": "released" },
                        { "data": "interest" },
                        { "data": "due" },
                        { "data": "paid" },

                        { "data": "balance" },
                        { "data": "status" },
                        { "data": "action" }
                    ]	
                });
                swal("Success",res.message, "success")
                $("select").find('option').attr("selected","") ;
                location.reload()
        }).fail(function(res){
                $("select").find('option').attr("selected","") ;
              $(".sender").attr("disabled",false)
                swal("Oops..",res.responseJSON.message, "error")
        })
        })
        
        $("#cstatus").change(function(){
            $("#data-table").dataTable().fnDestroy()
            var s = $(this).find(":selected").val()
            var url = "/borrower/group/{{$gid}}/loans?status="+s


                $('#data-table').DataTable({
                    "order": [[ 3, "desc" ]],
                    "processing": true,
                    "serverSide": true,
                    "ajax":{
                        "url": url,
                        "dataType": "json",
                        "type": "POST",
                        "data":{ _token: "{{csrf_token()}}"}
                    },
                    aoColumnDefs: [
                        {
                            bSortable: false,
                            aTargets: [ 0 ]
                        }
                    ],
                    "columns": [
                        { "data": "select" },
                        { "data": "borrowers" },
                        { "data": "id" },
                        { "data": "loan_officer" },
                        { "data": "branch" },
                        { "data": "principal" },
                        { "data": "released" },
                        { "data": "interest" },
                        { "data": "due" },
                        { "data": "paid" },

                        { "data": "balance" },
                        { "data": "status" },
                        { "data": "action" }
                    ]	
                });


        })
        $("#changestatus").change(function(){
            var status = $(this).find(":selected").val()
            if(status =="disburse"){
                disburse()
            }

            if(status == "undisburse"){
                undisburse()
            }
            if(status == "approved"){
                approvedloan()
            }
        })

        function c_all(){
           $('input[type=checkbox]').trigger('click'); 
        }
</script>
@endsection