@extends('layouts.master')
@section('title'){{$group->name}} Loans
@endsection
@section('content')

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">

        </h3>
        <div class="box-tools pull-right">


            @if(Sentinel::hasAccess('loans.create') && !DB::table("grouploan")->where(["group_id"=>$group->id])->exists())



            <a href='{{ url("/create/group/loan/$group->id") }}'
               class="btn btn-info btn-sm">{{trans_choice('general.add',1)}} {{trans_choice('general.loan',1)}}</a>
            @endif
        </div>

    <div class="box-body table-responsive">

        <table id="data-table" class="table table-bordered table-striped table-condensed table-hover">

            <thead>
                <tr>

 <th>Loan Account</th>
<th>Approved Amount</th>
                    <th>Principal</th>
                    <th>Loan Status</th>
                    <th>Created At</th>
                    <th>Action</th>
                </tr>
            </thead>
<tbody>
    
    @foreach($loan as $loans)
<?php $loanx = DB::table("loans")->where(["sc"=>$loans->sc])->sum("approved_amount");?>
    <?php $prin = DB::table("loans")->where(["sc"=>$loans->sc])->sum("principal");?>
            <tr>


                <td>#{{$loans->sc}}</td>
                <td>{{\App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value}}{{number_format($loanx,2)}}</td>
                <td>{{\App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value}}{{number_format($prin,2)}}</td>
                <td>{{$loans->status}}</td>
                <td>{{$loans->created_at}}</td>
                <td><div class="btn-group">
                    <button type="button" class="btn btn-info btn-xs dropdown-toggle"
                            data-toggle="dropdown" aria-expanded="false">
                        {{ trans('general.choose') }} <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/borrower/group/'.$loans->sc.'/loans') }}"
                               ><i
                                                 class="fa fa-eye"></i> Details </a></li>
                        <li><a href="/repayment/data?search={{$loans->sc}}"
                               ><i
                                   class="fa fa-sticky-note-o"></i> Repayments </a></li>
                        @if(Sentinel::hasAccess('loans.delete'))
                        <li><a href="{{ url('/borrower/group/loans/'.$loans->sc.'/delete') }}"
                               class="delete"><i
                                                 class="fa fa-trash"></i> {{ trans('general.delete') }} </a></li>
                        @endif
                    </ul>
                    </div></td>

    </tr>
    @endforeach
            </tbody>

            <tfoot>
                <tr>

 <th>Loan Account</th>
<th>Approved Amount</th>
                    <th>Principal</th>
                    <th>Loan Status</th>
                    <th>Created At</th>
                    <th>Action</th>
                </tr>
            </tfoot>

        </table>
        </div>
        <!-- /.box-body -->

    </div>
<!-- /.box -->
@endsection

@section('footer-scripts')
<script src="{{ asset('assets/plugins/datatable/media/js/jquery.dataTables.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/media/js/dataTables.bootstrap.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.colVis.min.js')}}"></script>
<script src="https://cdn.datatables.net/scroller/2.0.3/js/dataTables.scroller.min.js"></script>


 <script>
    $("table").DataTable()

    </script>
@endsection