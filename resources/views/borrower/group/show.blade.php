@extends('layouts.master')
@section('title')
    {{ $borrower_group->name }}
@endsection
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/js/standalone/selectize.js" integrity="sha512-pF+DNRwavWMukUv/LyzDyDMn8U2uvqYQdJN0Zvilr6DDo/56xPDZdDoyPDYZRSL4aOKO/FGKXTpzDyQJ8je8Qw==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/css/selectize.bootstrap3.css" integrity="sha512-IvEBCcESwoUfopjiLqNEtnEO7l1Flm1T3tEPe0aywpd1B5xG/PedbsB0nKMgbvQOLc+D+FesXFhOp/x4dgwd2A==" crossorigin="anonymous" />


    <div class="row">
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $borrower_group->name }}</h3>

                    <div class="box-tools pull-right">

                    </div>
                </div>
                <div class="box-body">
                    {!! $borrower_group->notes !!}
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {{ trans_choice('general.created_at',1) }}: {{$borrower_group->created_at}}
                </div>
            </div>
            <!-- /.box -->
        </div>
         <style>
                                            .hide{
                                                display: none;
                                                visibility: hidden;
                                            }
                                            
                                            .show{
                                                display: block;
                                            }
                                        </style>
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans_choice('general.borrower',2) }}</h3> @if(  \DB::table("grouploan")->where(["group_id"=>$borrower_group->id])->exists() && DB::table("grouploan")->where(["group_id"=>$borrower_group->id,"status"=>"approved"])->exists()) <a href="#" id="edit">Edit</a>@endif
<br>
<script>
    $("#edit").click(function(){
        $("#submitx").toggleClass("hide")
    })
</script>
<a href="#" id="submitx" class="hide">Submit</a>
                    <div class="box-tools pull-right">
                        @if(Sentinel::hasAccess('borrowers.groups'))
                            <a href="#" data-toggle="modal" data-target="#addBorrower"
                               class="btn btn-info btn-sm">{{trans_choice('general.add',1)}} {{trans_choice('general.borrower',1)}}</a>
                        @endif
                    </div>
                    
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="data-table checkb" class="table table-bordered table-condensed table-hover">
                            <thead>
                            <tr style="background-color: #D1F9FF">
                                <th>{{trans_choice('general.full_name',1)}}</th>
                                <th>{{trans_choice('general.unique',1)}}#</th>
                                <th>{{trans_choice('general.mobile',1)}}</th>
                                
                                <th>{{ trans_choice('general.action',1) }}</th>
                                <td></td>
                            </tr>
                            </thead>
                           
                            <tbody>
                            @foreach($borrower_group->members as $key)
                                @if(!empty($key->borrower))
                                    <tr>
                                       <?php $l = DB::table("loans")->where(["borrower_id"=>$key->borrower->id])->first();?>
                                       
                                        <script>
    $("#edit").click(function(){
        $("#checkuser-{{$key->borrower->id}}").toggleClass("hide")
        // $("#submitx").toggleClass("show")
         
    })
    
                               
                           
       
</script>
                                        <td><span class="hide" id="checkuser-{{$key->borrower->id}}"><input type="checkbox" id="usersx" class="checkuser-{{$key->borrower->id}}"  value="{{$key->borrower->id}}"/></span> {{ $key->borrower->first_name }} {{ $key->borrower->last_name }}</td>
                                        <td>{{ $key->borrower->unique_number }}</td>
                                        <td>{{ $key->borrower->mobile }}</td>
                                  
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info btn-xs dropdown-toggle"
                                                        data-toggle="dropdown" aria-expanded="false">
                                                    {{ trans('general.choose') }} <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                                    @if(Sentinel::hasAccess('borrowers.view'))
                                                        <li><a href="{{ url('borrower/'.$key->borrower->id.'/show') }}"><i
                                                                        class="fa fa-search"></i> {{trans_choice('general.detail',2)}}
                                                            </a></li>
                                                    @endif
                                                    @if(Sentinel::hasAccess('borrowers.update'))
                                                        <li><a href="{{ url('borrower/'.$key->borrower->id.'/edit') }}"><i
                                                                        class="fa fa-edit"></i> {{ trans('general.edit') }}
                                                            </a>
                                                        </li>
                                                    @endif
                                                    @if(Sentinel::hasAccess('borrowers.groups'))
                                                        <li>
                                                            <a href="{{ url('borrower/group/'.$key->id.'/remove_borrower') }}"
                                                               class="delete"><i
                                                                        class="fa fa-trash"></i> {{ trans('general.remove') }}
                                                            </a>
                                                        </li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </td>
                                        @if($l != null)
                                        @if($l->owing == 1)
                                        <script>
                                            $(document).ready(function(){
     $(".checkuser-{{$key->borrower->id}}").prop('checked', true);
     $(".checkuser-{{$key->borrower->id}}").attr("disabled", true);
 
       })
                                        </script>
                                        <td style="font-weight: bold; color: red;"> @if($loan->status == "closed")(oweing)( {{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value }}{{number_format($l->amount)}})@endif</td>
                                        @else
                                        <td></td>
                                        @endif
                                        @endif
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->

            </div>
            <!-- /.box -->
            
               
            
            
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Account Selection</h3>

                </div>

                <div class="box-body">
                    <div class="table-responsive">
                        <center>
                        <div class="btn-group mr-2" role="group" aria-label="First group">
                            <button type="button" class="btn btn-secondary" style="height: 100px;" onclick="location.href='/borrower/group/{{$req}}/vloans'"> ({{DB::table("grouploan")->where(["group_id"=>$req])->count()}}) View All Loans <i class="fa fa-arrow-right" aria-hidden="true"></i></button>

                        </div>
                        </center>
                    </div>
                </div> 
            </div>




  <div class="modal fade" id="addBorrower">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">*</span></button>
                    <h4 class="modal-title">{{trans_choice('general.add',1)}} {{trans_choice('general.borrower',1)}}</h4>
                </div>
                {!! Form::open(array('url' => url('borrower/group/'.$borrower_group->id.'/add_borrower'),'method'=>'post')) !!}
                <div class="modal-body">
                    <div class="form-group">
                        <div class="form-line">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Borrower ID</label>





                                    <select id="borrower_idx" name="borrower_id" class="form-control ">

                                    </select>  


                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info">{{trans_choice('general.save',1)}}</button>
                    <button type="button" class="btn default"
                            data-dismiss="modal">{{trans_choice('general.close',1)}}</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
        </div>
        
        </div>
        </div>
@endsection
@section('footer-scripts')

<script>
    $(document).ready(function(){
        $('#borrower_idx').selectize({
            preload: true,
            valueField: 'id',
            labelField: 'name',
            searchField: 'name',
            load: function(query, callback) {
                $.ajax({
                    url: "/borrower/search" + "/?q=" + encodeURIComponent(query),
                    type: 'GET',
                    error: function() {
                        callback();
                    },
                    success: function(result) {
                        callback(result)
                    }
                });
            }
        });
    })
    
    function approve(){
        $("#approve").modal("show")
    }

</script>
@endsection