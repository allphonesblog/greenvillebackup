<!DOCTYPE html>
<html>
<head>
  <title>Memory Exhausted - Server Error</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <style>
    body {
      font-family: Arial, sans-serif;
      margin: 0;
      padding: 0;
      background-color: #f8f8f8;
    }

    .container {
      max-width: 600px;
      margin: 0 auto;
      padding: 20px;
      background-color: #fff;
      box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
      border-radius: 4px;
      margin-top: 50px;
    }

    h1 {
      font-size: 24px;
      color: #333;
      margin: 0;
      margin-bottom: 10px;
    }

    p {
      color: #666;
      margin: 0;
    }

    table {
      border-collapse: collapse;
      width: 100%;
      margin-top: 20px;
    }

    th,
    td {
      border: 1px solid #ddd;
      padding: 8px;
      text-align: left;
    }

    th {
      background-color: #f2f2f2;
    }

    /* Responsive adjustments */
    @media screen and (max-width: 640px) {
      .container {
        margin: 20px;
        padding: 10px;
      }

      h1 {
        font-size: 20px;
      }

      table {
        font-size: 14px;
      }
    }
  </style>
</head>

<body>
  <div class="container">
    <center>
      <img src="https://static.vecteezy.com/system/resources/previews/004/607/677/original/caution-danger-sign-free-vector.jpg" width="100">
    </center>
    <h1>Memory Exhausted - Server Error</h1>
    <p>The server has encountered a memory exhaustion issue. Please move the 'green_back' folder to GitHub or GitLab. Currently, 70% of the RAM is used.</p>
    <h2>Server Information</h2>
    <table>
      <tr>
        <th>Parameter</th>
        <th>Value</th>
      </tr>
      <tr>
        <td>Server Storage</td>
        <td><?php echo disk_total_space('/') / (1024 * 1024 * 1024); ?> GB</td>
      </tr>
      <tr>
        <td>Server Storage Free</td>
        <td><?php echo 209.5325;?> GB</td>
      </tr>
      <tr>
        <td>RAM Total</td>
        <td><?php echo round(shell_exec('free -h | grep Mem | awk \'{print $2}\'')); ?></td>
      </tr>
      <tr>
        <td>RAM Used</td>
        <td><?php echo round(shell_exec('free -h | grep Mem | awk \'{print $3}\'')); ?></td>
      </tr>
      <tr>
        <td>PHP Version</td>
        <td><?php echo phpversion(); ?></td>
      </tr>
      <tr>
        <td>PHP Memory Limit</td>
        <td><?php echo ini_get('memory_limit'); ?></td>
      </tr>
      <!-- Add more PHP details as needed -->
    </table>
  </div>
</body>
</html>
