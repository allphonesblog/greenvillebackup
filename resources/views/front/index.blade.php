<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- SEO Meta Tags -->
    <meta name="description" content="Quickly build an efficient and attractive online presence for a loans and credit business with this Bootstrap 4 landing page template.">
    <meta name="author" content="Inovatik">
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-7506822283034287"
     crossorigin="anonymous"></script>
    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
	<meta property="og:site_name" content="" /> <!-- website name -->
	<meta property="og:site" content="" /> <!-- website link -->
	<meta property="og:title" content=""/> <!-- title shown in the actual shared post -->
	<meta property="og:description" content="" /> <!-- description shown in the actual shared post -->
	<meta property="og:image" content="" /> <!-- image link, make sure it's jpg -->
	<meta property="og:url" content="" /> <!-- where do you want your post to link to -->
	<meta property="og:type" content="article" />

    <!-- Website Title -->
    <title>GreenVilleMfb</title>

    <!-- Styles -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <link href="front/css/bootstrap.css" rel="stylesheet">
    <link href="front/css/fontawesome-all.css" rel="stylesheet">
    <link href="front/css/swiper.css" rel="stylesheet">
	<link href="front/css/magnific-popup.css" rel="stylesheet">
	<link href="front/css/styles.css" rel="stylesheet">
 
	<!-- Favicon  -->
    <link rel="icon" href="front/images/favicon.png">
  
</head>
<body data-spy="scroll" data-target=".fixed-top">
    
    <!-- Preloader -->

    <!-- end of preloader -->
 
    <!-- Navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top navbar-custom">

        <!-- Text Logo - Use this if you don't have a graphic logo -->
        <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Cedo</a> -->

        <!-- Image Logo -->
        <a class="navbar-brand logo-image page-scroll" href="index.html"><img src="/logox.png" alt="logo" style="height:50px;width:50px;"></a>
        
        <!-- Mobile Menu Toggle Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#header">Home <span class="sr-only">(current)</span></a>
                </li>
              

                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#contact">Contact</a>
                </li>
            </ul>
        </div>
    </nav> <!-- end of navbar -->
    <!-- end of navbar -->


    <!-- Header -->
    <header id="header" class="header">

        <!-- Header Slider -->
        <div class="swiper-container header-slider">
            <div class="swiper-wrapper">

                <!-- Slide -->
                <div class="swiper-slide first">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-container">
                                    <h1 class="h1-large">Greenville Microfinance Bank</h1>
                                             <a class="btn-solid-lg " href="/admin">Admin Area</a>
                                </div>
                            </div> <!-- end of col -->
        
                            <div class="col-md-6">
                                <div class="image-container">
                                    <img class="header-image" src="front/images/woman-at-desk.svg" alt="description">
                                </div>
                            </div> <!-- end of col -->
                        </div> <!-- end of row -->
                    </div> <!-- end of container -->
                </div> <!-- end of swiper-slide -->
                <!-- end of slide -->

                <!-- Slide -->
                <div class="swiper-slide second">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="image-container">
                                    <img class="header-image" src="front/images/woman-at-desk-2.svg" alt="description">
                                </div>
                            </div> <!-- end of col -->
                              <div class="col-md-6">
                                <div class="text-container">
                                    <h1 class="h1-large">Greenville Microfinance Bank</h1>
                                             <a class="btn-solid-lg " href="/admin">Admin Area</a>
                                </div>
                            </div> 
                        </div> <!-- end of row -->
                        </div> <!-- end of container -->
                    </div> <!-- end of swiper-slide -->
                    <!-- end of slide -->

                    <!-- Slide -->
                <div class="swiper-slide third">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-container">
                                    <h1 class="h1-large">Greenville Microfinance Bank</h1>
                                    <a class="btn-solid-lg " href="/admin">Admin Area</a>
                            </div> <!-- end of col -->
                        </div> <!-- end of row -->
                    </div> <!-- end of container -->
                </div> <!-- end of swiper-slide -->
                <!-- end of slide -->

            </div> <!-- end of swiper-wrapper -->
        </div> <!-- end of swiper-container -->
        <!-- end of header slider -->
        
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
        <!-- end of add pagination -->

    </header> <!-- end of header -->
    <!-- end of header -->





    
    <!-- Contact -->
    <div id="contact" class="form">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center">Contact Info</h2>
                    <p class="p-heading text-center">Address: 135 Igando Road
                        Alimosho
                        Lagos    Nigeria
                        <br>Phone: +234 0802 939 9432,
                        +234 0703 339 8213, Email: <a class="underline" href="mailto:info@greenvillemfb.com">info@greenvillemfb.com</a>, Web: <a class="underline" href="http://greenvillemfb.com.ng/">www.greenvillemfb.com.ng</a></p>
                </div> <!-- end of col -->
            </div> <!-- end of row -->

            <div class="row">
                <div class="col-lg-6">
                    <div class="map-responsive">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.7640596657757!2d3.252933814770848!3d6.551444495261555!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103b8522372e6307%3A0x52cea900444aa7de!2sGREENVILLE%20MFB!5e0!3m2!1sen!2suk!4v1624060922349!5m2!1sen!2suk" allowfullscreen></iframe>
                    </div>
                </div> <!-- end of col -->
                <div class="col-lg-6">
                    
                    <!-- Contact Form -->
                    <form id="ContactForm" data-toggle="validator" data-focus="false">
                        <div class="form-group">
                            <input type="text" class="form-control-input" id="cname" required>
                            <label class="label-control" for="cname">Name</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control-input" id="cemail" required>
                            <label class="label-control" for="cemail">Email</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control-textarea" id="cmessage" required></textarea>
                            <label class="label-control" for="cmessage">Your message</label>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="form-control-submit-button">Submit</button>
                        </div>
                        <div class="form-message">
                            <div id="cmsgSubmit" class="h3 text-center hidden"></div>
                        </div>
                    </form>
                    <!-- end of contact form -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of form -->
    <!-- end of contact -->


    <!-- Footer -->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="footer-col">
                        <h4>About Greenville</h4>
                        <p>Account Opening, Loans, Assets Loan, Mobile Banking and lots more</p>
                    </div>
                </div> <!-- end of col -->

               <!-- <div class="col-md-4">
                    <div class="footer-col float-lg-right">
                        <h4>Social Media</h4>
                        <span class="fa-stack fa-lg">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x facebook"></i>
                                <i class="fab fa-facebook-f fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack fa-lg">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x twitter"></i>
                                <i class="fab fa-twitter fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack fa-lg">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x google-plus"></i>
                                <i class="fab fa-google-plus-g fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack fa-lg">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x instagram"></i>
                                <i class="fab fa-instagram fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack fa-lg">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x linkedin"></i>
                                <i class="fab fa-linkedin-in fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack fa-lg">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x dribbble"></i>
                                <i class="fab fa-dribbble fa-stack-1x"></i>
                            </a>
                        </span>
                    </div> 
                </div>//--> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">Copyright ©  Greenville {{Date("Y")}}</p>
                    </div> <!-- end of col -->
                </div> <!-- enf of row -->
            </div> <!-- end of container -->
        </div> <!-- end of copyright -->
    </div> <!-- end of footer -->  
    <!-- end of footer -->
    
    	<style>
              .spinner-wrapper,    .navbar-custom.top-nav-collapse,  .footer{
                background: #337335 !important;
            }
            .footer .copyright{
                background: black !important;
            }
            .form-control-submit-button   {
                background: #97be5d !important;
                border: none;

            }
            .form-control-submit-button:hover{
                color: white !important;
            }
            
       a.back-to-top     {
           background:#337335 url("/front/images/up-arrow.png") no-repeat center 47% !important;
            }
        </style>
    <!-- Scripts -->
    <script src="front/js/jquery.min.js"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="front/js/popper.min.js"></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="front/js/bootstrap.min.js"></script> <!-- Bootstrap framework -->
    <script src="front/js/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="front/js/swiper.min.js"></script> <!-- Swiper for image and text sliders -->
    <script src="front/js/jquery.magnific-popup.js"></script> <!-- Magnific Popup for lightboxes -->
    <script src="front/js/validator.min.js"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="front/js/scripts.js"></script> <!-- Custom scripts -->
        <style>

            .navbar-custom {
                background-color: #337335 !important;
            }
        </style>
        <div class="loader"></div>
        <style>
            .loader{
                position: fixed;
                left: 0px;
                top: 0px;
                display: block;
                background-size: 200px 200px !important;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('https://greenvillemfb.com.ng/logox.png') 
                    50% 50% no-repeat rgb(249,249,249);
            }
        </style>
        <script>
            $(document).ready(function(){
                setTimeout(function(){
                    $(".loader").hide()
                },2000)
            })
        </script>
</body>
</html>