@extends('layouts.master')
@section('title')
    POS charges
@endsection
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">POS Fees</h3>

            <div class="box-tools pull-right">
                @if(Sentinel::hasAccess('expenses.create'))
                    <a href="#"
                    data-toggle="modal" data-target="#create"
                       class="btn btn-info btn-sm">{{trans_choice('general.add',1)}}  Fee</a>
                @endif
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table id="data-table" class="table table-bordered table-condensed table-hover">
                    <thead>
                    <tr>
                        
                        <th>Title</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $key)
                        <tr>
                        <td>{{ $key->title }}</td>
                            <td>
                                NGN{{number_format($key->amount,2)}}
                            </td>
                        
                            <td>{{ $key->date }}</td>
                            
                            
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info btn-xs dropdown-toggle"
                                            data-toggle="dropdown" aria-expanded="false">
                                        {{ trans('general.choose') }} <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        @if(Sentinel::hasAccess('expenses.update'))
                                            <li><a href="#" onclick="edit('{{$key->id}}','{{$key->title}}','{{$key->date}}','{{$key->amount}}')"><i
                                                            class="fa fa-edit"></i> {{ trans('general.edit') }} </a>
                                            </li>
                                        @endif
                                        @if(Sentinel::hasAccess('expenses.delete'))
                                            <li><a href="#" onclick="delete_fee({{$key->id}})"
                                                   class="delete"><i
                                                            class="fa fa-trash"></i> {{ trans('general.delete') }} </a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    <div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Fee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="create_fee">
      <div class="modal-body">
        <div class="form-group">
            <label>Title</label>
<input type="text" class="form-control" id="title" required>
</div>

<div class="form-group">
    <label>Amount</label>
<input type="number" class="form-control" id="amount" required>
</div>

<div class="form-group">
    <label>Date</label>
<input type="date" class="form-control" id="date" required>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save </button>
      </div>
</form>
    </div>
  </div>
</div>

<div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Fee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="update_fee">
      <div class="modal-body">
        <div class="form-group">
            <label>Title</label>
<input type="text" class="form-control" id="etitle" required>
</div>

<div class="form-group">
    <label>Amount</label>
<input type="number" class="form-control" id="eamount" required>
</div>

<div class="form-group">
    <label>Date</label>
<input type="date" class="form-control" id="edate" required>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
</form>
    </div>
  </div>
</div>

@endsection
@section('footer-scripts')
    <script src="{{ asset('assets/plugins/datatable/media/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/media/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.colVis.min.js')}}"></script>
    <input type="hidden" id="id">
    <script>

function edit(id,title,date,amount){
    $("#editmodal").modal("show")
    $("#etitle").val(title)
    $("#id").val(id)
    $("#edate").val(date)
    $("#eamount").val(amount)
}
        $('#data-table').DataTable( );

        $("#create_fee").submit((event)=>{
            event.preventDefault();
            $("button").attr("disabled",true)
            const title = $("#title").val()
            const amount = $("#amount").val()
            const date = $("#date").val()
            $.post('/expense/new_fee',{
                title,
                amount,
                date,
                "_token":"{{csrf_token()}}"
            }).then((res)=>{
                swal("",res.message,"success")
                location.reload()
            })
        })

        $("#update_fee").submit((event)=>{
            event.preventDefault();
            $("button").attr("disabled",true)
            const title = $("#etitle").val()
            const amount = $("#eamount").val()
            const date = $("#edate").val()
            const id = $("#id").val()
            $.post('/expense/update_fee',{
                title,
                amount,
                date,
                id,
                "_token":"{{csrf_token()}}"
            }).then((res)=>{
                swal("",res.message,"success")
                location.reload()
            })
        })
     
        function delete_fee(id){
           $("#id").val(id)
      
        }

      $(document).ready(()=>{
        $("#delete_btn").click(()=>{
            const feeid = $("#id").val()
            swal({
                title: 'Are you sure?',
                    text: '',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ok',
                    cancelButtonText: 'Cancel'
}).then((willDelete) => {
  
 
            $.post('/expense/delete_fee',{
               
                id : feeid,
                "_token":"{{csrf_token()}}"
            }).then((res)=>{
                swal("",res.message,"success")
                location.reload()
            })
  
});
        })
      })
    </script>
@endsection
