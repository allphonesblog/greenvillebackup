@extends('layouts.master')
@section('title')
    {{trans_choice('general.saving',2)}} {{trans_choice('general.fee',2)}}
@endsection
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans_choice('general.saving',2)}} {{trans_choice('general.fee',2)}}</h3>

            <div class="box-tools pull-right">
                @if(Sentinel::hasAccess('savings.fees'))
                    <a href="{{ url('saving/savings_fee/create') }}"
                       class="btn btn-info btn-sm">{{trans_choice('general.add',1)}} {{trans_choice('general.saving',2)}} {{trans_choice('general.fee',1)}}</a>
                @endif
            </div>
        </div>
        <div class="box-body ">
            <div class="table-responsive">
                <table id="data-table" class="table table-bordered table-condensed table-hover">
                    <thead>
                    <tr style="background-color: #D1F9FF">
                        <th>{{trans_choice('general.product',1)}}</th>
                        <th>{{trans_choice('general.fee_posting_frequency',1)}}</th>
                        <th>{{trans_choice('general.amount',1)}} </th>
                        <th>{{ trans_choice('general.action',1) }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $key)
                        <tr>
                            <td>{{ $key->name }}</td>
                            <td>
                                @if($key->fees_posting==1)
                                    {{trans_choice('general.every_1_month',1)}}
                                @endif
                                @if($key->fees_posting==2)
                                    {{trans_choice('general.every_2_month',1)}}
                                @endif
                                @if($key->fees_posting==3)
                                    {{trans_choice('general.every_3_month',1)}}
                                @endif
                                @if($key->fees_posting==4)
                                    {{trans_choice('general.every_4_month',1)}}
                                @endif
                                @if($key->fees_posting==5)
                                    {{trans_choice('general.every_6_month',1)}}
                                @endif
                                @if($key->fees_posting==6)
                                    {{trans_choice('general.every_12_month',1)}}

                                @endif
                            </td>
                            <td>{{ $key->amount }}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info btn-xs dropdown-toggle"
                                            data-toggle="dropdown" aria-expanded="false">
                                        {{ trans('general.choose') }} <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                        @if(Sentinel::hasAccess('savings.fees'))
                                            <li><a href="{{ url('saving/savings_fee/'.$key->id.'/edit') }}"><i
                                                            class="fa fa-edit"></i> {{ trans('general.edit') }} </a>
                                            </li>
                                        @endif
                                        @if(Sentinel::hasAccess('savings.fees'))
                                            <li><a href="{{ url('saving/savings_fee/'.$key->id.'/delete') }}"
                                                   class="delete"><i
                                                            class="fa fa-trash"></i> {{ trans('general.delete') }} </a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('footer-scripts')
    <script src="{{ asset('assets/plugins/datatable/media/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/media/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.colVis.min.js')}}"></script>
    <script>
        $('#data-table').DataTable();
    </script>
@endsection
