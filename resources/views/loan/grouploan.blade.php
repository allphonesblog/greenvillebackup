@extends('layouts.master')
@section('title')Group Loans
@endsection
@section('content')

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">
            <select id="changestatus">
                <option>Select Status type</option>
                <option value="approved">Approve Selected</option>
                <option value="disburse">Disburse Selected</option>
                <option value="undisburse">UnDisburse Selected</option>
                <option value="delete">Delete Selected</option>
            </select> |
            <select id="cstatus">
                <option>Change Status</option>
                <option value="pending">Pending Group Loan</option>
                <option value="approved">Approved Group Loan</option>
                <option value="disbursed">Disbursed Group Loan</option>
            </select>
        </h3>
        <div class="box-tools pull-right">


            @if(Sentinel::hasAccess('loans.create'))



            <a href='{{ url("/borrower/group/data") }}'
               class="btn btn-info btn-sm">View group</a>
            @endif
        </div>

        <div class="box-body table-responsive">

            <table id="data-table" class="table table-bordered table-striped table-condensed table-hover">

                <thead>
                    <tr>
                        <td><a href="#" onclick="c_all()">Toggle check</a></td>
                        <th>Group Name</th>
                        <th>Loan Account</th>
                        <th>Approved Amount</th>
                        <th>Principal</th>
                        <th>Loan Status</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                </thead>


                <tfoot>
                    <tr>
<td>-</td>
                        <th>Group Name</th>
                        <th>Loan Account</th>
                        <th>Approved Amount</th>
                        <th>Principal</th>
                        <th>Loan Status</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                </tfoot>

            </table>
        </div>
        <!-- /.box-body -->

    </div>
    <!-- /.box -->
    @endsection

    @section('footer-scripts')
    <script src="{{ asset('assets/plugins/datatable/media/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/media/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.colVis.min.js')}}"></script>
    <script src="https://cdn.datatables.net/scroller/2.0.3/js/dataTables.scroller.min.js"></script>

    <div class="modal fade" id="approvedloan">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">*</span></button>
                    <h4 class="modal-title">{{trans_choice('general.approve',1)}} {{trans_choice('general.loan',1)}}</h4>
                </div>
                {!! Form::open(array('url' => url('loan/approvebulk'),'method'=>'post', 'class'=>'lapprove')) !!}
                <div class="modal-body">
                    <div class="form-group">
                        <div class="form-line">
                            {!!  Form::label('approved_date',null,array('class'=>' control-label')) !!}
                            {!! Form::text('approved_date',date("Y-m-d"),array('class'=>'form-control date-picker approved_date','required'=>'required')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-line">

                            <label class="control-label">Approved Amount</label>
                            <input type="number" class="form-control" placeholder="Approved Amount" id="approved_amount">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-line">
                            {!!  Form::label( 'Notes',null,array('class'=>' control-label')) !!}
                            {!! Form::textarea('approved_notes','',array('class'=>'form-control approved_notes','rows'=>'3')) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info sender2">{{trans_choice('general.save',1)}}</button>
                    <button type="button" class="btn default "
                            data-dismiss="modal">{{trans_choice('general.close',1)}}</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="disburseLoan">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">*</span></button>
                    <h4 class="modal-title">{{trans_choice('general.disburse',1)}} {{trans_choice('general.loan',1)}}</h4>
                </div>
                <form id="disbursecheck">

                    <div class="modal-body">
                        <div class="form-group">
                            <div class="form-line">
                                {!!  Form::label('disbursed_date',null,array('class'=>' control-label')) !!}
                                {!! Form::text('disbursed_date',Date("Y-m-d"),array('class'=>'form-control date-picker gdisbursed_date','required'=>'required')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                {!!  Form::label('first_payment_date',null,array('class'=>' control-label')) !!}
                                {!! Form::text('first_payment_date',Date("Y-m-d"),array('class'=>'form-control date-picker gfirst_payment_date',''=>'')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                {!!  Form::label('loan_disbursed_by_id',"Disbursed By",array('class'=>' control-label')) !!}
                                {!! Form::select('loan_disbursed_by_id',$loan_disbursed_by,null,array('class'=>'form-control gloan_disbursed_by_id','required'=>'required')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                {!!  Form::label( 'Notes',null,array('class'=>' control-label')) !!}
                                {!! Form::textarea('disbursed_notes','',array('class'=>'form-control gnotes','rows'=>'3')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info sender">{{trans_choice('general.save',1)}}</button>
                        <button type="button" class="btn default"
                                data-dismiss="modal">{{trans_choice('general.close',1)}}</button>
                    </div>

                </form>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>


    <script>
        $('table').DataTable( {
            "ajax":"/loan/group/grouploanapi",
            "order": [[ 3, "desc" ]]
            ,
            aoColumnDefs: [
                {
                    bSortable: false,
                    aTargets: [ 0 ]
                }
            ]
        });
        $("#cstatus").change(function(){
            $("table").dataTable().fnDestroy()
            var s = $(this).find(":selected").val()
            var url = "/loan/group/grouploanapi?status="+s
            $('table').DataTable( {
                "ajax":url,
                "order": [[ 3, "desc" ]],
                aoColumnDefs: [
                    {
                        bSortable: false,
                        aTargets: [ 0 ]
                    }
                ]
            });
            })
        
        function c_all(){
            $('input[type=checkbox]').trigger('click'); 
        }

        function approved(){

            swal({
                title: '{{trans_choice('general.are_you_sure',1)}}',
                text: '',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{trans_choice('general.ok',1)}}',
                cancelButtonText: '{{trans_choice('general.cancel',1)}}'
            })
                .then((willDelete) => {
                if (willDelete) {
                    var ids = []

                    $("body").find("input[type='checkbox']:checked").map(function(){

                        ids.push($(this).val())

                    })
                    $(".sender2").attr("disabled",true)

                    $.post('/borrower/group/bulkapprovedcheck',{
                        approved_notes: $("#approved_notes").val(),
                        approved_date: $("#approved_date").val(),
                        approved_amount: $("#approved_amount").val(),
                        ids: ids ,
                        "_token":"{{csrf_token()}}"
                    }).done(function(res){
                        $(".sender").attr("disabled",false)

                        $("table").dataTable().fnDestroy()
                        $('table').DataTable( {
                            "ajax":"/loan/group/grouploanapi",
                            "order": [[ 3, "desc" ]]
                            ,
                            aoColumnDefs: [
                                {
                                    bSortable: false,
                                    aTargets: [ 0 ]
                                }
                            ]
                        });
                        $("select").find('option').attr("selected","") ;
                        swal("Success",res.message, "success")
                        $(".sender2").attr("disabled",false)
                        location.reload()
                    }).fail(function(res){
                        $("select").find('option').attr("selected","") ;
                        $(".sender2").attr("disabled",false)
                        swal("Oops..",res.responseJSON.message, "error")
                    })
                } else {
                    $(':input').val('');
                    return false;
                }
            });
        }

        
        function bulkdeleteloan(){

            swal({
                title: '{{trans_choice('general.are_you_sure',1)}}',
                text: '',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{trans_choice('general.ok',1)}}',
                cancelButtonText: '{{trans_choice('general.cancel',1)}}'
            })
                .then((willDelete) => {
                if (willDelete) {
                    var ids = []

                    $("body").find("input[type='checkbox']:checked").map(function(){

                        ids.push($(this).val())

                    })

                    $.post('/borrower/group/bulkdeleteloan',{

                        ids: ids ,
                        "_token":"{{csrf_token()}}"
                    }).done(function(res){
                        $(".sender").attr("disabled",false)
                        $("table").dataTable().fnDestroy()
                        $('table').DataTable( {
                            "ajax":"/loan/group/grouploanapi",
                            "order": [[ 3, "desc" ]]
                            ,
                            aoColumnDefs: [
                                {
                                    bSortable: false,
                                    aTargets: [ 0 ]
                                }
                            ]
                        });
                        $("select").find('option').attr("selected","") ;
                        swal("Success",res.message, "success")
                        location.reload()
                    }).fail(function(res){
                        $("select").find('option').attr("selected","") ;
                        $(".sender").attr("disabled",false)
                        swal("Oops..",res.responseJSON.message, "error")
                    })
                } else {
                    $(':input').val('');
                    return false;
                }
            });
        }
        function undisburse(){

            swal({
                title: '{{trans_choice('general.are_you_sure',1)}}',
                text: '',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{trans_choice('general.ok',1)}}',
                cancelButtonText: '{{trans_choice('general.cancel',1)}}'
            })
                .then((willDelete) => {
                if (willDelete) {
                    var ids = []

                    $("body").find("input[type='checkbox']:checked").map(function(){

                        ids.push($(this).val())

                    })

                    $.post('/borrower/group/bulkundisbursecheck',{

                        ids: ids ,
                        "_token":"{{csrf_token()}}"
                    }).done(function(res){
                        $(".sender").attr("disabled",false)
                        $("table").dataTable().fnDestroy()
                        $('table').DataTable( {
                            "ajax":"/loan/group/grouploanapi",
                            "order": [[ 3, "desc" ]]
                            ,
                            aoColumnDefs: [
                                {
                                    bSortable: false,
                                    aTargets: [ 0 ]
                                }
                            ]
                        });

                        swal("Success",res.message, "success")
                        location.reload()
                    }).fail(function(res){
                        $("select").find('option').attr("selected","") ;
                        $(".sender").attr("disabled",false)
                        swal("Oops..",res.responseJSON.message, "error")
                    })
                } else {
                    $(':input').val('');
                    return false;
                }
            });

        }
        $("#changestatus").change(function(){
            var status = $(this).find(":selected").val()
            if(status =="disburse"){
                disburse()
            }

            if(status == "undisburse"){
                undisburse()
            }
            if(status == "approved"){
                approvedloan()
            }
            
            if(status == "delete"){
                bulkdeleteloan()
            }
        })
        
        $("#disbursecheck").submit(function(){
            event.preventDefault();
            $(".sender").attr("disabled",true)
            var ids = []

            $("body").find("input[type='checkbox']:checked").map(function(){

                ids.push($(this).val())

            })

            const gdisbursed_date = $(".gdisbursed_date").val()
            const gfirst_payment_date = $(".gfirst_payment_date").val()
            const gloan_disbursed_by_id = $(".gloan_disbursed_by_id").val()
            const gnotes = $(".gnotes").val()
            $.post('/borrower/group/bulkdisbursecheck',{
                disbursed_date: gdisbursed_date,
                ids: ids,
                first_payment_date: gfirst_payment_date,
                loan_disbursed_by_id: gloan_disbursed_by_id,
                disbursed_notes: gnotes,
                "_token":"{{csrf_token()}}"
            }).done(function(res){
                $(".sender").attr("disabled",false)
                $("table").dataTable().fnDestroy()
                $('table').DataTable( {
                    "ajax":"/loan/group/grouploanapi",
                    "order": [[ 3, "desc" ]]
                    ,
                    aoColumnDefs: [
                        {
                            bSortable: false,
                            aTargets: [ 0 ]
                        }
                    ]
                });
                swal("Success",res.message, "success")
               location.reload()
            }).fail(function(res){
                $("select").find('option').attr("selected","") ;
                $(".sender").attr("disabled",false)
                swal("Oops..",res.responseJSON.message, "error")
            })
        })

        $(".lapprove").submit(function(){
            event.preventDefault();
            approved()

        })
        
        function disburse(){

            $("#disburseLoan").modal("show")


        }

        function approvedloan(){
            $("#approvedloan").modal("show")
        }
        
        function approve(){
            $("#approve").modal("show")
        }
    </script>
    @endsection