@extends('layouts.master')
@section('title')@if($g != null) {{$g->name}} @else {{trans_choice('general.add',1)}} Group {{trans_choice('general.bulk',1)}} {{trans_choice('general.repayment',2)}} @endif
@endsection
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/js/standalone/selectize.js" integrity="sha512-pF+DNRwavWMukUv/LyzDyDMn8U2uvqYQdJN0Zvilr6DDo/56xPDZdDoyPDYZRSL4aOKO/FGKXTpzDyQJ8je8Qw==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/css/selectize.bootstrap3.css" integrity="sha512-IvEBCcESwoUfopjiLqNEtnEO7l1Flm1T3tEPe0aywpd1B5xG/PedbsB0nKMgbvQOLc+D+FesXFhOp/x4dgwd2A==" crossorigin="anonymous" />


    <script type="text/javascript">
        var inputRepaymentAmountTotal = 0;
        function updatesum(id) {


        var inputRepaymentAmountTotal = $(".amountinput").map(function(i){
var a = $(this).val()
if(a == ""){
   a = 0
}else{
    a= a

}


            return parseInt(a) * 100
        }).get()

        document.getElementById("RepaymentAmountTotal").innerHTML = numberWithCommas((inputRepaymentAmountTotal.reduce((a, b) => a + b, 0) / 100).toFixed(2));
        }
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    </script>



{!! Form::open(array('url' => url('repayment/bulk/group/store'), 'method' => 'post','id'=>'form', 'class' => 'form-horizontal')) !!}
@if($gg != null)
<input type="hidden" name="sc" value="{{$gg->sc}}">
@else

@endif
    <input type="hidden" name="bulk_upload" value="1">

    <div class="box box-info">




        <div class="box-body">

            <p>{{trans_choice('general.bulk_repayments_msg',1)}}</p>
            <h4>
                Select group
            </h4>
            <select id="groups" name="loan_id" class="">

            </select>

            <script>

                    $('#groups').selectize({
                        preload: true,
                        valueField: 'id',
                        labelField: 'name',
                        searchField: 'name',
                        width: '100%',
                        load: function(query, callback) {
                            $.ajax({
                                url: "/loan/groups" + "/?q=" + encodeURIComponent(query),
                                type: 'GET',
                                error: function() {
                                    callback();
                                },
                                success: function(result) {
                                    callback(result)
                                }
                            });
                        }
                    });
                    
                
                $("#groups").change(function(){
                    var id = $(this).find(":selected").val()
                    location.href = '?groupid='+id
                })


            </script>
            <table id="editrow" class="table table-bordered table-hover">
                <thead>
                <tr class="bg-blue">
                    <th>{{trans_choice('general.row',1)}}</th>
                    <th>{{trans_choice('general.loan',1)}}</th>
                    <th>{{trans_choice('general.amount',1)}}</th>
                    <th>{{trans_choice('general.method',1)}}</th>
                    <th>{{trans_choice('general.collection',1)}} {{trans_choice('general.date',1)}}</th>
                    <th>{{trans_choice('general.description',1)}} ({{trans_choice('general.optional',1)}})</th>
                </tr>
                </thead>
                <tbody>

                    @foreach($users as $group)
                    <?php
                        $user= DB::table("borrowers")->where(["id"=>$group->borrower_id])->first();

                        $loan = DB::table("loans")->where(["sc"=>$group->sc,"borrower_id"=>$group->borrower_id])->first();
                        $count = $loan->id;


                        $id = "inputRepaymentAmount$count";
                ?>
@if($user != null && $loan != null)
                <tr>
                    <td>
                        {{$count}}
                    </td>
                    <td>

                        {{$user->first_name}} {{$user->last_name}} - DUE: {{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value }}{{number_format(\App\Helpers\GeneralHelper::loan_total_balance($loan->id))}}
                        <input type="hidden" name="loan_id{{$loan->id}}" value="{{$loan->id}}">
                    </td>
                    <td>
                        {!! Form::text('repayment_amount'.$count,null, array('class' => 'form-control touchspin amountinput', 'id'=>"inputRepaymentAmount$count",'onKeyUp'=>"updatesum('$id')")) !!}
                    </td>
                    <td>
                        {{Form::select('repayment_method_id'.$count,$repayment_methods,null,array('class'=>'form-control select2','id'=>'inputRepaymentMethodId'.$count,'style'=>''))}}
                        <small><a href="#" id="SetDefaultMethods">{{trans_choice('general.set_default',1)}}</a></small>
                        <script type="text/javascript">
                            $("#SetDefaultMethods{{$count}}").click(function () {
                                var inputRepaymentMethodId{{$count}} = $("#nputRepaymentMethodId{{$count}} option:selected").index() + 1;
                                for (var i = 2; i <= 20; i++) {
                                    $("#inputRepaymentMethodId" + {{$count}} + " :nth-child(" + inputRepaymentMethodId{{$count}} + ")").prop("selected", true);
                                }
                            });
                        </script>
                    </td>
                    <td>
                        {!! Form::text('repayment_collected_date'.$count,date("Y-m-d"), array('class' => 'form-control date-picker', 'id'=>"inputRepaymentDate".$count,)) !!}

                        <small><a href="#" id="SetDefaultDates{{$count}}">Set Default</a></small>
                        <script type="text/javascript">
                            $("#SetDefaultDates{{$count}}").click(function () {
                                var inputRepaymentDate{{$count}} = document.getElementById("inputRepaymentDate{{$count}}").value;

                                    $("#inputRepaymentDate{{$count}}").val(inputRepaymentDate{{$count}});

                            });
                        </script>
                    </td>
                    <td>
                        {!! Form::text('repayment_description'.$count,null, array('class' => 'form-control', 'id'=>"inputDescription".$count,)) !!}

                    </td>
                </tr>
                    @endif
                    <script>
                        $(document).ready(function(){
                            $('#inputLoanId{{$count}}').selectize({
                                preload: true,
                                valueField: 'id',
                                labelField: 'name',
                                searchField: 'name',
                                width: '100%',
                                load: function(query, callback) {
                                    $.ajax({
                                        url: "/bulkapi" + "/?q=" + encodeURIComponent(query),
                                        type: 'GET',
                                        error: function() {
                                            callback();
                                        },
                                        success: function(result) {
                                            callback(result)
                                        }
                                    });
                                }
                            });
                            $('.select2-container').css("width","100%");
                        })

                    </script>
              @endforeach

                <tr>
                    <td>&nbsp;</td>
                    <td class="text-bold text-right">
                        {{trans_choice('general.total',1)}}:
                    </td>
                    <td class="text-bold text-right">
                        <div id="RepaymentAmountTotal">0</div>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
            <button type="submit" class="btn btn-info pull-right" class="btn btn-info pull-right"
                    data-loading-text="<i class='fa fa-spinner fa-spin '></i> Please Wait. This can take a few minutes.">
                {{trans_choice('general.submit',1)}}
            </button>

            <script type="text/javascript">
                $('#form').on('submit', function (e) {

                    $(this).find('button[type=submit]').prop('disabled', true);
                    $('.btn').prop('disabled', true);
                    $('.btn').button('loading');
                    return true;
                });
            </script>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    {!! Form::close() !!}
<style>
    .selectize-input {
        min-width: 300px !important;
    }
</style>
@endsection
