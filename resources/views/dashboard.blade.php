@extends('layouts.master')

@section('title')
    {{ trans('general.dashboard') }}
@endsection
@section('current-page-title')
    {{ trans('general.dashboard') }}
@endsection
@section('current-page')
    {{ trans('general.dashboard') }}
@endsection
@section('content')
 @if(session("branch_id") == 1)
<div class="row">
    @if(Sentinel::hasAccess('dashboard.registered_borrowers'))
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">{{ trans_choice('general.registered',1) }}
                    <br>{{ trans_choice('general.borrower',2) }}</span>
                <span class="info-box-number">{{ \App\Models\Borrower::count() }}</span>
            </div>
        </div>
    </div>
    @endif
    <div class="clearfix visible-sm-block"></div>
    @if(Sentinel::hasAccess('dashboard.total_loans_released'))
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-files-o"></i></span>
<?php

$data = DB::table("loan_schedules")->sum("interest");
$prin = DB::table('loans')->sum("principal");

$totalre = $data  + $prin;

?>
            <div class="info-box-content">
                <span class="info-box-text">{{ trans_choice('general.total',1) }} {{ trans_choice('general.loan',2) }}
                    <br>{{ trans_choice('general.released',1) }}</span>
                @if(\App\Models\Setting::where('setting_key', 'currency_position')->first()->setting_value=='left')
                <span class="info-box-number"> {{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value }} {{ number_format($totalre,2) }} </span>
                @else
                <span class="info-box-number"> {{ number_format($totalre,2) }}  {{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value}}</span>
                @endif

            </div>
        </div>
    </div>
    @endif
    @if(Sentinel::hasAccess('dashboard.total_collections'))
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-dollar"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">{{ trans_choice('general.total',1) }}
                    <br>{{ trans_choice('general.collection',2) }}</span>

                <span class="info-box-number" id="t_1"> loading... </span>

            </div>

        </div>

    </div>
    @endif
    @if(Sentinel::hasAccess('dashboard.loans_disbursed'))
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-balance-scale"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">{{ trans_choice('general.total',1) }} {{ trans_choice('general.outstanding',1) }}
                    <br>{{ trans_choice('general.open',1) }} {{ trans_choice('general.loan',2) }}</span>

                <span class="info-box-number" id="t_2"> loading... </span>
            </div>

        </div>

    </div>
    @endif
    @if(Sentinel::hasAccess('dashboard.loans_disbursed'))
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green">O</span>

            <div class="info-box-content">
                <span class="info-box-text">{{ trans_choice('general.open',1) }}
                    <br>{{ trans_choice('general.loan',2) }}</span>
                <span class="info-box-number">{{ \App\Models\Loan::where('status','disbursed')->count() }}</span>
            </div>
        </div>
    </div>
    @endif
    @if(Sentinel::hasAccess('dashboard.loans_closed'))
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green">C</span>

            <div class="info-box-content">
                <span class="info-box-text">{{ trans_choice('general.closed',2) }}
                    <br> {{ trans_choice('general.loan',2) }}</span>
                <span class="info-box-number">{{ \App\Models\Loan::where('status','closed')->count() }}</span>
            </div>

        </div>

    </div>
    @endif
    @if(Sentinel::hasAccess('dashboard.loans_pending'))
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green">P</span>

            <div class="info-box-content">
                <span class="info-box-text">{{ trans_choice('general.pending',2) }}
                    <br> {{ trans_choice('general.loan',2) }}</span>
                <span class="info-box-number">{{ \App\Models\Loan::where('status','pending')->count() }}</span>
            </div>

        </div>

    </div>
    @endif
    @if(Sentinel::hasAccess('dashboard.loans_approved'))
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green">A</span>

            <div class="info-box-content">
                <span class="info-box-text">{{ trans_choice('general.approved',2) }}
                    <br> {{ trans_choice('general.loan',2) }}</span>
                <span class="info-box-number">{{ \App\Models\Loan::where('status','approved')->count() }}</span>
            </div>

        </div>

    </div>
    @endif
    @if(Sentinel::hasAccess('dashboard.loans_rescheduled'))
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green">R</span>

            <div class="info-box-content">
                <span class="info-box-text">{{ trans_choice('general.rescheduled',2) }}
                    <br> {{ trans_choice('general.loan',2) }}</span>
                <span class="info-box-number">{{ \App\Models\Loan::where('status','rescheduled')->count() }}</span>
            </div>

        </div>

    </div>
    @endif
    @if(Sentinel::hasAccess('dashboard.loans_written_off'))
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green">W</span>

            <div class="info-box-content">
                <span class="info-box-text">{{ trans_choice('general.written_off',2) }}
                    <br> {{ trans_choice('general.loan',2) }}</span>
                <span class="info-box-number">{{ \App\Models\Loan::where('status','written_off')->count() }}</span>
            </div>

        </div>

    </div>
    @endif
    @if(Sentinel::hasAccess('dashboard.loans_declined'))
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green">D</span>

            <div class="info-box-content">
                <span class="info-box-text">{{ trans_choice('general.declined',2) }}
                    <br> {{ trans_choice('general.loan',2) }}</span>
                <span class="info-box-number">{{ \App\Models\Loan::where('status','declined')->count() }}</span>
            </div>

        </div>

    </div>
    @endif
    @if(Sentinel::hasAccess('dashboard.loans_withdrawn'))
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green">W</span>

            <div class="info-box-content">
                <span class="info-box-text">{{ trans_choice('general.withdrawn',2) }}
                    <br> {{ trans_choice('general.loan',2) }}</span>
                <span class="info-box-number">{{ \App\Models\Loan::where('status','withdrawn')->count() }}</span>
            </div>

        </div>

    </div>
    @endif

    @if(Sentinel::hasAccess('dashboard.loans_withdrawn'))
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green">G</span>

            <div class="info-box-content">
                <span class="info-box-text">Group Loans

                    <span class="info-box-number">{{ \DB::table("grouploan")->join("borrower_groups","borrower_groups.id","=","grouploan.group_id")->where(["borrower_groups.branch_id"=>session("branch_id")])->select("borrower_groups.*")->count() }}</span>
                    </div>

            </div>

        </div>
        @endif

        @if(Sentinel::hasAccess('dashboard.loans_withdrawn'))
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green">GL</span>

                <div class="info-box-content">
                    <span class="info-box-text"> TOTAL GROUP LOANS
                        RELEASED</span>
                    <span class="info-box-number"> {{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value }} {{ number_format(\DB::table("grouploan")->join("borrower_groups","borrower_groups.id","=","grouploan.group_id")->where(["borrower_groups.branch_id"=>session("branch_id")])->where(["grouploan.status"=>"approved"])->select("grouploan.*")->sum("grouploan.loan_amount")) }}</span>
                </div>

            </div>

        </div>
        @endif

    </div>

@else
    <div class="row">
        @if(Sentinel::hasAccess('dashboard.registered_borrowers'))
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>

                    <div class="info-box-content">
                <span class="info-box-text">{{ trans_choice('general.registered',1) }}
                    <br>{{ trans_choice('general.borrower',2) }}</span>
                        <span class="info-box-number">{{ \App\Models\Borrower::where(["branch_id"=>session("branch_id")])->count() }}</span>
                    </div>
                </div>
            </div>
        @endif
        <div class="clearfix visible-sm-block"></div>
        @if(Sentinel::hasAccess('dashboard.total_loans_released'))
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-files-o"></i></span>
<?php

$data = DB::table("loan_schedules")->where(["branch_id"=>session("branch_id")])->sum("interest");
$prin = DB::table('loans')->where(["branch_id"=>session("branch_id")])->sum("principal");

$totalre = $data  + $prin;

?>
            <div class="info-box-content">
                <span class="info-box-text">{{ trans_choice('general.total',1) }} {{ trans_choice('general.loan',2) }}
                    <br>{{ trans_choice('general.released',1) }}</span>
                @if(\App\Models\Setting::where('setting_key', 'currency_position')->first()->setting_value=='left')
                <span class="info-box-number"> {{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value }} {{ number_format($totalre,2) }} </span>
                @else
                <span class="info-box-number"> {{ number_format($totalre,2) }}  {{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value}}</span>
                @endif

            </div>
        </div>
    </div>
    @endif
        @if(Sentinel::hasAccess('dashboard.total_collections'))
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-dollar"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">{{ trans_choice('general.total',1) }}
                        <br>{{ trans_choice('general.collection',2) }}</span>

                    <span class="info-box-number" id="t_1"> loading... </span>

                </div>

            </div>

        </div>
        @endif
        @if(Sentinel::hasAccess('dashboard.loans_disbursed'))
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-balance-scale"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">{{ trans_choice('general.total',1) }} {{ trans_choice('general.outstanding',1) }}
                        <br>{{ trans_choice('general.open',1) }} {{ trans_choice('general.loan',2) }}</span>

                    <span class="info-box-number" id="t_2"> loading... </span>
                </div>

            </div>

        </div>
        @endif
        @if(Sentinel::hasAccess('dashboard.loans_disbursed'))
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green">O</span>

                    <div class="info-box-content">
                        <span class="info-box-text">{{ trans_choice('general.open',1) }}
                            <br>{{ trans_choice('general.loan',2) }}</span>
                        <span class="info-box-number">{{ \App\Models\Loan::where('status','disbursed')->where(["branch_id"=>session("branch_id")])->count() }}</span>
                    </div>
                </div>
            </div>
        @endif
        @if(Sentinel::hasAccess('dashboard.loans_closed'))
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green">C</span>

                    <div class="info-box-content">
                        <span class="info-box-text">{{ trans_choice('general.closed',2) }}
                            <br> {{ trans_choice('general.loan',2) }}</span>
                        <span class="info-box-number">{{ \App\Models\Loan::where('status','closed')->where(["branch_id"=>session("branch_id")])->count() }}</span>
                    </div>

                </div>

            </div>
        @endif
        @if(Sentinel::hasAccess('dashboard.loans_pending'))
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green">P</span>

                    <div class="info-box-content">
                        <span class="info-box-text">{{ trans_choice('general.pending',2) }}
                            <br> {{ trans_choice('general.loan',2) }}</span>
                        <span class="info-box-number">{{ \App\Models\Loan::where('status','pending')->where(["branch_id"=>session("branch_id")])->count() }}</span>
                    </div>

                </div>

            </div>
        @endif
        @if(Sentinel::hasAccess('dashboard.loans_approved'))
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green">A</span>

                    <div class="info-box-content">
                        <span class="info-box-text">{{ trans_choice('general.approved',2) }}
                            <br> {{ trans_choice('general.loan',2) }}</span>
                        <span class="info-box-number">{{ \App\Models\Loan::where('status','approved')->where(["branch_id"=>session("branch_id")])->count() }}</span>
                    </div>

                </div>

            </div>
        @endif
        @if(Sentinel::hasAccess('dashboard.loans_rescheduled'))
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green">R</span>

                    <div class="info-box-content">
                        <span class="info-box-text">{{ trans_choice('general.rescheduled',2) }}
                            <br> {{ trans_choice('general.loan',2) }}</span>
                        <span class="info-box-number">{{ \App\Models\Loan::where('status','rescheduled')->where(["branch_id"=>session("branch_id")])->count() }}</span>
                    </div>

                </div>

            </div>
        @endif
        @if(Sentinel::hasAccess('dashboard.loans_written_off'))
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green">W</span>

                    <div class="info-box-content">
                        <span class="info-box-text">{{ trans_choice('general.written_off',2) }}
                            <br> {{ trans_choice('general.loan',2) }}</span>
                        <span class="info-box-number">{{ \App\Models\Loan::where('status','written_off')->where(["branch_id"=>session("branch_id")])->count() }}</span>
                    </div>

                </div>

            </div>
        @endif
        @if(Sentinel::hasAccess('dashboard.loans_declined'))
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green">D</span>

                    <div class="info-box-content">
                        <span class="info-box-text">{{ trans_choice('general.declined',2) }}
                            <br> {{ trans_choice('general.loan',2) }}</span>
                        <span class="info-box-number">{{ \App\Models\Loan::where('status','declined')->where(["branch_id"=>session("branch_id")])->count() }}</span>
                    </div>

                </div>

            </div>
        @endif
        @if(Sentinel::hasAccess('dashboard.loans_withdrawn'))
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green">W</span>

                    <div class="info-box-content">
                        <span class="info-box-text">{{ trans_choice('general.withdrawn',2) }}
                            <br> {{ trans_choice('general.loan',2) }}</span>
                        <span class="info-box-number">{{ \App\Models\Loan::where('status','withdrawn')->where(["branch_id"=>session("branch_id")])->count() }}</span>
                    </div>

                </div>

            </div>
        @endif

             @if(Sentinel::hasAccess('dashboard.loans_withdrawn'))
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green">G</span>

                    <div class="info-box-content">
                        <span class="info-box-text">Group Loans

                            <span class="info-box-number">{{ \DB::table("grouploan")->join("borrower_groups","borrower_groups.id","=","grouploan.group_id")->where(["borrower_groups.branch_id"=>session("branch_id")])->select("borrower_groups.*")->count() }}</span>
                    </div>

                </div>

            </div>
        @endif

             @if(Sentinel::hasAccess('dashboard.loans_withdrawn'))
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green">GL</span>

                    <div class="info-box-content">
                        <span class="info-box-text"> TOTAL GROUP LOANS
RELEASED</span>
                        <span class="info-box-number"> {{ \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value }} {{ number_format(\DB::table("grouploan")->join("borrower_groups","borrower_groups.id","=","grouploan.group_id")->where(["borrower_groups.branch_id"=>session("branch_id")])->where(["grouploan.status"=>"approved"])->select("grouploan.*")->sum("grouploan.loan_amount")) }}</span>
                    </div>

                </div>

            </div>
        @endif

    </div>

@endif
    @if(Sentinel::hasAccess('dashboard.loans_released_monthly_graph'))
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><b><span
                                        style="color: #D72828">{{trans_choice('general.loans_released_monthly',1)}}</span></b>
                        </h3>

                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div id="loans_released_monthly" class="chart" style="height: 300px;">
                        </div>
                    </div>
                </div>
                <!-- END PORTLET -->
            </div>
        </div>
    @endif
    @if(Sentinel::hasAccess('dashboard.loans_collected_monthly_graph'))
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><b><span
                                        style="color: #3C8DBC">{{trans_choice('general.loan_collections_monthly',1)}}</span></b>
                        </h3>

                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div id="loan_collections_monthly" class="chart" style="height: 300px;">
                        </div>
                    </div>
                </div>
                <!-- END PORTLET -->
            </div>
        </div>
    @endif
        
        
        <div class="modal fade" id="complain" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Report Centre</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <form id="my_form">


                    <div class="modal-body">
                        <p>
                            Having trouble with Gplat? 
                        </p>
                      <div class="form-group">
                          <input type="text" class="form-control" placeholder="Enter Title" id="ctitle" required>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Enter Complain" id="ccomplain"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="submit" class="btn btn-primary">Send Complain</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    <script src="{{ asset('assets/plugins/amcharts/amcharts.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/amcharts/serial.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/amcharts/pie.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/amcharts/themes/light.js') }}"
            type="text/javascript"></script>
    <script>
// $(document).ready(function(){
//     $("#complain").modal("show")
// })
        
        

        AmCharts.makeChart("loans_released_monthly", {
            "type": "serial",
            "theme": "light",
            "autoMargins": true,
            "marginLeft": 30,
            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,
            "fontFamily": 'Open Sans',
            "color": '#888',

            "dataProvider": {!! $loans_released_monthly !!},
            "valueAxes": [{
                "axisAlpha": 0,

            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b> [[value]]</b> [[additional]]</span>",
                "bullet": "round",
                "bulletSize": 8,
                "lineColor": "#d1655d",
                "lineThickness": 4,
                "negativeLineColor": "#637bb6",
                "title": "Amount",
                "type": "smoothedLine",
                "valueField": "amount"
            }],
            "categoryField": "month",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0,
                "labelRotation": 30,

            },


        });
        AmCharts.makeChart("loan_collections_monthly", {
            "type": "serial",
            "theme": "light",
            "autoMargins": true,
            "marginLeft": 30,
            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,
            "fontFamily": 'Open Sans',
            "color": '#888',

            "dataProvider": {!! $loan_collections_monthly !!},
            "valueAxes": [{
                "axisAlpha": 0,

            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b> [[value]]</b> [[additional]]</span>",
                "bullet": "round",
                "bulletSize": 8,
                "lineColor": "#3C8DBC",
                "lineThickness": 4,
                "negativeLineColor": "#637bb6",
                "title": "Amount",
                "type": "smoothedLine",
                "valueField": "amount"
            }],
            "categoryField": "month",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0,
                "labelRotation": 30,

            },


        });
       /*  $.get('/borrower/dashd',function(res){
$("#t_1").html(res.t_1)
            $("#t_2").html(res.t_2)
        }) */
        
        $.ajax({
            url: "/borrower/dashd",
            type: "GET",
            cache: false,           
            success: function(res){
                $("#t_1").html(res.t_1)
                $("#t_2").html(res.t_2)
            }
        });
    </script>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script>
            $("#my_form").submit(function(){
                event.preventDefault();
                $.ajax({
                    // the server script you want to send your data to
                    'url': '/addreport',
                    // all of your POST/GET variables
                    'data': {
                        // 'dataname': $('input').val(), ...
                    text: $("#ctitle").val(),
                    title:  $("#ccomplain").val(),
                       "_token":"{{csrf_token()}}"
                       },
                       // you may change this to GET, if you like...
                       'type': 'post',

                       'beforeSend': function () {
                    // anything you want to have happen before sending the data to the server...
                    // useful for "loading" animations
                    $("#button").attr("disabled",true)

                }
            })
                .done( function (response) {
                // what you want to happen when an ajax call to the server is successfully completed


                Swal.fire(response.message,"","success")

                location.reload()

            })
                .fail( function (code, status) {
                // what you want to happen if the ajax request fails (404 error, timeout, etc.)
                // 'code' is the numeric code, and 'status' is the text explanation for the error
                // I usually just output some fancy error messages
                $("#button").attr("disabled",false)
                Swal.fire("Something Went Wrong!","","success")

            })
                .always( function (xhr, status) {
                // what you want to have happen no matter if the response is success or error
                // here, you would "stop" your loading animations, and maybe output a footer at the end of your content, reading "done"
            });
            })
        </script>

@endsection