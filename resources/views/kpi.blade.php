@extends('layouts.master')
@section('title')
@endsection
@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            @if($branch_name)
            <h3 style="font-weight:bold">KPI - {{$branch_name}}</h3>
            @endif

            <div class="alert alert-danger">
   <i class="fa fa-warning" style="font-size:20px;"></i> 
   <li>The primary responsibility of MIS is to provide management the strategic oversight over branch activities.  </li>
   <li>MIS secondary responsibility is to provide necessary support to branch activities and a backup to the branch manager (BM).</li>
   <li>MIS must ensure on a daily basis that all revenue are duely entered on GPLAT.</li>
   <li>Total number of clients disbursed for the month must match ledger records submitted.</li>
   <li>The grace period for records not uploaded on GPLAT is 24hrs.</li>

  
</div>
<br>
            <!-- <a class="float-right" href="#" onclick="$('#sort').toggle()">Advance Search</a> |      <a href='#' onclick="$('form :input').val('');">Reset input field </a> -->
            <div id="sort">


            <form class="form-horizontal" id="sort"  action="">
                
               <div class="row">
            <div class="col-xs-5">
                    <label>From</label>
                    <br>
                   <input type="date" class="form-control " name="from" id="from" value='{{$from != "none"?  date("Y-m-d", strtotime($from)) : ""}}'>
                </div>

                <div class="col-xs-5">
                    <label>To</label>
                    <br>
                    <input type="date" class="form-control " name="to" id="to" value='{{$to != "none" ?  date("Y-m-d", strtotime($to)) : ""}}'>
                </div>
            
@if(session("branch_id") == 1)
                <div class="col-xs-5">
                    <label>Select Branch</label>
                    <select class="form-control" name="branch" id="branch" >
                        <option value="">select Branch</option>
                        @foreach(DB::table("branches")->get() as $b)
                 <option value="{{$b->id}}">{{$b->name}}</option>
                        @endforeach
                    </select>
                </div>
                @endif
                @if(session("branch_id") == 1)
            <div class="col-xs-5">
                    <label>MIS</label>
                    <select class="form-control" name="loan_officer" id="loan_officer">
                        <option value="">select mis</option>
                       
                    </select>
                </div>

                @endif
 
          
                    <div class="col-xs-5" style="margin-top:20px">
                        <span class="input-group-btn">



 
                <button type="submit" class="btn btn-primary">Submit</button>

                </span>
                    </div>
               
</div>
            </form>


            </div>
           
        </div>

        

        <div class="box-body table-responsive">

        @if($from != "none" && $to != "none")
       <h3> From {{ date("Y-m-d", strtotime($from))}} - to {{date("Y-m-d", strtotime($to))}}</h3>
       @endif
        <Br>
      
        
        <div class="row" mt-2>

        <div class="col-md-2">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Total Login</h3>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body" id="repayments">
{{number_format($login)}}
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>

<div class="col-md-2">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Total Account Created</h3>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body" id="accounts">
        {{number_format($accounts)}}
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>

<div class="col-md-2">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Total Repayments</h3>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body" id="repayments">
        {{number_format($repayments)}}
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>



<div class="col-md-2">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Total Loan Disbursed</h3>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body" id="repayments">
        {{number_format($disburse)}}
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>


<div class="col-md-2">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Total Defaulters</h3>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body" id="repayments">
        {{number_format($default)}}
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>



<div class="col-md-2">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Total Amount Disbursed</h3>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body" id="repayments">
        {{number_format($amount_disbursed,2)}}
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>


<div class="col-md-2">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Total Revenue Amount</h3>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body" id="repayments">
        {{number_format($total_revenue,2)}}
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>








</div>
        </div>
        <!-- /.box-body -->

</div>
<!-- /.box -->
@endsection

@section('footer-scripts')

<script>

$("#branch").change(function(){
        $.post('/loanofficer2',{
            id:$(this).find(":selected").val(),
            "_token":"{{csrf_token()}}"
        }).then(function(res){
            $(`#loan_officer`).html('  <option value="">select mis</option>')
            res.users.map(function(r){
                $("#loan_officer").append(`  <option value="${r.id}">${r.first_name} ${r.last_name}</option>`)
            })
        })
    })

$(document).ready(function(){
    $("#branch").val({{$branch}})
    $("#loan_officer").val({{$loan_officer}})
})
    </script>
<script src="{{ asset('assets/plugins/datatable/media/js/jquery.dataTables.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/media/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.colVis.min.js')}}"></script>
<script src="https://cdn.datatables.net/scroller/2.0.3/js/dataTables.scroller.min.js"></script>
 
@endsection
