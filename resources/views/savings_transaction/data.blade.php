@extends('layouts.master')
@section('title')
    {{trans_choice('general.saving',2)}} {{trans_choice('general.transaction',2)}}
@endsection
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans_choice('general.saving',2)}} {{trans_choice('general.transaction',2)}}</h3>

            <div class="box-tools pull-right">

            </div>
            <br>
            <a class="float-right" href="#" onclick="$('#sort').toggle()">Advance Search</a>  |      <a href='#' onclick="$('form :input').val('');">Reset input field </a>
            <form class="form-inline" id="sort" style="display: none;">


                <label>Time From</label>
                <input type="time" id="tfrom"   class="form-control mb-2 mr-sm-2">

                <label>Time To</label>
                <input type="time" id="tto"   class="form-control mb-2 mr-sm-2">

                <label>Date From</label>
                <input type="date" id="date"   class="form-control from">

                <label>Date To</label>
                <input type="date" id="date"   class="form-control mb-2 mr-sm-2 to">


                       <label>Transaction type</label>
                        {!! Form::select('type',array('deposit'=>trans_choice('general.deposit',1),'withdrawal'=>trans_choice('general.withdrawal',1),'bank_fees'=>trans_choice('general.bank_fee',2),'interest'=>trans_choice('general.interest',1),'dividend'=>trans_choice('general.dividend',1)),null, array('class' => 'form-control type')) !!}




                <button type="submit" class="btn btn-primary mb-2">Submit</button>
            </form>
        </div>
        <div class="box-body ">
            <div class="table-responsive">

                <br>
                <table id="data-table" class="table table-bordered table-condensed table-hover">
                    <thead>
                    <tr style="background-color: #D1F9FF">
                        <th>{{trans_choice('general.name',1)}}</th>
                        <th>{{trans_choice('general.account',1)}}</th>
                        <th>{{trans_choice('general.date',1)}}</th>
                        <th>{{trans_choice('general.transaction',1)}}</th>
                        <th>{{trans_choice('general.debit',1)}}</th>
                        <th>{{trans_choice('general.credit',1)}}</th>
                        <th>{{ trans_choice('general.action',1) }}</th>
                    </tr>
                    </thead>

                </table>
            </div>
        </div>
        <!-- /.box-body -->
       
    </div>
    <!-- /.box -->


@endsection
@section('footer-scripts')
    <script src="{{ asset('assets/plugins/datatable/media/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/media/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.colVis.min.js')}}"></script>
    <script>
        var id = "";
        <?php if($_GET['borrower_id']):?>
        
        var id = <?php echo $_GET['borrower_id'];?>
        <?php endif;?>
            
            
        $('#data-table').DataTable({
            "order": [[ 3, "desc" ]],
            "processing": true,
            "serverSide": true,
            "ajax":{
                "url": "/apisavingtrans?id="+id,
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}"}
            },

            "columns": [
                { "data": "name" },
                { "data": "amount" },
                { "data": "date" },
                { "data": "status" },
                { "data": "debit" },
                { "data": "credit" },
                { "data": "action" }

            ]	
        } );
        
        $("#sort").submit(function(){
            event.preventDefault()
            $("#data-table").dataTable().fnDestroy()
var from = $(".from").val()
var to = $(".to").val()
var tfrom = $("#tfrom").val()
var tto = $("#tto").val()
var type = $(".type").find(":selected").val()

           $('#data-table').DataTable({
            "order": [[ 3, "desc" ]],
            "processing": true,
            "serverSide": true,
            "ajax":{
                "url": "/apisavingtrans",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}",from:from,to:to,tfrom:tfrom,tto:tto,type:type}
            },

               "columns": [
                   { "data": "name" },
                   { "data": "amount" },
                   { "data": "date" },
                   { "data": "status" },
                   { "data": "debit" },
                   { "data": "credit" },
                   { "data": "action" }

               ]	
           } );


        })
</script>
@endsection
