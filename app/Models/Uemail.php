<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Uemail extends Model
{
    protected $table = "uemails";

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}