<?php

namespace App\Http\Controllers;

use Aloha\Twilio\Twilio;
use App\Helpers\GeneralHelper;
use App\Models\Asset;
use App\Models\AssetType;
use App\Models\AssetValuation;
use App\Models\Borrower;
use App\Models\LoanRepayment;
use DB;

use App\Models\CustomField;
use App\Models\CustomFieldMeta;
use Illuminate\Support\Facades\Schema;
use App\Models\Setting;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Clickatell\Api\ClickatellHttp;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;

class AssetController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentinel');
    }

    
    public function loanofficer(Request $request){
        $branchu = $request->id;
        if($branchu == null){
            $branchu = session("branch_id");
        } 
        $users = DB::table("users")->where(["branch_users.branch_id"=>$branchu])
            ->join("branch_users","branch_users.user_id","=","users.id")
            ->join("role_users","role_users.user_id","=","users.id")->where(["role_users.role_id"=>4])->select("users.first_name","users.last_name","users.id")->get();
        return response()->json(["users"=>$users]);

    }

    public function loanofficer2(Request $request){
        $branchu = $request->id;
        if($branchu == null){
            $branchu = session("branch_id");
        } 
        $users = DB::table("users")->where(["branch_users.branch_id"=>$branchu])
            ->join("branch_users","branch_users.user_id","=","users.id")
            ->join("role_users","role_users.user_id","=","users.id")->where(["role_users.role_id"=>3])->select("users.first_name","users.last_name","users.id")->get();
        return response()->json(["users"=>$users]);
    }
    public function searchgroup(){
        $q = $_GET['q'];
        $g = DB::table("borrower_groups")->where('name', 'LIKE', '%' . $q . '%')->orWhere('id',$q)->where(["branch_id"=>session("branch_id")])->get();
        return response()->json($g);
    }
    
    public function searchbranch(){
        $q = $_GET['q'];
        $g = DB::table("branches")->where('name', 'LIKE', '%' . $q . '%')->orWhere('id',$q)->get();
        return response()->json($g);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Sentinel::hasAccess('assets')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $data = Asset::where('branch_id', session('branch_id'))->get();

        return view('asset.data', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Sentinel::hasAccess('assets.create')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $types = array();
        foreach (AssetType::all() as $key) {
            $types[$key->id] = $key->name;
        }
        //get custom fields
        $custom_fields = CustomField::where('category', 'assets')->get();
        return view('asset.create', compact('types', 'custom_fields'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Sentinel::hasAccess('assets.create')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }

        $asset = new Asset();
        $asset->user_id = Sentinel::getUser()->id;
        $asset->asset_type_id = $request->asset_type_id;
        $asset->purchase_date = $request->purchase_date;
        $asset->branch_id = session('branch_id');
        $asset->purchase_price = $request->purchase_price;
        $asset->replacement_value = $request->replacement_value;
        $asset->serial_number = $request->serial_number;
        $asset->notes = $request->notes;
        $files = array();
        if (!empty(array_filter($request->file('files')))) {
            $count = 0;
            foreach ($request->file('files') as $key) {
                $file = array('files' => $key);
                $rules = array('files' => 'required|mimes:jpeg,jpg,bmp,png,pdf,docx,xlsx');
                $validator = Validator::make($file, $rules);
                if ($validator->fails()) {
                    Flash::warning(trans('general.validation_error'));
                    return redirect()->back()->withInput()->withErrors($validator);
                } else {
                    $files[$count] = $key->getClientOriginalName();
                    $key->move(public_path() . '/uploads',
                        $key->getClientOriginalName());
                }
                $count++;
            }
        }
        $asset->files = serialize($files);
        //files
        $asset->save();
        //save asset valuation
        if (!empty($request->asset_management_current_date)) {
            $count = count($request->asset_management_current_date);
            for ($i = 0; $i < $count; $i++) {
                $valuation = new AssetValuation();
                $valuation->user_id = Sentinel::getUser()->id;
                $valuation->asset_id = $asset->id;
                $valuation->date = $request->asset_management_current_date[$i];
                $valuation->amount = $request->asset_management_current_value[$i];
                $valuation->save();
            }

        }
        $custom_fields = CustomField::where('category', 'assets')->get();
        foreach ($custom_fields as $key) {
            $custom_field = new CustomFieldMeta();
            $id = $key->id;
            $custom_field->name = $request->$id;
            $custom_field->parent_id = $asset->id;
            $custom_field->custom_field_id = $key->id;
            $custom_field->category = "assets";
            $custom_field->save();
        }
        GeneralHelper::audit_trail("Added asset  with id:" . $asset->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect('asset/data');
    }


    public function show($borrower)
    {
        if (!Sentinel::hasAccess('assets.view')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $users = User::all();
        $user = array();
        foreach ($users as $key) {
            $user[$key->id] = $key->first_name . ' ' . $key->last_name;
        }
        //get custom fields
        $custom_fields = CustomField::where('category', 'borrowers')->get();
        return view('borrower.show', compact('borrower', 'user', 'custom_fields'));
    }


    public function edit($asset)
    {
        $types = array();
        foreach (AssetType::all() as $key) {
            $types[$key->id] = $key->name;
        }
        //get custom fields
        $custom_fields = CustomField::where('category', 'assets')->get();
        return view('asset.edit', compact('asset', 'types', 'custom_fields'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Sentinel::hasAccess('assets.update')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $asset = Asset::find($id);
        $asset->asset_type_id = $request->asset_type_id;
        $asset->purchase_date = $request->purchase_date;
        $asset->purchase_price = $request->purchase_price;
        $asset->replacement_value = $request->replacement_value;
        $asset->serial_number = $request->serial_number;
        $asset->notes = $request->notes;
        $files = unserialize($asset->files);
        $count = count($files);
        if (!empty(array_filter($request->file('files')))) {
            foreach ($request->file('files') as $key) {
                $count++;
                $file = array('files' => $key);
                $rules = array('files' => 'required|mimes:jpeg,jpg,bmp,png,pdf,docx,xlsx');
                $validator = Validator::make($file, $rules);
                if ($validator->fails()) {
                    Flash::warning(trans('general.validation_error'));
                    return redirect()->back()->withInput()->withErrors($validator);
                } else {
                    $files[$count] = $key->getClientOriginalName();
                    $key->move(public_path() . '/uploads',
                        $key->getClientOriginalName());
                }

            }
        }
        $asset->files = serialize($files);
        $asset->save();
        $custom_fields = CustomField::where('category', 'assets')->get();
        foreach ($custom_fields as $key) {
            if (!empty(CustomFieldMeta::where('custom_field_id', $key->id)->where('parent_id', $id)->where('category',
                'assets')->first())
            ) {
                $custom_field = CustomFieldMeta::where('custom_field_id', $key->id)->where('parent_id',
                    $id)->where('category', 'assets')->first();
            } else {
                $custom_field = new CustomFieldMeta();
            }
            $kid = $key->id;
            $custom_field->name = $request->$kid;
            $custom_field->parent_id = $id;
            $custom_field->custom_field_id = $key->id;
            $custom_field->category = "assets";
            $custom_field->save();
        }
        //delete current valuations
        AssetValuation::where('asset_id', $id)->delete();
        //save asset valuation
        if (!empty($request->asset_management_current_date)) {
            $count = count($request->asset_management_current_date);
            for ($i = 0; $i < $count; $i++) {
                $valuation = new AssetValuation();
                $valuation->user_id = Sentinel::getUser()->id;
                $valuation->asset_id = $id;
                $valuation->date = $request->asset_management_current_date[$i];
                $valuation->amount = $request->asset_management_current_value[$i];
                $valuation->save();
            }

        }
        GeneralHelper::audit_trail("Updated asset  with id:" . $asset->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect('asset/data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        if (!Sentinel::hasAccess('assets.delete')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        Asset::destroy($id);
        GeneralHelper::audit_trail("Deleted asset  with id:" . $id);
        Flash::success(trans('general.successfully_deleted'));
        return redirect('asset/data');
    }

    //expense type
    public function indexType()
    {
        $data = AssetType::all();

        return view('asset.type.data', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createType()
    {

        return view('asset.type.create', compact(''));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeType(Request $request)
    {
        $type = new AssetType();
        $type->name = $request->name;
        $type->type = $request->type;
        $type->save();
        Flash::success(trans('general.successfully_saved'));
        return redirect('asset/type/data');
    }

    public function editType($asset_type)
    {
        return view('asset.type.edit', compact('asset_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function updateType(Request $request, $id)
    {
        $type = AssetType::find($id);
        $type->name = $request->name;
        $type->type = $request->type;
        $type->save();
        Flash::success(trans('general.successfully_saved'));
        return redirect('asset/type/data');
    }

    public function deleteType($id)
    {
        AssetType::destroy($id);
        Flash::success(trans('general.successfully_deleted'));
        return redirect('asset/type/data');
    }

    public function deleteFile(Request $request, $id)
    {
        $asset = Asset::find($id);
        $files = unserialize($asset->files);
        @unlink(public_path() . '/uploads/' . $files[$request->id]);
        $files = array_except($files, [$request->id]);
        $asset->files = serialize($files);
        $asset->save();


    }
    
    public function search(Request $request){
        $text = trim($request->term);
        $searchValues = preg_split('/\s+/', $text, -1, PREG_SPLIT_NO_EMPTY); 
//        foreach($searchValues as $text){
      /*    $borrowers = DB::table("guarantors")->where('firstname', 'LIKE', '%' . $text . '%')
    ->orWhere('lastname', 'LIKE', '%' . $text . '%')->orWhere('phone', 'LIKE', '%' . $text . '%')->select("*")->get(); */
            /* $customers = DB::table("borrowers")->where(["branch_id"=>session("branch_id")])->where('first_name', 'LIKE', '%' . $text . '%')
               ->orWhere('last_name', 'LIKE', '%' . $text . '%')->orWhereRaw("concat(first_name, ' ', last_name) like '%$text%' ")->orWhere('phone', 'LIKE', '%' . $text . '%')->orWhere('unique_number', 'LIKE', '%' . $text . '%')->select("id","user_id","first_name","last_name","unique_number","city","state","phone","city")->get();
 */


            $query = Borrower::query();
           $query->whereRaw("concat(first_name, ' ', last_name) like '%$text%' ");
        $columns = ['mobile', 'first_name', 'last_name','created_at','email','unique_number','username','title','working_status','gender','dob','address','phone','business_name'];
           foreach($columns as $column){
               $query->orWhere($column, 'LIKE', '%' . $text . '%');
           }
           $customers = $query->where(["branch_id"=>session("branch_id")])->limit(50)->orderBy("id","DESC")->get();
           $users = DB::table("branch_users")->where(["branch_users.branch_id"=>session("branch_id")])->join("users","users.id","=","branch_users.user_id")->where('users.first_name', 'LIKE', '%' . $text . '%')
               ->orWhere('users.last_name', 'LIKE', '%' . $text . '%')->orWhere('users.phone', 'LIKE', '%' . $text . '%')->select("users.id","users.email","users.first_name","users.last_name","users.address","users.phone","users.city")->get();

        $bb = [];
       /*  if($borrowers != null || isset($borrowers)){
        foreach($borrowers as $b){
    $borrowers = DB::table("borrowers")->where(["id"=>$b->userid])->first();
    array_push($bb,$borrowers);
        }
        } */
        return response()->json(["customers"=>$customers,"users"=>[],"b"=>[]]);
//     }
    }
    
    public function redirect(Request $request){

            return redirect(url('borrower/'.$request->id.'/show'));
    }
    
    public function myreport(){
        $data = DB::table("reports")->where(["branch_id"=>session("branch_id")])->get();
        return view("report.mycomplain",compact("data"));
    }

    public function kpi(Request $request){
        $from = date('Y-m-d 00:00:00', strtotime($request->get('from')));
        $to = date('Y-m-d 00:00:00', strtotime($request->get('to')));
 
      
 
        $branch = $request->branch;
        $loan_officer  = $request->loan_officer == null  ?  Sentinel::getUser()->id : $request->loan_officer;
        $branch = DB::table("branches")->where(["id"=>$branch == null && session("branch_id") != 1? session("branch_id") : $branch])->first();

        function get_percentage($total, $number)
        {
            if ( $total > 0 ) {
                return $total * ($number / 100);
            } else {
                return 0;
            }
        }
       if(session("branch_id") == 1){
        if($loan_officer == null || $branch == null){
            $login = DB::table("audit_trail")->where(["notes"=>"Logged in to system"])->whereBetween('created_at',[$from, $to])->count();
            $accounts = DB::table("borrowers")->whereBetween('created_at',[$from, $to])->count();
            $repayments  = DB::table("loan_repayments")->whereBetween('created_at',[$from, $to])->count();
            $disburse  = DB::table("loans")->where(["status"=>"disbursed"])->whereBetween('release_date',[$from, $to])->count();
            $amount_disbursed = 0;
            $total_revenue  = 0;
            $datax =    LoanRepayment::whereBetween('collection_date', [$request->from, $request->to])
            ->whereNull('sc')->get();
            $default  = DB::table("loans")->where(["status"=>"disbursed"])->where("maturity_date",">=",$from == null ? date("Y-m-d") : $from)->where("maturity_date","<=",$to == null ? date("Y-m-d") : $to)->count();
            
        }else{
        $login = DB::table("audit_trail")->where('notes', 'like', '%' . "Logged in to system" . '%')->where(["user_id"=>$loan_officer])->whereBetween('created_at',[$from, $to])->count();
        $accounts = DB::table("borrowers")->where(["user_id"=>$loan_officer])->whereBetween('created_at',[$from, $to])->count();
        $repayments = DB::table("loan_repayments")->where(["user_id"=>$loan_officer])->whereBetween('created_at',[$from, $to])->count();
            $disburse = DB::table("loans")->where(["user_id"=>$loan_officer,"status"=>"disbursed"])->whereBetween('release_date',[$from, $to])->count();
            $amount_disbursed = 0;
            $total_revenue  = 0;
        $default  = DB::table("loans")->where(["status"=>"disbursed","user_id"=>$loan_officer])->where("maturity_date",">=",$from)->where("maturity_date","<=",$to)->count();
        $datax =    LoanRepayment::where(["branch_id"=>$request->branch])->whereBetween('collection_date',[$request->from, $request->to])
        ->whereNull('sc')->get();  
              
   
    }



       }else{
        if($loan_officer == null || $from == null){
            $login = DB::table("audit_trail")->where(["branch_id"=>session("branch_id")])->where(["notes"=>"Logged in to system"])->count();
            $accounts = DB::table("borrowers")->where(["branch_id"=>session("branch_id")])->count();
            $repayments  = DB::table("loan_repayments")->where(["branch_id"=>session("branch_id")])->count();
            $disburse  = DB::table("loans")->where(["status"=>"disbursed"])->where(["branch_id"=>session("branch_id")])->count();
            $amount_disbursed = 0;
            $total_revenue  = 0;
            $default  = DB::table("loans")->where(["status"=>"disbursed"])->where(["branch_id"=>session("branch_id")])->where("maturity_date",">=",$from == null ? date("Y-m-d") : $from)->where("maturity_date","<=",$to == null ? date("Y-m-d") : $to)->count();
            $datax =    LoanRepayment::where(["branch_id"=>session("branch_id")])->whereBetween('collection_date',[$request->from, $request->to])
            ->whereNull('sc')->get();  
        }else{
        $login = DB::table("audit_trail")->where(["branch_id"=>session("branch_id")])->where(["notes"=>"Logged in to system","user_id"=>$loan_officer])->whereBetween('created_at',[$from, $to])->count();
        $accounts = DB::table("borrowers")->where(["branch_id"=>session("branch_id")])->where(["user_id"=>$loan_officer])->whereBetween('created_at',[$from, $to])->count();
        $repayments = DB::table("loan_repayments")->where(["branch_id"=>session("branch_id")])->where(["user_id"=>$loan_officer])->whereBetween('created_at',[$from, $to])->count();
            $disburse = DB::table("loans")->where(["branch_id"=>session("branch_id")])->where(["user_id"=>$loan_officer,"status"=>"disbursed"])->whereBetween('release_date',[$from, $to])->count();
            $amount_disbursed = 0;
        $total_revenue  = 0;
        $default  = DB::table("loans")->where(["branch_id"=>session("branch_id")])->where(["status"=>"disbursed","user_id"=>$loan_officer])->where("maturity_date",">=",$from == null ? date("Y-m-d") : $from)->where("maturity_date","<=",$to == null ? date("Y-m-d") : $to)->count();
        $datax =    LoanRepayment::where(["branch_id"=>session("branch_id")])->whereBetween('collection_date',[$request->from, $request->to])
        ->whereNull('sc')->get();     
    }

       }
        $branch_name = "";

        if($branch != null){
        $branch_name  = $branch->name;
        }

        if($request->from  == null){
            $from = "none";
        }

        if($request->to  == null){
            $to = "none";
        }

if($request->loan_officer){

    foreach($datax as $key){
           
        $officer = unserialize($key->borrower->loan_officers);

        foreach($officer as $of){
           
            if($of == $request->loan_officer){
                $amount_disbursed += get_percentage($key->amount,83.325) + get_percentage($key->amount,16.675);     
           $total_revenue += get_percentage($key->amount,16.675);
            }
        }
}


}else{

    foreach($datax as $key){
           
        $officer = unserialize($key->borrower->loan_officers);

        foreach($officer as $of){
            $amount_disbursed += get_percentage($key->amount,83.325) + get_percentage($key->amount,16.675);    
           $total_revenue += get_percentage($key->amount,16.675);
        }
}


}
$branch = $request->branch;
$loan_officer = $request->loan_officer;
        return view("kpi",compact("total_revenue","amount_disbursed","login","from","to","branch_name","accounts","repayments","disburse","default","branch","loan_officer"));
    }
}
