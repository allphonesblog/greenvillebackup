<?php

namespace App\Http\Controllers;

use Aloha\Twilio\Twilio;
use App\Helpers\GeneralHelper;
use App\Models\CustomField;
use App\Models\SavingFee;
use App\Models\SavingProduct;
use App\Models\SavingTransaction;
use App\Models\Setting;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Clickatell\Api\ClickatellHttp;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;

class SavingTransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentinel');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function indexapi(Request $request){
        $datas = [];
        $columns = array( 
            0 =>'name', 
            1 =>'amount',
            2=> 'date',
            3=> 'status',
            4=> 'debit',
            5=> 'credit',
            6=> 'action'


        );
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $texts = $request->input('search.value');

        if(empty($texts) )
        { 
            $type = [];
            if($request->type != null){
                $type = ["type"=>$request->type];
            }
            if(isset($_GET['id']) && $_GET['id'] != ""){
                $data = SavingTransaction::where('branch_id', session('branch_id'))->where(["borrower_id"=>$_GET['id']])->offset($start)
                    ->limit($limit)


                    ->orWhereBetween("date", [\Carbon\Carbon::parse($request->from), \Carbon\Carbon::parse($request->to)])

                    ->orWhereBetween("time", [$request->tfrom, $request->tto])

                    ->orderBy("id",$dir)
                    ->get();

                $totalFiltered = SavingTransaction::where('branch_id', session('branch_id'))->where(["borrower_id"=>$_GET['id']])->orWhere(function ($query) {
                    $query->whereBetween("date", [$request->from, $request->to]);
                })
                    ->orWhere(function ($query) {
                        $query->whereBetween("time", [$request->tfrom, $request->tto]);
                    })->count();
                }else{

            $data = SavingTransaction::where('branch_id', session('branch_id'))->offset($start)
                ->limit($limit)


                 ->orWhereBetween("date", [\Carbon\Carbon::parse($request->from), \Carbon\Carbon::parse($request->to)])

 ->orWhereBetween("time", [$request->tfrom, $request->tto])

                ->orderBy("id",$dir)
                ->get();


                $totalFiltered = SavingTransaction::where('branch_id', session('branch_id'))->orWhere(function ($query) {
                    $query->whereBetween("date", [$request->from, $request->to]);
                })
                    ->orWhere(function ($query) {
                        $query->whereBetween("time", [$request->tfrom, $request->tto]);
                    })->count();
                }

}else{
            $data = SavingTransaction::join("borrowers","borrowers.id","=","savings_transactions.borrower_id")->where('borrowers.branch_id', session('branch_id'))->where(function ($query) use($texts) {
                $query
                    ->orWhere('borrowers.id','=',$texts)
                    ->orWhere('savings_transactions.amount', 'like', '%' . $texts . '%')
                    ->orWhere('savings_transactions.type', 'like', '%' . $texts . '%')
                    ->orWhere('savings_transactions.date', 'like', '%' . $texts . '%')
                    ->orWhere('savings_transactions.id', 'like', '%' . $texts . '%')
                    ->orWhere('savings_transactions.borrower_id', 'like', '%' . $texts . '%')
                    ->orWhere('savings_transactions.created_at', 'like', '%' . $texts . '%')
                    ->orWhere('savings_transactions.notes', 'like', '%' . $texts . '%')
                    ->orWhere('borrowers.first_name', 'like', '%' . $texts . '%')
                    ->orWhere('borrowers.last_name', 'like', '%' . $texts . '%')
                ->orWhere('borrowers.email', 'like', '%' . $texts . '%')
                    ->orWhereRaw("concat(borrowers.first_name, ' ', borrowers.last_name) like '%$texts%' ")
                ->orWhere('borrowers.phone', 'like', '%' . $texts . '%');
            })->offset($start)
                ->limit($limit)

                ->orderBy("borrowers.id",$dir)->get();
            $totalFiltered = SavingTransaction::join("borrowers","borrowers.id","=","savings_transactions.borrower_id")->where('borrowers.branch_id', session('branch_id'))->where(function ($query) use($texts) {
                $query
                    ->orWhere('borrowers.id','=',$texts)
                    ->orWhere('savings_transactions.amount', 'like', '%' . $texts . '%')
                    ->orWhere('savings_transactions.type', 'like', '%' . $texts . '%')
                    ->orWhere('savings_transactions.date', 'like', '%' . $texts . '%')
                    ->orWhere('savings_transactions.id', 'like', '%' . $texts . '%')
                    ->orWhere('savings_transactions.borrower_id', 'like', '%' . $texts . '%')
                    ->orWhere('savings_transactions.created_at', 'like', '%' . $texts . '%')
                    ->orWhere('savings_transactions.notes', 'like', '%' . $texts . '%')
                    ->orWhere('borrowers.first_name', 'like', '%' . $texts . '%')
                    ->orWhere('borrowers.last_name', 'like', '%' . $texts . '%')
                    ->orWhereRaw("concat(borrowers.first_name, ' ', borrowers.last_name) like '%$texts%' ")
                ->orWhere('borrowers.email', 'like', '%' . $texts . '%')
                    ->orWhere('borrowers.phone', 'like', '%' . $texts . '%');
            })->count();
        }


        $totalData = count($data);
        $totalFilter = $totalFiltered;
        $totalFiltered = $totalData; 

        foreach($data as $key){
            $name = "-";
            $userid = $key->borrower->id;
            if(!empty($key->borrower)){
            $name =   "<a href=/borrower/$userid/show>".$key->borrower->title ." ". $key->borrower->first_name ." ".$key->borrower->last_name."</a>";
                }

            $date = $key->date ."<br>".$key->time;
            $account = $key->id;
            if($key['type']=="deposit"){
          $status =   trans_choice('general.deposit',1);
           }
                if($key['type']=="withdrawal"){
              $status =   trans_choice('general.withdrawal',1);
           }
                if($key['type']=="bank_fees"){
                    $status =   trans_choice('general.bank_fee',1);
                }

                if($key['type']=="dividend"){
                    $status =   trans_choice('general.dividend',1);
                }

                if($key['type']=="interest"){
                    $status =   trans_choice('general.interest',1);
                }
            $debit = "-";
            $credit = "-";
            if($key['type']=="deposit" || $key['type']=="dividend" || $key['type']=="interest"){

                $debit = ' <td style="text-align:right">

                </td>';
                $credit = '<td style="text-align:right">
           '.number_format($key['amount'],2).'
            </td>';
            }else{

               $debit ='<td style="text-align:right">
            '.number_format($key['amount'],2).'
            </td>';
             $credit ='<td style="text-align:right">

                </td>';
             }
            $action1 = "-";
            $action2 = "-";

            if(Sentinel::hasAccess('savings.transactions.update')){
                $action1 = '<li><a href="'.url('saving/'.$key->savings_id.'/savings_transaction/'.$key->id.'/edit').'"><i
                class="fa fa-edit"></i> '. trans('general.edit').' </a>
                </li>';
            }


                if(Sentinel::hasAccess('savings.transactions.delete')){
                    $action2 = '<li><a href="'.url('saving/'.$key->savings_id.'/savings_transaction/'.$key->id.'/delete').'"
                class="delete"><i
                class="fa fa-trash"></i> '.trans('general.delete').' </a>
                </li>';
                }


            $action = '
            <div class="btn-group">
                                    <button type="button" class="btn btn-info btn-xs dropdown-toggle"
                                            data-toggle="dropdown" aria-expanded="false">
                                        '. trans('general.choose') .' <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                     '.$action1.'
                                        '.$action2.'
                                    </ul>
                                </div>
            ';

            $nestedData['name'] = $name;
            $nestedData['amount'] = $amount;
            $nestedData['date'] = $date;
            $nestedData['status'] = $status;
            $nestedData['debit'] = $status;
            $nestedData['credit'] = $credit;
            $nestedData['action'] = $action;
            $datas[] = $nestedData;
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => $totalFilter, 
            "data"            => $datas  
        );

        return response()->json($json_data);
    }
    public function index()
    {

        $data = SavingTransaction::where('branch_id', session('branch_id'))->limit(100)->get();
        if(isset($_GET['search'])){
            $text = $_GET['search'];
            $data = SavingTransaction::join("borrowers","borrowers.id","=","savings_transactions.borrower_id")->where(function ($query) use($text) {
                $query
                    ->orWhere('savings_transactions.amount', 'like', '%' . $text . '%')
                    ->orWhere('savings_transactions.type', 'like', '%' . $text . '%')
                    ->orWhere('savings_transactions.date', 'like', '%' . $text . '%')
                    ->orWhere('savings_transactions.id', 'like', '%' . $text . '%')
                    ->orWhere('savings_transactions.borrower_id', 'like', '%' . $text . '%')
                    ->orWhere('savings_transactions.created_at', 'like', '%' . $text . '%')
                    ->orWhere('savings_transactions.notes', 'like', '%' . $text . '%')
                    ->orWhere('borrowers.first_name', 'like', '%' . $text . '%')
                    ->orWhere('borrowers.last_name', 'like', '%' . $text . '%');
            })->paginate(1000);


            $text = $_GET['search'];
        }

        return view('savings_transaction.data', compact('data','text'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($saving)
    {
        return view('savings_transaction.create', compact('saving'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $saving)
    {
        $savings_transaction = new SavingTransaction();
        if ($request->type == "withdrawal" && GeneralHelper::savings_account_balance($saving->id) < $request->amount && $saving->savings_product->allow_overdraw == 0) {
            Flash::warning(trans('general.withdrawal_more_than_balance'));
            return redirect()->back()->withInput();
        }
        $savings_transaction->user_id = Sentinel::getUser()->id;
        $savings_transaction->borrower_id = $saving->borrower_id;
        $savings_transaction->amount = $request->amount;
        $savings_transaction->branch_id = session('branch_id');
        $savings_transaction->savings_id = $saving->id;
        $savings_transaction->type = $request->type;
        $savings_transaction->date =  \Carbon\Carbon::parse($request->date);
        $savings_transaction->time = $request->time;
        $date = explode('-', $request->date);
        $savings_transaction->year = $date[0];
        $savings_transaction->month = $date[1];
        $savings_transaction->notes = $request->notes;
        $savings_transaction->save();

        Flash::success(trans('general.successfully_saved'));
        return redirect('saving/' . $saving->id . '/show');
    }


    public function show($savings_transaction)
    {

        return view('savings_transaction.show', compact('savings_transaction'));
    }


    public function edit($saving, $savings_transaction)
    {
        return view('savings_transaction.edit', compact('savings_transaction', 'saving'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $saving, $id)
    {
        $savings_transaction = SavingTransaction::find($id);
        $savings_transaction->amount = $request->amount;
        $savings_transaction->type = $request->type;
        $savings_transaction->date = \Carbon\Carbon::parse($request->date);
        $savings_transaction->time = $request->time;
        $date = explode('-', $request->date);
        $savings_transaction->year = $date[0];
        $savings_transaction->month = $date[1];
        $savings_transaction->notes = $request->notes;
        $savings_transaction->save();

        Flash::success(trans('general.successfully_saved'));
        return redirect('saving/' . $saving->id . '/show');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($saving, $id)
    {
        SavingTransaction::destroy($id);
        Flash::success(trans('general.successfully_deleted'));
        return redirect('saving/' . $saving->id . '/show');
    }

}
