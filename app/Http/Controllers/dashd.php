<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class dashd extends Controller
{
    public  function loans_total_paid($start_date = '', $end_date = '')
    {

        if (empty($start_date)) {
            $paid = 0;
            foreach (Loan::where('branch_id', session('branch_id'))->where('status', 'disbursed')->get() as $key) {
                $paid = $paid + LoanRepayment::where('loan_id', $key->id)->sum('amount');
            }
            return $paid;
        } else {
            $paid = 0;
            foreach (Loan::where('branch_id', session('branch_id'))->where('status', 'disbursed')->whereBetween('release_date',
                                                                                                                [$start_date, $end_date])->get() as $key) {
                $paid = $paid + LoanRepayment::where('loan_id', $key->id)->sum('amount');
            }
            return $paid;

        }

    }
    
    public  function loans_total_due($start_date = '', $end_date = '')
    {
        if (empty($start_date)) {
            $due = 0;
            foreach (Loan::where('branch_id', session('branch_id'))->where('status', 'disbursed')->get() as $key) {
                $due = $due + GeneralHelper::loan_total_due_amount($key->id);
            }
            return $due;
        } else {
            $due = 0;
            foreach (Loan::where('branch_id', session('branch_id'))->where('status', 'disbursed')->whereBetween('release_date',
                                                                                                                [$start_date, $end_date])->get() as $key) {
                $due = $due + GeneralHelper::loan_total_due_amount($key->id);
            }
            return $due;

        }
    }
}
