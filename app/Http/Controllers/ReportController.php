<?php

namespace App\Http\Controllers;

use Aloha\Twilio\Twilio;
use App\Helpers\BulkSms;
use App\Helpers\GeneralHelper;
use App\Models\Borrower;
use DB;
use App\Models\Collateral;
use App\Models\CollateralType;
use App\Models\CustomField;
use App\Models\CustomFieldMeta;
use App\Models\Expense;
use App\Models\ExpenseType;
use App\Models\Loan;
use App\Models\LoanRepayment;
use App\Models\LoanSchedule;
use App\Models\OtherIncome;
use App\Models\Payroll;
use App\Models\SavingTransaction;
use App\Models\Setting;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Clickatell\Api\ClickatellHttp;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;
use Carbon\Carbon;
class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentinel');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function addreport(Request $request){
        $title = $request->title;
        $text = $request->text;
        DB::table("reports")->insert(["title"=>$title,"text"=>$text,"branch_id"=>session('branch_id'),"user_id"=>Sentinel::getUser()->id]);
return response()->json(["message"=>"Your Complain Have Been Sent!"]);
    }
    
    public function apicash_flow(Request $request){
        if (!Sentinel::hasAccess('reports')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $start_date = \Carbon\Carbon::parse($request->start_date);
        $end_date = \Carbon\Carbon::parse($request->end_date);
        $start_datex = $request->start_date; 
        $end_datex = $request->end_date; 
        if (isset($request->end_date)) {
            $date = \Carbon\Carbon::parse($request->end_date);
        } else {
            $date = date("Y-m-d");
        }
        $monthly_collections = array();
        $start_date1 = date_format(date_sub(date_create($date),
                                            date_interval_create_from_date_string('1 years')),
                                   'Y-m-d');
        for ($i = 1; $i < 14; $i++) {
            $d = explode('-', $start_date1);
            //get loans in that period
            $payments = 0;
            $payments_due = 0;
            foreach (Loan::where('branch_id', session('branch_id'))->where('year', $d[0])->where('month', $d[1])->with(['schedules', 'loan_product'])->where('status',
                                                                                                                        'disbursed')->get() as $key) {
                $payments = $payments + GeneralHelper::loan_paid_item($key->id, 'interest',
                                                                      $key->due_date) + GeneralHelper::loan_paid_item($key->id, 'fees',
                                                                                                                      $key->due_date) + GeneralHelper::loan_paid_item($key->id, 'penalty',
                        $key->due_date) + GeneralHelper::loan_paid_item($key->id, 'principal', $key->due_date);
                $payments_due = $payments_due + GeneralHelper::loan_total_principal($key->id) + GeneralHelper::loan_total_fees($key->id) + GeneralHelper::loan_total_penalty($key->id) + GeneralHelper::loan_total_interest($key->id);
            }
            $payments = round($payments, 2);
            $payments_due = round($payments_due, 2);

            if ($i == 1 or $i == 13) {
                $ext = ' ' . $d[0];
            } else {
                $ext = '';
            }
            array_push($monthly_collections, array(
                'month' => date_format(date_create($start_date1),
                                       'M' . $ext),
                'payments' => $payments,
                'due' => $payments_due

            ));
            //add 1 month to start date
            $start_date1 = date_format(date_add(date_create($start_date1),
                                                date_interval_create_from_date_string('1 months')),
                                       'Y-m-d');
        }
        $monthly_collections = json_encode($monthly_collections);
        return response()->json( compact('start_date', 'end_date', 'monthly_collections','start_datex','end_datex'));
    }
    public function cash_flow(Request $request)
    {
        if (!Sentinel::hasAccess('reports')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        $capital = GeneralHelper::total_capital(\Carbon\Carbon::parse($request->start_date), \Carbon\Carbon::parse($request->end_date));
        $expenses = GeneralHelper::total_expenses(\Carbon\Carbon::parse($request->start_date), \Carbon\Carbon::parse($request->end_date));
        $payroll = GeneralHelper::total_payroll(\Carbon\Carbon::parse($request->start_date), \Carbon\Carbon::parse($request->end_date));
        $principal = GeneralHelper::loans_total_principal(\Carbon\Carbon::parse($request->start_date), \Carbon\Carbon::parse($request->end_date));
        $other_income = GeneralHelper::total_other_income(\Carbon\Carbon::parse($request->start_date), \Carbon\Carbon::parse($request->end_date));
        $deposits = GeneralHelper::total_savings_deposits(\Carbon\Carbon::parse($request->start_date), \Carbon\Carbon::parse($request->end_date));

        $withdrawals = GeneralHelper::total_savings_withdrawals(\Carbon\Carbon::parse($request->start_date), \Carbon\Carbon::parse($request->end_date));

        $principal_paid = GeneralHelper::loans_total_paid_item('principal', \Carbon\Carbon::parse($request->start_date), \Carbon\Carbon::parse($request->end_date));

        $interest_paid = GeneralHelper::loans_total_paid_item('interest',  $request->start_date,  $request->end_date);

        $fees_paid = GeneralHelper::loans_total_paid_item('fees', \Carbon\Carbon::parse($request->start_date), \Carbon\Carbon::parse($request->end_date));

        $penalty_paid = GeneralHelper::loans_total_paid_item('penalty', \Carbon\Carbon::parse($request->start_date), \Carbon\Carbon::parse($request->end_date));

        $total_payments = $expenses + $payroll + $principal + $withdrawals;
        $total_receipts = $principal_paid + $fees_paid + $interest_paid + $penalty_paid + $other_income + $deposits + $capital;
        $cash_balance = $total_receipts - $total_payments;
$branch = DB::table("branches")->get();
        return view('report.cash_flow',
            compact('expenses', 'payroll', 'principal', 'total_payments', 'other_income', 'principal_paid',
                'interest_paid', 'fees_paid', 'penalty_paid', 'total_receipts', 'cash_balance', 'start_date',
                'end_date', 'withdrawals', 'deposits', 'capital','branch'));
    }

    public function profit_loss(Request $request)
    {
        if (!Sentinel::hasAccess('reports')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }

        if($request->branch){
                $branch = ["branch_id"=>$request->branch];
                $branch2 = ["loans.branch_id"=>session("branch_id")];
        }else{
            $branch = [];
            $branch2 = [];
        }

        if($request->isMethod('post')){
            $start_date = $request->start_date;
            $end_date = $request->end_date;
        }else{
            $start_date = null;
            $end_date = null;
        }
        $start_date =\Carbon\Carbon::parse($start_date);  

        $end_date = \Carbon\Carbon::parse($end_date); 
        function get_percentage($total, $number)
        {
            if ( $total > 0 ) {
                return (int)round($number * ($total / 100),2);
            } else {
                return 0;
            }
        }
        $expenses = DB::table("expenses")->where($branch)->whereBetween("date",[$start_date,$end_date])->sum("amount");
        $other_expenses = GeneralHelper::total_savings_interest(\Carbon\Carbon::parse($request->start_date), \Carbon\Carbon::parse($request->end_date));
        $payroll = GeneralHelper::total_payroll(\Carbon\Carbon::parse($request->start_date), \Carbon\Carbon::parse($request->end_date));

        $other_income = GeneralHelper::total_other_income(\Carbon\Carbon::parse($request->start_date), \Carbon\Carbon::parse($request->end_date));
 

        $datax =    LoanRepayment::where($branch)->whereBetween('collection_date', [$start_date, $end_date])
        ->whereNull('sc')->get();
        $interest_paid =  0;
        foreach($datax as $key){
           
                $officer = unserialize($key->borrower->loan_officers);
    
                foreach($officer as $of){
                   $interest_paid += get_percentage($key->amount,16.675);
                }
        }
       
        $fees_paid =   DB::table("loans")->where($branch2)->join("loan_schedules","loan_schedules.loan_id","=","loans.id")
            ->whereBetween('loans.release_date', [$start_date, $end_date])
            ->sum("loan_schedules.fees");
        $penalty_paid =   DB::table("loans")->where($branch2)->join("loan_schedules","loan_schedules.loan_id","=","loans.id")
            ->whereBetween('loans.release_date', [$start_date, $end_date])
            ->sum("loan_schedules.penalty");

           function set_date($date){
            $timestamp = strtotime($date); 
            $new_date = date('Y-m-d', $timestamp);
            return $new_date;
           }
        $pos =  DB::table("pos_charges")->where($branch)
        ->whereBetween('date',[set_date($request->start_date), set_date($request->end_date)])
        ->sum("amount");

        
 
        $loan_default = GeneralHelper::loans_total_default(\Carbon\Carbon::parse($request->start_date), \Carbon\Carbon::parse($request->end_date));

        $operating_expenses = $expenses + $payroll;
        $operating_profit = $fees_paid + $interest_paid + $penalty_paid + $other_income;
        $gross_profit = $pos - $operating_profit - $operating_expenses - $other_expenses;
        $net_profit = $gross_profit - $loan_default;
        //build graphs here
        $monthly_net_income_data = array();
        $monthly_operating_profit_expenses_data = array();
        $monthly_other_expenses_data = array();
        if (isset($request->end_date)) {
            $date = \Carbon\Carbon::parse($request->end_date);
        } else {
            $date = date("Y-m-d");
        }
        $start_date1 = date_format(date_sub(date_create($date),
            date_interval_create_from_date_string('1 years')),
            'Y-m-d');
        $start_date2 = date_format(date_sub(date_create($date),
            date_interval_create_from_date_string('1 years')),
            'Y-m-d');
        $start_date3 = date_format(date_sub(date_create($date),
            date_interval_create_from_date_string('1 years')),
            'Y-m-d');
/* 
        for ($i = 1; $i < 14; $i++) {
            $d = explode('-', $start_date1);
            //get loans in that period
          $o_profit = 0;
            $o_expense = round($o_expense, 2);
            $ot_expense = 0;
            foreach (Loan::where('branch_id', session('branch_id'))->where('year', $d[0])->where('month', $d[1])->where('status',
                                                                                                                        'disbursed')->get() as $key) {
                $o_profit = $o_profit +  DB::table("loan_schedules")->where(["loan_id"=>$key->id])->sum(DB::raw("interest + fees + penalty"));
                $ot_expense = $ot_expense + ($key->principal - DB::table("loan_repayments")->where(["loan_id"=>$key->id])->sum("amount"));
            }
            $o_profit = round($o_profit + OtherIncome::where('year', $d[0])->where('month',
                                                                                   $d[1])->sum('amount'), 2);
            $o_expense = Expense::where('branch_id', session('branch_id'))->where('year', $d[0])->where('month',
                                                                                                        $d[1])->sum('amount');
            foreach (Payroll::where('year', $d[0])->where('month',
                $d[1])->get() as $key) {
                $o_expense = $o_expense + GeneralHelper::single_payroll_total_pay($key->id);
            }

            $ot_expense = round($ot_expense, 2);
            if ($i == 1 or $i == 13) {
                $ext = ' ' . $d[0];
            } else {
                $ext = '';
            }
            $n_income = round(($o_profit - $o_expense - $ot_expense), 2);
            array_push($monthly_net_income_data, array(
                'month' => date_format(date_create($start_date1),
                    'M' . $ext),
                'amount' => $n_income

            ));
            //add 1 month to start date
            $start_date1 = date_format(date_add(date_create($start_date1),
                date_interval_create_from_date_string('1 months')),
                'Y-m-d');
        }



        for ($i = 1; $i < 14; $i++) {
            $d = explode('-', $start_date2);
            //get loans in that period
            $o_profit = 0;
            $o_expense = round($o_expense, 2);
            $ot_expense = 0;
            foreach (Loan::where('branch_id', session('branch_id'))->where('year', $d[0])->where('month', $d[1])->where('status',
                                                                                                                        'disbursed')->get() as $key) {
                $o_profit = $o_profit +  DB::table("loan_schedules")->where(["loan_id"=>$key->id])->sum(DB::raw("interest + fees + penalty"));
                $ot_expense = $ot_expense + ($key->principal - DB::table("loan_repayments")->where(["loan_id"=>$key->id])->sum("amount"));
            }
            $o_profit = round($o_profit + OtherIncome::where('branch_id', session('branch_id'))->where('year', $d[0])->where('month',
                                                                                                                             $d[1])->sum('amount'), 2);
            $o_expense = Expense::where('branch_id', session('branch_id'))->where('year', $d[0])->where('month',
                                                                                                        $d[1])->sum('amount');
            foreach (Payroll::where('branch_id', session('branch_id'))->where('year', $d[0])->where('month',
                $d[1])->get() as $key) {
                $o_expense = $o_expense + GeneralHelper::single_payroll_total_pay($key->id);
            }

            $ot_expense = round($ot_expense, 2);
            if ($i == 1 or $i == 13) {
                $ext = ' ' . $d[0];
            } else {
                $ext = '';
            }
            $n_income = round(($o_profit - $o_expense - $ot_expense), 2);
            array_push($monthly_operating_profit_expenses_data, array(
                'month' => date_format(date_create($start_date2),
                    'M' . $ext),
                'profit' => $o_profit,
                'expenses' => $o_expense

            ));
            //add 1 month to start date
            $start_date2 = date_format(date_add(date_create($start_date2),
                date_interval_create_from_date_string('1 months')),
                'Y-m-d');
        }
        for ($i = 1; $i < 14; $i++) {
            $d = explode('-', $start_date3);
            //get loans in that period
            $o_profit = 0;
            $o_expense = round($o_expense, 2);
            $ot_expense = 0;
            foreach (Loan::where('branch_id', session('branch_id'))->where('year', $d[0])->where('month', $d[1])->where('status',
                'disbursed')->get() as $key) {
               $o_profit = $o_profit +  DB::table("loan_schedules")->where(["loan_id"=>$key->id])->sum(DB::raw("interest + fees + penalty"));
                $ot_expense = $ot_expense + ($key->principal - DB::table("loan_repayments")->where(["loan_id"=>$key->id])->sum("amount"));
            }
            $o_profit = round($o_profit + OtherIncome::where('branch_id', session('branch_id'))->where('year', $d[0])->where('month',
                    $d[1])->sum('amount'), 2);
            $o_expense = Expense::where('branch_id', session('branch_id'))->where('year', $d[0])->where('month',
                $d[1])->sum('amount');
            foreach (Payroll::where('branch_id', session('branch_id'))->where('year', $d[0])->where('month',
                $d[1])->get() as $key) {
                $o_expense = $o_expense + GeneralHelper::single_payroll_total_pay($key->id);
            }


            foreach (SavingTransaction::where('branch_id', session('branch_id'))->where('year', $d[0])->where('month', $d[1])->where('type',
                'interest')->get() as $key) {
                $ot_expense = $ot_expense + $key->amount;
            }
            $ot_expense = round($ot_expense, 2);
            if ($i == 1 or $i == 13) {
                $ext = ' ' . $d[0];
            } else {
                $ext = '';
            }
            $n_income = round(($o_profit - $o_expense - $ot_expense), 2);
            array_push($monthly_other_expenses_data, array(
                'month' => date_format(date_create($start_date3),
                    'M' . $ext),
                'expenses' => $ot_expense

            ));
            //add 1 month to start date
            $start_date3 = date_format(date_add(date_create($start_date3),
                date_interval_create_from_date_string('1 months')),
                'Y-m-d');
        } */
        $monthly_net_income_data = json_encode($monthly_net_income_data);
        $monthly_operating_profit_expenses_data = json_encode($monthly_operating_profit_expenses_data);
        $monthly_other_expenses_data = json_encode($monthly_other_expenses_data);
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $branch = DB::table("branches")->get();

        return view('report.profit_loss',
            compact('expenses', 'payroll','pos', 'operating_expenses', 'other_income',
                'interest_paid', 'fees_paid', 'penalty_paid', 'operating_profit', 'gross_profit', 'start_date',
                'end_date', 'loan_default', 'net_profit', 'monthly_net_income_data',
                'monthly_operating_profit_expenses_data', 'monthly_other_expenses_data', 'other_expenses','branch'));
    }

    public function apicollection_report(Request $request){
        $start_date = \Carbon\Carbon::parse($request->start_date);
        $end_date = \Carbon\Carbon::parse($request->end_date);


        $num1 = '    <div class="col-md-2">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">'.trans_choice('general.principal',1).' '.trans_choice('general.due',1).'</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    '.number_format(DB::table("loans")->where('loans.branch_id', session('branch_id'))->where('loans.status', 'disbursed')->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->whereBetween('release_date',
                                                                                                   [$start_date, $end_date])->sum("loan_schedules.principal"),2).'
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-2">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">'.trans_choice('general.interest',1).' '.trans_choice('general.due',1).'</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    '.number_format(DB::table("loans")->where('loans.branch_id', session('branch_id'))->where('loans.status', 'disbursed')->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->whereBetween('release_date',
                                                                                                   [$start_date, $end_date])->sum("loan_schedules.interest"),2).'
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-2">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">'.trans_choice('general.penalty',1).' '.trans_choice('general.due',1).'</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    '.number_format(DB::table("loans")->where('loans.branch_id', session('branch_id'))->where('loans.status', 'disbursed')->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->whereBetween('release_date',
                                                                                                   [$start_date, $end_date])->sum("loan_schedules.penalty"),2).'
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-2">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">'.trans_choice('general.fee',2).' '.trans_choice('general.due',1).'</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    '.number_format(DB::table("loans")->where('loans.branch_id', session('branch_id'))->where('loans.status', 'disbursed')->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->whereBetween('release_date',
                                                                                                   [$start_date, $end_date])->sum("loan_schedules.fees"),2).'
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-4">
            <div class="box box-danger box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">'.trans_choice('general.total',1).' '.trans_choice('general.loan',2).' '.trans_choice('general.due',1).'</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    '.number_format(\App\Helpers\GeneralHelper::loans_total_due($start_date,$end_date),2).'
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>';


        $num2 = '

        <div class="col-md-2">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">'.trans_choice('general.principal',1).' '.trans_choice('general.paid',1).'</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    '.number_format(DB::table("loans")->where('loans.branch_id', session('branch_id'))->whereBetween('loans.release_date',
                                                                                                                     [$start_date, $end_date])->where('loans.status',
                                                                                                                                                      'disbursed')->join("loan_products","loan_products.id","=","loans.loan_product_id")->join("loan_repayments","loan_repayments.loan_id","=","loans.id")
                                    ->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where('loan_schedules.due_date', 'loan_schedules.due_date')->where('loan_schedules.due_date', '<=', 'loan_repayments.due_date')->sum("loan_schedules.principal"),2).'
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-2">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">'.trans_choice('general.interest',1).' '.trans_choice('general.paid',1).'</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    '.number_format(DB::table("loans")->where('loans.branch_id', session('branch_id'))->whereBetween('loans.release_date',
                                                                                                                     [$start_date, $end_date])->where('loans.status',
                                                                                                                                                      'disbursed')->join("loan_products","loan_products.id","=","loans.loan_product_id")->join("loan_repayments","loan_repayments.loan_id","=","loans.id")
                                    ->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where('loan_schedules.due_date', 'loan_schedules.due_date')->where('loan_schedules.due_date', '<=', 'loan_repayments.due_date')->sum("loan_schedules.interest"),2).'
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-2">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">'.trans_choice('general.penalty',1).' '.trans_choice('general.paid',1).'</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    '.number_format(DB::table("loans")->where('loans.branch_id', session('branch_id'))->whereBetween('loans.release_date',
                                                                                                                     [$start_date, $end_date])->where('loans.status',
                                                                                                                                                      'disbursed')->join("loan_products","loan_products.id","=","loans.loan_product_id")->join("loan_repayments","loan_repayments.loan_id","=","loans.id")
                                    ->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where('loan_schedules.due_date', 'loan_schedules.due_date')->where('loan_schedules.due_date', '<=', 'loan_repayments.due_date')->sum("loan_schedules.penalty"),2).'
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-2">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">'.trans_choice('general.fee',2).' '.trans_choice('general.paid',1).'</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    '.number_format(DB::table("loans")->where('loans.branch_id', session('branch_id'))->whereBetween('loans.release_date',
                                                                                                                     [$start_date, $end_date])->where('loans.status',
                                                                                                                                                      'disbursed')->join("loan_products","loan_products.id","=","loans.loan_product_id")->join("loan_repayments","loan_repayments.loan_id","=","loans.id")
                                    ->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where('loan_schedules.due_date', 'loan_schedules.due_date')->where('loan_schedules.due_date', '<=', 'loan_repayments.due_date')->sum("loan_schedules.fees"),2).'
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-4">
            <div class="box box-success box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">'.trans_choice('general.total',1).' '.trans_choice('general.paid',1).'</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    '.number_format(DB::table("loans")->where('loans.branch_id', session('branch_id'))->whereBetween('loans.release_date',
                                                                                                                     [$start_date, $end_date])->where('loans.status',
                                                                                                                                    'disbursed')->join("loan_products","loan_products.id","=","loans.loan_product_id")->join("loan_repayments","loan_repayments.loan_id","=","loans.id")
                                    ->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where('loan_schedules.due_date', 'loan_schedules.due_date')->where('loan_schedules.due_date', '<=', 'loan_repayments.due_date')->sum("loan_schedules.principal"),2).'
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        '; 



        $monthly_collections = array();

        $start_date1 = date_format(date_sub(date_create($start_date),
                                            date_interval_create_from_date_string('1 years')),
                                   'Y-m-d');


        for ($i = 1; $i < 14; $i++) {
            $d = explode("-",$start_date1);


            //get loans in that period
            $payments = 0;
            $payments_due = 0;
            foreach (DB::table("loans")->where('loans.branch_id', session('branch_id'))->where('loans.year', $d[0])->where('loans.month', $d[1])->where('loans.status',
                                                                                                                                    'disbursed')->join("loan_products","loan_products.id","=","loans.loan_product_id")->join("loan_repayments","loan_repayments.loan_id","=","loans.id")
                     ->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where('loan_schedules.due_date', 'loan_schedules.due_date')->where('loan_schedules.due_date', '<=', 'loan_repayments.due_date')->select("loans.id","loan_repayments.due_date","loan_products.repayment_order","loan_schedules.interest","loan_schedules.fees","loan_schedules.penalty","loan_schedules.principal",DB::raw('SUM(loan_repayments.amount) AS amount'))->get() as $key) {


                $id = $key->id;
                $date = $key->due_date;

                $principal = 0;
                $interest = 0;
                $penalty = 0;
                $fees = 0;




                $repayment_order = unserialize($key->repayment_order);
                $schedule = $key;
                    $payments = $key->amount;
                    if ($payments > 0) {
                        foreach ($repayment_order as $order) {
                            if ($payments > 0) {
                                if ($order == 'interest') {
                                    if ($payments > $schedule->interest) {
                                        $interest = $interest + $schedule->interest;
                                        $payments = $payments - $schedule->interest;
                                    } else {
                                        $interest = $interest + $payments;
                                        $payments = 0;
                                    }
                                }
                                if ($order == 'penalty') {
                                    if ($payments > $schedule->penalty) {
                                        $penalty = $penalty + $schedule->penalty;
                                        $payments = $payments - $schedule->penalty;
                                    } else {
                                        $penalty = $penalty + $payments;
                                        $payments = 0;
                                    }
                                }
                                if ($order == 'fees') {
                                    if ($payments > $schedule->fees) {
                                        $fees = $fees + $schedule->fees;
                                        $payments = $payments - $schedule->fees;
                                    } else {
                                        $fees = $fees + $payments;
                                        $payments = 0;
                                    }
                                }
                                if ($order == 'principal') {
                                    if ($payments > $schedule->principal) {
                                        $principal = $principal + $schedule->principal;
                                        $payments = $payments - $schedule->principal;
                                    } else {
                                        $principal = $principal + $payments;
                                        $payments = 0;
                                    }
                                }
                            }
                        }
                    }
                    //apply remainder to principal


                $payments = $payments + $interest + $fees + $penalty + $principal;
//                 $payments_due = $payments_due + GeneralHelper::loan_total_principal($key->id) + GeneralHelper::loan_total_fees($key->id) + GeneralHelper::loan_total_penalty($key->id) + GeneralHelper::loan_total_interest($key->id);
            }
            $payments = round($payments, 2);
            $payments_due = round($payments_due, 2);

            if ($i == 1 or $i == 13) {
                $ext = ' ' . $d[0];
            } else {
                $ext = '';
            }
            array_push($monthly_collections, array(
                'month' => date_format(date_create($start_date1),
                                       'M' . $ext),
                'payments' => $payments,
                'due' => $payments_due

            ));
            //add 1 month to start date
            $start_date1 = date_format(date_add(date_create($start_date1),
                                                date_interval_create_from_date_string('1 months')),
                                       'Y-m-d');
        }


        return response()->json(["num1"=>$num1,"num2"=>$num2,"monthly_collections"=>$monthly_collections]);
    }
    public function collection_report(Request $request)
    {
        if (!Sentinel::hasAccess('reports')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }


        return view('report.collection');
    }

    public function balance_sheet(Request $request)
    {
        if (!Sentinel::hasAccess('reports')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $start_datex = \Carbon\Carbon::parse($request->start_date);
        $end_datex = \Carbon\Carbon::parse($request->end_date);

        /* $start_date = date('Y-m-d', strtotime(str_replace('/','-',$start_date)));

        $end_date = date('Y-m-d', strtotime($end_date)); */


        $capital = GeneralHelper::total_capital($start_date, $end_date);
         $expenses = GeneralHelper::total_expenses($start_date, $end_date);
         $payroll = GeneralHelper::total_payroll($start_date, $end_date);
         $principal = GeneralHelper::loans_total_principal($start_date, $end_date);
         $other_income = GeneralHelper::total_other_income($start_date, $end_date);
         $deposits = GeneralHelper::total_savings_deposits($start_date, $end_date);
        $withdrawals = GeneralHelper::total_savings_withdrawals($start_date, $end_date);

        $interest_paid =   DB::table("loans")->where(["loans.branch_id"=>session("branch_id")])->join("loan_schedules","loan_schedules.loan_id","=","loans.id")
            ->whereBetween('loans.release_date', [$start_datex, $end_datex])
            ->sum("loan_schedules.interest");
        $fees_paid =   DB::table("loans")->where(["loans.branch_id"=>session("branch_id")])->join("loan_schedules","loan_schedules.loan_id","=","loans.id")
            ->whereBetween('loans.release_date', [$start_datex, $end_datex])
            ->sum("loan_schedules.fees");
        $penalty_paid =   DB::table("loans")->where(["loans.branch_id"=>session("branch_id")])->join("loan_schedules","loan_schedules.loan_id","=","loans.id")
            ->whereBetween('loans.release_date', [$start_datex, $end_datex])
            ->sum("loan_schedules.penalty");

        $principal_paid =  DB::table("loans")->where(["loans.branch_id"=>session("branch_id")])->join("loan_schedules","loan_schedules.loan_id","=","loans.id")
            ->whereBetween('loans.release_date', [$start_date, $end_date])
            ->sum("loan_schedules.principal");
        $total_payments = $expenses + $payroll + $principal + $withdrawals;
        $total_receipts = $principal_paid + $fees_paid + $interest_paid + $penalty_paid + $other_income + $deposits+$capital;
        $cash_balance = $total_receipts - $total_payments;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        return view('report.balance_sheet',
            compact('expenses', 'payroll', 'principal', 'total_payments', 'other_income', 'principal_paid',
                'interest_paid', 'fees_paid', 'penalty_paid', 'total_receipts', 'cash_balance', 'start_date',
                'end_date', 'withdrawals', 'deposits', 'capital','start_date','end_date'));
    }

    public function apiloan_list(Request $request){
        $datas = [];


        $columns = array( 
            0 =>'balance', 
            1 =>'principal',
            2=> 'interest',
            3=> 'fees',
            4=> 'balance',



        );

        
        



        $branchu = ["loans.branch_id"=>$request->branch];
        if($request->branch == null){
            $branchu = [];
        } 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $texts = $request->input('search.value');
        $data = array();
        $user = DB::table("users")->where(["id"=>$off])->first();
        $user = $user->first_name." ".$user->last_name;
        $start_date = \Carbon\Carbon::parse($request->start_date);
        $end_date = \Carbon\Carbon::parse($request->end_date);
   
        if($request->branch == null){
            $principal = 0;
            $balances = 0;
            $totalint = 0;
            $fees = 0;
            $totalb = 0;
            $totalfee = 0;


            $totalFiltere = 0;
            foreach(DB::table("branches")->get() as $b){
                $query = Loan::query()->whereBetween('loans.release_date', [$start_date, $end_date]);
                $branchu = ["branch_id"=>$b->id];

            if($request->inc == "on"){
                if ($request->status == 'all') {
                    $db = DB::table("loans")->join("loan_schedules","loan_schedules.loan_id","=","loans.id")
                        ->where(["loans.branch_id"=>$b->id])

                        ->join("loan_fees_meta","loan_fees_meta.parent_id","=","loans.id")
                        ->whereBetween('loans.release_date', [$start_date, $end_date])
                        ->where(["loans.disbursed_by_id"=>$request->loan_officer])
                        ->orWhere(["loans.approved_by_id"=>$request->loan_officer])
                        ->select(DB::raw("loans.id,SUM(loan_fees_meta.value) as value,SUM(loan_schedules.principal) as totalprincipal,SUM(loan_schedules.interest) as totalinterest,SUM(loan_schedules.interest) + SUM(loan_schedules.principal) as totalbalance,COUNT(DISTINCT(loans.borrower_id)) as totalborrower"))
                        ->get();


                    foreach($db as $d){
                           if($d->totalborrower > 0 && $d->totalprincipal > 0){
                            // $total_fee = DB::table("loan_fees_meta")->where(["parent_id"=>$d->id])->first()->value;
                            $total_fee = $d->value;
                            $nestedData['branch'] = $b->name;
                            $nestedData['borrower'] = number_format($d->totalborrower);
                            $nestedData['principal'] = number_format($d->totalprincipal);
                            $nestedData['interest'] =  number_format($d->totalinterest);
                            $nestedData['fees'] =  number_format($total_fee);
                            $nestedData['balance'] =  number_format($d->totalbalance);
                            $totalFiltere += 1;
                            $totalint += $d->totalinterest;
                            $balances +=$d->totalprincipal + $d->totalinterest;
                            $principal += $d->totalprincipal;
                            $totalb += $d->totalborrower;
                            $fees += $total_fee;
                            $totalfee += $total_fee;
                            $datas[] = $nestedData;  
                        }
                    }

                 /*    $data = $query->whereBetween('loans.release_date', [$start_date, $end_date])->select("loans.*") 
                        ->where(["disbursed_by_id"=>$request->loan_officer])
                        ->orWhere(["approved_by_id"=>$request->loan_officer])
                        ->orderBy("loans.id",$dir)->get();
                    $datax =    $query->whereBetween('release_date', [$start_date, $end_date])
                        ->where(["disbursed_by_id"=>$request->loan_officer])
                        ->orWhere(["approved_by_id"=>$request->loan_officer])

                        ->get(); 
                    $totalFiltered =  0;
                    $prin = $query->with("schedules")->sum("principal");
                    $int = $query->with("schedules")->sum("interest");
                    $nestedData['branch'] = $b->name;
                    $nestedData['borrower'] = $query->with("borrower")->count();
                    $nestedData['principal'] = $prin;
                    $nestedData['interest'] = $int;
                    $nestedData['balance'] =    $prin + $int;


                    $datas[] = $nestedData; */
                } else {
                    $db = DB::table("loans")->join("loan_schedules","loan_schedules.loan_id","=","loans.id")
                        ->where(["loans.branch_id"=>$b->id])
                        ->join("loan_fees_meta","loan_fees_meta.parent_id","=","loans.id")
                        ->whereBetween('loans.release_date', [$start_date, $end_date])
                        ->where(["loans.disbursed_by_id"=>$request->loan_officer])
                        ->orWhere(["loans.approved_by_id"=>$request->loan_officer])
                        ->where(["loans.status"=>$request->status])
                        ->select(DB::raw("loans.id,SUM(loan_fees_meta.value) as value,SUM(loan_schedules.principal) as totalprincipal,SUM(loan_schedules.interest) as totalinterest,SUM(loan_schedules.interest) + SUM(loan_schedules.principal) as totalbalance,COUNT(DISTINCT(loans.borrower_id)) as totalborrower"))
                        ->get();


                    foreach($db as $d){
                        if($d->totalborrower > 0 && $d->totalprincipal > 0){
                            // $total_fee = DB::table("loan_fees_meta")->where(["parent_id"=>$d->id])->first()->value;
        $total_fee = $d->value;
                            $nestedData['branch'] = $b->name;
                            $nestedData['borrower'] = number_format($d->totalborrower);
                            $nestedData['principal'] = number_format($d->totalprincipal);
                            $nestedData['interest'] =  number_format($d->totalinterest);
                            $nestedData['fees'] =  number_format($total_fee);
                            $nestedData['balance'] =  number_format($d->totalbalance);
                            $totalFiltere += 1;
                            $totalint += $d->totalinterest;
                            $balances +=$d->totalprincipal + $d->totalinterest;
                            $principal += $d->totalprincipal;
                            $totalb += $d->totalborrower;
                            $fees += $total_fee;
                            $totalfee += $total_fee;
                            $datas[] = $nestedData;  
                        }
                    }

                  /*   $data = $query->where(["status"=>$request->status])->whereBetween('loans.release_date', [$start_date, $end_date])->select("loans.*") 
                        ->where(["disbursed_by_id"=>$request->loan_officer])
                        ->orWhere(["approved_by_id"=>$request->loan_officer])
                        ->orderBy("loans.id",$dir)->get();

                    $datax =    $query->where(["status"=>$request->status])->whereBetween('loans.release_date', [$start_date, $end_date])->select("loans.*")
                        ->where(["disbursed_by_id"=>$request->loan_officer])
                        ->orWhere(["approved_by_id"=>$request->loan_officer])

                        ->get(); 
                    $totalFiltered = 0;

                  $prin = $query->with("schedules")->sum("principal");
                    $int = $query->with("schedules")->sum("interest");
                    $nestedData['branch'] = $b->name;
                    $nestedData['borrower'] = $query->with("borrower")->count();
                    $nestedData['principal'] = $prin;
                    $nestedData['interest'] = $int;
                    $nestedData['balance'] =    $prin + $int;
                    $datas[] = $nestedData; */
                }
            }else{
                if(empty($request->loan_type) ){
                    if ($request->status == 'all') {
                        $db = DB::table("loans")
                        ->join("loan_schedules","loan_schedules.loan_id","=","loans.id")
                        ->join("loan_fees_meta","loan_fees_meta.parent_id","=","loans.id")
                        ->where(["loans.branch_id"=>$b->id])
                        ->whereBetween('loans.release_date', [$start_date, $end_date])
                        ->select(DB::raw("loans.id,SUM(loan_fees_meta.value) as value,SUM(loan_schedules.principal) as totalprincipal,SUM(loan_schedules.interest) as totalinterest,SUM(loan_schedules.interest) + SUM(loan_schedules.principal) as totalbalance,COUNT(DISTINCT(loans.borrower_id)) as totalborrower")) 
                     
                        ->get();


                      
                        
                       
 
                      /*   $nestedData['branch'] = $b->name;
                        $nestedData['borrower'] = number_format($query->with("borrower")->where($branchu)->count());
                            $nestedData['principal'] = number_format(DB::table("loan_schedules")->where($branchu)->sum("principal"));


                            $nestedData['interest'] =  number_format(DB::table("loan_schedules")->where($branchu)->sum("interest"));


                            $nestedData['balance'] =  number_format(DB::table("loan_schedules")->where($branchu)->sum("interest") + DB::table("loan_schedules")->where($branchu)->sum("principal"));


                            $totalFiltere += 1;
                        $datas[] = $nestedData;
                            } */
                        
                         
                        foreach($db as $d){
                         
                            if($d->totalborrower > 0 && $d->totalprincipal > 0){
                                // $total_fee = DB::table("loan_fees_meta")->where(["parent_id"=>$d->id])->first()->value;
                                $total_fee = $d->value;
                                $nestedData['branch'] = $b->name;
                                $nestedData['borrower'] = number_format($d->totalborrower);
                                $nestedData['principal'] = number_format($d->totalprincipal);
                                $nestedData['interest'] =  number_format($d->totalinterest);
                                $nestedData['fees'] =  number_format($total_fee);
                                $nestedData['balance'] =  number_format($d->totalbalance);
                                $totalFiltere += 1;
                                $totalint += $d->totalinterest;
                                $balances +=$d->totalprincipal + $d->totalinterest;
                                $principal += $d->totalprincipal;
                                $totalb += $d->totalborrower;
                                $fees += $total_fee;
                                $totalfee += $total_fee;
                                $datas[] = $nestedData;  
                            }
                        }


                    } else {
                        $db = DB::table("loans")

                        ->join("loan_fees_meta","loan_fees_meta.parent_id","=","loans.id")
                        ->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where(["loans.status"=>$request->status,"loans.branch_id"=>$b->id])->whereBetween('loans.release_date', [$start_date, $end_date])->select(DB::raw("loans.id,SUM(loan_fees_meta.value) as value,SUM(loan_schedules.principal) as totalprincipal,SUM(loan_schedules.interest) as totalinterest,SUM(loan_schedules.interest) + SUM(loan_schedules.principal) as totalbalance,COUNT(DISTINCT(loans.borrower_id)) as totalborrower"))->get();

                    
                       /*  $query = Loan::query()->join("loan_schedules","loan_schedules.loan_id","=","loans.loan_id")->where(["status"=>$request->status])->whereBetween('loans.release_date', [$start_date, $end_date])->select("loans.*",DB::raw("SUM(loan_schedules.principal) as totalprincipal"));

                        foreach($query->get() as $q){
                            var_dump($q->totalprincipal);
                        }
                        return;

                        $data = $query->where($branchu)->get();
                        if($query->where($branchu)->sum("principal") > 0){
                            $principal += $query->where($branchu)->sum("principal");

                        foreach($query->where($branchu)->get() as  $bb){
                            $totalb[] = $bb->borrower->id;
                        }
                        $bal = 0;
                        $ints = 0;
                        foreach($query->where($branchu)->get() as $datax){
                            $bal += DB::table("loan_schedules")->where(["loan_id"=>$datax->id])->sum("interest")  + DB::table("loan_schedules")->where(["loan_id"=>$datax->id])->sum("principal");
                            $ints += DB::table("loan_schedules")->where(["loan_id"=>$datax->id])->sum("interest");
                        }
                        $totalint += $ints;
                        $balances +=$bal;
                        $nestedData['branch'] = $b->name;
                        $nestedData['borrower'] = number_format($query->with("borrower")->where($branchu)->count());
                        $nestedData['principal'] = number_format($query->where($branchu)->sum("principal"));
                        $nestedData['interest'] =  number_format($ints);


                        $nestedData['balance'] =  number_format($bal);
                            $totalFiltere += 1;

                        $datas[] = $nestedData;  */
//                     }
                        


                        foreach($db as $d){
                            if($d->totalborrower > 0 && $d->totalprincipal > 0){
                                // $total_fee = DB::table("loan_fees_meta")->where(["parent_id"=>$d->id])->first()->value;
                                $total_fee = $d->value;
                                
                       $nestedData['branch'] = $b->name;
                            $nestedData['borrower'] = number_format($d->totalborrower);
                        $nestedData['principal'] = number_format($d->totalprincipal);
                        $nestedData['interest'] =  number_format($d->totalinterest);
                        $nestedData['fees'] =  number_format($total_fee);
                        $nestedData['balance'] =  number_format($d->totalbalance);
                        $totalFiltere += 1;
                            $totalint += $d->totalinterest;
                            $balances +=$d->totalprincipal + $d->totalinterest;
                            $principal += $d->totalprincipal;
                                $totalb += $d->totalborrower;
                                $totalfee += $total_fee;
                        $datas[] = $nestedData;  
                                }
                            }
                        }
                }

                if($request->loan_type == "group"){
                    if ($request->status == 'all') {

                        
                        

                        if($request->group == null){
                            $db = DB::table("loans")->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where("loans.group_id","!=",null)->where(["loans.branch_id"=>$b->id])->whereBetween('loans.release_date', [$start_date, $end_date])->select(DB::raw("loans.id,SUM(loan_schedules.principal) as totalprincipal,SUM(loan_schedules.interest) as totalinterest,SUM(loan_schedules.interest) + SUM(loan_schedules.principal) as totalbalance,COUNT(DISTINCT(loans.borrower_id)) as totalborrower"))->get();

                        }else{
                            $db = DB::table("loans")->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where("loans.group_id","!=",$request->group)->where(["loans.branch_id"=>$b->id])->whereBetween('loans.release_date', [$start_date, $end_date])->select(DB::raw("loans.id,SUM(loan_schedules.principal) as totalprincipal,SUM(loan_schedules.interest) as totalinterest,SUM(loan_schedules.interest) + SUM(loan_schedules.principal) as totalbalance,COUNT(DISTINCT(loans.borrower_id)) as totalborrower"))->get();

                        }

                       /*  $data = $query->where($branchu)->get();
                        if($query->where($branchu)->sum("principal") > 0){
                            $principal += $query->where($branchu)->sum("principal");

                            foreach($query->where($branchu)->get() as  $bb){
                                $totalb[] = $bb->borrower->id;
                            }
                            $bal = 0;
                            $ints = 0;
                            foreach($query->where($branchu)->get() as $datax){
                                $bal += DB::table("loan_schedules")->where(["loan_id"=>$datax->id])->sum("interest")  + DB::table("loan_schedules")->where(["loan_id"=>$datax->id])->sum("principal");
                                $ints += DB::table("loan_schedules")->where(["loan_id"=>$datax->id])->sum("interest");
                            }
                            $totalint += $ints;
                            $balances +=$bal;
                            $nestedData['branch'] = $b->name;
                            $nestedData['borrower'] = number_format($query->with("borrower")->where($branchu)->count());
                            $nestedData['principal'] = number_format($query->where($branchu)->sum("principal"));
                            $nestedData['interest'] =  number_format($ints);


                            $nestedData['balance'] =  number_format($bal);
                            $totalFiltere += 1;

                            $datas[] = $nestedData; 
                            } */
                        
                        
                        
                        foreach($db as $d){
                            if($d->totalborrower > 0 && $d->totalprincipal > 0){
                                $total_fee = DB::table("loan_fees_meta")->where(["parent_id"=>$d->id])->first()->value;
        
                       $nestedData['branch'] = $b->name;
                            $nestedData['borrower'] = number_format($d->totalborrower);
                        $nestedData['principal'] = number_format($d->totalprincipal);
                        $nestedData['interest'] =  number_format($d->totalinterest);
                        $nestedData['fees'] =  number_format($total_fee);
                        $nestedData['balance'] =  number_format($d->totalbalance);
                        $totalFiltere += 1;
                            $totalint += $d->totalinterest;
                            $balances +=$d->totalprincipal + $d->totalinterest;
                            $principal += $d->totalprincipal;
                                $totalb += $d->totalborrower;
                                $totalfee += $total_fee;
                                $datas[] = $nestedData;  
                            }
                        }

                    } else {
                        

                        if($request->group == null){
                            $db = DB::table("loans")->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where("loans.group_id","!=",null)->where(["loans.branch_id"=>$b->id])->where(["status"=>$request->status])->whereBetween('loans.release_date', [$start_date, $end_date])->select(DB::raw("SUM(loan_schedules.principal) as totalprincipal,SUM(loan_schedules.interest) as totalinterest,SUM(loan_schedules.interest) + SUM(loan_schedules.principal) as totalbalance,COUNT(DISTINCT(loans.borrower_id)) as totalborrower"))->get();

                        }else{
                            $db = DB::table("loans")->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where("loans.group_id","!=",$request->group)->where(["loans.branch_id"=>$b->id])->where(["status"=>$request->status])->whereBetween('loans.release_date', [$start_date, $end_date])->select(DB::raw("SUM(loan_schedules.principal) as totalprincipal,SUM(loan_schedules.interest) as totalinterest,SUM(loan_schedules.interest) + SUM(loan_schedules.principal) as totalbalance,COUNT(DISTINCT(loans.borrower_id)) as totalborrower"))->get();

                        }

                    /*     if($request->group == null){
                            $query = Loan::query()->where(["status"=>$request->status])->where("group_id","!=",NULL)->whereBetween('loans.release_date', [$start_date, $end_date]);
                        }else{
                            $query = Loan::query()->where(["group_id"=>$request->group,"status"=>$request->status])->whereBetween('loans.release_date', [$start_date, $end_date]);
                        }
 */
                     /*    $data = $query->where($branchu)->get();
                        if($query->where($branchu)->sum("principal") > 0){
                            $principal += $query->where($branchu)->sum("principal");

                            foreach($query->where($branchu)->get() as  $bb){
                                $totalb[] = $bb->borrower->id;
                            }
                            $bal = 0;
                            $ints = 0;
                            foreach($query->where($branchu)->get() as $datax){
                                $bal += DB::table("loan_schedules")->where(["loan_id"=>$datax->id])->sum("interest")  + DB::table("loan_schedules")->where(["loan_id"=>$datax->id])->sum("principal");
                                $ints += DB::table("loan_schedules")->where(["loan_id"=>$datax->id])->sum("interest");
                            }
                            $totalint += $ints;
                            $balances +=$bal;
                            $nestedData['branch'] = $b->name;
                            $nestedData['borrower'] = number_format($query->with("borrower")->where($branchu)->count());
                            $nestedData['principal'] = number_format($query->where($branchu)->sum("principal"));
                            $nestedData['interest'] =  number_format($ints);


                            $nestedData['balance'] =  number_format($bal);
                            $totalFiltere += 1;

                            $datas[] = $nestedData; 
                        } */
                        
                        
                        foreach($db as $d){
                            if($d->totalborrower > 0 && $d->totalprincipal > 0){
                                $total_fee = DB::table("loan_fees_meta")->where(["parent_id"=>$d->id])->first()->value;
        
                                $nestedData['branch'] = $b->name;
                                     $nestedData['borrower'] = number_format($d->totalborrower);
                                 $nestedData['principal'] = number_format($d->totalprincipal);
                                 $nestedData['interest'] =  number_format($d->totalinterest);
                                 $nestedData['fees'] =  number_format($total_fee);
                                 $nestedData['balance'] =  number_format($d->totalbalance);
                                 $totalFiltere += 1;
                                     $totalint += $d->totalinterest;
                                     $balances +=$d->totalprincipal + $d->totalinterest;
                                     $principal += $d->totalprincipal;
                                         $totalb += $d->totalborrower;
                                         $totalfee += $total_fee;
                                $datas[] = $nestedData;  
                               
                            }
                        }
                    }
                }

                if($request->loan_type == "individual"){



                    if ($request->status == 'all') {
                        

                            $db = DB::table("loans")->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where("loans.group_id","=",null)->where(["loans.branch_id"=>$b->id])->whereBetween('loans.release_date', [$start_date, $end_date])->select(DB::raw("loans.id,SUM(loan_schedules.principal) as totalprincipal,SUM(loan_schedules.interest) as totalinterest,SUM(loan_schedules.interest) + SUM(loan_schedules.principal) as totalbalance,COUNT(DISTINCT(loans.borrower_id)) as totalborrower"))->get();


                           

                            foreach($db as $d){
                            if($d->totalborrower > 0 && $d->totalprincipal > 0){
                                $total_fee = DB::table("loan_fees_meta")->where(["parent_id"=>$d->id])->first()->value;
        
                                $nestedData['branch'] = $b->name;
                                     $nestedData['borrower'] = number_format($d->totalborrower);
                                 $nestedData['principal'] = number_format($d->totalprincipal);
                                 $nestedData['interest'] =  number_format($d->totalinterest);
                                 $nestedData['fees'] =  number_format($total_fee);
                                 $nestedData['balance'] =  number_format($d->totalbalance);
                                 $totalFiltere += 1;
                                     $totalint += $d->totalinterest;
                                     $balances +=$d->totalprincipal + $d->totalinterest;
                                     $principal += $d->totalprincipal;
                                         $totalb += $d->totalborrower;
                                         $totalfee += $total_fee;
                                $datas[] = $nestedData;  
                            }
                        }
                       /*  $query = Loan::query()->where(["group_id"=>NULL])->whereBetween('loans.release_date', [$start_date, $end_date]);
                        $data = $query->where($branchu)->get();
                        if($query->where($branchu)->sum("principal") > 0){
                            $principal += $query->where($branchu)->sum("principal");

                        foreach($query->where($branchu)->get() as  $bb){
                            $totalb[] = $bb->borrower->id;
                        }
                        $bal = 0;
                        $ints = 0;
                        foreach($query->where($branchu)->get() as $datax){
                            $bal += DB::table("loan_schedules")->where(["loan_id"=>$datax->id])->sum("interest")  + DB::table("loan_schedules")->where(["loan_id"=>$datax->id])->sum("principal");
                            $ints += DB::table("loan_schedules")->where(["loan_id"=>$datax->id])->sum("interest");
                        }
                        $totalint += $ints;
                        $balances +=$bal;
                        $nestedData['branch'] = $b->name;
                        $nestedData['borrower'] = number_format($query->with("borrower")->where($branchu)->count());
                            $nestedData['principal'] = number_format($query->where($branchu)->sum("principal"));
                            $nestedData['interest'] =  number_format($ints);


                            $nestedData['balance'] =  number_format($bal);
                            $totalFiltere += 1;

                            $datas[] = $nestedData; 
                        } */


                    } else {
                        
                        $db = DB::table("loans")->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where("loans.group_id","=",null)->where(["loans.branch_id"=>$b->id,"loans.status"=>$request->status])->whereBetween('loans.release_date', [$start_date, $end_date])->select(DB::raw("loans.id,SUM(loan_schedules.principal) as totalprincipal,SUM(loan_schedules.interest) as totalinterest,SUM(loan_schedules.interest) + SUM(loan_schedules.principal) as totalbalance,COUNT(DISTINCT(loans.borrower_id)) as totalborrower"))->get();


                        foreach($db as $d){
                            if($d->totalborrower > 0 && $d->totalprincipal > 0){
                                $total_fee = DB::table("loan_fees_meta")->where(["parent_id"=>$d->id])->first()->value;
        
                                $nestedData['branch'] = $b->name;
                                     $nestedData['borrower'] = number_format($d->totalborrower);
                                 $nestedData['principal'] = number_format($d->totalprincipal);
                                 $nestedData['interest'] =  number_format($d->totalinterest);
                                 $nestedData['fees'] =  number_format($total_fee);
                                 $nestedData['balance'] =  number_format($d->totalbalance);
                                 $totalFiltere += 1;
                                     $totalint += $d->totalinterest;
                                     $balances +=$d->totalprincipal + $d->totalinterest;
                                     $principal += $d->totalprincipal;
                                         $totalb += $d->totalborrower;
                                         $totalfee += $total_fee;
                                $datas[] = $nestedData;  

                            
                            }
                        }
                    /*     $query = Loan::query()->where(["status"=>$request->status,"group_id"=>NULL])->whereBetween('loans.release_date', [$start_date, $end_date]);
                        $data = $query->where($branchu)->get();
                        if($query->where($branchu)->sum("principal") > 0){
                            $principal += $query->where($branchu)->sum("principal");

                        foreach($query->where($branchu)->get() as  $bb){
                            $totalb[] = $bb->borrower->id;
                        }
                        $bal = 0;
                        $ints = 0;
                        foreach($query->where($branchu)->get() as $datax){
                            $bal += DB::table("loan_schedules")->where(["loan_id"=>$datax->id])->sum("interest")  + DB::table("loan_schedules")->where(["loan_id"=>$datax->id])->sum("principal");
                            $ints += DB::table("loan_schedules")->where(["loan_id"=>$datax->id])->sum("interest");
                        }
                        $totalint += $ints;
                        $balances +=$bal;
                        $nestedData['branch'] = $b->name;
                            $nestedData['borrower'] = number_format($query->with("borrower")->where($branchu)->count());
                            $nestedData['principal'] = number_format($query->where($branchu)->sum("principal"));
                            $nestedData['interest'] =  number_format($ints);


                            $nestedData['balance'] =  number_format($bal);
                            $totalFiltere += 1;

                            $datas[] = $nestedData; 
                        } */
                    }

                }
            }



                $totalData = count($data);
                $totalFilter = $totalFiltere;
                $totalFiltered = $totalData; 

            }
        }else{
            if($request->inc == "on"){
                if ($request->status == 'all') {
                    $data = Loan::whereBetween('loans.release_date', [$start_date, $end_date])->select("loans.*") 
                        ->where(["disbursed_by_id"=>$request->loan_officer])
                    ->orWhere(["approved_by_id"=>$request->loan_officer])
                    ->orderBy("loans.id",$dir) ;
                $datax =    Loan::whereBetween('release_date', [$start_date, $end_date])
                    ->where(["disbursed_by_id"=>$request->loan_officer])
                    ->orWhere(["approved_by_id"=>$request->loan_officer])

                   ; 
                $totalFiltered =  0;
            } else {
                $data = Loan::where(["loans.status"=>$request->status])->whereBetween('loans.release_date', [$start_date, $end_date])->select("loans.*") 
                    ->where(["disbursed_by_id"=>$request->loan_officer])
                    ->orWhere(["approved_by_id"=>$request->loan_officer])
                    ->orderBy("loans.id",$dir) ;

                $datax =    Loan::where(["loans.status"=>$request->status])->whereBetween('loans.release_date', [$start_date, $end_date])->select("loans.*")
                    ->where(["disbursed_by_id"=>$request->loan_officer])
                    ->orWhere(["approved_by_id"=>$request->loan_officer])

                     ; 
                $totalFiltered = 0;
            }
        }else{
            if(empty($request->loan_type) ){
                if ($request->status == 'all') {
                    $data = Loan::where($branchu)->whereBetween('loans.release_date', [$start_date, $end_date])->select("loans.*") 
                        ->orderBy("loans.id",$dir) ;
                    $datax =    Loan::where($branchu)->whereBetween('release_date', [$start_date, $end_date])


                         ; 
                         
                    $totalFiltered =  0; 
                } else {
                    $data = Loan::where($branchu)->where(["loans.status"=>$request->status])->whereBetween('loans.release_date', [$start_date, $end_date])->select("loans.*") 

                        ->orderBy("loans.id",$dir) ;

                    $datax =    Loan::where($branchu)->where(["loans.status"=>$request->status])->whereBetween('loans.release_date', [$start_date, $end_date])->select("loans.*")


                         ; 
                    $totalFiltered = 0;
                }
               
            }

            if($request->loan_type == "group"){
                if ($request->status == 'all') {
                    $data = Loan::where($branchu)->whereBetween('loans.release_date', [$start_date, $end_date])->select("loans.*")
                        ->where(["group_id"=>$request->group])


                        ->orderBy("loans.id",$dir) ;
                    $datax =    Loan::where(['branch_id'=> $branchu])->whereBetween('release_date', [$start_date, $end_date])
                        ->where(["group_id"=>$request->group])

                        ; 
                    $totalFiltered =  0;
                } else {
                    $data = Loan::where($branchu)->where(["status"=>$request->status])->whereBetween('loans.release_date', [$start_date, $end_date]) 
                        ->where(["group_id"=>$request->group])
                        ->orderBy("loans.id",$dir) ;

                    /*  $totalFiltered =    Loan::where(['loans.branch_id'=> session('branch_id')])->where(["status"=>$request->status])->whereBetween('loans.release_date', [$start_date, $end_date])->select("loans.*")


                    ->count(); */
                    $datax =    Loan::where(['branch_id'=> $branchu])->where(["status"=>$request->status])->whereBetween('release_date', [$start_date, $end_date])
                        ->where(["group_id"=>$request->group])

                       ; 
                    $totalFiltered = 0;
                }
            }

            if($request->loan_type == "individual"){


           
                if ($request->status == 'all') {
                    $data = Loan::where($branchu)->whereBetween('loans.release_date', [$start_date, $end_date]) 
                        ->where(["group_id"=>NULL])
                        ->orderBy("loans.id",$dir);
                     
                    $datax =    Loan::where($branchu)->whereBetween('loans.release_date', [$start_date, $end_date])

                        ->where(["group_id"=>NULL])

                        ->orderBy("loans.id",$dir)
                        ; 
                    $totalFiltered =  0;

            
                
                } else {
                    $data = Loan::where($branchu)->where(["status"=>$request->status])->whereBetween('loans.release_date', [$start_date, $end_date])->select("loans.*") 
                        ->where(["group_id"=>null])
                        ->orderBy("loans.id",$dir);

                    $datax =    Loan::where($branchu)->where(["status"=>$request->status])->whereBetween('loans.release_date', [$start_date, $end_date])->select("loans.*")
                        ->where(["group_id"=>null])

                        ; 
                    $totalFiltered = 0;
                }

             

            }
        }




        

        $principal = 0;
        $balances = 0;
        $due = 0;
        $paid = 0;
        $totalint = 0;
        $total_fee = 0;
        $fee = 0;
        $totalb = [];
        if(empty($request->loan_type) &&  !empty($request->loan_officer)){
          
            // $totalFiltered = $datax->count();
            // $totalb = $datax->with("borrower")->count();
            // $principal = $datax->sum("principal");
            // $total_fee = $datax->join("loan_fees_meta","loan_fees_meta.parent_id","=","loans.id")->sum("value");
            // $totalint = $datax->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->sum("loan_schedules.interest");
            // $balances = $interest + $principal;
     
            $datax  =  $datax->leftJoin("loan_fees_meta","loan_fees_meta.parent_id","=","loans.id")
            ->leftJoin("loan_schedules","loan_schedules.loan_id","=","loans.id")
            ->leftJoin("borrowers","borrowers.id","=","loans.borrower_id")
            ->select("loan_fees_meta.value",
            "borrowers.loan_officers",
            DB::raw("SUM(loans.principal) as principal"),
            DB::raw("COUNT(DISTINCT loans.borrower_id) as totalb"),
            DB::raw("SUM(loan_schedules.interest) as interest"),
            DB::raw("SUM(loan_schedules.interest + loans.principal) as balance")
            )->groupBy("loans.id")
         ->get();
            foreach($datax as $d){
                
                $officer = unserialize($d->loan_officers);
              
                foreach($officer as $of){
                    if($request->loan_officer == $of){
                        // $total_fee = DB::table("loan_fees_meta")->where(["parent_id"=>$d->id])->first()->value;
        
                        $totalFiltered = count( $datax);
                        $totalb = $d->totalb;
                        $principal =  $d->principal;
                        $total_fee += $d->value;
                        $balances = $d->balance ;
                        $totalint  = $d->interest;
                    }
                }
            }
       
        }


        if(empty($request->loan_type) &&  empty($request->loan_officer)){

            $totalFiltered = $datax->count();
            $totalb = $datax->with("borrower")->count();
            $principal += $datax->sum("principal");
           
            $total_fee = $datax->join("loan_fees_meta","loan_fees_meta.parent_id","=","loans.id")->sum("value");
            $totalint = $datax->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->sum("loan_schedules.interest");
            $balances = $interest + $principal;

          


        }else{
            if($request->loan_type == "group"){
                $of = DB::table("loan_officers2")->where(["branch_id"=>$request->group])->get();
                $off = [];
                foreach($of as $oo){
                    $off[] = $oo->officer_id;
                }
                $of = serialize($off);



                foreach($datax as $d){
                   
                    $officer = unserialize($of);

                    foreach($officer as $of){
                     

                        if($of == $request->loan_officer){
                            $total_fee = DB::table("loan_fees_meta")->where(["parent_id"=>$d->id])->first()->value;
        
                            $totalb[] = $d->borrower->id;
                            $principal +=  $d->principal;
                            $totalfee += $total_fee;
                            $balances += DB::table("loan_schedules")->where(["loan_id"=>$d->id])->sum("interest") + $d->principal ;
                            $totalint +=  DB::table("loan_schedules")->where(["loan_id"=>$d->id])->sum("interest");


                        }
                    }
                }
            }
            if(empty($request->loan_officer)){
            
                $totalFiltered = $datax->count();
                $totalb = $datax->with("borrower")->count();
                $principal = $datax->sum("principal");
                $total_fee = $datax->join("loan_fees_meta","loan_fees_meta.parent_id","=","loans.id")->sum("value");
                $totalint = $datax->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->sum("loan_schedules.interest");
                $balances = $totalint + $principal;
        
              
                // foreach($datax as $d){
                //     $total_fee = DB::table("loan_fees_meta")->where(["parent_id"=>$d->id])->first()->value;
        
                //     $totalFiltered += 1;
                //     $totalb[] = $d->borrower->id;
                //     $principal +=  $d->principal;
                //     $totalfee += $total_fee;
                //     $balances += DB::table("loan_schedules")->where(["loan_id"=>$d->id])->sum("interest") + $d->principal ;
                //     $totalint +=  DB::table("loan_schedules")->where(["loan_id"=>$d->id])->sum("interest");
                // }

              
            }

          
            if($request->loan_type == "individual"){
            
                $datax  =  $datax->leftJoin("loan_fees_meta","loan_fees_meta.parent_id","=","loans.id")
           ->leftJoin("loan_schedules","loan_schedules.loan_id","=","loans.id")
           ->leftJoin("borrowers","borrowers.id","=","loans.borrower_id")
           ->select("loan_fees_meta.value",
           "borrowers.loan_officers",
           DB::raw("SUM(loans.principal) as principal"),
           DB::raw("COUNT(borrowers.id) as totalb"),
           DB::raw("SUM(loan_schedules.interest) as interest"),
           DB::raw("SUM(loan_schedules.interest + loans.principal) as balance")
           )->groupBy("loans.id")
        ->get();
  
            foreach($datax as $d){
                
                $officer = unserialize($d->loan_officers);
                foreach($officer as $of){
                    if($request->loan_officer == $of){
                        // $total_fee = DB::table("loan_fees_meta")->where(["parent_id"=>$d->id])->first()->value;
        
                        $totalFiltered = count( $datax);
                        $totalb = $d->totalb;
                        $principal =  $d->principal;
                        $totalfee = $d->value;
                        $balances = $d->balance ;
                        $totalint  = $d->interest;
                    }
                }
            }

              
            }else{

            }

        }




        $totalData = count($data);
        $totalFilter = $totalFiltered;
        $totalFiltered = $totalData; 

        if($request->loan_type == "group"){
            $of = DB::table("loan_officers2")->where(["branch_id"=>$request->group])->get();
            $off = [];
            foreach($of as $oo){
                $off[] = $oo->officer_id;
            }
            $of = serialize($off);



            foreach($data as $d){
            
                $officer = unserialize($of);

                foreach($officer as $of){


                    if($of == $request->loan_officer){


                        $id = $d->id;
                        if ($loan->override == 1) {
                            $balance = $d->payments->sum('amount') - $loan->balance;
                        } else {
                            $balance = $d->payments->sum('amount') - $d->payments->sum('interest') + $d->payments->sum('principal');
                        }

                        $key = $d;
                        $bname = "";
                        $branch = DB::table("branches")->where(["id"=>$key->borrower->branch_id])->first();
                        if(!empty($key->borrower)){
                            $bname = '<a href="'.url('borrower/'.$key->borrower_id.'/show').'">'.$key->borrower->first_name.' '.$key->borrower->last_name.'</a>'."$key->name";
                        }else{
                            $bname =' <span class="label label-danger">'.trans_choice('general.broken',1).'<i
                class="fa fa-exclamation-triangle"></i> </span>'."$key->name";
                        }


                        $fe = DB::table("loan_fees_meta")->where(["parent_id"=>$d->id])->first()->value;

                        if(\App\Models\Setting::where('setting_key', 'currency_position')->first()->setting_value=='left'){
                            $d2 =  \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value." ".number_format($key->principal,2);
                            $paid = \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value." ".number_format($d->payments->sum('amount'),2);
                            $bal = \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value." ".number_format(DB::table("loan_schedules")->where(["loan_id"=>$d->id])->sum("interest") + $d->principal,2);
                        }else{
                            $d2 =  number_format($key->principal,2)." ".\App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value;
                            $paid = number_format($d->payments->sum('amount'),2)." ".\App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value;
                            $bal = number_format(DB::table("loan_schedules")->where(["loan_id"=>$d->id])->sum("interest") + $d->principal,2)." ".\App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value;
                        }
                        //                         $balances +=  GeneralHelper::loan_total_balance($key->id);
                        $d3 = number_format(DB::table("loan_schedules")->where(["loan_id"=>$key->id])->sum("interest"),2);


                        /*   if($key->override==1){
                $due = $due + $key->balance; 
                $d4 = '<s>'.number_format(\App\Helpers\GeneralHelper::loan_total_due_amount($key->id),2).'</s><br>'.number_format($key->balance,2);
            }else{
                $due = $due + \App\Helpers\GeneralHelper::loan_total_due_amount($key->id);
                $d4 = number_format(\App\Helpers\GeneralHelper::loan_total_due_amount($key->id),2);
            }  */

                        if ($loan->override == 1) {

                            $d4 = '<s>'.number_format($loan->balance - $d->payments->sum('amount'),2).'</s><br>'.number_format($key->balance,2);


                        } else {
                            $d4 = $d->payments->sum('amount') - $d->payments->sum('interest') + $d->principal;

                            $d4 = number_format($d4,2);
                        }




                        $nestedData['branch'] = $branch->name;
                        $nestedData['borrower'] = $bname;
                        $nestedData['principal'] = $d2;
                        $nestedData['interest'] = $d3;
                        $nestedData['balance'] = $bal;
                        $nestedData['fees'] = $fe;
                        


                        $datas[] = $nestedData;
                    }
                }
            }
        }else{
            if(empty($request->loan_officer)){
 
                $datas = $data
                ->leftJoin("borrowers","borrowers.id","=","loans.borrower_id")
                ->leftJoin("branches","branches.id","=","loans.branch_id")
                ->leftJoin("loan_schedules","loan_schedules.loan_id","=","loans.id")
                ->leftJoin("loan_fees_meta","loan_fees_meta.parent_id","=","loans.id")
                ->select("branches.name as branch",
                
                DB::raw("CONCAT(borrowers.first_name,' ',borrowers.last_name) AS borrower"),
                DB::raw("loans.principal"),
                DB::raw("SUM(loan_schedules.interest) as interest"),
                DB::raw("loan_fees_meta.value as fees"),
                DB::raw("loans.principal + SUM(loan_schedules.interest) as balance"),

                )
                ->groupBy("loans.id")
                ->get()->toArray();
 
            

 
            //     foreach($data->get() as $d){



            //         $id = $d->id;
            //         if ($loan->override == 1) {
            //             $balance = $d->payments->sum('amount') - $loan->balance;
            //         } else {
            //             $balance = $d->payments->sum('amount') - $d->payments->sum('interest') + $d->payments->sum('principal');
            //         }

            //         $key = $d;
            //         $bname = "";
            //         $branch = DB::table("branches")->where(["id"=>$key->borrower->branch_id])->first();
            //         if(!empty($key->borrower)){
            //             if($key->group_id != null){
            //                 $bname =' <a href="'.url('borrower/'.$key->borrower_id.'/show').'">'.$key->borrower->first_name.' '.$key->borrower->last_name.'</a>'.'<a href="/borrower/group/'.$key->group_id.'/vloans" target="_blank"> <span class="badge badge-danger bg-red">group loan</span> </a>';
            //             }else{
            //                 $bname =' <a href="'.url('borrower/'.$key->borrower_id.'/show').'">'.$key->borrower->first_name.' '.$key->borrower->last_name.'</a>';
            //             }

            //         }else{
            //             $bname =' <span class="label label-danger">'.trans_choice('general.broken',1).'<i
            //     class="fa fa-exclamation-triangle"></i> </span>'."$key->name";
            //         }

            //         $total_fee = DB::table("loan_fees_meta")->where(["parent_id"=>$d->id])->first()->value;
        


            //         if(\App\Models\Setting::where('setting_key', 'currency_position')->first()->setting_value=='left'){
            //             $d2 =  \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value." ".$key->principal;
            //             $paid = \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value." ".number_format($d->payments->sum('amount'),2);
            //             $bal = \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value." ".number_format(DB::table("loan_schedules")->where(["loan_id"=>$d->id])->sum("interest") + $d->principal,2);
            //         }else{
            //             $d2 =  number_format($key->principal,2)." ".\App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value;
            //             $paid = number_format($d->payments->sum('amount'),2)." ".\App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value;
            //             $bal = number_format(DB::table("loan_schedules")->where(["loan_id"=>$d->id])->sum("interest") + $d->principal,2)." ".\App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value;
            //         }

            //         $d3 = number_format(DB::table("loan_schedules")->where(["loan_id"=>$key->id])->sum("interest"),2);


            //         /*   if($key->override==1){
            //     $due = $due + $key->balance; 
            //     $d4 = '<s>'.number_format(\App\Helpers\GeneralHelper::loan_total_due_amount($key->id),2).'</s><br>'.number_format($key->balance,2);
            // }else{
            //     $due = $due + \App\Helpers\GeneralHelper::loan_total_due_amount($key->id);
            //     $d4 = number_format(\App\Helpers\GeneralHelper::loan_total_due_amount($key->id),2);
            // }  */

            //         if ($loan->override == 1) {

            //             $d4 = '<s>'.number_format($loan->balance - $d->payments->sum('amount'),2).'</s><br>'.number_format($key->balance,2);


            //         } else {
            //             $d4 = $d->payments->sum('amount') - $d->payments->sum('interest') + $d->principal;

            //             $d4 = number_format($d4,2);
            //         }




            //         $nestedData['branch'] = $branch->name;
            //         $nestedData['borrower'] = $bname;
            //         $nestedData['principal'] = $d2;
            //         $nestedData['interest'] = $d3;
            //         $nestedData['balance'] = $bal;
            //         $nestedData['fees'] = $total_fee;


            //         $datas[] = $nestedData;
            //     }


            }else{

                $data = $data
                ->leftJoin("borrowers","borrowers.id","=","loans.borrower_id")
                ->leftJoin("branches","branches.id","=","loans.branch_id")
                ->leftJoin("loan_schedules","loan_schedules.loan_id","=","loans.id")
                ->leftJoin("loan_fees_meta","loan_fees_meta.parent_id","=","loans.id")
                ->select("loans.group_id","loans.override","branches.name as branch","borrowers.loan_officers","borrowers.id as borrower_id","borrowers.branch_id",
                
                DB::raw("CONCAT(borrowers.first_name,' ',borrowers.last_name) AS borrower"),
                DB::raw("loans.principal"),
                DB::raw("SUM(loan_schedules.interest) as interest"),
                DB::raw("loan_fees_meta.value as fees"),
                DB::raw("loans.principal + SUM(loan_schedules.interest) as balance"),

                ) 
                ->groupBy("loans.id") 
                ->get();

          
 
 
 
                foreach($data as $d){

                    $officer = unserialize($d->loan_officers);

                    foreach($officer as $of){

                        if($of == $request->loan_officer){
                            //                             $principal +=  $d->principal;

                //             $id = $d->id;
                //             if ($loan->override == 1) {
                //                 $balance = $d->payments->sum('amount') - $loan->balance;
                //             } else {
                //                 $balance = $d->payments->sum('amount') - $d->payments->sum('interest') + $d->payments->sum('principal');
                //             }

                            $key = $d;
                //             $bname = "";
                //             $branch = DB::table("branches")->where(["id"=>$key->branch_id])->first();
                            if(!empty($key->borrower)){
                                if($key->group_id != null){
                                    $bname =' <a href="'.url('borrower/'.$key->borrower_id.'/show').'">'.$key->borrower.' <a href="/borrower/group/'.$key->group_id.'/vloans" target="_blank"> <span class="badge badge-danger bg-red">group loan</span> </a>';
                                }else{
                                    $bname =' <a href="'.url('borrower/'.$key->borrower_id.'/show').'">'.$key->borrower.' </a>';
                                }

                            }else{
                                $bname =' <span class="label label-danger">'.trans_choice('general.broken',1).'<i
                class="fa fa-exclamation-triangle"></i> </span>'."$key->borrower";
                            }
                //             $total_fee = DB::table("loan_fees_meta")->where(["parent_id"=>$d->id])->first()->value;
        

                            // if(\App\Models\Setting::where('setting_key', 'currency_position')->first()->setting_value=='left'){
                            //     $d2 =  \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value." ".number_format($key->principal,2);
                            //     $paid = \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value." ".number_format($d->payments->sum('amount'),2);
                            //     $bal = \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value." ".number_format(DB::table("loan_schedules")->where(["loan_id"=>$d->id])->sum("interest") + $d->principal,2);
                            // }else{
                            //     $d2 =  number_format($key->principal,2)." ".\App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value;
                            //     $paid = number_format($d->payments->sum('amount'),2)." ".\App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value;
                            //     $bal = number_format(DB::table("loan_schedules")->where(["loan_id"=>$d->id])->sum("interest") + $d->principal,2)." ".\App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value;
                            // }

                            // $d3 = number_format(DB::table("loan_schedules")->where(["loan_id"=>$key->id])->sum("interest"),2);


                            /*   if($key->override==1){
                $due = $due + $key->balance; 
                $d4 = '<s>'.number_format(\App\Helpers\GeneralHelper::loan_total_due_amount($key->id),2).'</s><br>'.number_format($key->balance,2);
            }else{
                $due = $due + \App\Helpers\GeneralHelper::loan_total_due_amount($key->id);
                $d4 = number_format(\App\Helpers\GeneralHelper::loan_total_due_amount($key->id),2);
            }  */

                            // if ($loan->override == 1) {

                            //     $d4 = '<s>'.number_format($loan->balance - $d->payments->sum('amount'),2).'</s><br>'.number_format($key->balance,2);


                            // } else {
                            //     $d4 = $d->payments->sum('amount') - $d->payments->sum('interest') + $d->principal;

                            //     $d4 = number_format($d4,2);
                            // }




                            $nestedData['branch'] = $d->branch;
                            $nestedData['borrower'] = $bname;
                            $nestedData['principal'] = $d->principal;
                            $nestedData['interest'] = $d->interest;
                            $nestedData['balance'] = $d->balance;
                            $nestedData['fees'] = $d->fees;


                            $datas[] = $nestedData;
                        }

                    }
                }

            }
        }
        }


       
        $principal = 0;
        $totalint = 0;
        $total_fee = 0;
        

        
        foreach($datas as $d){
           
            $principal +=  intval(str_replace(',', '', $d['principal']));
            $totalint +=   intval(str_replace(',', '', $d['interest']));
            $total_fee +=   intval(str_replace(',', '', $d['fees']));
           
        }

        if( $request->loan_officers){
            $totalb = $totalb;
        }elseif(!$request->branch){
            $totalb = $totalb;
        }else{
            $totalb = count($datas);
        }
 
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => count($datas), 
            "data"            => array_slice($datas, $start, $limit),
            "principal"=>number_format($principal),
            "balance"=>number_format($principal + $totalint),
            "interest"=>number_format($totalint),
            "total_fee"=>number_format($total_fee),
            "totalb"=>   $totalb 
        );
 
 
        return response()->json($json_data);
    }

    public function loan_list(Request $request)
    {
        if (!Sentinel::hasAccess('reports')) {
        return response()->json(["data"=>$json_data,"principal"=>$principal,"balance"=>$bal]);
            return redirect('/');
        }
        $start_date = \Carbon\Carbon::parse($request->start_date);
        $end_date = \Carbon\Carbon::parse($request->end_date);
        $sdate = $request->start_date;
        $edate = $request->end_date;
        $off = $request->loan_officer;
        $user = null;
        $status = '';

        if (!empty($request->status)) {
            if ($request->status == 'all') {
                $status = "All Status";
            } else {
                $status = $request->status;
            }
        } else {

        }
        if ($request->isMethod('post')) {
            $user = DB::table("users")->where(["id"=>$off])->first();
            $user = $user->first_name." ".$user->last_name;
            $start_date = date('Y-m-d', strtotime($start_date));

            $end_date = date('Y-m-d', strtotime($end_date));

            if ($request->status == 'all') {
                $data = Loan::where('loans.branch_id', session('branch_id'))->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")->orWhere(["loan_officers.officer_id"=>$request->loan_officer])->whereBetween('loans.release_date', [$start_date, $end_date])->select("loans.*")->get();
            } else {
                $data = Loan::where('loans.branch_id', session('branch_id'))->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")->orWhere(["loan_officers.officer_id"=>$request->loan_officer])->whereBetween('loans.release_date', [$start_date, $end_date])->where('loans.status',
                    $request->status)->get();
            }

        } else {
            $data = array();
        }
        $users = DB::table("users")->where(["branch_users.branch_id"=>session("branch_id")])
            ->join("branch_users","branch_users.user_id","=","users.id")
            ->join("role_users","role_users.user_id","=","users.id")->where(["role_users.role_id"=>4])->select("users.first_name","users.last_name","users.id")->get();



        $users = array_unique($users, SORT_REGULAR);
        $branch = DB::table("branches")->get();
        $group = DB::table("borrower_groups")->where(["branch_id"=>session("branch_id")])->get();
        return view('report.loan_list',
            compact('data', 'start_date',
                'end_date', 'status','users','user','sdate','edate','branch','group'));
    }

    public function loan_balance(Request $request)
    {
        if (!Sentinel::hasAccess('reports')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        if ($request->isMethod('post')) {
            $start_date = date('Y-m-d', strtotime($start_date));

            $end_date = date('Y-m-d', strtotime($end_date));
            $data = Loan::where('branch_id', session('branch_id'))->whereBetween('disbursed_date', [$start_date, $end_date])->where('status', 'disbursed')->get();

        } else {
            $data = array();
        }
        return view('report.loan_balance',
            compact('data', 'start_date',
                'end_date'));
    }

    public function loan_arrears(Request $request)
    {
        if (!Sentinel::hasAccess('reports')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $start_date = date('Y-m-d', strtotime($start_date));

        $end_date = date('Y-m-d', strtotime($end_date));

        return view('report.balance_sheet',
            compact('expenses'));
    }

    public function loan_transaction(Request $request)
    {
        if (!Sentinel::hasAccess('reports')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        

        $am = 0;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        if ($request->isMethod('post')) {
            $start_date = date('Y-m-d', strtotime($start_date));

            $end_date = date('Y-m-d', strtotime($end_date));


            $data = LoanRepayment::where('branch_id', session('branch_id'))->whereBetween('collection_date', [$start_date, $end_date])->paginate(100);
            $am = LoanRepayment::where('branch_id', session('branch_id'))->whereBetween('collection_date', [$start_date, $end_date])->sum("amount");


        } else {
            $data = array();
        }

          $users = DB::table("users")->where(["branch_users.branch_id"=>session("branch_id")])
              ->join("branch_users","branch_users.user_id","=","users.id")
              ->join("role_users","role_users.user_id","=","users.id")->where(["role_users.role_id"=>4])->select("users.first_name","users.last_name","users.id")->get();
$branch = DB::table("branches")->get();

        $users = array_unique($users, SORT_REGULAR);
        return view('report.loan_transaction',
            compact('data', 'start_date',
                'end_date',"am","users","branch"));
    }

    public function apiloan_transaction(Request $request){
        $datas  = [];
        if (!Sentinel::hasAccess('repayments')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        
        function get_percentage($total, $number)
        {
            if ( $total > 0 ) {
                return $total * ($number / 100);
            } else {
                return 0;
            }
        }
        $branchu = ["branch_id"=>$request->branch];
        if($request->branch == null){
            $branchu = [];
        } 
        $start_date = $request->start_date;
        $end_date = $request->end_date;
   $start_date = \Carbon\Carbon::parse($request->start_date);
        $end_date = \Carbon\Carbon::parse($request->end_date);

        $principal = 0;
        $interest = 0;
        $balance = 0;
        $totalb = [];

        if($request->branch == null){
            foreach(DB::table("branches")->get() as $b){
                $query = LoanRepayment::query();
                $branchu = ["branch_id"=>$b->id];


                if(empty($request->loan_type) ){

                    $datax =    $query->where($branchu)->whereBetween('collection_date', [$start_date, $end_date])


                        ->get(); 
                }

                if($request->loan_type == "group"){

                    if($request->group == null){
                        $datax =    $query->where($branchu)->whereBetween('collection_date', [$start_date, $end_date])
                            ->whereNotNull('sc')

                            ->get(); 
                    }else{
                        $datax =    $query->where($branchu)->whereBetween('collection_date', [$start_date, $end_date])
                            ->whereNotNull('sc')
                            ->whereHas('loan', function ($query) use ($request) {

                                $query->where(["group_id"=>$request->group]);  
                            })
                            ->get(); 
                    }

                }

                if($request->loan_type == "individual"){



                    $datax =    $query->where($branchu)->whereBetween('collection_date', [$start_date, $end_date])
                        ->whereNull('sc')

                        ->get(); 

                }
                if($query->with("borrower")->count() != 0){
  $prin = get_percentage($query->sum("amount"),83.325);
                $int = get_percentage($query->sum("amount"),16.675);
                foreach($datax as $d){
                    $totalb[] = $datax->borrower->id;
                }

                $bal = $prin + $int ;
                $principal += $prin;
                $interest += $int;
                $balance += $bal;
                array_push($datas,[$b->name,$query->with("borrower")->count(),number_format($prin,2),number_format($int,2),number_format($bal,2)]);
                    }
            }
        }else{
        if(empty($request->loan_type) ){
            $datax =    LoanRepayment::where($branchu)->whereBetween('collection_date', [$start_date, $end_date])


                    ->get(); 
        }

        if($request->loan_type == "group"){
            if($request->group == null){
                $datax =    LoanRepayment::where($branchu)->whereBetween('collection_date', [$start_date, $end_date])
                  ->whereNotNull('sc')

                    ->get(); 
            }else{
                $datax =    LoanRepayment::where($branchu)->whereBetween('collection_date', [$start_date, $end_date])
                    ->whereNotNull('sc')
                    ->whereHas('loan', function ($query) use ($request) {

                        $query->where(["group_id"=>$request->group]);  
                    })
                    ->get(); 
            }

        }

           if($request->loan_type == "individual"){



               $datax =    LoanRepayment::where($branchu)->whereBetween('collection_date', [$start_date, $end_date])
                   ->whereNull('sc')

                    ->get(); 

        }






    if(!empty($request->loan_officer)){

        foreach($datax as $key){
            $officer = unserialize($key->borrower->loan_officers);

            foreach($officer as $of){

                if($request->loan_officer == $of){


                        $borrower = "-";
                        $branch = DB::table("branches")->where(["id"=>$key->borrower->branch_id])->first();
                        if(!empty($key->borrower)){
                            if($key->loan->group_id != null){
                                $name =' <a href="'.url('borrower/'.$key->borrower_id.'/show').'">'.$key->borrower->first_name.' '.$key->borrower->last_name.'</a>'.'<a href="/borrower/group/'.$key->loan->group_id.'/vloans" target="_blank"> <span class="badge badge-danger bg-red">group loan</span> </a>';
                            }else{
                                $name =' <a href="'.url('borrower/'.$key->borrower_id.'/show').'">'.$key->borrower->first_name.' '.$key->borrower->last_name.'</a>';
                            }

                        }else{
                            $name =' <span class="label label-danger">'.trans_choice('general.broken',1).' <i
                class="fa fa-exclamation-triangle"></i> </span>';
                        }
                        $borrower = $name;
                    $prin = get_percentage($key->amount,83.325);
                    $int = get_percentage($key->amount,16.675);
                        $bal = $prin + $int ;
                        $principal += $prin;
                        $interest += $int;
                        $balance += $bal;
                    $totalb[] = $datax->borrower->id;

                    array_push($datas,[$branch->name,$borrower,number_format($prin,2),number_format($int,2),number_format($bal,2)]);

                }
            }
        }
    }else{
        

        foreach($datax as $key){

            $officer = unserialize($key->borrower->loan_officers);

            foreach($officer as $of){

                $borrower = "-";
                $branch = DB::table("branches")->where(["id"=>$key->borrower->branch_id])->first();
                if(!empty($key->borrower)){
                    if($key->loan->group_id != null){
                        $name =' <a href="'.url('borrower/'.$key->borrower_id.'/show').'">'.$key->borrower->first_name.' '.$key->borrower->last_name.'</a>'.'<a href="/borrower/group/'.$key->loan->group_id.'/vloans" target="_blank"> <span class="badge badge-danger bg-red">group loan</span> </a>';
                    }else{
                        $name =' <a href="'.url('borrower/'.$key->borrower_id.'/show').'">'.$key->borrower->first_name.' '.$key->borrower->last_name.'</a>';
                    }

                }else{
                    $name =' <span class="label label-danger">'.trans_choice('general.broken',1).' <i
                class="fa fa-exclamation-triangle"></i> </span>';
                }
                $borrower = $name;
            $prin = get_percentage($key->amount,83.325);
            $int = get_percentage($key->amount,16.675);
                $bal = $prin + $int ;
                $principal += $prin;
                $interest += $int;
                $balance += $bal;
            $totalb[] = $datax->borrower->id;
                array_push($datas,[$branch->name,$borrower,number_format($prin,2),number_format($int,2),number_format($bal,2)]);

        }

            }
    }
}
        $total_b = array_unique($totalb);

        return response()->json(["data"=>$datas,"principal"=>number_format($principal,2),"interest"=>number_format($interest,2),"balance"=>number_format($balance,2),"totalb"=>count($totalb)]);
    }

    public function apisaving_transactions(Request $request){
        $datas = [];
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $start_date = \Carbon\Carbon::parse($request->start_date);
        $end_date = \Carbon\Carbon::parse($request->end_date);
        $balance = 0;
      $datax =   SavingTransaction::where(['branch_id'=> session('branch_id')])->whereBetween('date', [$start_date, $end_date])->get();

        foreach($datax as $key){
            $schedule = DB::table("loan_schedules")->where(["loan_id"=>$key->loan->id])->first();


                $borrower = "-";
                if(!empty($key->borrower)){
                    if($key->loan->group_id != null){
                        $name =' <a href="'.url('borrower/'.$key->borrower_id.'/show').'">'.$key->borrower->first_name.' '.$key->borrower->last_name.'</a>'.'<a href="/borrower/group/'.$key->loan->group_id.'/vloans" target="_blank"> <span class="badge badge-danger bg-red">group loan</span> </a>';
                    }else{
                        $name =' <a href="'.url('borrower/'.$key->borrower_id.'/show').'">'.$key->borrower->first_name.' '.$key->borrower->last_name.'</a>';
                    }

                }else{
                    $name =' <span class="label label-danger">'.trans_choice('general.broken',1).' <i
                class="fa fa-exclamation-triangle"></i> </span>';
                }
$balance += $key->amount;
            array_push($datas,[$name,number_format($key->amount)]);
         }

        return response()->json(["data"=>$datas,"balance"=>number_format($balance)]);

    }
    
    public function expense_report(){
        return view("report.expense_report");
    }
    public function loan_classification(Request $request)
    {
        if (!Sentinel::hasAccess('reports')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $start_date = $request->start_date;
        $end_date = $request->end_date;


        $data = Loan::where('branch_id', session('branch_id'))->where('status', 'disbursed')->paginate(50);

        return view('report.loan_classification',
                    compact('data', 'start_date',
                            'end_date'));
    }
    
    public function apiexpense(Request $request){
        $datas = [];
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $start_date = \Carbon\Carbon::parse($request->start_date);
        $end_date = \Carbon\Carbon::parse($request->end_date);
        $balance = 0;
        $datax =   Expense::where(['branch_id'=> session('branch_id')])->whereBetween('date', [$start_date, $end_date])->get();

        foreach($datax as $key){
            $des = $key->expense_type->name;
            $balance += $key->amount;
            array_push($datas,[$des,number_format($key->amount)]);
        }

        return response()->json(["data"=>$datas,"balance"=>number_format($balance)]);
    }

    public function loan_projection(Request $request)
    {
        if (!Sentinel::hasAccess('reports')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $start_date = date('Y-m-d', strtotime($start_date));

        $end_date = date('Y-m-d', strtotime($end_date));
        $monthly_collections = array();
        $start_date1 = date("Y-m-d");
        for ($i = 1; $i < 14; $i++) {
            $d = explode('-', $start_date1);
            //get loans in that period
            $payments = 0;
            $payments_due = 0;
            foreach (LoanSchedule::where('branch_id', session('branch_id'))->where('year', $d[0])->where('month', $d[1])->get() as $key) {
                $payments_due = $payments_due + $key->principal + $key->interest + $key->fees + $key->penalty;
            }
            $payments_due = round($payments_due, 2);
            $ext = ' ' . $d[0];
            array_push($monthly_collections, array(
                'month' => date_format(date_create($start_date1),
                                       'M' . $ext),
                'due' => $payments_due

            ));
            //add 1 month to start date
            $start_date1 = date_format(date_add(date_create($start_date1),
            date_interval_create_from_date_string('1 months')),
            'Y-m-d');
            }
            $monthly_collections = json_encode($monthly_collections);
            return view('report.loan_projection',
            compact('monthly_collections', 'start_date',
            'end_date'));
            }


            public function saving_trans(){
            return view("report.saving_trans");
            }

            public function schedule_report(){
                $users = DB::table("users")->where(["branch_users.branch_id"=>session("branch_id")])
                ->join("branch_users","branch_users.user_id","=","users.id")
                ->join("role_users","role_users.user_id","=","users.id")->where(["role_users.role_id"=>4])->select("users.first_name","users.last_name","users.id")->get();
  $branch = DB::table("branches")->get();
  
          $users = array_unique($users, SORT_REGULAR);
                return view("report.schedules",compact("users","branch"));
            }


            
            public function schedule_report3(Request $request){

                $columns = array( 
                    0 =>'branch', 
                    1 =>'borrower',
                    2=> 'phone',
                    3=> 'amount', 
        
        
        
                );

                $draw = array( 
                    0 =>'branches.id', 
                    1 =>'borrowers.id',
                    2=> 'borrowers.phone',
                    3=> 'loans.id', 
        
        
        
                );

                $limit = $request->input('length');
                $startx = $request->input('start');
                $order = $columns[$request->input('order.0.column')];
                $orderby = $draw[$request->input('order.0.column')];
                $dir = $request->input('order.0.dir');
                

               
                $branch = $request->branch;
                $start = \Carbon\Carbon::parse($request->start_date);
                $end = \Carbon\Carbon::parse($request->end_date);
                $date = \Carbon\Carbon::parse($request->date);
                $loan_officer  = $request->loan_officer;

                if($branch){
                    $branch = ["loans.branch_id"=>$branch];
                }else{
                    $branch = [];
                }

                
$expected = 0;
$above = 0;
$notabove = 0;
$data = [];
$datas =  [];
if($request->query_type == "expected"){
    if(!$request->loan_officer){
                $expected = DB::table("loans")
                ->where($branch)
                ->leftJoin("loan_schedules","loan_schedules.loan_id","=","loans.id")
                ->leftJoin("loan_repayments","loan_repayments.loan_id","=","loans.id")
           
                ->where(["loans.status"=>"closed"])
                ->selectRaw(" format(case when coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) > 0 then coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0)  end ,2)  as expected ") 
           
                ->whereBetween("loan_schedules.due_date",[$start->toDateString(),$end->toDateString()])
                // ->groupBy("loan_schedules.id")
                ->get()[0]->expected;


              if($request->branch){
                $data = DB::table("loans")
                ->where($branch)
                ->leftJoin("borrowers","borrowers.id","=","loans.borrower_id")
                ->leftJoin("branches","branches.id","=","borrowers.branch_id")
                ->leftJoin("loan_schedules","loan_schedules.loan_id","=","loans.id")
                ->leftJoin("loan_repayments","loan_repayments.loan_id","=","loans.id")
                ->orderBy($orderby,$order)
                ->where(["loans.status"=>"closed"])
                ->selectRaw("borrowers.phone as phone , borrowers.loan_officers , format(case when coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) > 0 then coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0)  end ,2)  as amount , concat(borrowers.first_name,' ',borrowers.last_name) AS borrower , branches.name as branch") 
           
                ->whereBetween("loan_schedules.due_date",[$start->toDateString(),$end->toDateString()])
              
                ->groupBy("loans.id")
                
                ;
              }else{
                $data = DB::table("loans")
                ->where($branch)
                ->leftJoin("borrowers","borrowers.id","=","loans.borrower_id")
                ->leftJoin("branches","branches.id","=","borrowers.branch_id")
                ->leftJoin("loan_schedules","loan_schedules.loan_id","=","loans.id")
                ->leftJoin("loan_repayments","loan_repayments.loan_id","=","loans.id")
                ->orderBy($orderby,$order)
                ->where(["loans.status"=>"closed"])
                ->selectRaw("REPLACE(borrowers.phone,borrowers.phone,'-') as phone , borrowers.loan_officers , format(case when coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) > 0 then coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0)  end ,2)  as amount , REPLACE(borrowers.first_name,borrowers.first_name,'-') AS borrower , branches.name as branch")
                
                ->whereBetween("loan_schedules.due_date",[$start->toDateString(),$end->toDateString()])
              
                ->groupBy("branches.id") 
                
                ;
              }


            

    }else{
        if($request->branch){ 
        
        $data = DB::table("loans")
        ->where($branch)
        ->leftJoin("borrowers","borrowers.id","=","loans.borrower_id")
        ->leftJoin("branches","branches.id","=","borrowers.branch_id")
        ->leftJoin("loan_schedules","loan_schedules.loan_id","=","loans.id")
        ->leftJoin("loan_repayments","loan_repayments.loan_id","=","loans.id")
        ->orderBy($orderby,$order)
        ->where(["loans.status"=>"closed"])
        ->selectRaw("borrowers.phone as phone , borrowers.loan_officers , format(case when coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) > 0 then coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0)  end ,2)  as amount , concat(borrowers.first_name,' ',borrowers.last_name) AS borrower , branches.name as branch") 
        ->whereBetween("loan_schedules.due_date",[$start->toDateString(),$end->toDateString()])
      
        ->groupBy("loans.id")
        
        ;
        }else{
            $data = DB::table("loans")
            ->where($branch)
            ->leftJoin("borrowers","borrowers.id","=","loans.borrower_id")
            ->leftJoin("branches","branches.id","=","borrowers.branch_id")
            ->leftJoin("loan_schedules","loan_schedules.loan_id","=","loans.id")
            ->leftJoin("loan_repayments","loan_repayments.loan_id","=","loans.id")
            ->orderBy($orderby,$order)
            ->where(["loans.status"=>"closed"])
            ->selectRaw("REPLACE(borrowers.phone,borrowers.phone,'-') as phone , borrowers.loan_officers , format(case when coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) > 0 then coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0)  end ,2)  as amount , REPLACE(borrowers.first_name,borrowers.first_name,'-') AS borrower , branches.name as branch")
              ->whereBetween("loan_schedules.due_date",[$start->toDateString(),$end->toDateString()])
          
            ->groupBy("branches.id")
            
            ;    
        }
                foreach($data->get() as $d){
                    foreach(unserialize($d->loan_officers) as $dd){
                        if($dd == $request->loan_officer){
                                array_push($datas,$d);
                        }
                    }
                }
             }
              
                // ->toArray();


             
}elseif($request->query_type == "above"){
    if(!$request->loan_officer){
                $above = DB::table("loans")
                ->where(["loans.status"=>"closed"])
                ->leftJoin("loan_schedules","loan_schedules.loan_id","=","loans.id")
                ->leftJoin("loan_repayments","loan_repayments.loan_id","=","loans.id") 
                ->whereBetween("maturity_date",[$start->toDateString(),$end->toDateString()])
                ->where($branch)
 
                ->selectRaw(" format(case when coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) > 0 then coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0)  end ,2)  as above ") 
           
                ->get()[0]->above;

                if($request->branch){
                $data = DB::table("loans")
                ->where($branch)
                ->leftJoin("borrowers","borrowers.id","=","loans.borrower_id")
                ->leftJoin("branches","branches.id","=","borrowers.branch_id")
                ->leftJoin("loan_schedules","loan_schedules.loan_id","=","loans.id")
                ->leftJoin("loan_repayments","loan_repayments.loan_id","=","loans.id")
                ->orderBy($orderby,$order)
                ->where(["loans.status"=>"closed"])
                ->selectRaw("borrowers.phone as phone , borrowers.loan_officers , format(case when coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) > 0 then coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0)  end ,2)  as amount , concat(borrowers.first_name,' ',borrowers.last_name) AS borrower , branches.name as branch") 
       
                ->whereBetween("maturity_date",[$start->toDateString(),$end->toDateString()])
                ->groupBy("loans.id")
                
                ;  
                }else{
                    $data = DB::table("loans")
                    ->where($branch)
                    ->leftJoin("borrowers","borrowers.id","=","loans.borrower_id")
                    ->leftJoin("branches","branches.id","=","borrowers.branch_id")
                    ->leftJoin("loan_schedules","loan_schedules.loan_id","=","loans.id")
                    ->leftJoin("loan_repayments","loan_repayments.loan_id","=","loans.id")
                    ->orderBy($orderby,$order)
                    ->where(["loans.status"=>"closed"])
                    ->selectRaw("REPLACE(borrowers.phone,borrowers.phone,'-') as phone , borrowers.loan_officers , format(case when coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) > 0 then coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0)  end ,2)  as amount , REPLACE(borrowers.first_name,borrowers.first_name,'-') AS borrower , branches.name as branch") 
                    ->whereBetween("maturity_date",[$start->toDateString(),$end->toDateString()])
                    ->groupBy("branches.id")
                    
                    ;  
                }
            
            }else{
            if($request->branch){
                $data = DB::table("loans")
                ->where($branch)
                ->leftJoin("borrowers","borrowers.id","=","loans.borrower_id")
                ->leftJoin("branches","branches.id","=","borrowers.branch_id")
                ->leftJoin("loan_schedules","loan_schedules.loan_id","=","loans.id")
                ->leftJoin("loan_repayments","loan_repayments.loan_id","=","loans.id")
                ->orderBy($orderby,$order)
                ->where(["loans.status"=>"closed"])
                ->selectRaw("borrowers.phone as phone , borrowers.loan_officers , format(case when coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) > 0 then coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0)  end ,2)  as amount , concat(borrowers.first_name,' ',borrowers.last_name) AS borrower , branches.name as branch") 
       
                ->whereBetween("maturity_date",[$start->toDateString(),$end->toDateString()])
                ->groupBy("loans.id")
                
                ;  
            }else{
                $data = DB::table("loans")
                ->where($branch)
                ->leftJoin("borrowers","borrowers.id","=","loans.borrower_id")
                ->leftJoin("branches","branches.id","=","borrowers.branch_id")
                ->leftJoin("loan_schedules","loan_schedules.loan_id","=","loans.id")
                ->leftJoin("loan_repayments","loan_repayments.loan_id","=","loans.id")
                ->orderBy($orderby,$order)
                ->where(["loans.status"=>"closed"])
                ->selectRaw("REPLACE(borrowers.phone,borrowers.phone,'-') as phone , borrowers.loan_officers , format(case when coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) > 0 then coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0)  end ,2)  as amount , REPLACE(borrowers.first_name,borrowers.first_name,'-') AS borrower , branches.name as branch") 
                ->whereBetween("maturity_date",[$start->toDateString(),$end->toDateString()])
                ->groupBy("branches.id")
                
                ;  
            }
                    foreach($data->get() as $d){
                        foreach(unserialize($d->loan_officers) as $dd){
                            if($dd == $request->loan_officer){
                                    array_push($datas,$d);
                            }
                        }
                    }
                 }


}elseif($request->query_type == "notabove"){
 
    if(!$request->loan_officer){

                $notabove = DB::table("loans")
                ->where(["status"=>"disbursed"])
                ->join("loan_schedules","loan_schedules.loan_id","=","loans.id")
                ->join("loan_repayments","loan_repayments.loan_id","=","loans.id") 
                ->whereNotBetween("loan_schedules.due_date",[$start->toDateString(),$end->toDateString()])
                ->where($branch)
                
                ->selectRaw(" format(case when coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) > 0 then coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) else 0 end ,2)  as notabove ") 
           
                ->get()[0]->notabove;


                
                if($request->branch){
                    $data = DB::table("loans")
                    ->where(["borrowers.branch_id"=>$request->branch])
                    ->join("borrowers","borrowers.id","=","loans.borrower_id")
                    ->leftjoin("branches","branches.id","=","borrowers.branch_id")
                    ->join("loan_schedules","loan_schedules.loan_id","=","loans.id")
                    ->join("loan_repayments","loan_repayments.loan_id","=","loans.id")
                    ->orderBy($orderby,$order)
                    ->where(["loans.status"=>"disbursed"])
                    ->selectRaw("borrowers.phone as phone , borrowers.loan_officers , format(case when coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) > 0 then coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) else 0  end ,2)  as amount , concat(borrowers.first_name,' ',borrowers.last_name) AS borrower , branches.name as branch") 
                   
                    ->whereNotBetween("loan_schedules.due_date",[$start->toDateString(),$end->toDateString()])
                    ->groupBy("borrowers.id")
                    
                    ;
                   
                }    else{
                    $data = DB::table("loans")
                    ->where($branch)
                    ->leftJoin("borrowers","borrowers.id","=","loans.borrower_id")
                    ->leftJoin("branches","branches.id","=","borrowers.branch_id")
                    ->leftJoin("loan_schedules","loan_schedules.loan_id","=","loans.id")
                    ->leftJoin("loan_repayments","loan_repayments.loan_id","=","loans.id")
                    ->orderBy($orderby,$order)
                    ->where(["loans.status"=>"disbursed"])
                    ->selectRaw("REPLACE(borrowers.phone,borrowers.phone,'-') as phone , borrowers.loan_officers , format(case when coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) > 0 then coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) else 0  end ,2) else 0 as amount , REPLACE(borrowers.first_name,borrowers.first_name,'-') AS borrower , branches.name as branch") 
                    ->whereNotBetween("loan_schedules.due_date",[$start->toDateString(),$end->toDateString()])
                    ->groupBy("branches.id")
                    
                    ;
                }           

            }else{

                if($request->branch){
                $data = DB::table("loans")
                ->where($branch)
                ->leftJoin("borrowers","borrowers.id","=","loans.borrower_id")
                ->leftJoin("branches","branches.id","=","borrowers.branch_id")
                ->leftJoin("loan_schedules","loan_schedules.loan_id","=","loans.id")
                ->leftJoin("loan_repayments","loan_repayments.loan_id","=","loans.id")
                ->orderBy($orderby,$order)
                ->where(["loans.status"=>"disbursed"])
                ->selectRaw("borrowers.phone as phone , borrowers.loan_officers , format(case when coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) > 0 then coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0)  else 0 end ,2)  as amount , concat(borrowers.first_name,' ',borrowers.last_name) AS borrower , branches.name as branch") 
       
                ->whereNotBetween("loan_schedules.due_date",[$start->toDateString(),$end->toDateString()])
                ->groupBy("loans.id")
                
                ;

             
            }else{
                $data = DB::table("loans")
                ->where($branch)
                ->leftJoin("borrowers","borrowers.id","=","loans.borrower_id")
                ->leftJoin("branches","branches.id","=","borrowers.branch_id")
                ->leftJoin("loan_schedules","loan_schedules.loan_id","=","loans.id")
                ->leftJoin("loan_repayments","loan_repayments.loan_id","=","loans.id")
                ->orderBy($orderby,$order)
                ->where(["loans.status"=>"disbursed"])
                ->selectRaw("REPLACE(borrowers.phone,borrowers.phone,'-') as phone , borrowers.loan_officers , format(case when coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) > 0 then coalesce(SUM(loan_schedules.principal + loan_schedules.interest),0) - coalesce(SUM(loan_repayments.amount),0) else 0  end ,2)  as amount , REPLACE(borrowers.first_name,borrowers.first_name,'-') AS borrower , branches.name as branch") 
                ->whereNotBetween("loan_schedules.due_date",[$start->toDateString(),$end->toDateString()])
                ->groupBy("branches.id")
                
                ;
             
            }
                foreach($data->get() as $d){
                    foreach(unserialize($d->loan_officers) as $dd){
                        if($dd == $request->loan_officer){
                                array_push($datas,$d);
                        }
                    }
                }
             }
}
   
                // $expected = DB::table("loan_schedules")
                // ->where($branch)
               
                
                // ->select(DB::raw("SUM(loan_schedules.principal + loan_schedules.interest) as expected"))
                // ->join
                // ->whereBetween("loan_schedules.due_date",[$start->toDateString(),$end->toDateString()])
                // ->get()[0]->expected;

                // $total_repayment = DB::table("loan_repayments")->where($branch)->sum("")
                 

 
                // return;


             
               

                if($request->loan_officer){
                    $json_data = array(
                        "draw"            => intval($request->input('draw')),  
                        "recordsTotal"    => intval(count($datas)),  
                        "recordsFiltered" =>intval(count($datas)), 
                        "data"            =>  array_slice($datas,$startx,$limit),
                        "above"=>number_format(array_sum(array_column($datas,'amount')),2),
                        "notabove"=>number_format(array_sum(array_column($datas,'amount')),2),
                        "expected"=> array_sum(array_column($datas,'amount'),2)
                    );
                }else{
                $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval(count($data->get()) > 0 ? count($data->get()) : []),  
                    "recordsFiltered" =>intval(count($data->get()) > 0 ?  count($data->get()) : []), 
                    "data"            => $data->offset($startx)->limit($limit)->count() > 0 ? $data->offset($startx)->limit($limit)->get() : [],
                    "above"=>$above,
                    "notabove"=>$notabove,
                    "expected"=>$expected
                );
            }

               

                return $json_data;
             
            }



            public function schedule_report2(Request $request){

                $columns = array( 
                    0 =>'branch', 
                    1 =>'borrower',
                    2=> 'phone',
                    3=> 'amount', 
        
        
        
                );

                $draw = array( 
                    0 =>'branches.id', 
                    1 =>'borrowers.id',
                    2=> 'borrowers.phone',
                    3=> 'loans.id', 
        
        
        
                );

                $limit = $request->input('length');
                $startx = $request->input('start');
                $order = $columns[$request->input('order.0.column')];
                $orderby = $draw[$request->input('order.0.column')];
                $dir = $request->input('order.0.dir');
                

               
                $branch = $request->branch;
                $start = \Carbon\Carbon::parse($request->start_date);
                $end = \Carbon\Carbon::parse($request->end_date);
                $date = \Carbon\Carbon::parse($request->date);
                $loan_officer  = $request->loan_officer;

                if($branch){
                    $branch = ["loans.branch_id"=>$branch];
                    $by = "loans.id";
                }else{
                    $branch = [];
                    $by = "branches.id";
                }
            $query = DB::table("loan_schedules")
            ->where($branch)
            ->leftJoin("loans","loans.id","=","loan_schedules.loan_id")
            ->leftJoin("borrowers","borrowers.id","=","loans.borrower_id")
            ->leftJoin("loan_repayments","loan_repayments.loan_id","=","loans.id")
            ->leftJoin("branches","branches.id","=","loans.branch_id")
            ->groupby($by)
            ->where(["loan_repayments.deleted_at"=>null])
            ->where(["loans.deleted_at"=>null])
            ->orderBy($orderby,$order)
        
            ;

            $query2 = DB::table("loan_schedules")
            ->where($branch)
            ->leftJoin("loans","loans.id","=","loan_schedules.loan_id")
            ->leftJoin("borrowers","borrowers.id","=","loans.borrower_id")
            ->leftJoin("loan_repayments","loan_repayments.loan_id","=","loans.id")
            ->leftJoin("branches","branches.id","=","loans.branch_id")
            ->where(["loan_repayments.deleted_at"=>null])
            ->where(["loans.deleted_at"=>null])
            ;
            $datas = [];
            if($request->query_type == "notabove"){
         
                $stage = $query2->where(["loans.status"=>"disbursed"])
                ->whereBetween("loan_schedules.due_date",[$start->toDateString(),$end->toDateString()])
                
                ->selectRaw("ABS(SUM(loan_schedules.principal + loan_schedules.interest)  - SUM(loan_repayments.amount)) as amount")
                ->get();

 

                $data = $query->where(["loans.status"=>"disbursed"])
                ->whereBetween("loan_schedules.due_date",[$start->toDateString(),$end->toDateString()])
              
                ->selectRaw("
                format(ABS(SUM(loan_schedules.principal + loan_schedules.interest)  - SUM(loan_repayments.amount)),2) as amount,
                branches.name as branch,borrowers.phone as phone
                ,concat(borrowers.first_name,' ',borrowers.last_name) AS borrower
                 ")
                // ->select("branches.name","loans.id")
                // ->get()

                ->chunk(1000, function ($d) use (&$datas) {
                    foreach($d as $dd){
                        if($dd->amount > 0){
                            array_push($datas, $dd);
                        }
                    }
                
                  });

                       
             

                 
            }   

            $json_data = array(
                "draw"            => intval($request->input('draw')),  
                "recordsTotal"    => count(array_slice($datas, $startx, $limit)),  
                "recordsFiltered" =>  count($datas), 
                "data"            => array_slice($datas, $startx, $limit),
                "notabove"=>number_format($stage[0]->amount,2),
                // "notabove"=>$notabove,
                // "expected"=>$expected
            );

            return $json_data;

            }

            public function weekly_due(){
                return view("report.weekly_due");
            }

            public function weekly_data(Request $request){
                $datas = [];
        
                $columns = array( 
                    0 =>'borrowers', 
                    1 =>'id',
                    2=> 'loan_officer',
                    3=> 'branch',
                    4=> 'principal',
                    5=> 'released',
                    6=> 'interest',
                    7=> 'due',
                    8=> 'paid',
                    9=> 'balance',
                    10=> 'status',
                    11=> 'action',
        
                );
        
        
                $limit = $request->input('length');
                $start = $request->input('start');
                $order = $columns[$request->input('order.0.column')];
                $dir = $request->input('order.0.dir');
                $texts = $request->input('search.value');
                $text = $request->loan_officer;
                $text2 = $request->from;
                $text3 = $request->to;
                $text4 = $request->ma;
                $text5 = $request->payment_due;
        
                if(isset($_GET['status'])){
                    $status = ["status"=>$request->status];
                }else{
                    $status = [];
                }
        
                if(empty($texts) || isset($_GET['date']))
                {      
                    $status = [];
                if($request->status != null){
                    $status = ["status"=>$request->status];
                }
                  
            
$startDate = Carbon::now()->startOfWeek();  // Get the start of the current week
$endDate = Carbon::now()->endOfWeek();  // Get the end of the current week

$data = Loan::where('branch_id', session('branch_id'))
    ->whereHas('payments', function ($query) use ($startDate, $endDate) {
        $query->where('collection_date', '>=', $startDate)
            ->where('collection_date', '<=', $endDate);
    })
    ->where('status', 'disbursed')
    ->offset($start)
    ->limit($limit)
    ->orderBy('loans.id', $dir)
    ->get();

$totalFiltered = Loan::where('branch_id', session('branch_id'))
    ->whereHas('payments', function ($query) use ($startDate, $endDate) {
        $query->where('collection_date', '>=', $startDate)
            ->where('collection_date', '<=', $endDate);
    })
    ->where('status', 'disbursed')
    ->count();
            }else{
                    
        
                    $data = Loan::where('loans.branch_id', session('branch_id'))
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy("loans.id",$dir)
                        ->where($status)
                        ->whereHas('borrower', function ($query) use ($texts) {
                          $query ->where('borrowers.first_name', 'like', '%' . $texts . '%')
                              ->orWhere('borrowers.last_name', 'like', '%' . $texts . '%')
                              ->orWhere('borrowers.unique_number', 'like', '%' . $texts . '%')
                              ->orWhere('borrowers.username', 'like', '%' . $texts . '%')
                              ->orWhere('borrowers.title', 'like', '%' . $texts . '%')
                              ->orWhere('borrowers.phone', 'like', '%' . $texts . '%')
                              ->orWhere('borrowers.address', 'like', '%' . $texts . '%')
                              ->orWhere('borrowers.dob', 'like', '%' . $texts . '%')
                              ->orWhere('borrowers.gender', 'like', '%' . $texts . '%')
                              ->orWhere('borrowers.business_name', 'like', '%' . $texts . '%')
                              ->orWhere('borrowers.created_at', 'like', '%' . $texts . '%')
                              ->orWhere('borrowers.id', 'like', '%' . $texts . '%')
                              ->orWhereRaw("concat(first_name, ' ', last_name) like '%$texts%' ");
                        })
        
        
        ->select("loans.*")
                        ->get();
                    $totalFiltered = Loan::where('loans.branch_id', session('branch_id'))->join("borrowers","borrowers.id","=","loans.borrower_id")
                        ->where($status)
                        
                        ->whereHas('borrower', function ($query) use ($texts) {
                            $query ->where('borrowers.first_name', 'like', '%' . $texts . '%')
                                ->orWhere('borrowers.last_name', 'like', '%' . $texts . '%')
                                ->orWhere('borrowers.unique_number', 'like', '%' . $texts . '%')
                                ->orWhere('borrowers.username', 'like', '%' . $texts . '%')
                                ->orWhere('borrowers.title', 'like', '%' . $texts . '%')
                                ->orWhere('borrowers.phone', 'like', '%' . $texts . '%')
                                ->orWhere('borrowers.address', 'like', '%' . $texts . '%')
                                ->orWhere('borrowers.dob', 'like', '%' . $texts . '%')
                                ->orWhere('borrowers.gender', 'like', '%' . $texts . '%')
                                ->orWhere('borrowers.business_name', 'like', '%' . $texts . '%')
                                ->orWhere('borrowers.created_at', 'like', '%' . $texts . '%')
                                ->orWhere('borrowers.id', 'like', '%' . $texts . '%')
                                ->orWhereRaw("concat(first_name, ' ', last_name) like '%$texts%' ");
                        })
                        ->select("loans.*")->count();
        
                }
        
        
        
                if(!empty($text4)  || !empty($text5)){
                    $data = Loan::join("loan_schedules","loan_schedules.loan_id","=","loans.id")
                    ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
                    ->where(['loans.branch_id'=> session('branch_id')])
                    ->whereHas('borrower', function ($query) use ($text,$text3,$text2) {
                        $query->orWhereBetween('loan_schedules.due_date', [$text2, $text3])
                        ->orWhereBetween('loans.maturity_date', [$text2, $text3])
                        ->orWhere(["loan_officers.officer_id"=>$text]);
        
                    })
                    ->where("status","!=","closed")->where("status","!=","declined")
           
                    ->get();
        
                $totalFiltered  = Loan::join("loan_schedules","loan_schedules.loan_id","=","loans.id")
                ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
                ->where(['loans.branch_id'=> session('branch_id')])
                ->whereHas('borrower', function ($query) use ($text,$text3,$text2) {
                    $query->orWhereBetween('loan_schedules.due_date', [$text2, $text3])
                    ->orWhereBetween('loans.maturity_date', [$text2, $text3])
                    ->orWhere(["loan_officers.officer_id"=>$text]);
        
                })
          
                ->where("status","!=","closed")->where("status","!=","declined")->count();
        
         
                }
        
                // if(!empty($text)){
                //     $data = Loan::where(['loans.branch_id'=> session('branch_id'),"loan_officers.officer_id"=>$request->loan_officer])
                //         ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
                //         ->get();
        
                //     $totalFiltered = Loan::where(['loans.branch_id'=> session('branch_id'),"loan_officers.officer_id"=>$request->loan_officer])
                //         ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")->count();
        
                // }
        
        
                // if(!empty($text2)){
                //     if($text4 != 0){
                //         $data = Loan::where(['loans.branch_id'=> session('branch_id')])->whereBetween('loans.maturity_date', [$text2, $text3])
                //         ->where("status","!=","closed")->where("status","!=","declined")
                //         ->get();
        
                //     $totalFiltered  = Loan::where(['loans.branch_id'=> session('branch_id')])->whereBetween('loans.maturity_date', [$text2, $text3])->where("status","!=","closed")->where("status","!=","declined")->count();
                //     }else{
                //     $data = Loan::where(['loans.branch_id'=> session('branch_id')])->whereBetween('loans.release_date', [$text2, $text3])
        
                //         ->get();
        
                //     $totalFiltered  = Loan::where(['loans.branch_id'=> session('branch_id')])->whereBetween('loans.release_date', [$text2, $text3])->count();
        
                //     }
                // }
        
               
                //     if($text4 != 0){
                //         $data = Loan::where(['loans.branch_id'=> session('branch_id')])->whereBetween('loans.maturity_date', [$text2, $text3])
                //         ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
                //         ->where(["loan_officers.officer_id"=>$text])
                //         ->where("status","!=","closed")->where("status","!=","declined")
                //         ->get();
        
                //     $totalFiltered  = Loan::where(['loans.branch_id'=> session('branch_id')])->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
                //     ->where(["loan_officers.officer_id"=>$text])->whereBetween('loans.maturity_date', [$text2, $text3])->where("status","!=","closed")->where("status","!=","declined")->count();
                   
                // }elseif($text5 != 0){
                //         $data = Loan::join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where(['loans.branch_id'=> session('branch_id')])->whereBetween('loan_schedules.due_date', [$text2, $text3])
                //         ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
                //         ->orWhere(["loan_officers.officer_id"=>$text])
                //         ->where("status","!=","closed")->where("status","!=","declined")
                //         ->get();
        
                //     $totalFiltered  = Loan::where(['loans.branch_id'=> session('branch_id')])->whereBetween('loan_schedules.due_date', [$text2, $text3])
                //     ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
                //     ->orWhere(["loan_officers.officer_id"=>$text])
                //     ->where("status","!=","closed")->where("status","!=","declined")->count();
        
                   
                //     }elseif($text5 != 0 &&  $text4 != 0){
                //         $data = Loan::join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where(['loans.branch_id'=> session('branch_id')])->whereBetween('loan_schedules.due_date', [$text2, $text3])
                //         ->whereBetween('loans.maturity_date', [$text2, $text3])
                //         ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
                //         ->where(["loan_officers.officer_id"=>$text])
                //         ->where("status","!=","closed")->where("status","!=","declined")
                //         ->get();
        
                //     $totalFiltered  = Loan::join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where(['loans.branch_id'=> session('branch_id')])->whereBetween('loan_schedules.due_date', [$text2, $text3])
                //     ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
                //     ->where(["loan_officers.officer_id"=>$text])
                //     ->where("status","!=","closed")->where("status","!=","declined")->count();
                    
                //     }else{
                //     $data = Loan::where(['loans.branch_id'=> session('branch_id')])->whereBetween('loans.release_date', [$text2, $text3])
                //     ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
                //     ->where(["loan_officers.officer_id"=>$text])
                //         ->get();
        
                //     $totalFiltered  = Loan::where(['loans.branch_id'=> session('branch_id')])
                //     ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
                //     ->where(["loan_officers.officer_id"=>$text])->whereBetween('loans.release_date', [$text2, $text3])->count();
        
                //     }
         
               
        
                $totalData = count($data);
                $totalFilter = $totalFiltered;
                $totalFiltered = $totalData; 
        
                foreach($data as $key){
        
                    if(!empty($key->borrower)){
                        if($key->group_id != null){
                            $name =' <a href="'.url('borrower/'.$key->borrower_id.'/show').'">'.$key->borrower->first_name.' '.$key->borrower->last_name.'</a>'.'<a href="/borrower/group/'.$key->group_id.'/vloans" target="_blank"> <span class="badge badge-danger bg-red">group loan</span> </a>';
                        }else{
                            $name =' <a href="'.url('borrower/'.$key->borrower_id.'/show').'">'.$key->borrower->first_name.' '.$key->borrower->last_name.'</a>';
                        }
        
                  }else{
                       $name =' <span class="label label-danger">'.trans_choice('general.broken',1).' <i
                        class="fa fa-exclamation-triangle"></i> </span>';
              }
                    $uu = [];
                    $b = DB::table("branches")->where(["id"=>$key->branch_id])->first();
                    $u = DB::table("users")->where(["id"=>$key->user_id])->first();
                    $lname = "";
                    $bname = "";
                    $lid = "";
                    $datax = [];
                    if($b){
                        $bname = $b->name;
                    }
                    if(unserialize($key->borrower->loan_officers)){
        
                        $datax = unserialize($key->borrower->loan_officers);
        
                    }
        
        
        
                    foreach($datax as $d){
        
                         $u = DB::table("users")->where(["id"=>$d])->first();
        
        
        $uu2 = '<a href="/user/'.$d.'/show">'.$u->first_name.' '.$u->last_name.'</a><br>
        <br>';
                        array_push($uu,$uu2);
                        }
                        $b = DB::table("branches")->where(["id"=>$key->branch_id])->first();
        
                    if(\App\Models\Setting::where('setting_key', 'currency_position')->first()->setting_value=='left'){
                        $prin = \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value."".number_format($key->principal,2);
                    }else{
                        $prin = number_format($key->principal,2)."".\App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value;
                    }
                    $rd = $key->release_date;
                    $in =   number_format($key->interest_rate,5).'%/'.$key->interest_period;
        
                    if($key->override==1){
                        $due = '<s>'.number_format(\App\Helpers\GeneralHelper::loan_total_due_amount($key->id),2).'</s><br>
                '.number_format($key->balance,2).'';
                    }else{
                        $due = number_format(\App\Helpers\GeneralHelper::loan_total_due_amount($key->id),2);
                    }
                    $paid = number_format(\App\Helpers\GeneralHelper::loan_total_paid($key->id),2);
                    $bal =  number_format(\App\Helpers\GeneralHelper::loan_total_balance($key->id),2);
        
        
                    if($key->maturity_date<date("Y-m-d") && \App\Helpers\GeneralHelper::loan_total_balance($key->id)>0){
                        $status ='   <span class="label label-danger">'.trans_choice('general.past_maturity',1).'</span>';
                    }else{
                        if($key->status=='pending'){
                            $status =  '<span class="label label-warning">'.trans_choice('general.pending',1).' '.trans_choice('general.approval',1).'</span>';
                        }
                        if($key->status=='approved'){
                            $status =  '<span class="label label-info">'.trans_choice('general.awaiting',1).' '.trans_choice('general.disbursement',1).'</span>';
        
                        }
                        if($key->status=='disbursed'){
        
                            $status =  '<span class="label label-info">'.trans_choice('general.active',1).'</span>';
                        }
                        if($key->status=='declined'){
                            $status =  '<span class="label label-danger">'.trans_choice('general.declined',1).'</span>';
        
                        }
                        if($key->status=='withdrawn'){
                            $status =  '<span class="label label-danger">'.trans_choice('general.withdrawn',1).'</span>';
        
                        }
                        if($key->status=='written_off'){
                            $status =  '<span class="label label-danger">'.trans_choice('general.written_off',1).'</span>';
        
                        }
                        if($key->status=='closed'){
                            $status =  '<span class="label label-success">'.trans_choice('general.closed',1).'</span>';
        
                        }
                        if($key->status=='pending_reschedule'){
                            $status =  '<span class="label label-warning">'.trans_choice('general.pending',1).' '.trans_choice('general.reschedule',1).'</span>';
        
                        }
                        if($key->status=='rescheduled'){
                            $status =  '<span class="label label-info">'.trans_choice('general.rescheduled',1).'</span>';
        
                        }
                    }
                    $ac1 = "";
                    $ac2 = "";
                    $ac3 = "";
                    if(Sentinel::hasAccess('loans.view')){
                        $ac1 ='   <li><a href="'.url('loan/'.$key->id.'/show') .'"><i
                class="fa fa-search"></i> '. trans_choice('general.detail',2).'
            </a>
                </li>';
                    }
                    if(Sentinel::hasAccess('loans.create')){
                        $ac2 ='     <li><a href="'.url('loan/'.$key->id.'/edit').'"><i
                class="fa fa-edit"></i> '. trans('general.edit') .'</a></li>';
                    }
                    if(Sentinel::hasAccess('loans.delete')){
                        $ac3 = ' <li><a href="'.url('loan/'.$key->id.'/delete').'"
                class="delete"><i
                class="fa fa-trash"></i> '. trans('general.delete').' </a></li>';
                    }
                    if($key->group_id != null){
        $action = "--";
                        }else{
                        $action = '   <div class="btn-group">
                    <a href="'.url('loan/'.$key->id.'/show').'" class="btn btn-info btn-xs"
                   aria-expanded="false">
                 view  
                    
                    </a>
                   
                    </div>';
                    }
                    if(isset($_GET['status'])){
                    $select = "<input type='checkbox' value=".$key->id.">";
                        }else{
                        $select = "-";
                    }
                    $nestedData['select'] = $select;
                    $nestedData['borrowers'] = $name;
                    $nestedData['id'] = $key->id;
                    $nestedData['loan_officer'] = $uu;
                    $nestedData['branch'] = $b->name;
                    $nestedData['principal'] = $prin;
                    $nestedData['released'] = $rd;
                    $nestedData['interest'] = $in;
                    $nestedData['due'] = $due;
                    $nestedData['paid'] = $paid;
                    $nestedData['balance'] = $bal;
                    $nestedData['status'] = $status;
                    $nestedData['action'] = $action;
                    $datas[] = $nestedData;
        
        
                }
        
        
                $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => $totalFilter, 
                    "data"            => $datas  
                );
        
                return response()->json($json_data);
        
            }
            }