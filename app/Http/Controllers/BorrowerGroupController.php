<?php

namespace App\Http\Controllers;

use Aloha\Twilio\Twilio;
use App\Models\Borrower;
use App\Models\BorrowerGroup;
use App\Models\BorrowerGroupMember;
use App\Models\CustomField;
use App\Models\CustomFieldMeta;
use App\Models\Setting;
use Illuminate\Support\Facades\View;
use App\Models\User;
use App\Models\Loan;
use App\Models\Guarantor;
use App\Models\LoanProduct;
use PDF;
 use App\Models\LoanRepaymentMethod;
 use App\Models\LoanSchedule;
use App\Helpers\GeneralHelper;
use App\Models\LoanDisbursedBy;
use App\Models\LoanRepayment;
use App\Models\LoanFee;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;
use DB;
class BorrowerGroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentinel');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function searchgrp(Request $request){
        $grp = DB::table("loan_officers2")->where(["loan_officers2.officer_id"=>$request->id])->join("borrower_groups","borrower_groups.id","=","loan_officers2.branch_id")->select("borrower_groups.*")->get();
   return response()->json(["data"=>$grp]);
    }

    public function deleteloan(Request $request){
        if (!Sentinel::hasAccess('loans.delete')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $id = $request->id;
        

        $check = DB::table("grouploan")->where(["sc"=>$id])->first();
        if(!$check){
            Flash::warning("Invalid loan ID");
            return redirect()->back();
        }
        DB::table("grouploan")->where(["sc"=>$id])->delete();
        $loan = DB::table("loans")->where(["sc"=>$id])->get();
        foreach($loan as $loans){
            $id = $loans->id;
            Loan::destroy($id);
            LoanSchedule::where('loan_id', $id)->delete();
            LoanRepayment::where('loan_id', $id)->delete();
            Guarantor::where('loan_id', $id)->delete();
        }
        DB::table("loans")->where(["sc"=>$id])->delete();


        GeneralHelper::audit_trail("Deleted loan with id:" . $id);
        Flash::success(trans('general.successfully_deleted'));
        return redirect()->back();
    }
    
    public function vloans(Request $request){
        $id = $request->id;
        $check = DB::table("borrower_groups")->where(["id"=>$id])->first();
        if(!$check){
            Flash::warning("Invalid Group ID");
            return redirect()->back();
        }
        $loan  = DB::table('grouploan')->where(["group_id"=>$id])->get();




        $group =  $check;
        return view("borrower.group.vloans",compact("group","loan"));
    }
    public function index()
    {
        $date = "";
        $name = "";
        $data = BorrowerGroup::where(["branch_id"=>session("branch_id")])->orderBy("id","DESC")->get();
       $users = DB::table("users")->where(["branch_users.branch_id"=>session("branch_id")])
            ->join("branch_users","branch_users.user_id","=","users.id")
            ->join("role_users","role_users.user_id","=","users.id")->where(["role_users.role_id"=>4])->select("users.first_name","users.last_name","users.id")->get();


        if(!empty($_GET['date'])){
            $data = BorrowerGroup::where(["borrower_groups.branch_id"=>session("branch_id")])
                ->join("loan_officers2","loan_officers2.branch_id","=","borrower_groups.branch_id")
                ->join("users","users.id","=","loan_officers2.officer_id")
                ->where('borrower_groups.created_at', 'like', '%' . $_GET['date'] . '%')


                ->orderBy("borrower_groups.id","DESC")
                ->select("borrower_groups.*")
                ->get();
        }
        
        if(!empty($_GET['name'])){
            $data = BorrowerGroup::where(["borrower_groups.branch_id"=>session("branch_id")])
                ->join("loan_officers2","loan_officers2.branch_id","=","borrower_groups.branch_id")
                ->join("users","users.id","=","loan_officers2.officer_id")
                ->where('borrower_groups.name', 'like', '%' . $_GET['name'] . '%')


                ->orderBy("borrower_groups.id","DESC")
                ->select("borrower_groups.*")
                ->get();
        }
        
        if(!empty($_GET['loan_officer'])){
            $data =DB::table("loan_officers2")->join("borrower_groups","borrower_groups.id","=","loan_officers2.branch_id")
                ->where(["borrower_groups.branch_id"=>session("branch_id"),"loan_officers2.officer_id"=>$_GET['loan_officer']])->select("borrower_groups.*")
                ->get();
        }

        if(!empty($_GET['name']) && !empty($_GET['date']) && !empty($_GET['loan_officer'])){

            $data = BorrowerGroup::where(["borrower_groups.branch_id"=>session("branch_id")])
                ->join("loan_officers2","loan_officers2.branch_id","=","borrower_groups.branch_id")
                ->join("users","users.id","=","loan_officers2.officer_id")
                ->where('borrower_groups.name', 'like', '%' . $_GET['name'] . '%')
                ->orWhere('borrower_groups.created_at', 'like', '%' . $_GET['date'] . '%')
                ->orWhere('users.id', 'like', '%' . $_GET['loan_officer'] . '%')
               
                ->orderBy("borrower_groups.id","DESC")
                ->select("borrower_groups.*")
                ->get();

        }

        return view('borrower.group.data', compact('data','users'));
    }


public function createloan(Request $request){
        if (!Sentinel::hasAccess('loans.create')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
           $id = $request->id;
    $b = DB::table("borrower_groups")->where(["id"=>$id])->first();
    if($b == null){
           Flash::warning("Invalid Group Loan #ID");
            return redirect()->back();
    }
    $users = DB::table("borrower_group_members")->where(["borrower_group_id"=>$id])->get();
        $borrowers = array();
        foreach($users as $user){
            $userid = $user->borrower_id;
        foreach (Borrower::where(["id"=>$userid])->get() as $key) {
            $borrowers[$key->id] = $key->first_name . ' ' . $key->last_name . '(' . $key->unique_number . ')';
        }
        }
        
      
        $loan_products = array();
        foreach (LoanProduct::all() as $key) {
            $loan_products[$key->id] = $key->name;
        }

        $loan_disbursed_by = array();
        foreach (LoanDisbursedBy::all() as $key) {
            $loan_disbursed_by[$key->id] = $key->name;
        }
        if (isset($request->product_id)) {
            $loan_product = LoanProduct::find($request->product_id);
        } else {
            $loan_product = LoanProduct::first();
        }
       
        if (empty($loan_product)) {
            Flash::warning("No loan product set. You must first set a loan product");
            return redirect()->back();
        }
        
        $loan_fees = LoanFee::all();
        return view('borrower.grouploan',
            compact('borrowers', 'loan_disbursed_by', 'loan_products', 'loan_product', 'borrower_id', 'custom_fields',
                'loan_fees'),["b"=>$b]);
    
 
   
}

public function loanlist(Request $request){
    $id = $request->id;
    $check = DB::table("borrower_groups")->where(["id"=>$id])->first();
    if($check == null){
        Flash::warning("Not Found!!");
        return redirect("/borrower/group/data");
    }
    return view("borrower.grouploans");
}
public function owe(Request $request){
    // var_dump($request);
  $id = $request->id;
  $req = (int)$request->reqid;
    $u = DB::table("loans")->where(["borrower_id"=>$id])->first();
    $g= DB::table("grouploan")->where(["group_id"=>$req,"status"=>"approved"])->first();
    
 
    $amount = $g->loan_amount / DB::table("loans")->where(["sc"=>$g->sc])->count();
    
  if($g->loan_amount < $amount && $g->loan_amount == $amount){
      return response()->json(["message"=>"Loan Amount Is Less Or Equal To Loan Balance, Can't Process!","xstatus"=>4]);
  }
elseif($g->loan_amount == $amount){
      return response()->json(["message"=>"Loan Amount Can't Be Equal To Loan Balance","xstatus"=>4]);
}  
  else{
    
  DB::table("loans")->where(["borrower_id"=>$id,"status"=>"approved"])->update(["owing"=>1,"amount"=>$amount]);
  $user = DB::table("loans")->where(["borrower_id"=>$id,"status"=>"approved"])->first();
 
   
   
$sum =  DB::table("grouploan")->where(["sc"=>$u->sc,"status"=>"approved"])->sum("mainamount");
DB::table("grouploan")->where(["sc"=>$u->sc,"status"=>"approved"])->update(["mainamount"=>$amount + $sum]);
   return response()->json(["message"=>"Updated SuccessFully!!"]);
  }
}
public function storeloan(Request $request){




    $groupid = $request->groupid;
     $check = DB::table("grouploan")->where(["group_id"=>$groupid,"status"=>"pending"])->exists();

         if($check == true){
             
        Flash::warning('There Is An Onging Loan Running On This Group!!');
        return redirect()->back();
         }
         

         
        //  var_dump($check);
        //  return;
     $sc = rand();
       $ids = DB::table("borrower_group_members")->where(["borrower_group_id"=>$groupid])->get();


    $bg = DB::table("borrower_group_members")->where(["borrower_group_id"=>$groupid])->count();
   $amount = 0;
    // return;
    foreach($ids as $id){
        $userid = $id->borrower_id;
        $users = DB::table("borrowers")->where(["id"=>$userid])->first();
        if($users != null){
        $userloan = DB::table("loans")->where(["borrower_id"=>$userid,"status"=>"disbursed"])->exists();
         if($users != null && !$userloan){
            
             if (!Sentinel::hasAccess('loans.create')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        



        $amm = "principal$userid";
             $amount += $request->$amm;
             if($request->$amm == 0){
                 Flash::warning("Amount Must Be Greater Than 0");
                 return redirect()->back()->withInput();
             }
        $durationtype = "loan_duration_type$userid";
        $duration = "loan_duration$userid";
        $loan = new Loan();
        $loan->sc = $sc;
        $loan->principal = $request->$amm;
        $loan->interest_method = $request->interest_method;
        $loan->interest_rate = $request->interest_rate;
        $loan->branch_id = session('branch_id');
        $loan->interest_period = $request->interest_period;
        $loan->loan_duration = $request->$duration;
        $loan->loan_duration_type = $request->$durationtype;
        $loan->repayment_cycle = $request->repayment_cycle;
        $loan->decimal_places = $request->decimal_places;
        $loan->override_interest = $request->override_interest;
        $loan->override_interest_amount = $request->override_interest_amount;
        $loan->grace_on_interest_charged = $request->grace_on_interest_charged;
        $loan->borrower_id = $userid;
        $loan->applied_amount = $request->$amm;
        $loan->user_id = Sentinel::getUser()->id;
        $loan->loan_product_id = $request->loan_product_id;
        $loan->release_date = $request->release_date;
        // $date = explode('-', $request->release_date);
        // var_dump($date[1]);
  $date = explode('-', $request->release_date);
        // return;
        $loan->month = $date[1];
        $loan->year = $date[0];
        if (!empty($request->first_payment_date)) {
            $loan->first_payment_date = $request->first_payment_date;
        }
        $loan->description = $request->description;
        $files = array();
        if (!empty(array_filter($request->file('files')))) {
            $count = 0;
            foreach ($request->file('files') as $key) {
                $file = array('files' => $key);
                $rules = array('files' => 'required|mimes:jpeg,jpg,bmp,png,pdf,docx,xlsx');
                $validator = Validator::make($file, $rules);
                if ($validator->fails()) {
                    Flash::warning(trans('general.validation_error'));
                    return redirect()->back()->withInput()->withErrors($validator);
                } else {
                    $files[$count] = $key->getClientOriginalName();
                    $key->move(public_path() . '/uploads',
                        $key->getClientOriginalName());
                }
                $count++;
            }
        }
        $loan->files = serialize($files);
        $loan->save();

        //save custom meta
        $custom_fields = CustomField::where('category', 'loans')->get();
        foreach ($custom_fields as $key) {
            $custom_field = new CustomFieldMeta();
            $id = $key->id;
            $custom_field->name = $request->$id;
            $custom_field->parent_id = $loan->id;
            $custom_field->custom_field_id = $key->id;
            $custom_field->category = "loans";
            $custom_field->save();
        }
        //save loan fees
        $fees_distribute = 0;
        $fees_first_payment = 0;
        $fees_last_payment = 0;
        foreach (LoanFee::all() as $key) {
            $loan_fee = new LoanFeeMeta();
            $value = 'loan_fees_amount_' . $key->id;
            $loan_fees_schedule = 'loan_fees_schedule_' . $key->id;
            $loan_fee->user_id = Sentinel::getUser()->id;
            $loan_fee->category = 'loan';
            $loan_fee->parent_id = $loan->id;
            $loan_fee->loan_fees_id = $key->id;
            $loan_fee->value = $request->$value;
            $loan_fee->loan_fees_schedule = $request->$loan_fees_schedule;
            $loan_fee->save();
            //determine amount to use
            if ($key->loan_fee_type == 'fixed') {
                if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                    $fees_distribute = $fees_distribute + $loan_fee->value;
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                    $fees_first_payment = $fees_first_payment + $loan_fee->value;
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                    $fees_last_payment = $fees_last_payment + $loan_fee->value;
                }
            } else {
                if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                    $fees_distribute = $fees_distribute + ($loan_fee->value * $loan->principal / 100);
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                    $fees_first_payment = $fees_first_payment + ($loan_fee->value * $loan->principal / 100);
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                    $fees_last_payment = $fees_last_payment + ($loan_fee->value * $loan->principal / 100);
                }
            }
        }
        //lets create schedules here
        //determine interest rate to use

        $interest_rate = GeneralHelper::determine_interest_rate($loan->id);

        $period = GeneralHelper::loan_period($loan->id);
        $loan = Loan::find($loan->id);
        if ($loan->repayment_cycle == 'daily') {
            $repayment_cycle = 'day';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' days')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'weekly') {
            $repayment_cycle = 'week';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' weeks')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'monthly') {
            $repayment_cycle = 'month';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' months')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'bi_monthly') {
            $repayment_cycle = 'month';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' months')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'quarterly') {
            $repayment_cycle = 'month';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' months')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'semi_annually') {
            $repayment_cycle = 'month';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' months')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'yearly') {
            $repayment_cycle = 'year';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' years')),
                'Y-m-d');
        }
   
        $loan->group_id = $request->groupid;
        $loan->save();
             }
    }
        }
    DB::table("grouploan")->insertGetId(["userid"=>Sentinel::getUser()->id,"sc"=>$sc,"loan_amount"=>$amount,"group_id"=>$request->groupid,"status"=>"pending"]);

         GeneralHelper::audit_trail("Added loan with id:" . $loan->id);
        Flash::success(trans('general.successfully_saved'));
    return redirect("/borrower/group/$groupid/vloans");
      
    
}
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        //get custom fields
        $users = User::all();
        $user = array();
        foreach ($users as $key) {
            $user[$key->id] = $key->first_name . ' ' . $key->last_name;
        }
        return view('borrower.group.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if(!isset($request->loan_officers)){
            Flash::warning("Loan Officer Required");
            redirect()->back()->withInput();
            return redirect()->back();
        }
        $group = new BorrowerGroup();
        $group->name = $request->name;
        $group->notes = $request->notes;
        $group->branch_id = session("branch_id");

        $group->save();
        $loan_officers = serialize($request->loan_officers);
        foreach(unserialize($loan_officers) as $u){
            DB::table("loan_officers2")->insert(["branch_id"=>$group->id,"officer_id"=>$u]);
        }

        Flash::success(trans('general.successfully_saved'));
        return redirect('borrower/group/data');
    }

    public function loans(Request $request){


        $check = DB::table("grouploan")->where(["sc"=>$request->id])->exists();
        if(!$check){
            Flash::warning("Invalid Group ID provided!");
            return redirect()->back();
        }
        $users = DB::table("users")->where(["branch_users.branch_id"=>session("branch_id")])
            ->join("branch_users","branch_users.user_id","=","users.id")
            ->join("role_users","role_users.user_id","=","users.id")->where(["role_users.role_id"=>4])->select("users.first_name","users.last_name","users.id")->get();


        $gid = $request->id;
        $loan_disbursed_by = array();
        foreach (LoanDisbursedBy::all() as $key) {
            $loan_disbursed_by[$key->id] = $key->name;
        }
        $loan = DB::table("loans")->where(["sc"=>$request->id])->get();

        $balance = 0;
        $balance2 = 0;
        foreach($loan as $loans){
            $balance += \App\Helpers\GeneralHelper::loan_total_paid($loans->id);
            $balance2 += \App\Helpers\GeneralHelper::loan_total_balance($loans->id);
        }
        $gr = DB::table("grouploan")->where(["sc"=>$request->id])->first();
        return view("borrower.group.loans",compact("loans","users","gid","loan_disbursed_by","balance","balance2","gr"));
    }
    
    public function bulkrepayment(Request $request){
        $id = $request->id;
        $group = DB::table("borrower_groups")->where(["id"=>$id])->first();
        if($group == null){
            Flash::error("Invalid Group ID provided!");
            return redirect()->back();
        }
        
        return view("borrower.group.bulkpayment",compact("group"));
    }
    
    public function gloans(Request $request){
        $datas = [];

        $columns = array( 
            0 =>'borrowers', 
            1 =>'id',
            2=> 'loan_officer',
            3=> 'branch',
            4=> 'principal',
            5=> 'released',
            6=> 'interest',
            7=> 'due',
            8=> 'paid',
            9=> 'balance',
            10=> 'status',
            11=> 'action',

        );


        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $texts = $request->input('search.value');
        $text = $request->loan_officer;
        $text2 = $request->date;
        if(isset($_GET['status'])){
            $status = $_GET['status'];
        }else{
            $status = "";
        }

        if(empty($texts) )
        {      
            $status = [];
            if($request->status != null){
                $status = ["status"=>$request->status];
            }
            $data = Loan::where('branch_id', session('branch_id'))->offset($start)
                ->limit($limit)
                ->where($status)
                ->where(["sc"=>$request->id])
                ->orderBy("loans.id",$dir)->get();

            $totalFiltered = Loan::where('branch_id', session('branch_id'))->where($status) ->where(["sc"=>$request->id])->count();
        }else{
            $data = Loan::where('loans.branch_id', session('branch_id'))->join("borrowers","borrowers.id","=","loans.borrower_id")
                ->offset($start)
                ->limit($limit)
                ->orderBy("loans.id",$dir)
                ->where(["status"=>$status])
                ->where(["sc"=>$request->id])
                ->where(function ($query) use($texts) {
                    $query
                        ->orWhere('borrowers.first_name', 'like', '%' . $texts . '%')
                        ->orWhere('borrowers.last_name', 'like', '%' . $texts . '%')
                        ->orWhereRaw("concat(first_name, ' ', last_name) like '%$texts%' ")
                        ->orWhere('loans.id', 'like', '%' . $texts . '%')
                        ->orWhere('loans.borrower_id', 'like', '%' . $texts . '%')
                        ->orWhere('loans.principal', 'like', '%' . $texts . '%')
                        ->orWhere('loans.interest_rate', 'like', '%' . $texts . '%')
                        ->orWhere('loans.loan_status', 'like', '%' . $texts . '%')
                        ->orWhere('loans.status', 'like', '%' . $texts . '%')
                        ->orWhere('loans.balance', 'like', '%' . $texts . '%')
                        ->orWhere('loans.created_at', 'like', '%' . $texts . '%');
                })

                ->select("loans.*")
                ->get();
            $totalFiltered = Loan::where('loans.branch_id', session('branch_id'))->join("borrowers","borrowers.id","=","loans.borrower_id")
                ->where(["status"=>$request->status])
                ->where(["sc"=>$request->id])
                ->where(function ($query) use($text) {
                    $query
                        ->orWhere('borrowers.first_name', 'like', '%' . $text . '%')
                        ->orWhere('borrowers.last_name', 'like', '%' . $text . '%')
                        ->orWhereRaw("concat(first_name, ' ', last_name) like '%$text%' ")
                        ->orWhere('loans.id', 'like', '%' . $text . '%')
                        ->orWhere('loans.borrower_id', 'like', '%' . $text . '%')
                        ->orWhere('loans.principal', 'like', '%' . $text . '%')
                        ->orWhere('loans.interest_rate', 'like', '%' . $text . '%')
                        ->orWhere('loans.loan_status', 'like', '%' . $text . '%')
                        ->orWhere('loans.status', 'like', '%' . $text . '%')
                        ->orWhere('loans.balance', 'like', '%' . $text . '%')
                        ->orWhere('loans.created_at', 'like', '%' . $text . '%');
                })->select("loans.*")->count();

        }




        if(!empty($text)){
            $data = Loan::where(['loans.branch_id'=> session('branch_id'),"loan_officers.officer_id"=>$request->loan_officer])
                ->where(["sc"=>$request->id])
                ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
                ->get();

            $totalFiltered = Loan::where(['loans.branch_id'=> session('branch_id'),"loan_officers.officer_id"=>$request->loan_officer])
                ->where(["sc"=>$request->id])
                ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")->count();

        }


        if(!empty($text2)){
            $data = Loan::where(['loans.branch_id'=> session('branch_id')])->where('loans.created_at', 'like', '%' . $text2 . '%')
                ->where(["sc"=>$request->id])
                ->get();

            $totalFiltered  = Loan::where(['loans.branch_id'=> session('branch_id')])->where('loans.created_at', 'like', '%' . $text2 . '%')   ->where(["sc"=>$_GET['id']])->count();
        }

        if(!empty($text) && !empty($text2)){
            $data = Loan::where(['loans.branch_id'=> session('branch_id'),"loans.user_id"=>$text])->join("loan_officers","loan_officers.officer_id","=","loans.user_id")
                ->where(["sc"=>$request->id])
                ->where('loans.created_at', 'like', '%' . $text2 . '%')->join("borrowers","borrowers.id","=","loans.borrower_id")->select("loans.*","borrowers.first_name","borrowers.last_name","borrowers.loan_officers")
                ->get();
            $totalFiltered = Loan::where(['loans.branch_id'=> session('branch_id'),"loans.user_id"=>$text])->join("loan_officers","loan_officers.officer_id","=","loans.user_id")
                ->where(["sc"=>$request->id])
                ->where('loans.created_at', 'like', '%' . $text2 . '%')->join("borrowers","borrowers.id","=","loans.borrower_id")->select("loans.*","borrowers.first_name","borrowers.last_name","borrowers.loan_officers")
                ->count();
        }

        $totalData = count($data);
        $totalFilter = $totalFiltered;
        $totalFiltered = $totalData; 

        foreach($data as $key){

            if(!empty($key->borrower)){
                $name =' <a href="'.url('borrower/'.$key->borrower_id.'/show').'">'.$key->borrower->first_name.' '.$key->borrower->last_name.'</a>';
            }else{
                $name =' <span class="label label-danger">'.trans_choice('general.broken',1).' <i
                class="fa fa-exclamation-triangle"></i> </span>';
            }
            $uu = [];
            $b = DB::table("branches")->where(["id"=>$key->branch_id])->first();
            $u = DB::table("users")->where(["id"=>$key->user_id])->first();
            $lname = "";
            $bname = "";
            $lid = "";
            $datax = [];
            if($b){
                $bname = $b->name;
            }
            if(unserialize($key->borrower->loan_officers)){

                $datax = unserialize($key->borrower->loan_officers);

            }



            foreach($datax as $d){

                $u = DB::table("users")->where(["id"=>$d])->first();


                $uu2 = '<a href="/user/'.$d.'/show">'.$u->first_name.' '.$u->last_name.'</a><br>
<br>';
                array_push($uu,$uu2);
            }
            $b = DB::table("branches")->where(["id"=>$key->branch_id])->first();

            if(\App\Models\Setting::where('setting_key', 'currency_position')->first()->setting_value=='left'){
                $prin = \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value."".number_format($key->principal,2);
            }else{
                $prin = number_format($key->principal,2)."".\App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value;
            }
            $rd = $key->release_date;
            $in =   number_format($key->interest_rate,2).'%/'.$key->interest_period;

            if($key->override==1){
                $due = '<s>'.number_format(\App\Helpers\GeneralHelper::loan_total_due_amount($key->id),2).'</s><br>
        '.number_format($key->balance,2).'';
            }else{
                $due = number_format(\App\Helpers\GeneralHelper::loan_total_due_amount($key->id),2);
            }
            $paid = number_format(\App\Helpers\GeneralHelper::loan_total_paid($key->id),2);
            $bal =  number_format(\App\Helpers\GeneralHelper::loan_total_balance($key->id),2);


            if($key->maturity_date<date("Y-m-d") && \App\Helpers\GeneralHelper::loan_total_balance($key->id)>0){
                $status ='   <span class="label label-danger">'.trans_choice('general.past_maturity',1).'</span>';
            }else{
                if($key->status=='pending'){
                    $status =  '<span class="label label-warning">'.trans_choice('general.pending',1).' '.trans_choice('general.approval',1).'</span>';
                }
                if($key->status=='approved'){
                    $status =  '<span class="label label-info">'.trans_choice('general.awaiting',1).' '.trans_choice('general.disbursement',1).'</span>';

                }
                if($key->status=='disbursed'){

                    $status =  '<span class="label label-info">'.trans_choice('general.active',1).'</span>';
                }
                if($key->status=='declined'){
                    $status =  '<span class="label label-danger">'.trans_choice('general.declined',1).'</span>';

                }
                if($key->status=='withdrawn'){
                    $status =  '<span class="label label-danger">'.trans_choice('general.withdrawn',1).'</span>';

                }
                if($key->status=='written_off'){
                    $status =  '<span class="label label-danger">'.trans_choice('general.written_off',1).'</span>';

                }
                if($key->status=='closed'){
                    $status =  '<span class="label label-success">'.trans_choice('general.closed',1).'</span>';

                }
                if($key->status=='pending_reschedule'){
                    $status =  '<span class="label label-warning">'.trans_choice('general.pending',1).' '.trans_choice('general.reschedule',1).'</span>';

                }
                if($key->status=='rescheduled'){
                    $status =  '<span class="label label-info">'.trans_choice('general.rescheduled',1).'</span>';

                }
            }
            $ac1 = "";
            $ac2 = "";
            $ac3 = "";
            if(Sentinel::hasAccess('loans.view')){
                $ac1 ='   <li><a href="'.url('loan/'.$key->id.'/show') .'"><i
        class="fa fa-search"></i> '. trans_choice('general.detail',2).'
    </a>
        </li>';
            }
            if(Sentinel::hasAccess('loans.create')){
                $ac2 ='     <li><a href="'.url('loan/'.$key->id.'/edit').'"><i
        class="fa fa-edit"></i> '. trans('general.edit') .'</a></li>';
            }
           /*  if(Sentinel::hasAccess('loans.delete')){
                $ac3 = ' <li><a href="'.url('loan/'.$key->id.'/delete').'"
        class="delete"><i
        class="fa fa-trash"></i> '. trans('general.delete').' </a></li>';
            } */
            $action = '   <div class="btn-group">
            <button type="button" class="btn btn-info btn-xs dropdown-toggle"
            data-toggle="dropdown" aria-expanded="false">
        '.trans('general.choose').' <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu dropdown-menu-right" role="menu">
           '.$ac1.'
             '.$ac2.'

            </ul>
            </div>';
            $select = "<input type='checkbox' value=".$key->id.">";

            $nestedData['select'] = $select;
            $nestedData['borrowers'] = $name;
            $nestedData['id'] = $key->id;
            $nestedData['loan_officer'] = $uu;
            $nestedData['branch'] = $b->name;
            $nestedData['principal'] = $prin;
            $nestedData['released'] = $rd;
            $nestedData['interest'] = $in;
            $nestedData['due'] = $due;
            $nestedData['paid'] = $paid;
            $nestedData['balance'] = $bal;
            $nestedData['status'] = $status;
            $nestedData['action'] = $action;
            $datas[] = $nestedData;


        }


        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => $totalFilter, 
            "data"            => $datas  
        );

        return response()->json($json_data);
    }
    
    public function undisbursecheck(Request $request){
        $ids = $request->ids;
        if($ids == []){
            return response()->json(["message"=>"Something Went Wrong, Or Invalid Selection!"],400);
        }
        foreach($ids as $id){
            LoanSchedule::where('loan_id', $id)->delete();
            LoanRepayment::where('loan_id', $id)->delete();
            $loan = Loan::find($id);
            DB::table("grouploan")->where(["sc"=>$loan->sc])->update(["status"=>"approved"]);
            $loan->status = 'approved';
            $loan->save();

            }
        return response()->json(["message"=>"Undisbursed Successfully!!"]);
    }
    
    
    public function bulkundisbursecheck(Request $request){
        $ids = $request->ids;
        if($ids == []){
            return response()->json(["message"=>"Something Went Wrong, Or Invalid Selection!"],400);
        }
        foreach($ids as $id){
            $loans = DB::table("loans")->where(["sc"=>$id])->first();


            LoanSchedule::where('loan_id', $loans->id)->delete();
            LoanRepayment::where('loan_id', $loans->id)->delete();

            DB::table("grouploan")->where(["sc"=>$id])->update(["status"=>"approved"]);
            DB::table("loans")->where(["sc"=>$id])->update(["status"=>"approved"]);


        }
        return response()->json(["message"=>"Undisbursed Successfully!!"]);
    }

    public function approvedcheck(Request $request){
        $ids = $request->ids;

        if($ids == []){
            return response()->json(["message"=>"Something Went Wrong, Or Invalid Selection!"],400);
        }
        foreach($ids as $id){
            $loan = DB::table("loans")->where(["id"=>$id])->first();
            $loan = Loan::find($id);
            $loan->status = 'approved';
            $loan->approved_date = $request->approved_date;
            $loan->approved_notes = $request->approved_notes;
            $loan->approved_by_id = Sentinel::getUser()->id;
            $loan->approved_amount = $loan->principal;
            $loan->principal = $loan->principal;
            $loan->save();
            DB::table("grouploan")->where(["sc"=>$loan->sc])->update(["status"=>"approved"]);

        }
        return response()->json(["message"=>"Approved Successfully!!"]);
    }
    
    
    public function bulkapprovedcheck(Request $request){
        $ids = $request->ids;

        if($ids == []){
            return response()->json(["message"=>"Something Went Wrong, Or Invalid Selection!"],400);
        }
        foreach($ids as $id){
            $loan = DB::table("loans")->where(["id"=>$id])->first();

            DB::table("loans")->where(["sc"=>$id])->update([
            "status"=>"approved",
                "approved_date"=>$request->approved_date,
                "approved_notes"=>$request->approved_notes,
                "approved_by_id"=>Sentinel::getUser()->id,
                "approved_amount"=>$loan->principal,
                "principal"=>$loan->principal
            ]);
            DB::table("grouploan")->where(["sc"=>$id])->update(["status"=>"approved"]);

        }
        return response()->json(["message"=>"Approved Successfully!!"]);
    }
    public function bulkdisbursecheck(Request $request){
        if (!Sentinel::hasAccess('loans.disburse')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $ids = $request->ids;
        if($ids == []){
            return response()->json(["message"=>"Something Went Wrong, Or Invalid Selection!"],400);
        }
        foreach($ids as $id){
            $sc = DB::table("loans")->where(["sc"=>$id])->get();
            foreach($sc as $d){
            $loan = Loan::find($d->id);

            DB::table("grouploan")->where(["sc"=>$loan->sc])->update(["status"=>"disbursed"]);
            //delete previously created schedules and payments
            LoanSchedule::where('loan_id', $loan->id)->delete();
            LoanRepayment::where('loan_id', $loan->id)->delete();
            $interest_rate = GeneralHelper::determine_interest_rate($loan->id);
            $period = GeneralHelper::loan_period($loan->id);
                $loan = Loan::find($loan->id);
                if($loan != null){
            if ($loan->repayment_cycle == 'daily') {
                $repayment_cycle = '1 days';
                $repayment_type = 'days';
            }
            if ($loan->repayment_cycle == 'weekly') {
                $repayment_cycle = '1 weeks';
                $repayment_type = 'weeks';
            }
            if ($loan->repayment_cycle == 'monthly') {
                $repayment_cycle = 'month';
                $repayment_type = 'months';
            }
            if ($loan->repayment_cycle == 'bi_monthly') {
                $repayment_cycle = '2 months';
                $repayment_type = 'months';

            }
            if ($loan->repayment_cycle == 'quarterly') {
                $repayment_cycle = '4 months';
                $repayment_type = 'months';
            }
            if ($loan->repayment_cycle == 'semi_annually') {
                $repayment_cycle = '6 months';
                $repayment_type = 'months';
            }
            if ($loan->repayment_cycle == 'yearly') {
                $repayment_cycle = '1 years';
                $repayment_type = 'years';
            }
            if (empty($request->first_payment_date)) {
                $first_payment_date = date_format(date_add(date_create($request->disbursed_date),
                                                           date_interval_create_from_date_string($repayment_cycle)),
                                                  'Y-m-d');


            } else {
                $first_payment_date =  date('Y-m-d', strtotime($request->first_payment_date));
            }
            $loan->maturity_date = date_format(date_add(date_create($first_payment_date),
                                                        date_interval_create_from_date_string($period . ' ' . $repayment_type)),
                                               'Y-m-d');
            $loan->status = 'disbursed';
            $loan->loan_disbursed_by_id = $request->loan_disbursed_by_id;
            $loan->disbursed_notes = $request->disbursed_notes;
            $loan->first_payment_date = $first_payment_date;
            $loan->disbursed_by_id = Sentinel::getUser()->id;
            $loan->disbursed_date = $request->disbursed_date;
            $loan->release_date = date('Y-m-d', strtotime($request->disbursed_date));
            $date = explode('-', $request->disbursed_date);
            $loan->month = $date[1];
            $loan->year = $date[0];

            $loan->save();
            $fees_distribute = 0;
            $fees_first_payment = 0;
            $fees_last_payment = 0;

            foreach (LoanFee::all() as $key) {
                if (!empty(LoanFeeMeta::where('loan_fees_id', $key->id)->where('parent_id', $loan->id)->where('category',
                                                                                                              'loan')->first())
                   ) {
                    $loan_fee = LoanFeeMeta::where('loan_fees_id', $key->id)->where('parent_id',
                                                                                    $loan->id)->where('category',
                                                                                                      'loan')->first();
                    //determine amount to use
                    if ($key->loan_fee_type == 'fixed') {
                        if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                            $fees_distribute = $fees_distribute + $loan_fee->value;
                        }
                        if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                            $fees_first_payment = $fees_first_payment + $loan_fee->value;
                        }
                        if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                            $fees_last_payment = $fees_last_payment + $loan_fee->value;
                        }
                    } else {
                        if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                            $fees_distribute = $fees_distribute + ($loan_fee->value * $loan->principal / 100);
                        }
                        if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                            $fees_first_payment = $fees_first_payment + ($loan_fee->value * $loan->principal / 100);
                        }
                        if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                            $fees_last_payment = $fees_last_payment + ($loan_fee->value * $loan->principal / 100);
                        }
                    }
                }

            }
            //generate schedules until period finished
            $next_payment = $first_payment_date;
            $balance = $loan->principal;
            for ($i = 1; $i <= $period; $i++) {
                $fees = 0;
                if ($i == 1) {
                    $fees = $fees + ($fees_first_payment);
                }
                if ($i == $period) {
                    $fees = $fees + ($fees_last_payment);
                }
                $fees = $fees + ($fees_distribute / $period);
                $loan_schedule = new LoanSchedule();
                $loan_schedule->loan_id = $loan->id;
                $loan_schedule->fees = $fees;
                $loan_schedule->branch_id = session('branch_id');
                $loan_schedule->borrower_id = $loan->borrower_id;
                $loan_schedule->description = trans_choice('general.repayment', 1);
                $loan_schedule->due_date = $next_payment;


                $loan_schedule->month = $date[1];
                $loan_schedule->year = $date[0];
                //determine which method to use
                $due = 0;
                //reducing balance equal installments
                if ($loan->interest_method == 'declining_balance_equal_installments') {
                    $due = GeneralHelper::amortized_monthly_payment($loan->id, $loan->principal);

                    if ($loan->decimal_places == 'round_off_to_two_decimal') {
                        //determine if we have grace period for interest

                        $interest = round(($interest_rate * $balance), 2);
                        $loan_schedule->principal = round(($due - $interest), 2);
                        if ($loan->grace_on_interest_charged >= $i) {
                            $loan_schedule->interest = 0;
                        } else {
                            $loan_schedule->interest = round($interest, 2);
                        }
                        $loan_schedule->due = round($due, 2);
                        //determine next balance
                        $balance = round(($balance - ($due - $interest)), 2);
                        $loan_schedule->principal_balance = round($balance, 2);
                    } else {
                        //determine if we have grace period for interest

                        $interest = round(($interest_rate * $balance));
                        $loan_schedule->principal = round(($due - $interest));
                        if ($loan->grace_on_interest_charged >= $i) {
                            $loan_schedule->interest = 0;
                        } else {
                            $loan_schedule->interest = round($interest);
                        }
                        $loan_schedule->due = round($due);
                        //determine next balance
                        $balance = round(($balance - ($due - $interest)));
                        $loan_schedule->principal_balance = round($balance);
                    }


                }
                //reducing balance equal principle
                if ($loan->interest_method == 'declining_balance_equal_principal') {
                    $principal = $loan->principal / $period;
                    if ($loan->decimal_places == 'round_off_to_two_decimal') {

                        $interest = round(($interest_rate * $balance), 2);
                        $loan_schedule->principal = round($principal, 2);
                        if ($loan->grace_on_interest_charged >= $i) {
                            $loan_schedule->interest = 0;
                        } else {
                            $loan_schedule->interest = round($interest, 2);
                        }
                        $loan_schedule->due = round(($principal + $interest), 2);
                        //determine next balance
                        $balance = round(($balance - ($principal + $interest)), 2);
                        $loan_schedule->principal_balance = round($balance, 2);
                    } else {

                        $loan_schedule->principal = round(($principal));

                        $interest = round(($interest_rate * $balance));
                        if ($loan->grace_on_interest_charged >= $i) {
                            $loan_schedule->interest = 0;
                        } else {
                            $loan_schedule->interest = round($interest);
                        }
                        $loan_schedule->due = round($principal + $interest);
                        //determine next balance
                        $balance = round(($balance - ($principal + $interest)));
                        $loan_schedule->principal_balance = round($balance);
                    }

                }
                //flat  method
                if ($loan->interest_method == 'flat_rate') {
                    $principal = $loan->principal / $period;
                    if ($loan->decimal_places == 'round_off_to_five_decimal') {
                        $interest = round(($interest_rate * $loan->principal), 5);
                        $loan_schedule->principal = round(($principal), 5);
                        if ($loan->grace_on_interest_charged >= $i) {
                            $loan_schedule->interest = 0;
                        } else {
                            $loan_schedule->interest = round($interest, 5);
                        }
                        $loan_schedule->principal = round(($principal), 5);
                        $loan_schedule->due = round(($principal + $interest), 5);
                        //determine next balance
                        $balance = round(($balance - $principal), 2);
                        $loan_schedule->principal_balance = round($balance, 5);
                    } else {
                        $interest = round(($interest_rate * $loan->principal));
                        if ($loan->grace_on_interest_charged >= $i) {
                            $loan_schedule->interest = 0;
                        } else {
                            $loan_schedule->interest = round($interest);
                        }
                        $loan_schedule->principal = round($principal);
                        $loan_schedule->due = round($principal + $interest);
                        //determine next balance
                        $balance = round(($balance - $principal));
                        $loan_schedule->principal_balance = round($balance);
                    }
                }
                //interest only method
                if ($loan->interest_method == 'interest_only') {
                    if ($i == $period) {
                        $principal = $loan->principal;
                    } else {
                        $principal = 0;
                    }
                    if ($loan->decimal_places == 'round_off_to_two_decimal') {
                        $interest = round(($interest_rate * $loan->principal), 2);
                        if ($loan->grace_on_interest_charged >= $i) {
                            $loan_schedule->interest = 0;
                        } else {
                            $loan_schedule->interest = round($interest, 2);
                        }
                        $loan_schedule->principal = round(($principal), 2);
                        $loan_schedule->due = round(($principal + $interest), 2);
                        //determine next balance
                        $balance = round(($balance - $principal), 2);
                        $loan_schedule->principal_balance = round($balance, 2);
                    } else {
                        $interest = round(($interest_rate * $loan->principal));
                        if ($loan->grace_on_interest_charged >= $i) {
                            $loan_schedule->interest = 0;
                        } else {
                            $loan_schedule->interest = round($interest);
                        }
                        $loan_schedule->principal = round($principal);
                        $loan_schedule->due = round($principal + $interest);
                        //determine next balance
                        $balance = round(($balance - $principal));
                        $loan_schedule->principal_balance = round($balance);
                    }
                }
                //determine next due date
                if ($loan->repayment_cycle == 'daily') {
                    $next_payment = date_format(date_add(date_create($next_payment),
                                                         date_interval_create_from_date_string('1 days')),
                                                'Y-m-d');
                    //$loan_schedule->due_date = $next_payment;
                }
                if ($loan->repayment_cycle == 'weekly') {
                    $next_payment = date_format(date_add(date_create($next_payment),
                                                         date_interval_create_from_date_string('1 weeks')),
                                                'Y-m-d');
                    //$loan_schedule->due_date = $next_payment;
                }
                if ($loan->repayment_cycle == 'monthly') {
                    $next_payment = date_format(date_add(date_create($next_payment),
                                                         date_interval_create_from_date_string('1 months')),
                                                'Y-m-d');
                    //$loan_schedule->due_date = $next_payment;
                }
                if ($loan->repayment_cycle == 'bi_monthly') {
                    $next_payment = date_format(date_add(date_create($next_payment),
                                                         date_interval_create_from_date_string('2 months')),
                                                'Y-m-d');
                    //$loan_schedule->due_date = $next_payment;
                }
                if ($loan->repayment_cycle == 'quarterly') {
                    $next_payment = date_format(date_add(date_create($next_payment),
                                                         date_interval_create_from_date_string('4 months')),
                                                'Y-m-d');
                    //$loan_schedule->due_date = $next_payment;
                }
                if ($loan->repayment_cycle == 'semi_annually') {
                    $next_payment = date_format(date_add(date_create($next_payment),
                                                         date_interval_create_from_date_string('6 months')),
                                                'Y-m-d');
                    //$loan_schedule->due_date = $next_payment;
                }
                if ($loan->repayment_cycle == 'yearly') {
                    $next_payment = date_format(date_add(date_create($next_payment),
                                                         date_interval_create_from_date_string('1 years')),
                                                'Y-m-d');
                    //$loan_schedule->due_date = $next_payment;
                }
                if ($i == $period) {
                    $loan_schedule->principal_balance = round($balance);
                }
                $loan_schedule->save();
            }
            $loan = Loan::find($loan->id);
            $loan->maturity_date = $next_payment;
            $loan->save();
        }
                }
}
        return response()->json(["message"=>"Disbursed SuccessFully!"]);
    }


    public function disbursecheck(Request $request){
        $ids = $request->ids;
        if($ids == []){
            return response()->json(["message"=>"Something Went Wrong, Or Invalid Selection!"],400);
        }
        foreach($ids as $id){
            $loan = Loan::find($id);

            if (!Sentinel::hasAccess('loans.disburse')) {
                Flash::warning(trans('general.permission_denied'));
                return redirect('/');
            }
            DB::table("grouploan")->where(["sc"=>$loan->sc])->update(["status"=>"disbursed"]);
            //delete previously created schedules and payments
            LoanSchedule::where('loan_id', $loan->id)->delete();
            LoanRepayment::where('loan_id', $loan->id)->delete();
            $interest_rate = GeneralHelper::determine_interest_rate($loan->id);
            $period = GeneralHelper::loan_period($loan->id);
            $loan = Loan::find($loan->id);
            if ($loan->repayment_cycle == 'daily') {
                $repayment_cycle = '1 days';
                $repayment_type = 'days';
            }
            if ($loan->repayment_cycle == 'weekly') {
                $repayment_cycle = '1 weeks';
                $repayment_type = 'weeks';
            }
            if ($loan->repayment_cycle == 'monthly') {
                $repayment_cycle = 'month';
                $repayment_type = 'months';
            }
            if ($loan->repayment_cycle == 'bi_monthly') {
                $repayment_cycle = '2 months';
                $repayment_type = 'months';

            }
            if ($loan->repayment_cycle == 'quarterly') {
                $repayment_cycle = '4 months';
                $repayment_type = 'months';
            }
            if ($loan->repayment_cycle == 'semi_annually') {
                $repayment_cycle = '6 months';
                $repayment_type = 'months';
            }
            if ($loan->repayment_cycle == 'yearly') {
                $repayment_cycle = '1 years';
                $repayment_type = 'years';
            }
            if (empty($request->first_payment_date)) {
                $first_payment_date = date_format(date_add(date_create($request->disbursed_date),
                                                           date_interval_create_from_date_string($repayment_cycle)),
                                                  'Y-m-d');


            } else {
                $first_payment_date =  date('Y-m-d', strtotime($request->first_payment_date));
            }
            $loan->maturity_date = date_format(date_add(date_create($first_payment_date),
                                                        date_interval_create_from_date_string($period . ' ' . $repayment_type)),
                                               'Y-m-d');
            $loan->status = 'disbursed';
            $loan->loan_disbursed_by_id = $request->loan_disbursed_by_id;
            $loan->disbursed_notes = $request->disbursed_notes;
            $loan->first_payment_date = $first_payment_date;
            $loan->disbursed_by_id = Sentinel::getUser()->id;
            $loan->disbursed_date = $request->disbursed_date;
            $loan->release_date = date('Y-m-d', strtotime($request->disbursed_date));
            $date = explode('-', $request->disbursed_date);
            $loan->month = $date[1];
            $loan->year = $date[0];
            $loan->save();
            $fees_distribute = 0;
            $fees_first_payment = 0;
            $fees_last_payment = 0;

            foreach (LoanFee::all() as $key) {
                if (!empty(LoanFeeMeta::where('loan_fees_id', $key->id)->where('parent_id', $loan->id)->where('category',
                                                                                                              'loan')->first())
                   ) {
                    $loan_fee = LoanFeeMeta::where('loan_fees_id', $key->id)->where('parent_id',
                                                                                    $loan->id)->where('category',
                                                                                                      'loan')->first();
                    //determine amount to use
                    if ($key->loan_fee_type == 'fixed') {
                        if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                            $fees_distribute = $fees_distribute + $loan_fee->value;
                        }
                        if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                            $fees_first_payment = $fees_first_payment + $loan_fee->value;
                        }
                        if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                            $fees_last_payment = $fees_last_payment + $loan_fee->value;
                        }
                    } else {
                        if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                            $fees_distribute = $fees_distribute + ($loan_fee->value * $loan->principal / 100);
                        }
                        if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                            $fees_first_payment = $fees_first_payment + ($loan_fee->value * $loan->principal / 100);
                        }
                        if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                            $fees_last_payment = $fees_last_payment + ($loan_fee->value * $loan->principal / 100);
                        }
                    }
                }

            }
            //generate schedules until period finished
            $next_payment = $first_payment_date;
            $balance = $loan->principal;
            for ($i = 1; $i <= $period; $i++) {
                $fees = 0;
                if ($i == 1) {
                    $fees = $fees + ($fees_first_payment);
                }
                if ($i == $period) {
                    $fees = $fees + ($fees_last_payment);
                }
                $fees = $fees + ($fees_distribute / $period);
                $loan_schedule = new LoanSchedule();
                $loan_schedule->loan_id = $loan->id;
                $loan_schedule->fees = $fees;
                $loan_schedule->branch_id = session('branch_id');
                $loan_schedule->borrower_id = $loan->borrower_id;
                $loan_schedule->description = trans_choice('general.repayment', 1);
                $loan_schedule->due_date = $next_payment;


                $loan_schedule->month = $date[1];
                $loan_schedule->year = $date[0];
                //determine which method to use
                $due = 0;
                //reducing balance equal installments
                if ($loan->interest_method == 'declining_balance_equal_installments') {
                    $due = GeneralHelper::amortized_monthly_payment($loan->id, $loan->principal);

                    if ($loan->decimal_places == 'round_off_to_two_decimal') {
                        //determine if we have grace period for interest

                        $interest = round(($interest_rate * $balance), 2);
                        $loan_schedule->principal = round(($due - $interest), 2);
                        if ($loan->grace_on_interest_charged >= $i) {
                            $loan_schedule->interest = 0;
                        } else {
                            $loan_schedule->interest = round($interest, 2);
                        }
                        $loan_schedule->due = round($due, 2);
                        //determine next balance
                        $balance = round(($balance - ($due - $interest)), 2);
                        $loan_schedule->principal_balance = round($balance, 2);
                    } else {
                        //determine if we have grace period for interest

                        $interest = round(($interest_rate * $balance));
                        $loan_schedule->principal = round(($due - $interest));
                        if ($loan->grace_on_interest_charged >= $i) {
                            $loan_schedule->interest = 0;
                        } else {
                            $loan_schedule->interest = round($interest);
                        }
                        $loan_schedule->due = round($due);
                        //determine next balance
                        $balance = round(($balance - ($due - $interest)));
                        $loan_schedule->principal_balance = round($balance);
                    }


                }
                //reducing balance equal principle
                if ($loan->interest_method == 'declining_balance_equal_principal') {
                    $principal = $loan->principal / $period;
                    if ($loan->decimal_places == 'round_off_to_two_decimal') {

                        $interest = round(($interest_rate * $balance), 2);
                        $loan_schedule->principal = round($principal, 2);
                        if ($loan->grace_on_interest_charged >= $i) {
                            $loan_schedule->interest = 0;
                        } else {
                            $loan_schedule->interest = round($interest, 2);
                        }
                        $loan_schedule->due = round(($principal + $interest), 2);
                        //determine next balance
                        $balance = round(($balance - ($principal + $interest)), 2);
                        $loan_schedule->principal_balance = round($balance, 2);
                    } else {

                        $loan_schedule->principal = round(($principal));

                        $interest = round(($interest_rate * $balance));
                        if ($loan->grace_on_interest_charged >= $i) {
                            $loan_schedule->interest = 0;
                        } else {
                            $loan_schedule->interest = round($interest);
                        }
                        $loan_schedule->due = round($principal + $interest);
                        //determine next balance
                        $balance = round(($balance - ($principal + $interest)));
                        $loan_schedule->principal_balance = round($balance);
                    }

                }
                //flat  method
                if ($loan->interest_method == 'flat_rate') {
                    $principal = $loan->principal / $period;
                    if ($loan->decimal_places == 'round_off_to_five_decimal') {
                        $interest = round(($interest_rate * $loan->principal), 5);
                        $loan_schedule->principal = round(($principal), 5);
                        if ($loan->grace_on_interest_charged >= $i) {
                            $loan_schedule->interest = 0;
                        } else {
                            $loan_schedule->interest = round($interest, 5);
                        }
                        $loan_schedule->principal = round(($principal), 5);
                        $loan_schedule->due = round(($principal + $interest), 5);
                        //determine next balance
                        $balance = round(($balance - $principal), 2);
                        $loan_schedule->principal_balance = round($balance, 5);
                    } else {
                        $interest = round(($interest_rate * $loan->principal));
                        if ($loan->grace_on_interest_charged >= $i) {
                            $loan_schedule->interest = 0;
                        } else {
                            $loan_schedule->interest = round($interest);
                        }
                        $loan_schedule->principal = round($principal);
                        $loan_schedule->due = round($principal + $interest);
                        //determine next balance
                        $balance = round(($balance - $principal));
                        $loan_schedule->principal_balance = round($balance);
                    }
                }
                //interest only method
                if ($loan->interest_method == 'interest_only') {
                    if ($i == $period) {
                        $principal = $loan->principal;
                    } else {
                        $principal = 0;
                    }
                    if ($loan->decimal_places == 'round_off_to_two_decimal') {
                        $interest = round(($interest_rate * $loan->principal), 2);
                        if ($loan->grace_on_interest_charged >= $i) {
                            $loan_schedule->interest = 0;
                        } else {
                            $loan_schedule->interest = round($interest, 2);
                        }
                        $loan_schedule->principal = round(($principal), 2);
                        $loan_schedule->due = round(($principal + $interest), 2);
                        //determine next balance
                        $balance = round(($balance - $principal), 2);
                        $loan_schedule->principal_balance = round($balance, 2);
                    } else {
                        $interest = round(($interest_rate * $loan->principal));
                        if ($loan->grace_on_interest_charged >= $i) {
                            $loan_schedule->interest = 0;
                        } else {
                            $loan_schedule->interest = round($interest);
                        }
                        $loan_schedule->principal = round($principal);
                        $loan_schedule->due = round($principal + $interest);
                        //determine next balance
                        $balance = round(($balance - $principal));
                        $loan_schedule->principal_balance = round($balance);
                    }
                }
                //determine next due date
                if ($loan->repayment_cycle == 'daily') {
                    $next_payment = date_format(date_add(date_create($next_payment),
                                                         date_interval_create_from_date_string('1 days')),
                                                'Y-m-d');
                    //$loan_schedule->due_date = $next_payment;
                }
                if ($loan->repayment_cycle == 'weekly') {
                    $next_payment = date_format(date_add(date_create($next_payment),
                                                         date_interval_create_from_date_string('1 weeks')),
                                                'Y-m-d');
                    //$loan_schedule->due_date = $next_payment;
                }
                if ($loan->repayment_cycle == 'monthly') {
                    $next_payment = date_format(date_add(date_create($next_payment),
                                                         date_interval_create_from_date_string('1 months')),
                                                'Y-m-d');
                    //$loan_schedule->due_date = $next_payment;
                }
                if ($loan->repayment_cycle == 'bi_monthly') {
                    $next_payment = date_format(date_add(date_create($next_payment),
                                                         date_interval_create_from_date_string('2 months')),
                                                'Y-m-d');
                    //$loan_schedule->due_date = $next_payment;
                }
                if ($loan->repayment_cycle == 'quarterly') {
                    $next_payment = date_format(date_add(date_create($next_payment),
                                                         date_interval_create_from_date_string('4 months')),
                                                'Y-m-d');
                    //$loan_schedule->due_date = $next_payment;
                }
                if ($loan->repayment_cycle == 'semi_annually') {
                    $next_payment = date_format(date_add(date_create($next_payment),
                                                         date_interval_create_from_date_string('6 months')),
                                                'Y-m-d');
                    //$loan_schedule->due_date = $next_payment;
                }
                if ($loan->repayment_cycle == 'yearly') {
                    $next_payment = date_format(date_add(date_create($next_payment),
                                                         date_interval_create_from_date_string('1 years')),
                                                'Y-m-d');
                    //$loan_schedule->due_date = $next_payment;
                }
                if ($i == $period) {
                    $loan_schedule->principal_balance = round($balance);
                }
                $loan_schedule->save();
            }
            $loan = Loan::find($loan->id);
            $loan->maturity_date = $next_payment;
            $loan->save();
        }

        return response()->json(["message"=>"Disbursed SuccessFully!"]);
    }


    public function show($borrower_group)
    {
        $borrowers = array();
        foreach (Borrower::all() as $key) {
            $borrowers[$key->id] = $key->first_name . ' ' . $key->last_name . '(' . $key->unique_number . ')';
        }
        
        $balances = DB::table("grouploan")->where(["group_id"=>$borrower_group->id])->first();
        if($balances != null){
       $userss = DB::table("loans")->where(["sc"=>$balances->sc,"owing"=>null])->count();
          $users = DB::table("loans")->where(["sc"=>$balances->sc])->get();
        }
           $loans = \DB::table("grouploan")->where(["group_id"=>$borrower_group->id])->get();
    //       foreach($loans as $loan){
    //       if($loan->status == "approved"){
    //                              if($loan->loan_amount > $loan->balance || $loan->loan_amount >= $loan->balance){
    //                               DB::table("grouploan")->where(["sc"=>$loan->sc])->update(["status"=>"closed"]);
    //   DB::table("grouploan")->where(["sc"=>$loan->sc])->update(["balance"=>$loan->loan_amount]);
      
    //   DB::table("loans")->where(["sc"=>$loan->sc])->update(["balance"=>$loan->loan_amount]); 
    //       foreach($users as $user){
    //   DB::table("loans")->where(["sc"=>$loan->sc])->update(["status"=>"closed","loan_status"=>"closed"]);   
    //             }
    //                              }
    //                              }
    //       }
    $req =$borrower_group->id;

        $users = DB::table("users")->where(["branch_users.branch_id"=>session("branch_id")])
            ->join("branch_users","branch_users.user_id","=","users.id")
            ->join("role_users","role_users.user_id","=","users.id")->where(["role_users.role_id"=>4])->select("users.first_name","users.last_name","users.id")->get();


       $loansx = DB::table("loans")->where(["group_id"=>$req])->get();

        return view('borrower.group.show', compact('borrower_group', 'borrowers','balances','userss','users','req','usersx',"loansx"));
    }
public function approve(Request $request)
    {
        if (!Sentinel::hasAccess('loans.approve')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $id = $request->id;

        $update = DB::table("grouploan")->where(["sc"=>$id])->update(["status"=>"approved"]);
    $$loans = Loan::where(["sc"=>$id])->select("*")->get();
                      
        
foreach($loans as $laonx){
    $loan = Loan::find($loanx->id);
    $insert  = DB::table("loans")->where(["sc"=>$id])->update([

                
                  "approved_date" => $loan->applied_date,
        "approved_notes" => $request->approved_notes,
        "approved_by_id" => Sentinel::getUser()->id,
        "approved_amount" => $loan->applied_date

                 ]);
               
    LoanSchedule::where('loan_id', $loan->id)->delete();
    LoanRepayment::where('loan_id', $loan->id)->delete();
    $interest_rate = GeneralHelper::determine_interest_rate($loan->id);
    $period = GeneralHelper::loan_period($loan->id);

    if ($loan->repayment_cycle == 'daily') {
        $repayment_cycle = '1 days';
        $repayment_type = 'days';
    }
    if ($loan->repayment_cycle == 'weekly') {
        $repayment_cycle = '1 weeks';
        $repayment_type = 'weeks';
    }
    if ($loan->repayment_cycle == 'monthly') {
        $repayment_cycle = 'month';
        $repayment_type = 'months';
    }
    if ($loan->repayment_cycle == 'bi_monthly') {
        $repayment_cycle = '2 months';
        $repayment_type = 'months';

    }
    if ($loan->repayment_cycle == 'quarterly') {
        $repayment_cycle = '4 months';
        $repayment_type = 'months';
    }
    if ($loan->repayment_cycle == 'semi_annually') {
        $repayment_cycle = '6 months';
        $repayment_type = 'months';
    }
    if ($loan->repayment_cycle == 'yearly') {
        $repayment_cycle = '1 years';
        $repayment_type = 'years';
    }
    if (empty($request->first_payment_date)) {
        $first_payment_date = date_format(date_add(date_create($request->disbursed_date),
                                                   date_interval_create_from_date_string($repayment_cycle)),
                                          'Y-m-d');


    } else {
        $first_payment_date =  date('Y-m-d', strtotime($request->first_payment_date));
    }
    $loan->maturity_date = date_format(date_add(date_create($first_payment_date),
                                                date_interval_create_from_date_string($period . ' ' . $repayment_type)),
                                       'Y-m-d');
    $loan->status = 'disbursed';
    $loan->loan_disbursed_by_id = $request->loan_disbursed_by_id;
    $loan->disbursed_notes = $request->disbursed_notes;
    $loan->first_payment_date = $first_payment_date;
    $loan->disbursed_by_id = Sentinel::getUser()->id;
    $loan->disbursed_date = $request->disbursed_date;
    $loan->release_date = date('Y-m-d', strtotime($request->disbursed_date));
    $date = explode('-', $request->disbursed_date);
    $loan->month = $date[1];
    $loan->year = $date[0];
    $loan->save();
    $fees_distribute = 0;
    $fees_first_payment = 0;
    $fees_last_payment = 0;

    foreach (LoanFee::all() as $key) {
        if (!empty(LoanFeeMeta::where('loan_fees_id', $key->id)->where('parent_id', $loan->id)->where('category',
                                                                                                      'loan')->first())
           ) {
            $loan_fee = LoanFeeMeta::where('loan_fees_id', $key->id)->where('parent_id',
                                                                            $loan->id)->where('category',
                                                                                              'loan')->first();
            //determine amount to use
            if ($key->loan_fee_type == 'fixed') {
                if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                    $fees_distribute = $fees_distribute + $loan_fee->value;
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                    $fees_first_payment = $fees_first_payment + $loan_fee->value;
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                    $fees_last_payment = $fees_last_payment + $loan_fee->value;
                }
            } else {
                if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                    $fees_distribute = $fees_distribute + ($loan_fee->value * $loan->principal / 100);
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                    $fees_first_payment = $fees_first_payment + ($loan_fee->value * $loan->principal / 100);
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                    $fees_last_payment = $fees_last_payment + ($loan_fee->value * $loan->principal / 100);
                }
            }
        }

    }
    //generate schedules until period finished
    $next_payment = $first_payment_date;
    $balance = $loan->principal;
    for ($i = 1; $i <= $period; $i++) {
        $fees = 0;
        if ($i == 1) {
            $fees = $fees + ($fees_first_payment);
        }
        if ($i == $period) {
            $fees = $fees + ($fees_last_payment);
        }
        $fees = $fees + ($fees_distribute / $period);
        $loan_schedule = new LoanSchedule();
        $loan_schedule->loan_id = $loan->id;
        $loan_schedule->fees = $fees;
        $loan_schedule->branch_id = session('branch_id');
        $loan_schedule->borrower_id = $loan->borrower_id;
        $loan_schedule->description = trans_choice('general.repayment', 1);
        $loan_schedule->due_date = $next_payment;


        $loan_schedule->month = $date[1];
        $loan_schedule->year = $date[0];
        //determine which method to use
        $due = 0;
        //reducing balance equal installments
        if ($loan->interest_method == 'declining_balance_equal_installments') {
            $due = GeneralHelper::amortized_monthly_payment($loan->id, $loan->principal);

            if ($loan->decimal_places == 'round_off_to_two_decimal') {
                //determine if we have grace period for interest

                $interest = round(($interest_rate * $balance), 2);
                $loan_schedule->principal = round(($due - $interest), 2);
                if ($loan->grace_on_interest_charged >= $i) {
                    $loan_schedule->interest = 0;
                } else {
                    $loan_schedule->interest = round($interest, 2);
                }
                $loan_schedule->due = round($due, 2);
                //determine next balance
                $balance = round(($balance - ($due - $interest)), 2);
                $loan_schedule->principal_balance = round($balance, 2);
            } else {
                //determine if we have grace period for interest

                $interest = round(($interest_rate * $balance));
                $loan_schedule->principal = round(($due - $interest));
                if ($loan->grace_on_interest_charged >= $i) {
                    $loan_schedule->interest = 0;
                } else {
                    $loan_schedule->interest = round($interest);
                }
                $loan_schedule->due = round($due);
                //determine next balance
                $balance = round(($balance - ($due - $interest)));
                $loan_schedule->principal_balance = round($balance);
            }


        }
        //reducing balance equal principle
        if ($loan->interest_method == 'declining_balance_equal_principal') {
            $principal = $loan->principal / $period;
            if ($loan->decimal_places == 'round_off_to_two_decimal') {

                $interest = round(($interest_rate * $balance), 2);
                $loan_schedule->principal = round($principal, 2);
                if ($loan->grace_on_interest_charged >= $i) {
                    $loan_schedule->interest = 0;
                } else {
                    $loan_schedule->interest = round($interest, 2);
                }
                $loan_schedule->due = round(($principal + $interest), 2);
                //determine next balance
                $balance = round(($balance - ($principal + $interest)), 2);
                $loan_schedule->principal_balance = round($balance, 2);
            } else {

                $loan_schedule->principal = round(($principal));

                $interest = round(($interest_rate * $balance));
                if ($loan->grace_on_interest_charged >= $i) {
                    $loan_schedule->interest = 0;
                } else {
                    $loan_schedule->interest = round($interest);
                }
                $loan_schedule->due = round($principal + $interest);
                //determine next balance
                $balance = round(($balance - ($principal + $interest)));
                $loan_schedule->principal_balance = round($balance);
            }

        }
        //flat  method
        if ($loan->interest_method == 'flat_rate') {
            $principal = $loan->principal / $period;
            if ($loan->decimal_places == 'round_off_to_five_decimal') {
                $interest = round(($interest_rate * $loan->principal), 5);
                $loan_schedule->principal = round(($principal), 5);
                if ($loan->grace_on_interest_charged >= $i) {
                    $loan_schedule->interest = 0;
                } else {
                    $loan_schedule->interest = round($interest, 5);
                }
                $loan_schedule->principal = round(($principal), 5);
                $loan_schedule->due = round(($principal + $interest), 5);
                //determine next balance
                $balance = round(($balance - $principal), 2);
                $loan_schedule->principal_balance = round($balance, 5);
            } else {
                $interest = round(($interest_rate * $loan->principal));
                if ($loan->grace_on_interest_charged >= $i) {
                    $loan_schedule->interest = 0;
                } else {
                    $loan_schedule->interest = round($interest);
                }
                $loan_schedule->principal = round($principal);
                $loan_schedule->due = round($principal + $interest);
                //determine next balance
                $balance = round(($balance - $principal));
                $loan_schedule->principal_balance = round($balance);
            }
        }
        //interest only method
        if ($loan->interest_method == 'interest_only') {
            if ($i == $period) {
                $principal = $loan->principal;
            } else {
                $principal = 0;
            }
            if ($loan->decimal_places == 'round_off_to_two_decimal') {
                $interest = round(($interest_rate * $loan->principal), 2);
                if ($loan->grace_on_interest_charged >= $i) {
                    $loan_schedule->interest = 0;
                } else {
                    $loan_schedule->interest = round($interest, 2);
                }
                $loan_schedule->principal = round(($principal), 2);
                $loan_schedule->due = round(($principal + $interest), 2);
                //determine next balance
                $balance = round(($balance - $principal), 2);
                $loan_schedule->principal_balance = round($balance, 2);
            } else {
                $interest = round(($interest_rate * $loan->principal));
                if ($loan->grace_on_interest_charged >= $i) {
                    $loan_schedule->interest = 0;
                } else {
                    $loan_schedule->interest = round($interest);
                }
                $loan_schedule->principal = round($principal);
                $loan_schedule->due = round($principal + $interest);
                //determine next balance
                $balance = round(($balance - $principal));
                $loan_schedule->principal_balance = round($balance);
            }
        }
        //determine next due date
        if ($loan->repayment_cycle == 'daily') {
            $next_payment = date_format(date_add(date_create($next_payment),
                                                 date_interval_create_from_date_string('1 days')),
                                        'Y-m-d');
            //$loan_schedule->due_date = $next_payment;
        }
        if ($loan->repayment_cycle == 'weekly') {
            $next_payment = date_format(date_add(date_create($next_payment),
                                                 date_interval_create_from_date_string('1 weeks')),
                                        'Y-m-d');
            //$loan_schedule->due_date = $next_payment;
        }
        if ($loan->repayment_cycle == 'monthly') {
            $next_payment = date_format(date_add(date_create($next_payment),
                                                 date_interval_create_from_date_string('1 months')),
                                        'Y-m-d');
            //$loan_schedule->due_date = $next_payment;
        }
        if ($loan->repayment_cycle == 'bi_monthly') {
            $next_payment = date_format(date_add(date_create($next_payment),
                                                 date_interval_create_from_date_string('2 months')),
                                        'Y-m-d');
            //$loan_schedule->due_date = $next_payment;
        }
        if ($loan->repayment_cycle == 'quarterly') {
            $next_payment = date_format(date_add(date_create($next_payment),
                                                 date_interval_create_from_date_string('4 months')),
                                        'Y-m-d');
            //$loan_schedule->due_date = $next_payment;
        }
        if ($loan->repayment_cycle == 'semi_annually') {
            $next_payment = date_format(date_add(date_create($next_payment),
                                                 date_interval_create_from_date_string('6 months')),
                                        'Y-m-d');
            //$loan_schedule->due_date = $next_payment;
        }
        if ($loan->repayment_cycle == 'yearly') {
            $next_payment = date_format(date_add(date_create($next_payment),
                                                 date_interval_create_from_date_string('1 years')),
                                        'Y-m-d');
            //$loan_schedule->due_date = $next_payment;
        }
        if ($i == $period) {
            $loan_schedule->principal_balance = round($balance);
        }
        $loan_schedule->save();
    }
    $loan = Loan::find($loan->id);
    $loan->maturity_date = $next_payment;
    $loan->save();
    }
        // $loan->status = 'approved';
        // $loan->approved_date = $request->approved_date;
        // $loan->approved_notes = $request->approved_notes;
        // $loan->approved_by_id = Sentinel::getUser()->id;
        // $loan->approved_amount = $request->approved_amount;
        // $loan->principal = $request->approved_amount;
        // $loan->save();

        GeneralHelper::audit_trail("Approved loan with id:" . $id);
        Flash::success(trans('general.successfully_saved'));
        return redirect()->back();
    }


    public function edit($borrower_group)
    {

        $of = DB::table("loan_officers2")->where(["branch_id"=>$borrower_group->id])->select("loan_officers2.officer_id")->get();

        $off = [];
        foreach($of as $o){
            array_push($off,$o->officer_id);
        }

        $users = User::all();
        $user = array();
        foreach ($users as $key) {
            $user[$key->id] = $key->first_name . ' ' . $key->last_name;
        }
        return view('borrower.group.edit', compact('borrower_group','off','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        DB::table("loan_officers2")->where(["branch_id"=>$id])->delete();
        $group = BorrowerGroup::find($id);
        $group->name = $request->name;
        $group->notes = $request->notes;

        $group->save();
        $loan_officers = serialize($request->loan_officers);
        foreach(unserialize($loan_officers) as $u){
            DB::table("loan_officers2")->insert(["branch_id"=>$group->id,"officer_id"=>$u]);
        }
        Flash::success(trans('general.successfully_saved'));
        return redirect('borrower/group/data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        BorrowerGroup::destroy($id);
        Flash::success(trans('general.successfully_deleted'));
        return redirect('borrower/group/data');
    }

    public function addBorrower(Request $request, $id)
    {
        if(BorrowerGroupMember::where('borrower_id',$request->borrower_id)->count()>0){
            Flash::warning(trans('general.borrower_already_added_to_group'));
            return redirect()->back();
        }
        $member = new BorrowerGroupMember();
        $member->borrower_group_id = $id;
        $member->borrower_id = $request->borrower_id;
        $member->save();
        Flash::success(trans('general.successfully_saved'));
        return redirect()->back();
    }
    public function removeBorrower(Request $request, $id)
    {
        BorrowerGroupMember::destroy($id);
        Flash::success(trans('general.successfully_saved'));
        return redirect()->back();
    }
public function repayment(Request $request){
    $id = $request->sc;
    $check = DB::table("grouploan")->where(["sc"=>$id,"status"=>"disbursed"])->exists();
    if($check){
           $group = DB::table("grouploan")->where(["sc"=>$id,"status"=>"approved"])->first();
        $loan = DB::table("loans")->where(["sc"=>$group->sc,"status"=>"approved"])->first();
           if (!Sentinel::hasAccess('repayments.create')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $repayment_methods = array();
        foreach (LoanRepaymentMethod::all() as $key) {
            $repayment_methods[$key->id] = $key->name;
        }

        $custom_fields = CustomField::where('category', 'repayments')->get();

        return view("borrower.group.repayment", compact('loan', 'repayment_methods', 'custom_fields','id'));
    }else{
        Flash::warning("Group Loan Not Found!");
        return redirect("/borrower/group/data");
    }
}
public function makerepay(Request $request){
    
   $users = DB::table("loans")->where(["sc"=>$request->sc])->get();
   $group = DB::table("grouploan")->where(["sc"=>$request->sc])->first();
   
   
  
    $am= $group->loan_amount - $group->balance;
   if($request->amount > $am){
      
       Flash::warning("Amount Can't Be Greater Than $am");
       return redirect()->back();
   }else{
   $repaymentt = DB::table("loan_repayment_methods")->where(["id"=>$request->repayment_method_id])->first();
   $store = DB::table("repaymentgrouphis")->insert(["userid"=>Sentinel::getUser()->id,"amount"=>$request->amount,"date"=>$request->collection_date,"type"=>$repaymentt->name,"groupid"=>$group->group_id,"sc"=>$group->sc]);
   $his = DB::table("repaymentgrouphis")->where(["sc"=>$request->sc])->sum("amount");
   
   
 
   
  if($group->loan_amount == $his){
      DB::table("grouploan")->where(["sc"=>$request->sc])->update(["status"=>"closed","mainamount"=>'']);
      DB::table("grouploan")->where(["sc"=>$request->sc])->update(["balance"=>$his]);
      
      DB::table("loans")->where(["sc"=>$request->sc])->update(["balance"=>$his,"status"=>"closed","loan_status"=>"closed","owing"=>null]); 
       
    //   DB::table("loans")->where(["sc"=>$request->sc])->update([]);   
                
  }else{
     DB::table("grouploan")->where(["sc"=>$request->sc])->update(["balance"=>$his]);
   
      DB::table("loans")->where(["sc"=>$request->sc])->update(["balance"=>$his]); 
                
}
            //notify borrower


        
        GeneralHelper::audit_trail("Added  group repayment for $group->sc");
        Flash::success("Repayment successfully saved, Total Balance $his ");
        return redirect()->back();
   }
         
}

    
 
    public function storeRepayment(Request $request, $loan)
    {



        if (!Sentinel::hasAccess('repayments.create')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }

        $loans = Loan::where(["sc"=>$request->sc])->get();
        $amount = 0;
        foreach($loans as $loan){
        $amount += $request->amount;
            if ($request->amount > round(GeneralHelper::loan_total_balance($loan->id), 2)) {
            Flash::warning("Amount is more than the balance(" . GeneralHelper::loan_total_balance($loan->id) . ')');
            return redirect()->back()->withInput();

        } else {

            $repayment = new LoanRepayment();
            $repayment->user_id = Sentinel::getUser()->id;
            $repayment->amount = $request->amount;
            $repayment->loan_id = $loan->id;
            $repayment->borrower_id = $loan->borrower_id;
            $repayment->branch_id = session('branch_id');
            $repayment->collection_date = \Carbon\Carbon::parse($request->collection_date);
            $repayment->repayment_method_id = $request->repayment_method_id;
            $repayment->notes = $request->notes;
            $date = explode('-', $request->collection_date);

            $repayment->year = (int)$date[0];
            $repayment->month = (int)$date[1];

            //determine which schedule due date the payment applies too
            $schedule = LoanSchedule::where('due_date', '>=', $request->collection_date)->where('loan_id',
                                                                                                $loan->id)->orderBy('due_date',
                                                                                                                    'asc')->first();
            if (!empty($schedule)) {
                $repayment->due_date = $schedule->due_date;
            } else {
                $schedule = LoanSchedule::where('loan_id',
                                                $loan->id)->orderBy('due_date',
                                                                    'desc')->first();
                if ($request->collection_date > $schedule->due_date) {
                    $repayment->due_date = $schedule->due_date;
                } else {
                    $schedule = LoanSchedule::where('due_date', '>', $request->collection_date)->where('loan_id',
                                                                                                       $loan->id)->orderBy('due_date',
                                                                                                                           'asc')->first();
                    $repayment->due_date = $schedule->due_date;
                }

            }
            $repayment->save();
            //save custom meta
            $custom_fields = CustomField::where('category', 'repayments')->get();
            foreach ($custom_fields as $key) {
                $custom_field = new CustomFieldMeta();
                $id = $key->id;
                $custom_field->name = $request->$id;
                $custom_field->parent_id = $repayment->id;
                $custom_field->custom_field_id = $key->id;
                $custom_field->category = "repayments";
                $custom_field->save();
            }
            //update loan status if need be
            if (round(GeneralHelper::loan_total_balance($loan->id), 2) == 0) {
                $l = Loan::find($loan->id);
                $l->status = "closed";
                $l->save();

            }
            //check if late repayment is to be applied when adding payment
            if ($request->apply_penalty == 1) {
                if (!empty($loan->loan_product)) {
                    if ($loan->loan_product->enable_late_repayment_penalty == 1) {
                        $schedules = LoanSchedule::where('due_date', '<',
                                                         $repayment->due_date)->where('missed_penalty_applied',
                                                                                      0)->orderBy('due_date', 'asc')->get();
                        foreach ($schedules as $schedule) {
                            if (GeneralHelper::loan_total_due_period($loan->id,
                                                                     $schedule->due_date) > GeneralHelper::loan_total_paid_period($loan->id,
                                                                                                                                  $schedule->due_date)
                               ) {
                                $sch = LoanSchedule::find($schedule->id);
                                $sch->missed_penalty_applied = 1;
                                //determine which amount to use
                                if ($loan->loan_product->late_repayment_penalty_type == "fixed") {
                                    $sch->penalty = $sch->penalty + $loan->loan_product->late_repayment_penalty_amount;
                                } else {
                                    if ($loan->loan_product->late_repayment_penalty_calculate == 'overdue_principal') {
                                        $principal = (GeneralHelper::loan_total_principal($loan->id,
                                                                                          $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                                                                                                               'principal', $schedule->due_date));
                                        $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                    }
                                    if ($loan->loan_product->late_repayment_penalty_calculate == 'overdue_principal_interest') {
                                        $principal = (GeneralHelper::loan_total_principal($loan->id,
                                                                                          $schedule->due_date) + GeneralHelper::loan_total_interest($loan->id,
                                                                                                                                                    $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                'principal',
                                                $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                                                                     'interest', $schedule->due_date));
                                        $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                    }
                                    if ($loan->loan_product->late_repayment_penalty_calculate == 'overdue_principal_interest_fees') {
                                        $principal = (GeneralHelper::loan_total_principal($loan->id,
                                                                                          $schedule->due_date) + GeneralHelper::loan_total_interest($loan->id,
                                                                                                                                                    $schedule->due_date) + GeneralHelper::loan_total_fees($loan->id,
                                                $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                                                                     'principal',
                                                                                                     $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                'interest',
                                                $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id, 'fees',
                                                                                                     $schedule->due_date));
                                        $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                    }
                                    if ($loan->loan_product->late_repayment_penalty_calculate == 'total_overdue') {
                                        $principal = (GeneralHelper::loan_total_due_amount($loan->id,
                                                                                           $schedule->due_date) - GeneralHelper::loan_total_paid($loan->id,
                                                                                                                                                 $schedule->due_date));
                                        $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                    }
                                }
                                $sch->save();
                            }
                        }
                    }
                }
            }
            //notify borrower
            if ($request->notify_borrower == 1) {
                if ($request->notify_method == 'both') {
                    $borrower = $loan->borrower;
                    //sent via email
                    if (!empty($borrower->email)) {
                        $body = Setting::where('setting_key',
                                               'payment_received_email_template')->first()->setting_value;
                        $body = str_replace('{borrowerTitle}', $borrower->title, $body);
                        $body = str_replace('{borrowerFirstName}', $borrower->first_name, $body);
                        $body = str_replace('{borrowerLastName}', $borrower->last_name, $body);
                        $body = str_replace('{borrowerAddress}', $borrower->address, $body);
                        $body = str_replace('{borrowerUniqueNumber}', $borrower->unique_number, $body);
                        $body = str_replace('{borrowerMobile}', $borrower->mobile, $body);
                        $body = str_replace('{borrowerPhone}', $borrower->phone, $body);
                        $body = str_replace('{borrowerEmail}', $borrower->email, $body);
                        $body = str_replace('{loanNumber}', '#' . $loan->id, $body);
                        $body = str_replace('{paymentAmount}', $request->amount, $body);
                        $body = str_replace('{paymentDate}', $request->date, $body);
                        $body = str_replace('{loanAmount}', $loan->principal, $body);
                        $body = str_replace('{loanDue}',
                                            round(GeneralHelper::loan_total_due_amount($loan->id), 2), $body);
                        $body = str_replace('{loanBalance}',
                                            round(GeneralHelper::loan_total_due_amount($loan->id) - GeneralHelper::loan_total_paid($loan->id),
                                                  2), $body);
                        $body = str_replace('{loansDue}',
                                            round(GeneralHelper::borrower_loans_total_due($borrower->id), 2), $body);
                        $body = str_replace('{loansBalance}',
                                            round((GeneralHelper::borrower_loans_total_due($borrower->id) - GeneralHelper::borrower_loans_total_paid($borrower->id)),
                                                  2), $body);
                        $body = str_replace('{loansPayments}',
                                            GeneralHelper::borrower_loans_total_paid($borrower->id),
                                            $body);
                        PDF::AddPage();
                        PDF::writeHTML(View::make('loan_repayment.pdf', compact('loan', 'repayment'))->render());
                        PDF::SetAuthor('Tererai Mugova');
                        PDF::Output(public_path() . '/uploads/temporary/repayment_receipt' . $loan->id . ".pdf", 'F');
                        $file_name = $loan->borrower->title . ' ' . $loan->borrower->first_name . ' ' . $loan->borrower->last_name . " - Repayment Receipt.pdf";
                        Mail::raw($body, function ($message) use ($loan, $request, $borrower, $file_name) {
                            $message->from(Setting::where('setting_key', 'company_email')->first()->setting_value,
                                           Setting::where('setting_key', 'company_name')->first()->setting_value);
                            $message->to($borrower->email);
                            $headers = $message->getHeaders();
                            $message->attach(public_path() . '/uploads/temporary/repayment_receipt' . $loan->id . ".pdf",
                                             ["as" => $file_name]);
                            $message->setContentType('text/html');
                            $message->setSubject(Setting::where('setting_key',
                                                                'payment_received_email_subject')->first()->setting_value);

                        });
                        unlink(public_path() . '/uploads/temporary/repayment_receipt' . $loan->id . ".pdf");
                        $mail = new Email();
                        $mail->user_id = Sentinel::getUser()->id;
                        $mail->message = $body;
                        $mail->subject = $request->subject;
                        $mail->recipients = 1;
                        $mail->send_to = $borrower->first_name . ' ' . $borrower->last_name . '(' . $borrower->unique_number . ')';
                        $mail->save();
                    }
                    if (!empty($borrower->mobile)) {
                        if (Setting::where('setting_key', 'sms_enabled')->first()->setting_value == 1) {
                            //lets build and replace available tags
                            $body = Setting::where('setting_key',
                                                   'payment_received_sms_template')->first()->setting_value;
                            $body = str_replace('{borrowerTitle}', $borrower->title, $body);
                            $body = str_replace('{borrowerFirstName}', $borrower->first_name, $body);
                            $body = str_replace('{borrowerLastName}', $borrower->last_name, $body);
                            $body = str_replace('{borrowerAddress}', $borrower->address, $body);
                            $body = str_replace('{borrowerMobile}', $borrower->mobile, $body);
                            $body = str_replace('{borrowerEmail}', $borrower->email, $body);
                            $body = str_replace('{loanNumber}', '#' . $loan->id, $body);
                            $body = str_replace('{paymentAmount}', $request->amount, $body);
                            $body = str_replace('{paymentDate}', $request->date, $body);
                            $body = str_replace('{loanAmount}', $loan->principal, $body);
                            $body = str_replace('{loanTotalDue}',
                                                round(GeneralHelper::loan_total_due_amount($loan->id), 2), $body);
                            $body = str_replace('{loanBalance}',
                                                round(GeneralHelper::loan_total_due_amount($loan->id) - GeneralHelper::loan_total_paid($loan->id),
                                                      2), $body);
                            $body = str_replace('{lLoansDue}',
                                                round(GeneralHelper::borrower_loans_total_due($borrower->id), 2), $body);
                            $body = str_replace('{loansBalance}',
                                                round((GeneralHelper::borrower_loans_total_due($borrower->id) - GeneralHelper::borrower_loans_total_paid($borrower->id)),
                                                      2), $body);
                            $body = str_replace('{loansPayments}',
                                                GeneralHelper::borrower_loans_total_paid($borrower->id),
                                                $body);
                            $body = trim(strip_tags($body));
                            if (!empty($borrower->mobile)) {
                                $active_sms = Setting::where('setting_key', 'active_sms')->first()->setting_value;
                                if ($active_sms == 'twilio') {
                                    $twilio = new Twilio(Setting::where('setting_key',
                                                                        'twilio_sid')->first()->setting_value,
                                                         Setting::where('setting_key', 'twilio_token')->first()->setting_value,
                                                         Setting::where('setting_key', 'twilio_phone_number')->first()->setting_value);
                                    $twilio->message('+' . $borrower->mobile, $body);
                                }
                                if ($active_sms == 'routesms') {
                                    $host = Setting::where('setting_key', 'routesms_host')->first()->setting_value;
                                    $port = Setting::where('setting_key', 'routesms_port')->first()->setting_value;
                                    $username = Setting::where('setting_key',
                                                               'routesms_username')->first()->setting_value;
                                    $password = Setting::where('setting_key',
                                                               'routesms_password')->first()->setting_value;
                                    $sender = Setting::where('setting_key', 'sms_sender')->first()->setting_value;
                                    $SMSText = $body;
                                    $GSM = $borrower->mobile;
                                    $msgtype = 2;
                                    $dlr = 1;
                                    $routesms = new RouteSms($host, $port, $username, $password, $sender, $SMSText,
                                                             $GSM, $msgtype,
                                                             $dlr);
                                    $routesms->Submit();
                                }
                                if ($active_sms == 'clickatell') {
                                    $clickatell = new Rest(Setting::where('setting_key',
                                                                          'clickatell_api_id')->first()->setting_value);
                                    $response = $clickatell->sendMessage(array($borrower->mobile), $body);
                                }
                                if ($active_sms == 'infobip') {
                                    $infobip = new Infobip(Setting::where('setting_key',
                                                                          'sms_sender')->first()->setting_value, $body,
                                                           $borrower->mobile);
                                }
                                $sms = new Sms();
                                $sms->user_id = Sentinel::getUser()->id;
                                $sms->message = $body;
                                $sms->gateway = $active_sms;
                                $sms->recipients = 1;
                                $sms->send_to = $borrower->first_name . ' ' . $borrower->last_name . '(' . $borrower->unique_number . ')';
                                $sms->save();

                            }

                        }
                    }
                    //send via sms
                }
                if ($request->notify_method == 'email') {
                    $borrower = $loan->borrower;
                    //sent via email
                    if (!empty($borrower->email)) {
                        $body = Setting::where('setting_key',
                                               'payment_received_email_template')->first()->setting_value;
                        $body = str_replace('{borrowerTitle}', $borrower->title, $body);
                        $body = str_replace('{borrowerFirstName}', $borrower->first_name, $body);
                        $body = str_replace('{borrowerLastName}', $borrower->last_name, $body);
                        $body = str_replace('{borrowerAddress}', $borrower->address, $body);
                        $body = str_replace('{borrowerUniqueNumber}', $borrower->unique_number, $body);
                        $body = str_replace('{borrowerMobile}', $borrower->mobile, $body);
                        $body = str_replace('{borrowerPhone}', $borrower->phone, $body);
                        $body = str_replace('{borrowerEmail}', $borrower->email, $body);
                        $body = str_replace('{loanNumber}', '#' . $loan->id, $body);
                        $body = str_replace('{paymentAmount}', $request->amount, $body);
                        $body = str_replace('{paymentDate}', $request->date, $body);
                        $body = str_replace('{loanAmount}', $loan->principal, $body);
                        $body = str_replace('{loanDue}',
                                            round(GeneralHelper::loan_total_due_amount($loan->id), 2), $body);
                        $body = str_replace('{loanBalance}',
                                            round(GeneralHelper::loan_total_due_amount($loan->id) - GeneralHelper::loan_total_paid($loan->id),
                                                  2), $body);
                        $body = str_replace('{loansDue}',
                                            round(GeneralHelper::borrower_loans_total_due($borrower->id), 2), $body);
                        $body = str_replace('{loansBalance}',
                                            round((GeneralHelper::borrower_loans_total_due($borrower->id) - GeneralHelper::borrower_loans_total_paid($borrower->id)),
                                                  2), $body);
                        $body = str_replace('{loansPayments}',
                                            GeneralHelper::borrower_loans_total_paid($borrower->id),
                                            $body);
                        PDF::AddPage();
                        PDF::writeHTML(View::make('loan_repayment.pdf', compact('loan', 'repayment'))->render());
                        PDF::SetAuthor('Tererai Mugova');
                        PDF::Output(public_path() . '/uploads/temporary/repayment_receipt' . $loan->id . ".pdf", 'F');
                        $file_name = $loan->borrower->title . ' ' . $loan->borrower->first_name . ' ' . $loan->borrower->last_name . " - Repayment Receipt.pdf";
                        Mail::raw($body, function ($message) use ($loan, $request, $borrower, $file_name) {
                            $message->from(Setting::where('setting_key', 'company_email')->first()->setting_value,
                                           Setting::where('setting_key', 'company_name')->first()->setting_value);
                            $message->to($borrower->email);
                            $headers = $message->getHeaders();
                            $message->attach(public_path() . '/uploads/temporary/repayment_receipt' . $loan->id . ".pdf",
                                             ["as" => $file_name]);
                            $message->setContentType('text/html');
                            $message->setSubject(Setting::where('setting_key',
                                                                'payment_received_email_subject')->first()->setting_value);

                        });
                        unlink(public_path() . '/uploads/temporary/repayment_receipt' . $loan->id . ".pdf");
                        $mail = new Email();
                        $mail->user_id = Sentinel::getUser()->id;
                        $mail->message = $body;
                        $mail->subject = $request->subject;
                        $mail->recipients = 1;
                        $mail->send_to = $borrower->first_name . ' ' . $borrower->last_name . '(' . $borrower->unique_number . ')';
                        $mail->save();
                    }
                }
                if ($request->notify_method == 'sms') {
                    $borrower = $loan->borrower;
                    if (!empty($borrower->mobile)) {
                        if (Setting::where('setting_key', 'sms_enabled')->first()->setting_value == 1) {
                            //lets build and replace available tags
                            $body = Setting::where('setting_key',
                                                   'payment_received_sms_template')->first()->setting_value;
                            $body = str_replace('{borrowerTitle}', $borrower->title, $body);
                            $body = str_replace('{borrowerFirstName}', $borrower->first_name, $body);
                            $body = str_replace('{borrowerLastName}', $borrower->last_name, $body);
                            $body = str_replace('{borrowerAddress}', $borrower->address, $body);
                            $body = str_replace('{borrowerMobile}', $borrower->mobile, $body);
                            $body = str_replace('{borrowerEmail}', $borrower->email, $body);
                            $body = str_replace('{loanNumber}', '#' . $loan->id, $body);
                            $body = str_replace('{paymentAmount}', $request->amount, $body);
                            $body = str_replace('{paymentDate}', $request->date, $body);
                            $body = str_replace('{loanAmount}', $loan->principal, $body);
                            $body = str_replace('{loanTotalDue}',
                                                round(GeneralHelper::loan_total_due_amount($loan->id), 2), $body);
                            $body = str_replace('{loanBalance}',
                                                round(GeneralHelper::loan_total_due_amount($loan->id) - GeneralHelper::loan_total_paid($loan->id),
                                                      2), $body);
                            $body = str_replace('{lLoansDue}',
                                                round(GeneralHelper::borrower_loans_total_due($borrower->id), 2), $body);
                            $body = str_replace('{loansBalance}',
                                                round((GeneralHelper::borrower_loans_total_due($borrower->id) - GeneralHelper::borrower_loans_total_paid($borrower->id)),
                                                      2), $body);
                            $body = str_replace('{loansPayments}',
                                                GeneralHelper::borrower_loans_total_paid($borrower->id),
                                                $body);
                            $body = trim(strip_tags($body));
                            if (!empty($borrower->mobile)) {
                                $active_sms = Setting::where('setting_key', 'active_sms')->first()->setting_value;
                                if ($active_sms == 'twilio') {
                                    $twilio = new Twilio(Setting::where('setting_key',
                                                                        'twilio_sid')->first()->setting_value,
                                                         Setting::where('setting_key', 'twilio_token')->first()->setting_value,
                                                         Setting::where('setting_key', 'twilio_phone_number')->first()->setting_value);
                                    $twilio->message('+' . $borrower->mobile, $body);
                                }
                                if ($active_sms == 'routesms') {
                                    $host = Setting::where('setting_key', 'routesms_host')->first()->setting_value;
                                    $port = Setting::where('setting_key', 'routesms_port')->first()->setting_value;
                                    $username = Setting::where('setting_key',
                                                               'routesms_username')->first()->setting_value;
                                    $password = Setting::where('setting_key',
                                                               'routesms_password')->first()->setting_value;
                                    $sender = Setting::where('setting_key', 'sms_sender')->first()->setting_value;
                                    $SMSText = $body;
                                    $GSM = $borrower->mobile;
                                    $msgtype = 2;
                                    $dlr = 1;
                                    $routesms = new RouteSms($host, $port, $username, $password, $sender, $SMSText,
                                                             $GSM, $msgtype,
                                                             $dlr);
                                    $routesms->Submit();
                                }
                                if ($active_sms == 'clickatell') {
                                    $clickatell = new Rest(Setting::where('setting_key',
                                                                          'clickatell_api_id')->first()->setting_value);
                                    $response = $clickatell->sendMessage(array($borrower->mobile), $body);
                                }
                                if ($active_sms == 'infobip') {
                                    $infobip = new Infobip(Setting::where('setting_key',
                                                                          'sms_sender')->first()->setting_value, $body,
                                                           $borrower->mobile);
                                }
                                $sms = new Sms();
                                $sms->user_id = Sentinel::getUser()->id;
                                $sms->message = $body;
                                $sms->gateway = $active_sms;
                                $sms->recipients = 1;
                                $sms->send_to = $borrower->first_name . ' ' . $borrower->last_name . '(' . $borrower->unique_number . ')';
                                $sms->save();

                            }

                        }
                    }
                }
            }

            $lbalance = DB::table("loan_repayments")->where(["loan_id"=>$loan->id])->sum("amount");
            if($lbalance == round(\App\Helpers\GeneralHelper::loan_total_due_amount($loan->id),2)){
                DB::table("loans")->where(["id"=>$loan->id])->update(["status"=>"closed"]);
            }

        }
            }
        $group = DB::table("grouploan")->where(["sc"=>$request->sc])->first();
        DB::table("grouploan")->where(["sc"=>$request->sc])->increment("balance",$amount);
        DB::table("repaymentgrouphis")->insert([
            "amount"=>$amount,
            "type"=>$request->repayment_method_id,
            "date"=>\Carbon\Carbon::parse($request->collection_date),
            "group_id"=>$group->group_id,
            "sc"=>$request->sc,
            "userid"=>Sentinel::getUser()->id
        ]);
    GeneralHelper::audit_trail("Added repayment for group loan with id:" . $request->sc);
            Flash::success("Repayment successfully saved");
            return redirect()->back();
        
    }
    
}
