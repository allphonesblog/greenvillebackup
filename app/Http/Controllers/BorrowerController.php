<?php

namespace App\Http\Controllers;
 
use Aloha\Twilio\Twilio;
use App\Helpers\GeneralHelper;
use App\Models\Borrower;
use App\Models\Saving;
use App\Models\CustomField;
use App\Models\CustomFieldMeta;
use App\Models\Loan;
use App\Models\LoanRepayment;
use App\Models\SavingProduct;
use App\Models\Setting;
use App\Models\User;
use App\Models\BorrowerGroupMember;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Clickatell\Api\ClickatellHttp;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\SavingTransaction;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;
use DB;

class BorrowerController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentinel');

    }

    public function approvetransfer(Request $request){
        $id = $request->id;
        $b = DB::table("transfers")->where(["id"=>$id])->first();

        DB::table("borrowers")->where(["id"=>$b->borrower_id])->update(["branch_id"=>$b->branch_id]);
        DB::table("loans")->where(["borrower_id"=>$b->borrower_id])->update(["branch_id"=>$b->branch_id]);
        DB::table("savings")->where(["borrower_id"=>$b->borrower_id])->update(["branch_id"=>$b->branch_id]);
        DB::table("savings_transactions")->where(["borrower_id"=>$b->borrower_id])->update(["branch_id"=>$b->branch_id]);
        DB::table("loan_repayments")->where(["borrower_id"=>$b->borrower_id])->update(["branch_id"=>$b->branch_id]);
        DB::table("loan_schedules")->where(["borrower_id"=>$b->borrower_id])->update(["branch_id"=>$b->branch_id]);
        DB::table("transfers")->where(["id"=>$id])->delete();
        Flash::success("SuccessFully Transfered");
        return redirect("/borrower/transfers");
    }
    
    
    public function bulkapprove(Request $request){
        if (!Sentinel::hasAccess('loans.approve')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $ids = $request->ids;
        if($ids == []){
            return response()->json(["message"=>"Something Went Wrong, Or Invalid Selection!"],400);
        }

        foreach($ids as $id){

            $b = DB::table("transfers")->where(["id"=>$id])->first();

            DB::table("borrowers")->where(["id"=>$b->borrower_id])->update(["branch_id"=>$b->branch_id]);
            DB::table("loans")->where(["borrower_id"=>$b->borrower_id])->update(["branch_id"=>$b->branch_id]);
            DB::table("savings")->where(["borrower_id"=>$b->borrower_id])->update(["branch_id"=>$b->branch_id]);
            DB::table("savings_transactions")->where(["borrower_id"=>$b->borrower_id])->update(["branch_id"=>$b->branch_id]);
            DB::table("loan_repayments")->where(["borrower_id"=>$b->borrower_id])->update(["branch_id"=>$b->branch_id]);
            DB::table("loan_schedules")->where(["borrower_id"=>$b->borrower_id])->update(["branch_id"=>$b->branch_id]);
            DB::table("transfers")->where(["id"=>$id])->delete();


            }

        return response()->json(["message"=>"Approved SuccessFully"]);
    }

    public function disapprovebulk(Request $request){
        if (!Sentinel::hasAccess('loans.approve')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $ids = $request->ids;
        if($ids == []){
            return response()->json(["message"=>"Something Went Wrong, Or Invalid Selection!"],400);
        }

        foreach($ids as $id){


        $b = DB::table("transfers")->where(["id"=>$id])->delete();


            return response()->json(["message"=>"Disapproved SuccessFully"]);
            

        }

        return response()->json(["message"=>"Approved SuccessFully"]);
    }

    public function disapprovetransfer(Request $request){
       
        $b = DB::table("transfers")->where(["id"=>$id])->delete();

        Flash::success("SuccessFully Disapproved");
        return redirect("/borrower/transfers");
    }
    public function ptransfer(){
        $b = DB::table("transfers")->where(["transfers.status"=>"pending","transfers.branch_id"=>session("branch_id")])
            ->join("borrowers","borrowers.id","=","transfers.borrower_id")
            ->select("borrowers.first_name as bfname","borrowers.last_name as blname","transfers.*")
            ->get();
        return view("borrower.pendingtransfer",compact("b"));
    }
    
    public function transfer(Request $request){
            $id = $request->id;
        $branch = $request->branch;
        if($branch == session("branch_id")){
            Flash::warning("You Can't Transfer To Same Branch");
            return redirect("/borrower/$id/show");
        }
        $b = DB::table("borrowers")->where(["id"=>$id])->first();
        if($b != null){
            if(!DB::table("transfers")->where(["from_branch"=>$branch,"borrower_id"=>$id])->exists()){
                DB::table("transfers")->insert([
                    "borrower_id"=>$id,
                    "branch_id"=>$branch,
                    "from_branch"=>$b->branch_id

                ]);
                Flash::warning("Transfer Pending");
                return redirect("/borrower/$id/show");
            }else{
                return redirect("/borrower/$id/show");
            }

        }
    }
    
    public function bulktransfer(Request $request){
        $ids = $request->ids;
        $branch = $request->branch;
        
        
        if($ids == []){

                return response()->json(["message"=>"Something went wrong!"],400);


        }

        foreach($ids as $id){
        if($branch == session("branch_id")){

            return response()->json(["message"=>"You Can't Transfer To Same Branch"],400);
            return redirect("/borrower/$id/show");
        }
        $b = DB::table("borrowers")->where(["id"=>$id])->first();
        if($b != null){
            if(!DB::table("transfers")->where(["from_branch"=>$branch,"borrower_id"=>$id])->exists()){
                DB::table("transfers")->insert([
                    "borrower_id"=>$id,
                    "branch_id"=>$branch,
                    "from_branch"=>$b->branch_id

                ]);


            }else{

            }

        }
            }
       return response()->json(["message"=>"Transfered successful"]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function borrowers(){
        $datas = [];
        $actions = "";
        $actions2 = "";
        $actions3 = "";
        $actions4 = "";
        $actions5 = "";
        $actions6 = "";
        $actions7 = "";
        $text = $_GET['loan_officer'];
        $text2 = $_GET['date'];
        $b = DB::table("borrowers")->orderBy("id","DESC")->where(["branch_id"=>session("branch_id")])->get();

        if(!empty($text)){

            $b = DB::table("loan_officers")->join("borrowers","borrowers.id","=","loan_officers.borrower_id")->join("users","users.id","=","loan_officers.officer_id")
                ->orderBy("id","DESC")   ->where(["borrowers.branch_id"=>session("branch_id"),"users.id"=>$text])
                ->select("borrowers.*")->get();

            $date =  $request->date;
        }
        if(!empty($text2)){
            $b = DB::table("loan_officers")->join("borrowers","borrowers.id","=","loan_officers.borrower_id")->join("users","users.id","=","loan_officers.officer_id")
                ->orderBy("id","DESC")  ->where(["borrowers.branch_id"=>session("branch_id")])->where('borrowers.created_at', 'like', '%' . $text2 . '%')->select("borrowers.*")->get();
        }

        if(!empty($text) && !empty($text2)){
            $b = DB::table("loan_officers")->join("borrowers","borrowers.id","=","loan_officers.borrower_id")->join("users","users.id","=","loan_officers.officer_id")
                ->where(["borrowers.branch_id"=>session("branch_id")]) ->orderBy("id","DESC")

                ->where(function ($query) use($text) {
                    $query
                        ->where('borrowers.created_at', 'like', '%' . $text2 . '%')
                        ->where('users.id', $text );


                })
                ->select("borrowers.*")->get();


        }

        foreach($b as $key){
            $b = DB::table("branches")->where(["id"=>$key->branch_id])->first();
            $u = DB::table("users")->where(["id"=>unserialize($key->loan_officers)])->first();
            $lname = "";
            $bname = "";
            $lid = "";
            $data = [];
            if($b){
                $bname = $b->name;
            }
            if(unserialize($key->loan_officers)){

                $data = unserialize($key->loan_officers);
            }
            $btn1 = [];
            foreach($data as $d){
                  $u = DB::table("users")->where(["id"=>$d])->first();


                $btn1x = $u->first_name.' '.$u->last_name;
                array_push($btn1,$btn1x);
            }

            if($key->active==1){
                $btn2 =  '<span class="label label-success">'.trans_choice('general.active',1).'</span>';
            }
            if($key->active==0){
                $btn2 =  '<span class="label label-success">'.trans_choice('general.pending',1).'</span>';
            }

            $name = '<a href="/borrower/'.$key->id.'/show">'.$key->first_name.' '.$key->last_name.'</a><br>
<br>';
            if($key->active==0){
                if(Sentinel::hasAccess('borrowers.approve')){
                    $acions =  '<li><a href="'.url('borrower/'.$key->id.'/approve').'"><i
                class="fa fa-check"></i> '.trans_choice('general.approve',1).'
            </a></li>';
                }
            }
            if($key->active==1){
                if(Sentinel::hasAccess('borrowers.approve')){
                    $actions2 = ' <li><a href="'.url('borrower/'.$key->id.'/decline').'"><i
                class="fa fa-minus-circle"></i> '.trans_choice('general.decline',1).'
            </a></li>';
                }
            }
            if(Sentinel::hasAccess('borrowers.blacklist')){
                if($key->blacklisted==1){
                    $actions3 = '  <li><a href="'.url('borrower/'.$key->id.'/unblacklist').'"
                class="delete"><i
                class="fa fa-check"></i>'.trans_choice('general.undo',1).' '.trans_choice('general.blacklist',1).'
            </a>
                </li>';
                }
                if($key->blacklisted==0){
                    $actions3 ='     <li>
                <a href="'. url('borrower/'.$key->id.'/blacklist').'"
                class="delete"><i
                class="fa fa-minus-circle"></i> '.trans_choice('general.blacklist',1).'
            </a>
                </li>';
                }
            }
            if(Sentinel::hasAccess('borrowers.view')){
                $actions5 =' <li><a href="'.url('borrower/'.$key->id.'/show').'"><i
                class="fa fa-search"></i> '.trans_choice('general.detail',2).'
            </a></li>';
            }
            if(Sentinel::hasAccess('borrowers.update')){
                $actions6 ='    <li><a href="'.url('borrower/'.$key->id.'/edit').'"><i
                class="fa fa-edit"></i> '.trans('general.edit').'</a>
                </li>';
            }
            if(Sentinel::hasAccess('borrowers.delete')){
                $actions7 = '   <li><a href="'.url('borrower/'.$key->id.'/delete').'" class="delete"><i
                class="fa fa-trash"></i> '.trans('general.delete').'</a>
                </li>';
            }
            $action = '
           <div class="btn-group">
                                    <button type="button" class="btn btn-info btn-xs dropdown-toggle"
                                            data-toggle="dropdown" aria-expanded="false">
                                      '.trans('general.choose').' <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                '.$actions.'
                                  '.$actions2.'
                                    '.$actions3.'
                                      '.$actions4.'
                                        '.$actions5.'
                                          '.$actions6.'
                                            '.$actions7.'
                                    </ul>
                                </div>

        ';
            $select = "<input type='checkbox' value=".$key->id.">";
            array_push($datas,[$select,$btn1,$bname,$name,$key->business_name,$key->unique_number,$key->mobile,$key->email,$btn2,$key->created_at,$action]);
        }
        return response()->json(["data"=>$datas]);
    }

    public function search(Request $request){
        $text = $request->q;
        $find = DB::table("borrowers")->where(["branch_id"=>session("branch_id")])->where(function ($query) use($text) {
            $query
                ->orWhere('first_name', 'like', '%' . $text . '%')
                ->orWhere('last_name', 'like', '%' . $text . '%');
        })->select("first_name","last_name","id")->get();
        
        $f = [];
        foreach($find as $ff){
            array_push($f,[
                "name"=>"$ff->first_name  $ff->last_name",
                "id"=>$ff->id
            ]);
        }
        return response()->json($f);
    }
    public function index(Request $request)
    {

        if (!Sentinel::hasAccess('borrowers')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $data = Borrower::where('branch_id', session('branch_id'))->where("id","!=",null)->latest()->orderBy("id","DESC")->get();
      if(isset($_GET['search'])){
          $text = $_GET['search'];


              $data = DB::table("borrowers")->where('borrowers.branch_id', session('branch_id'))
              ->join("branches","branches.id","=","borrowers.branch_id")

              ->where(function ($query) use($text) {
                  $query
                  ->orWhere('borrowers.first_name', 'like', '%' . $text . '%')
                  ->orWhere('borrowers.last_name', 'like', '%' . $text . '%')
                  ->orWhere('borrowers.business_name', 'like', '%' . $text . '%')
                  ->orWhere('borrowers.unique_number', 'like', '%' . $text . '%')
                  ->orWhere('borrowers.mobile', 'like', '%' . $text . '%')
                  ->orWhere('borrowers.email', 'like', '%' . $text . '%')
                  ->orWhere('borrowers.business_name', 'like', '%' . $text . '%')
              ->orWhere('branches.name', 'like', '%' . $text . '%');

          })->orderBy("borrowers.id","DESC")->select("borrowers.*")->paginate(1000);



      }
        

        $text = $request->loan_officer;
        $text2 = $request->date;
        if(isset($request->loan_officer)){

            $data = DB::table("loan_officers")->join("borrowers","borrowers.id","=","loan_officers.borrower_id")->join("users","users.id","=","loan_officers.officer_id")
                ->where(["borrowers.branch_id"=>session("branch_id"),"users.id"=>$text])

                ->where(function ($query) use($text) {
                    $query
                        ->orWhere('users.first_name', 'like', '%' . $text . '%')
                        ->orWhere('users.id', 'like', '%' . $text . '%')
                        ->orWhere('users.last_name', 'like', '%' . $text . '%');




                })->select("borrowers.*")
                ->get();

            $date =  $request->date;
        }
        if(!empty($text2)){
            $data = DB::table("loan_officers")->join("borrowers","borrowers.id","=","loan_officers.borrower_id")->join("users","users.id","=","loan_officers.officer_id")
                ->where(["borrowers.branch_id"=>session("branch_id")])->where('borrowers.created_at', 'like', '%' . $request->date . '%')->select("borrowers.*")->get();
        }


      $users = DB::table("users")->where(["branch_users.branch_id"=>session("branch_id")])
            ->join("branch_users","branch_users.user_id","=","users.id")
          ->join("role_users","role_users.user_id","=","users.id")->where(["role_users.role_id"=>4])->select("users.first_name","users.last_name","users.id")->get();


        return view('borrower.data', compact('data','date','users'));
    }
    
    public function index2(Request $request)
    {

        if (!Sentinel::hasAccess('borrowers')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }

$text = $request->text;
        $data = Borrower::where('branch_id', session('branch_id')) ->where('first_name', 'like', '%' . $text . '%') ->orWhere('last_name', 'like', '%' . $text . '%')->orWhere('unique_number', 'like', '%' . $text . '%')->get();

        return response()->json($data);
    }
    


    public function pending()
    {
        if (!Sentinel::hasAccess('borrowers')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $data = Borrower::where('branch_id', session('branch_id'))->where('active', 0)->get();

        return view('borrower.pending', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Sentinel::hasAccess('borrowers.create')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $users = User::all();
        $user = array();
        foreach ($users as $key) {
            $user[$key->id] = $key->first_name . ' ' . $key->last_name;
        }
        $savings_products = array();
        foreach (SavingProduct::all() as $key) {
            $savings_products[$key->id] = $key->name;

        }
        //get custom fields
        $custom_fields = CustomField::where('category', 'borrowers')->get();

        return view('borrower.create', compact('user', 'custom_fields','savings_products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if(!isset($request->loan_officers)){
            Flash::warning("Loan Officer Required");
            redirect()->back()->withInput();
            return redirect()->back();
        }

    //    $checkphone = DB::table("borrowers")->where(["mobile"=>$request->mobile])->exists();
    //     if($checkphone){
    //        Flash::warning("Mobile Already Exists!");
    //        redirect()->back()->withInput();
    //         return redirect()->back();
    //     }

    //     $checkphone = DB::table("borrowers")->where(["phone"=>$request->phone])->exists();
    //     if($checkphone){
    //        Flash::warning("Phone Number Already Exists!");
    //         redirect()->back()->withInput();
    //         return redirect()->back();
    //    }
       $gfirstname = $request->gfirstname;
       $gsurname = $request->gsurname;
       $gothername = $request->gothername;
       $gaddress = $request->gaddress;
       $gphone = $request->gphone;
       $gbiz = $request->gbiz;
       $gphoto = $request->file("gphoto");
      
        if (!Sentinel::hasAccess('borrowers.create')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $borrower = new Borrower();
        $borrower->first_name = $request->first_name;
        $borrower->last_name = $request->last_name;
        $borrower->user_id = Sentinel::getUser()->id;
        $borrower->gender = $request->gender;
        $borrower->country = $request->country;
        $borrower->title = $request->title;
        $borrower->branch_id = session('branch_id');
        $borrower->mobile = $request->mobile;
        $borrower->notes = $request->notes;
        $borrower->email = $request->email;
        if ($request->hasFile('photo')) {
            $file = array('photo' => Input::file('photo'));
            $rules = array('photo' => 'required|mimes:jpeg,jpg,bmp,png');
            $validator = Validator::make($file, $rules);
            if ($validator->fails()) {
                Flash::warning(trans('general.validation_error'));
                return redirect()->back()->withInput()->withErrors($validator);
            } else {
                $borrower->photo = $request->file('photo')->getClientOriginalName();
                $request->file('photo')->move(public_path() . '/uploads',
                    $request->file('photo')->getClientOriginalName());
            }

        }
        $borrower->unique_number = $request->unique_number;
        $borrower->dob = $request->dob;
        $borrower->address = $request->address;
        $borrower->city = $request->city;
        $borrower->state = $request->state;
        $borrower->zip = $request->zip;
        $borrower->phone = $request->phone;
        $borrower->business_name = $request->business_name;
        $borrower->working_status = $request->working_status;
        $borrower->loan_officers = serialize($request->loan_officers);
        foreach(serialize($request->loan_officers) as $d){
            DB::table("loan_officers")->insert(["borrower_id"=>$bb->id,"officer_id"=>$d]);
        }
        $borrower->user_id = Sentinel::getUser()->id;

        $date = explode('-', date("Y-m-d"));

        $borrower->year = $date[0];
        $borrower->month = $date[1];
        $files = array();
        if (!empty(array_filter($request->file('files')))) {
            $count = 0;
            foreach ($request->file('files') as $key) {
                $file = array('files' => $key);
                $rules = array('files' => 'required|mimes:jpeg,jpg,bmp,png,pdf,docx,xlsx');
                $validator = Validator::make($file, $rules);
                if ($validator->fails()) {
                    Flash::warning(trans('general.validation_error'));
                    return redirect()->back()->withInput()->withErrors($validator);
                } else {
                    $files[$count] = $key->getClientOriginalName();
                    $key->move(public_path() . '/uploads',
                        $key->getClientOriginalName());
                }
                $count++;
            }
        }
        $borrower->files = serialize($files);
        $borrower->username = $request->username;
        if (!empty($request->password)) {
            $rules = array(
                'repeatpassword' => 'required|same:password',
                'username' => 'required|unique:borrowers'
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                Flash::warning('Passwords do not match');
                return redirect()->back()->withInput()->withErrors($validator);

            } else {
                $borrower->password = md5($request->password);
            }
        }
        $borrower->save();
        if($request->group != null){
            $b = DB::table("borrowers")->where(["unique_number"=>$request->unique_number])->first();
            $member = new BorrowerGroupMember();
            $member->borrower_group_id = $request->group;
            $member->borrower_id = $b->id;
            $member->save();

        }
        $custom_fields = CustomField::where('category', 'borrowers')->get();
        foreach ($custom_fields as $key) {
            $custom_field = new CustomFieldMeta();
            $id = $key->id;
            $custom_field->name = $request->$id;
            $custom_field->parent_id = $borrower->id;
            $custom_field->custom_field_id = $key->id;
            $custom_field->category = "borrowers";
            $custom_field->save();
        }
        
        $saving = new Saving();
        $saving->user_id = Sentinel::getUser()->id;
        $saving->savings_product_id = $request->savings_product_id;
        $saving->borrower_id = $borrower->id;
        $saving->branch_id = session('branch_id');
        $saving->notes = $request->notes;
        $saving->date = date("Y-m-d");
        $saving->save();
        GeneralHelper::audit_trail("Added borrower  with id:" . $borrower->id);
         if($gfirstname != null){
             $gphoto = "";
             if ($request->hasFile('gphoto')) {
                 $id = rand();
       $gphoto = $id.$request->file('gphoto')->getClientOriginalName();
                $request->file('gphoto')->move(public_path() . '/guarantors',
                    $id.$request->file('gphoto')->getClientOriginalName());
                 }
       DB::table("guarantors")->insert(["firstname"=>$gfirstname,"lastname"=>$gsurname,"othername"=>$gothername,"address"=>$gaddress,"phone"=>$gphone,"biz_type"=>$gbiz,"photo"=>$gphoto,"userid"=>$borrower->id]);
       }
        Flash::success(trans('general.successfully_saved'));
        return redirect("/borrower/$borrower->id/show");
    }


    public function show($borrower)
    {
        if (!Sentinel::hasAccess('borrowers.view')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $users = User::all();
        $user = array();
        foreach ($users as $key) {
            $user[$key->id] = $key->first_name . ' ' . $key->last_name;
        }
        //get custom fields
        $custom_fields = CustomFieldMeta::where('category', 'borrowers')->where('parent_id', $borrower->id)->get();
        $guarantors = DB::table("guarantors")->where(["userid"=>$borrower->id])->first();
            $savings = DB::table("savings")->where(["borrower_id"=>$borrower->id])->first();
        $saving = SavingTransaction::where('branch_id', session('branch_id'))->where(["borrower_id"=>$borrower->id])->get();
        return view('borrower.show', compact('guarantors','borrower', 'user', 'custom_fields','savings','saving'));
    }


    public function edit($borrower)
    {
        if (!Sentinel::hasAccess('borrowers.update')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }

        $users = User::all();
        $user = array();
        foreach ($users as $key) {
            $user[$key->id] = $key->first_name . ' ' . $key->last_name;
        }

        $off =  $borrower->loan_officers;




        //get custom fields
        $custom_fields = CustomField::where('category', 'borrowers')->get();
        $g  = DB::table("guarantors")->where(["userid"=>$borrower->id])->first();

        $group = DB::table("borrower_group_members")->where('borrower_id',$borrower->id)->first();



        if($group != null){
            $group = DB::table("borrower_groups")->where(["id"=>$group->borrower_group_id])->first();
            $gid = $group->id;
            $gtext = $group->name;

        }

        return view('borrower.edit', compact('borrower', 'user', 'custom_fields','off','g','gid','gtext'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Sentinel::hasAccess('borrowers.update')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }


        $borrower = Borrower::find($id);
        $borrower->first_name = $request->first_name;
        $borrower->last_name = $request->last_name;
        $borrower->gender = $request->gender;
        $borrower->country = $request->country;
        $borrower->title = $request->title;
        $borrower->mobile = $request->mobile;
        $borrower->notes = $request->notes;
        $borrower->email = $request->email;
        $gfirstname = $request->gfirstname;
        $gsurname = $request->gsurname;
        $gothername = $request->gothername;
        $gaddress = $request->gaddress;
        $gphone = $request->gphone;
        $gbiz = $request->gbiz;
        $gphoto = $request->file("gphoto");
        $gid = $request->gid;
        if ($request->hasFile('photo')) {
            $file = array('photo' => Input::file('photo'));
            $rules = array('photo' => 'required|mimes:jpeg,jpg,bmp,png');
            $validator = Validator::make($file, $rules);
            if ($validator->fails()) {
                Flash::warning(trans('general.validation_error'));
                return redirect()->back()->withInput()->withErrors($validator);
            } else {
                $borrower->photo = $request->file('photo')->getClientOriginalName();
                $request->file('photo')->move(public_path() . '/uploads',
                    $request->file('photo')->getClientOriginalName());
            }

        }
        $borrower->unique_number = $request->unique_number;
        $borrower->dob = $request->dob;
        $borrower->address = $request->address;
        $borrower->city = $request->city;
        $borrower->state = $request->state;
        $borrower->zip = $request->zip;
        $borrower->phone = $request->phone;
        $borrower->business_name = $request->business_name;
        $borrower->working_status = $request->working_status;
        $borrower->loan_officers = serialize($request->loan_officers);
        $files = unserialize($borrower->files);
        $count = count($files);
        if (!empty(array_filter($request->file('files')))) {
            foreach ($request->file('files') as $key) {
                $count++;
                $file = array('files' => $key);
                $rules = array('files' => 'required|mimes:jpeg,jpg,bmp,png,pdf,docx,xlsx');
                $validator = Validator::make($file, $rules);
                if ($validator->fails()) {
                    Flash::warning(trans('general.validation_error'));
                    return redirect()->back()->withInput()->withErrors($validator);
                } else {
                    $files[$count] = $key->getClientOriginalName();
                    $key->move(public_path() . '/uploads',
                        $key->getClientOriginalName());
                }

            }
        }

        $borrower->files = serialize($files);
        $borrower->username = $request->username;
        if (!empty($request->password)) {
            $rules = array(
                'repeatpassword' => 'required|same:password'
            );
            $validator = Validator::make(Input::all(), $rules);
            // if ($validator->fails()) {
            //     Flash::warning('Passwords do not match');
            //     return redirect()->back()->withInput()->withErrors($validator);

            // } else {
                $borrower->password = md5($request->password);
            // }
        }
        $borrower->save();
        $custom_fields = CustomField::where('category', 'borrowers')->get();
        foreach ($custom_fields as $key) {
            if (!empty(CustomFieldMeta::where('custom_field_id', $key->id)->where('parent_id', $id)->where('category',
                'borrowers')->first())
            ) {
                $custom_field = CustomFieldMeta::where('custom_field_id', $key->id)->where('parent_id',
                    $id)->where('category', 'borrowers')->first();
            } else {
                $custom_field = new CustomFieldMeta();
            }
            $kid = $key->id;
            $custom_field->name = $request->$kid;
            $custom_field->parent_id = $id;
            $custom_field->custom_field_id = $key->id;
            $custom_field->category = "borrowers";
            $custom_field->save();
        }
        if($request->group != null){
            $b = DB::table("borrowers")->where(["unique_number"=>$request->unique_number])->first();
            DB::table("borrower_group_members")->where(["borrower_id"=>$b->id])->update(["borrower_group_id"=>$request->group]);
        }


        if(  DB::table("guarantors")->where(["id"=>$gid])->exists()){

        if($gfirstname != null && $gid != null){

            $gphoto = $request->ggphoto;
            if ($request->hasFile('gphoto')) {
                $id = rand();
                $gphoto = $id.$request->file('gphoto')->getClientOriginalName();
                $request->file('gphoto')->move(public_path() . '/guarantors',
                                               $id.$request->file('gphoto')->getClientOriginalName());
            }
            DB::table("guarantors")->where(["id"=>$gid])->update(["firstname"=>$gfirstname,"lastname"=>$gsurname,"othername"=>$gothername,"address"=>$gaddress,"phone"=>$gphone,"biz_type"=>$gbiz,"photo"=>$gphoto,"userid"=>$borrower->id]);
        }
            }else{
            if($gfirstname != null){
                $gphoto = "";
                if ($request->hasFile('gphoto')) {
                    $id = rand();
                    $gphoto = $id.$request->file('gphoto')->getClientOriginalName();
                    $request->file('gphoto')->move(public_path() . '/guarantors',
                                                   $id.$request->file('gphoto')->getClientOriginalName());
                }
                DB::table("guarantors")->insert(["firstname"=>$gfirstname,"lastname"=>$gsurname,"othername"=>$gothername,"address"=>$gaddress,"phone"=>$gphone,"biz_type"=>$gbiz,"photo"=>$gphoto,"userid"=>$borrower->id]);
            }
        }
        GeneralHelper::audit_trail("Updated borrower  with id:" . $borrower->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect("/borrower/$borrower->id/edit");
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        if (!Sentinel::hasAccess('borrowers.delete')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }

        Borrower::destroy($id);
        DB::table("borrowers")->where(["id"=>$id])->delete();
        Loan::where('borrower_id', $id)->delete();
        LoanRepayment::where('borrower_id', $id)->delete();

        GeneralHelper::audit_trail("Deleted borrower  with id:" . $id);
        Flash::success(trans('general.successfully_deleted'));
        return redirect('borrower/data');
    }

    public function deletecheck(Request $request){
        $ids = $request->ids;
        if($ids == []){
            return response()->json(["message"=>"Something Went Wrong, Or Invalid Selection!"],400);
        }
        foreach($ids as $id){

            Borrower::destroy($id);
            DB::table("borrowers")->where(["id"=>$id])->delete();
            Loan::where('borrower_id', $id)->delete();
            LoanRepayment::where('borrower_id', $id)->delete();

            GeneralHelper::audit_trail("Deleted borrower  with id:" . $id);

            }


       return response()->json(["message"=>"Deleted Successfully!"]);
    }
    public function deleteFile(Request $request, $id)
    {
        if (!Sentinel::hasAccess('borrowers.delete')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $borrower = Borrower::find($id);
        $files = unserialize($borrower->files);
        @unlink(public_path() . '/uploads/' . $files[$request->id]);
        $files = array_except($files, [$request->id]);
        $borrower->files = serialize($files);
        $borrower->save();


    }

    public function approve(Request $request, $id)
    {
        if (!Sentinel::hasAccess('borrowers.approve')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $borrower = Borrower::find($id);
        $borrower->active = 1;
        $borrower->save();
        GeneralHelper::audit_trail("Approved borrower  with id:" . $borrower->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect()->back();
    }

    public function decline(Request $request, $id)
    {
        if (!Sentinel::hasAccess('borrowers.approve')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $borrower = Borrower::find($id);
        $borrower->active = 0;
        $borrower->save();
        GeneralHelper::audit_trail("Declined borrower  with id:" . $borrower->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect()->back();
    }
    public function declinebulk(Request $request)
    {
        if (!Sentinel::hasAccess('borrowers.approve')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }

        $ids = $request->ids;
        if($ids == []){
            return response()->json(["message"=>"Something Went Wrong, Or Invalid Selection!"],400);
        }
        foreach($ids as $id){

               $borrower = Borrower::find($id);
        $borrower->active = 0;
        $borrower->save();
        GeneralHelper::audit_trail("Declined borrower  with id:" . $borrower->id);

        }


        return response()->json(["message"=>"Decline Successfully!"]);
    }
  
    public function blacklistbulk(Request $request)
    {
        if (!Sentinel::hasAccess('borrowers.blacklist')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }



        $ids = $request->ids;
        if($ids == []){
            return response()->json(["message"=>"Something Went Wrong, Or Invalid Selection!"],400);
        }
        foreach($ids as $id){
            if($id != "on"){
    $borrower = Borrower::find($id);
        $borrower->blacklisted = 1;
        $borrower->save();
        GeneralHelper::audit_trail("Blacklisted borrower  with id:" . $id);
}
        }


        return response()->json(["message"=>"BlackListed Successfully!"]);
    }

    public function blacklist(Request $request, $id)
    {
        if (!Sentinel::hasAccess('borrowers.blacklist')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $borrower = Borrower::find($id);
        $borrower->blacklisted = 1;
        $borrower->save();
        GeneralHelper::audit_trail("Blacklisted borrower  with id:" . $id);
        Flash::success(trans('general.successfully_saved'));
        return redirect()->back();
    }

   public function unBlacklist(Request $request, $id)
    {
        if (!Sentinel::hasAccess('borrowers.blacklist')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $borrower = Borrower::find($id);
        $borrower->blacklisted = 0;
        $borrower->save();
        GeneralHelper::audit_trail("Undo Blacklist for borrower  with id:" . $id);
        Flash::success(trans('general.successfully_saved'));
        return redirect()->back();
    }

    public function unBlacklistbulk(Request $request)
    {
        if (!Sentinel::hasAccess('borrowers.blacklist')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }



        $ids = $request->ids;
        if($ids == []){
            return response()->json(["message"=>"Something Went Wrong, Or Invalid Selection!"],400);
        }
        foreach($ids as $id){
            if($id != "on"){
                $borrower = Borrower::find($id);
                $borrower->blacklisted = 0;
                $borrower->save();
                GeneralHelper::audit_trail("Undo Blacklist for borrower  with id:" . $id);
            }
        }


        return response()->json(["message"=>"Undo Blacklist Success!"]);
    }
    
    public function dashd(){


        $t_1 = 0;
        $t_2 = 0;
        if(session("branch_id") != 1){ 
        $total_c = DB::table("loan_repayments")
        ->where(["branch_id"=>session("branch_id")])
        ->sum("amount");

        
                    if(\App\Models\Setting::where('setting_key', 'currency_position')->first()->setting_value=='left'){
                        $t_1 =  \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value ."". number_format($total_c,2);
        
        
                    }else{
        
                        $t_1 =   number_format($total_c,2) ."". \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value;
                    }
                    
                    if(\App\Models\Setting::where('setting_key', 'currency_position')->first()->setting_value=='left'){
                        $data = DB::table("loan_schedules")->where(["branch_id"=>session("branch_id")])->sum("interest");
                        $prin = DB::table('loans')->where(["branch_id"=>session("branch_id")])->sum("principal");
                      
                        $data2 = DB::table("loan_repayments")->where(["branch_id"=>session("branch_id")])->sum("amount");
                        $total_x = $data + $prin - $data2;
                        if(session("branch_id") == 1){
                            $t_2 =  \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value ."". number_format($total_x,2);
                        }else{
                            $t_2 =  \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value ."". number_format($total_x,2);
        
                        }
                    }

       
        }else{
            $total_c = DB::table("loan_repayments")->sum("amount");
            if(\App\Models\Setting::where('setting_key', 'currency_position')->first()->setting_value=='left'){
                $t_1 =  \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value ."". number_format($total_c,2);


            }else{

                $t_1 =   number_format($total_c,2) ."". \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value;
            }
            if(\App\Models\Setting::where('setting_key', 'currency_position')->first()->setting_value=='left'){
                $data = DB::table("loan_schedules")->sum("interest");
                $prin = DB::table('loans')->sum("principal");
              
                $data2 = DB::table("loan_repayments")->sum("amount");
                $total_x = $data + $prin - $data2;

                if(session("branch_id") == 1){
                    $t_2 =  \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value ."". number_format($total_x,2);
                }else{
                    $t_2 =  \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value ."". number_format($total_x,2);

                }

              }
        }
   
        return response()->json(["t_1"=>$t_1,"t_2"=>$t_2]);
 
 
        
    }
}
