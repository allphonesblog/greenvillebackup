<?php

namespace App\Http\Controllers;
use DB;
use Aloha\Twilio\Twilio;
use App\Helpers\GeneralHelper;
use App\Helpers\Infobip;
use App\Helpers\RouteSms;
use App\Models\CustomField;
use App\Models\CustomFieldMeta;
use App\Models\Email;
use App\Models\Guarantor;
use App\Models\Loan;
use App\Models\LoanApplication;
use App\Models\LoanFee;
use App\Models\LoanFeeMeta;
use App\Models\LoanProduct;
use App\Models\LoanRepayment;
use App\Models\LoanRepaymentMethod;
use App\Models\LoanDisbursedBy;
use App\Models\Borrower;
use App\Models\LoanSchedule;
use App\Models\Setting;
use App\Models\Sms;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Clickatell\Rest;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\View;
use PDF;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;

class LoanController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentinel');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function storebulkgrepayment(Request $request){
        
        if($request->sc == null){
            Flash::warning("Something Went Wrong!");
            return redirect()->back();
        }

        if (!Sentinel::hasAccess('repayments.create')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }

  $loan = DB::table("loans")->where(["sc"=>$request->sc])->get();

        if(Loan::where(["sc"=>$request->sc])->sum("balance") == 0){
            DB::table("grouploan")->where(["sc"=>$request->sc])->update([
                "status"=>"closed"
            ]);
        }
        foreach($loan as $loans){
$i = $loans->id;
            $amount = "repayment_amount" . $i;
            $loan_id = "loan_id" . $i;
            $repayment_method = "repayment_method_id" . $i;
            $collected_date = "repayment_collected_date" . $i;
            $repayment_description = "repayment_description" . $i;


            if (!empty($request->$amount && !empty($request->$loan_id))) {


                $loanss = Loan::where(["id"=>$request->$loan_id])->get();

                foreach($loanss as $loanxx){
                $loan = Loan::find($loanxx->id);
                if ($request->$amount > round(GeneralHelper::loan_total_balance($loan->id), 2)) {
                    Flash::warning("Amount is more than the balance(" . GeneralHelper::loan_total_balance($loan->id) . ')');
                    return redirect()->back()->withInput();

                } else {
                    $repayment = new LoanRepayment();
                    $repayment->user_id = Sentinel::getUser()->id;
                    $repayment->amount = $request->$amount;
                    $repayment->loan_id = $loan->id;
                    $repayment->borrower_id = $loan->borrower_id;
                    $repayment->branch_id = session('branch_id');

                    $repayment->collection_date = \Carbon\Carbon::parse($request->$collected_date);
                    $repayment->repayment_method_id = $request->$repayment_method;
                    $repayment->notes = $request->$repayment_description;

                    $repayment->year = $date[0];
                    $repayment->month = $date[1];
                    //determine which schedule due date the payment applies too
                    $schedule = LoanSchedule::where('due_date', '>=', $request->$collected_date)->where('loan_id',
                                                                                                        $loan->id)->orderBy('due_date',
                                                                                                                            'asc')->first();
                    if (!empty($schedule)) {
                        $repayment->due_date = $schedule->due_date;
                    } else {
                        $schedule = LoanSchedule::where('loan_id',
                                                        $loan->id)->orderBy('due_date',
                                                                            'desc')->first();
                        if ($request->$collected_date > $schedule->due_date) {
                            $repayment->due_date = $schedule->due_date;
                        } else {
                            $schedule = LoanSchedule::where('due_date', '>',
                                                            $request->$collected_date)->where('loan_id',
                                                                                              $loan->id)->orderBy('due_date',
                                                                                                                  'asc')->first();
                            $repayment->due_date = $schedule->due_date;
                        }

                    }
                    $repayment->save();

                    //update loan status if need be
                    if (round(GeneralHelper::loan_total_balance($loan->id), 2) == 0) {
                        $l = Loan::find($loan->id);
                        $l->status = "closed";
                        $l->save();
                     

                    }
                    //check if late repayment is to be applied when adding payment

                    if (!empty($loan->loan_product)) {
                        if ($loan->loan_product->enable_late_repayment_penalty == 1) {
                            $schedules = LoanSchedule::where('due_date', '<',
                                                             $repayment->due_date)->where('missed_penalty_applied',
                                                                                          0)->orderBy('due_date', 'asc')->get();
                            foreach ($schedules as $schedule) {
                                if (GeneralHelper::loan_total_due_period($loan->id,
                                                                         $schedule->due_date) > GeneralHelper::loan_total_paid_period($loan->id,
                                                                                                                                      $schedule->due_date)
                                   ) {
                                    $sch = LoanSchedule::find($schedule->id);
                                    $sch->missed_penalty_applied = 1;
                                    //determine which amount to use
                                    if ($loan->loan_product->late_repayment_penalty_type == "fixed") {
                                        $sch->penalty = $sch->penalty + $loan->loan_product->late_repayment_penalty_amount;
                                    } else {
                                        if ($loan->loan_product->late_repayment_penalty_calculate == 'overdue_principal') {
                                            $principal = (GeneralHelper::loan_total_principal($loan->id,
                                                                                              $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                                                                                                                   'principal', $schedule->due_date));
                                            $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                        }
                                        if ($loan->loan_product->late_repayment_penalty_calculate == 'overdue_principal_interest') {
                                            $principal = (GeneralHelper::loan_total_principal($loan->id,
                                                                                              $schedule->due_date) + GeneralHelper::loan_total_interest($loan->id,
                                                    $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                                                                         'principal',
                                                                                                         $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                    'interest', $schedule->due_date));
                                            $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                        }
                                        if ($loan->loan_product->late_repayment_penalty_calculate == 'overdue_principal_interest_fees') {
                                            $principal = (GeneralHelper::loan_total_principal($loan->id,
                                                                                              $schedule->due_date) + GeneralHelper::loan_total_interest($loan->id,
                                                    $schedule->due_date) + GeneralHelper::loan_total_fees($loan->id,
                                                                                                          $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                    'principal',
                                                    $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                                                                         'interest',
                                                                                                         $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                    'fees',
                                                    $schedule->due_date));
                                            $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                        }
                                        if ($loan->loan_product->late_repayment_penalty_calculate == 'total_overdue') {
                                            $principal = (GeneralHelper::loan_total_due_amount($loan->id,
                                                                                               $schedule->due_date) - GeneralHelper::loan_total_paid($loan->id,
                                                                                                                                                     $schedule->due_date));
                                            $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                        }
                                    }
                                    $sch->save();
                                }
                            }
                        }

                    }
                }
            }
            //notify borrower
}

        }
        GeneralHelper::audit_trail("Added  bulk repayment");
        Flash::success("Repayment successfully saved");
        return redirect()->back();
    }
    public function bulkapigroup(){

        $loans = array();
        $data = [];


        foreach (DB::table("grouploan")->where(["status"=>"disbursed"])->cursor() as $key) {
            $group = DB::table("borrower_groups")->where(["id"=>$key->group_id])->first();
            $loan =  Loan::where(["sc"=>$key->sc])->get() ;
            $loan_bal = 0;
            foreach($loan as $loanx){ 
                $loan_bal += \App\Helpers\GeneralHelper::loan_total_balance($loanx->id);
            }

            //rest of your code...
            $name = $group->name . '(' . trans_choice('general.loan',
                                                                                                      1) . '#' . $key->sc . ',' . trans_choice('general.due',
                                                                                                                                               1) . ':' . $loan_bal. ')';
            $id = $key->sc;

            array_push($loans,["name"=>$name,"id"=>$id]);
        }




        //         GeneralHelper::loan_total_balance($key->id)
        /*   foreach ($data as $key) {

            $name = $key->borrower->first_name . ' ' . $key->borrower->last_name . '(' . trans_choice('general.loan',
                                                                                                      1) . '#' . $key->id . ',' . trans_choice('general.due',
                                                                                                                                               1) . ':' . $loan_bal. ')';
            $id = $key->id;

            array_push($loans,["name"=>$name,"id"=>$id]);
        }
        $loans = array_unique($loans, SORT_REGULAR); */

        return response()->json($loans);
    }
    
    public function groups(){
        $group  = DB::table("borrower_groups")->where(["borrower_groups.branch_id"=>session("branch_id"),"grouploan.status"=>"disbursed"])->join("grouploan","grouploan.group_id","=","borrower_groups.id")->select("grouploan.sc as id","borrower_groups.name")->get();
return response()->json($group);
    }
    public function GroupcreateRepayment(Request $request){
        $repayment_methods = array();
        foreach (LoanRepaymentMethod::all() as $key) {
            $repayment_methods[$key->id] = $key->name;
        }
        $custom_fields = CustomField::where('category', 'repayments')->get();
$g = null;
        $gg = null;
        if(isset($_GET['groupid'])){
            $users = DB::table("borrower_group_members")->where(["grouploan.sc"=>$_GET['groupid']])->join("grouploan","grouploan.group_id","=","borrower_group_members.borrower_group_id")->get();
            $gg = DB::table("borrower_group_members")->where(["grouploan.sc"=>$_GET['groupid']])->join("grouploan","grouploan.group_id","=","borrower_group_members.borrower_group_id")->select("grouploan.group_id as id","grouploan.sc","grouploan.status")->first();
            
            if($gg->status == "closed"){
                return redirect("/loan/repayment/group/bulk/create");
            }
            $g = DB::table("borrower_groups")->where(["id"=>$gg->id])->first();
        }else{
            $users = [];
        }

        return view("loan.grouprepaymentbulk",compact('repayment_methods', 'custom_fields','users','g','gg'));
    }


    public  function loan_paid_item2($id, $item = 'principal', $date = '')
    {    
        $loan = Loan::find($id)->where('loans.status', 'disbursed')->join("loan_schedules","loan_schedules.loan_id","=","loans.id")->wheredueDate('loan_schedules.due_date', '<=', $date)->get();
        $schedules = $loan;
        $principal = 0;
        $interest = 0;
        $penalty = 0;
        $fees = 0;

        $repayment_order = unserialize($loan->loan_product->repayment_order);
        foreach ($schedules as $schedule) {
            $payments = LoanRepayment::where('loan_id', $id)->where('due_date', $schedule->due_date)->sum('amount');
            if ($payments > 0) {
                foreach ($repayment_order as $order) {
                    if ($payments > 0) {
                        if ($order == 'interest') {
                            if ($payments > $schedule->interest) {
                                $interest = $interest + $schedule->interest;
                                $payments = $payments - $schedule->interest;
                            } else {
                                $interest = $interest + $payments;
                                $payments = 0;
                            }
                        }
                        if ($order == 'penalty') {
                            if ($payments > $schedule->penalty) {
                                $penalty = $penalty + $schedule->penalty;
                                $payments = $payments - $schedule->penalty;
                            } else {
                                $penalty = $penalty + $payments;
                                $payments = 0;
                            }
                        }
                        if ($order == 'fees') {
                            if ($payments > $schedule->fees) {
                                $fees = $fees + $schedule->fees;
                                $payments = $payments - $schedule->fees;
                            } else {
                                $fees = $fees + $payments;
                                $payments = 0;
                            }
                        }
                        if ($order == 'principal') {
                            if ($payments > $schedule->principal) {
                                $principal = $principal + $schedule->principal;
                                $payments = $payments - $schedule->principal;
                            } else {
                                $principal = $principal + $payments;
                                $payments = 0;
                            }
                        }
                    }
                }
            }
            //apply remainder to principal
            $principal = $principal + $payments;
        }
        if ($item == 'principal') {
            return $principal;
        }
        if ($item == 'fees') {
            return $fees;
        }
        if ($item == 'penalty') {
            return $penalty;
        }
        if ($item == 'interest') {
            return $interest;
        }
        return $principal;
    }

    public function loan_paid_item(){
       
        $loan_total_penalty = 0;
        $l = LoanSchedule::chunk(10000, function($properties) use (&$loan_total_penalty){
            $loan_total_penalty += $properties->where('loan_id', $id)->where('due_date', '<=', $date)->sum("penalty");
        });
        
        $fees = 0;

        $l = LoanSchedule::chunk(10000, function($properties) use (&$fees){
            $fees += $properties->where('loan_id', $id)->where('due_date', '<=', $date)->sum('fees');
        });
        
        $loan_total_interest = 0;

        $l = LoanSchedule::chunk(10000, function($properties) use (&$loan_total_interest){
            $loan_total_interest += $properties->where('loan_id', $id)->where('due_date', '<=', $date)->sum('interest');
        });
        
        $loan_total_principal = 0;

        $l = LoanSchedule::chunk(10000, function($properties) use (&$loan_total_principal){
            $loan_total_principal += $properties->where('loan_id', $id)->where('due_date', '<=', $date)->sum('principal');
        });

        return $loan_total_penalty + $fees + $loan_total_interest + $loan_total_principal;

    }

    public function loans(Request $request){
        $datas = [];

        $columns = array( 
            0 =>'borrowers', 
            1 =>'id',
            2=> 'loan_officer',
            3=> 'branch',
            4=> 'principal',
            5=> 'released',
            6=> 'interest',
            7=> 'due',
            8=> 'paid',
            9=> 'balance',
            10=> 'status',
            11=> 'action',

        );


        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $texts = $request->input('search.value');
        $text = $request->loan_officer;
        $text2 = $request->from;
        $text3 = $request->to;
        $text4 = $request->ma;
        $text5 = $request->payment_due;

        if(isset($_GET['status'])){
            $status = ["status"=>$request->status];
        }else{
            $status = [];
        }

        if(empty($texts) || isset($_GET['date']))
        {      
            $status = [];
        if($request->status != null){
            $status = ["status"=>$request->status];
        }
            $data = Loan::where('branch_id', session('branch_id'))->offset($start)
                ->limit($limit)
                ->where($status)
                ->orderBy("loans.id",$dir)->get();

            $totalFiltered = Loan::where('branch_id', session('branch_id'))->where($status)->count();
            }else{
            

            $data = Loan::where('loans.branch_id', session('branch_id'))
                ->offset($start)
                ->limit($limit)
                ->orderBy("loans.id",$dir)
                ->where($status)
                ->whereHas('borrower', function ($query) use ($texts) {
                  $query ->where('borrowers.first_name', 'like', '%' . $texts . '%')
                      ->orWhere('borrowers.last_name', 'like', '%' . $texts . '%')
                      ->orWhere('borrowers.unique_number', 'like', '%' . $texts . '%')
                      ->orWhere('borrowers.username', 'like', '%' . $texts . '%')
                      ->orWhere('borrowers.title', 'like', '%' . $texts . '%')
                      ->orWhere('borrowers.phone', 'like', '%' . $texts . '%')
                      ->orWhere('borrowers.address', 'like', '%' . $texts . '%')
                      ->orWhere('borrowers.dob', 'like', '%' . $texts . '%')
                      ->orWhere('borrowers.gender', 'like', '%' . $texts . '%')
                      ->orWhere('borrowers.business_name', 'like', '%' . $texts . '%')
                      ->orWhere('borrowers.created_at', 'like', '%' . $texts . '%')
                      ->orWhere('borrowers.id', 'like', '%' . $texts . '%')
                      ->orWhereRaw("concat(first_name, ' ', last_name) like '%$texts%' ");
                })


->select("loans.*")
                ->get();
            $totalFiltered = Loan::where('loans.branch_id', session('branch_id'))->join("borrowers","borrowers.id","=","loans.borrower_id")
                ->where($status)
                
                ->whereHas('borrower', function ($query) use ($texts) {
                    $query ->where('borrowers.first_name', 'like', '%' . $texts . '%')
                        ->orWhere('borrowers.last_name', 'like', '%' . $texts . '%')
                        ->orWhere('borrowers.unique_number', 'like', '%' . $texts . '%')
                        ->orWhere('borrowers.username', 'like', '%' . $texts . '%')
                        ->orWhere('borrowers.title', 'like', '%' . $texts . '%')
                        ->orWhere('borrowers.phone', 'like', '%' . $texts . '%')
                        ->orWhere('borrowers.address', 'like', '%' . $texts . '%')
                        ->orWhere('borrowers.dob', 'like', '%' . $texts . '%')
                        ->orWhere('borrowers.gender', 'like', '%' . $texts . '%')
                        ->orWhere('borrowers.business_name', 'like', '%' . $texts . '%')
                        ->orWhere('borrowers.created_at', 'like', '%' . $texts . '%')
                        ->orWhere('borrowers.id', 'like', '%' . $texts . '%')
                        ->orWhereRaw("concat(first_name, ' ', last_name) like '%$texts%' ");
                })
                ->select("loans.*")->count();

        }



        if(!empty($text4)  || !empty($text5)){
            $data = Loan::join("loan_schedules","loan_schedules.loan_id","=","loans.id")
            ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
            ->where(['loans.branch_id'=> session('branch_id')])
            ->whereHas('borrower', function ($query) use ($text,$text3,$text2) {
                $query->orWhereBetween('loan_schedules.due_date', [$text2, $text3])
                ->orWhereBetween('loans.maturity_date', [$text2, $text3])
                ->orWhere(["loan_officers.officer_id"=>$text]);

            })
            ->where("status","!=","closed")->where("status","!=","declined")
   
            ->get();

        $totalFiltered  = Loan::join("loan_schedules","loan_schedules.loan_id","=","loans.id")
        ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
        ->where(['loans.branch_id'=> session('branch_id')])
        ->whereHas('borrower', function ($query) use ($text,$text3,$text2) {
            $query->orWhereBetween('loan_schedules.due_date', [$text2, $text3])
            ->orWhereBetween('loans.maturity_date', [$text2, $text3])
            ->orWhere(["loan_officers.officer_id"=>$text]);

        })
  
        ->where("status","!=","closed")->where("status","!=","declined")->count();

 
        }

        // if(!empty($text)){
        //     $data = Loan::where(['loans.branch_id'=> session('branch_id'),"loan_officers.officer_id"=>$request->loan_officer])
        //         ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
        //         ->get();

        //     $totalFiltered = Loan::where(['loans.branch_id'=> session('branch_id'),"loan_officers.officer_id"=>$request->loan_officer])
        //         ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")->count();

        // }


        // if(!empty($text2)){
        //     if($text4 != 0){
        //         $data = Loan::where(['loans.branch_id'=> session('branch_id')])->whereBetween('loans.maturity_date', [$text2, $text3])
        //         ->where("status","!=","closed")->where("status","!=","declined")
        //         ->get();

        //     $totalFiltered  = Loan::where(['loans.branch_id'=> session('branch_id')])->whereBetween('loans.maturity_date', [$text2, $text3])->where("status","!=","closed")->where("status","!=","declined")->count();
        //     }else{
        //     $data = Loan::where(['loans.branch_id'=> session('branch_id')])->whereBetween('loans.release_date', [$text2, $text3])

        //         ->get();

        //     $totalFiltered  = Loan::where(['loans.branch_id'=> session('branch_id')])->whereBetween('loans.release_date', [$text2, $text3])->count();

        //     }
        // }

       
        //     if($text4 != 0){
        //         $data = Loan::where(['loans.branch_id'=> session('branch_id')])->whereBetween('loans.maturity_date', [$text2, $text3])
        //         ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
        //         ->where(["loan_officers.officer_id"=>$text])
        //         ->where("status","!=","closed")->where("status","!=","declined")
        //         ->get();

        //     $totalFiltered  = Loan::where(['loans.branch_id'=> session('branch_id')])->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
        //     ->where(["loan_officers.officer_id"=>$text])->whereBetween('loans.maturity_date', [$text2, $text3])->where("status","!=","closed")->where("status","!=","declined")->count();
           
        // }elseif($text5 != 0){
        //         $data = Loan::join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where(['loans.branch_id'=> session('branch_id')])->whereBetween('loan_schedules.due_date', [$text2, $text3])
        //         ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
        //         ->orWhere(["loan_officers.officer_id"=>$text])
        //         ->where("status","!=","closed")->where("status","!=","declined")
        //         ->get();

        //     $totalFiltered  = Loan::where(['loans.branch_id'=> session('branch_id')])->whereBetween('loan_schedules.due_date', [$text2, $text3])
        //     ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
        //     ->orWhere(["loan_officers.officer_id"=>$text])
        //     ->where("status","!=","closed")->where("status","!=","declined")->count();

           
        //     }elseif($text5 != 0 &&  $text4 != 0){
        //         $data = Loan::join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where(['loans.branch_id'=> session('branch_id')])->whereBetween('loan_schedules.due_date', [$text2, $text3])
        //         ->whereBetween('loans.maturity_date', [$text2, $text3])
        //         ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
        //         ->where(["loan_officers.officer_id"=>$text])
        //         ->where("status","!=","closed")->where("status","!=","declined")
        //         ->get();

        //     $totalFiltered  = Loan::join("loan_schedules","loan_schedules.loan_id","=","loans.id")->where(['loans.branch_id'=> session('branch_id')])->whereBetween('loan_schedules.due_date', [$text2, $text3])
        //     ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
        //     ->where(["loan_officers.officer_id"=>$text])
        //     ->where("status","!=","closed")->where("status","!=","declined")->count();
            
        //     }else{
        //     $data = Loan::where(['loans.branch_id'=> session('branch_id')])->whereBetween('loans.release_date', [$text2, $text3])
        //     ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
        //     ->where(["loan_officers.officer_id"=>$text])
        //         ->get();

        //     $totalFiltered  = Loan::where(['loans.branch_id'=> session('branch_id')])
        //     ->join("loan_officers","loan_officers.borrower_id","=","loans.borrower_id")
        //     ->where(["loan_officers.officer_id"=>$text])->whereBetween('loans.release_date', [$text2, $text3])->count();

        //     }
 
       

        $totalData = count($data);
        $totalFilter = $totalFiltered;
        $totalFiltered = $totalData; 

        foreach($data as $key){

            if(!empty($key->borrower)){
                if($key->group_id != null){
                    $name =' <a href="'.url('borrower/'.$key->borrower_id.'/show').'">'.$key->borrower->first_name.' '.$key->borrower->last_name.'</a>'.'<a href="/borrower/group/'.$key->group_id.'/vloans" target="_blank"> <span class="badge badge-danger bg-red">group loan</span> </a>';
                }else{
                    $name =' <a href="'.url('borrower/'.$key->borrower_id.'/show').'">'.$key->borrower->first_name.' '.$key->borrower->last_name.'</a>';
                }

          }else{
               $name =' <span class="label label-danger">'.trans_choice('general.broken',1).' <i
                class="fa fa-exclamation-triangle"></i> </span>';
      }
            $uu = [];
            $b = DB::table("branches")->where(["id"=>$key->branch_id])->first();
            $u = DB::table("users")->where(["id"=>$key->user_id])->first();
            $lname = "";
            $bname = "";
            $lid = "";
            $datax = [];
            if($b){
                $bname = $b->name;
            }
            if(unserialize($key->borrower->loan_officers)){

                $datax = unserialize($key->borrower->loan_officers);

            }



            foreach($datax as $d){

                 $u = DB::table("users")->where(["id"=>$d])->first();


$uu2 = '<a href="/user/'.$d.'/show">'.$u->first_name.' '.$u->last_name.'</a><br>
<br>';
                array_push($uu,$uu2);
                }
                $b = DB::table("branches")->where(["id"=>$key->branch_id])->first();

            if(\App\Models\Setting::where('setting_key', 'currency_position')->first()->setting_value=='left'){
                $prin = \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value."".number_format($key->principal,2);
            }else{
                $prin = number_format($key->principal,2)."".\App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value;
            }
            $rd = $key->release_date;
            $in =   number_format($key->interest_rate,5).'%/'.$key->interest_period;

            if($key->override==1){
                $due = '<s>'.number_format(\App\Helpers\GeneralHelper::loan_total_due_amount($key->id),2).'</s><br>
        '.number_format($key->balance,2).'';
            }else{
                $due = number_format(\App\Helpers\GeneralHelper::loan_total_due_amount($key->id),2);
            }
            $paid = number_format(\App\Helpers\GeneralHelper::loan_total_paid($key->id),2);
            $bal =  number_format(\App\Helpers\GeneralHelper::loan_total_balance($key->id),2);


            if($key->maturity_date<date("Y-m-d") && \App\Helpers\GeneralHelper::loan_total_balance($key->id)>0){
                $status ='   <span class="label label-danger">'.trans_choice('general.past_maturity',1).'</span>';
            }else{
                if($key->status=='pending'){
                    $status =  '<span class="label label-warning">'.trans_choice('general.pending',1).' '.trans_choice('general.approval',1).'</span>';
                }
                if($key->status=='approved'){
                    $status =  '<span class="label label-info">'.trans_choice('general.awaiting',1).' '.trans_choice('general.disbursement',1).'</span>';

                }
                if($key->status=='disbursed'){

                    $status =  '<span class="label label-info">'.trans_choice('general.active',1).'</span>';
                }
                if($key->status=='declined'){
                    $status =  '<span class="label label-danger">'.trans_choice('general.declined',1).'</span>';

                }
                if($key->status=='withdrawn'){
                    $status =  '<span class="label label-danger">'.trans_choice('general.withdrawn',1).'</span>';

                }
                if($key->status=='written_off'){
                    $status =  '<span class="label label-danger">'.trans_choice('general.written_off',1).'</span>';

                }
                if($key->status=='closed'){
                    $status =  '<span class="label label-success">'.trans_choice('general.closed',1).'</span>';

                }
                if($key->status=='pending_reschedule'){
                    $status =  '<span class="label label-warning">'.trans_choice('general.pending',1).' '.trans_choice('general.reschedule',1).'</span>';

                }
                if($key->status=='rescheduled'){
                    $status =  '<span class="label label-info">'.trans_choice('general.rescheduled',1).'</span>';

                }
            }
            $ac1 = "";
            $ac2 = "";
            $ac3 = "";
            if(Sentinel::hasAccess('loans.view')){
                $ac1 ='   <li><a href="'.url('loan/'.$key->id.'/show') .'"><i
        class="fa fa-search"></i> '. trans_choice('general.detail',2).'
    </a>
        </li>';
            }
            if(Sentinel::hasAccess('loans.create')){
                $ac2 ='     <li><a href="'.url('loan/'.$key->id.'/edit').'"><i
        class="fa fa-edit"></i> '. trans('general.edit') .'</a></li>';
            }
            if(Sentinel::hasAccess('loans.delete')){
                $ac3 = ' <li><a href="'.url('loan/'.$key->id.'/delete').'"
        class="delete"><i
        class="fa fa-trash"></i> '. trans('general.delete').' </a></li>';
            }
            if($key->group_id != null){
$action = "--";
                }else{
                $action = '   <div class="btn-group">
            <button type="button" class="btn btn-info btn-xs dropdown-toggle"
            data-toggle="dropdown" aria-expanded="false">
        '.trans('general.choose').' <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu dropdown-menu-right" role="menu">
           '.$ac1.'
             '.$ac2.'
               '.$ac3.'
            </ul>
            </div>';
            }
            if(isset($_GET['status'])){
            $select = "<input type='checkbox' value=".$key->id.">";
                }else{
                $select = "-";
            }
            $nestedData['select'] = $select;
            $nestedData['borrowers'] = $name;
            $nestedData['id'] = $key->id;
            $nestedData['loan_officer'] = $uu;
            $nestedData['branch'] = $b->name;
            $nestedData['principal'] = $prin;
            $nestedData['released'] = $rd;
            $nestedData['interest'] = $in;
            $nestedData['due'] = $due;
            $nestedData['paid'] = $paid;
            $nestedData['balance'] = $bal;
            $nestedData['status'] = $status;
            $nestedData['action'] = $action;
            $datas[] = $nestedData;


        }


        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => $totalFilter, 
            "data"            => $datas  
        );

        return response()->json($json_data);

    }
    public function index(Request $request)
    {
//sleep(100);

        if (!Sentinel::hasAccess('loans')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }



        $users = DB::table("users")->where(["branch_users.branch_id"=>session("branch_id")])
            ->join("branch_users","branch_users.user_id","=","users.id")
            ->join("role_users","role_users.user_id","=","users.id")->where(["role_users.role_id"=>4])->select("users.first_name","users.last_name","users.id")->get();


        $loan_disbursed_by = array();
        foreach (LoanDisbursedBy::all() as $key) {
            $loan_disbursed_by[$key->id] = $key->name;
        }


        if(isset($_GET['search'])){
            $text = $_GET['search'];
            $data = Loan::where('loans.branch_id', session('branch_id'))->join("borrowers","borrowers.id","=","loans.borrower_id")
                ->where(function ($query) use($text) {
                    $query
                        ->orWhere('borrowers.first_name', 'like', '%' . $text . '%')
                        ->orWhere('borrowers.last_name', 'like', '%' . $text . '%')
                        ->orWhereRaw("concat(first_name, ' ', last_name) like '%$text%' ")
                        ->orWhere('loans.id', 'like', '%' . $text . '%')
                        ->orWhere('loans.borrower_id', 'like', '%' . $text . '%')
                        ->orWhere('loans.principal', 'like', '%' . $text . '%')
                        ->orWhere('loans.interest_rate', 'like', '%' . $text . '%')
                        ->orWhere('loans.loan_status', 'like', '%' . $text . '%')
                        ->orWhere('loans.status', 'like', '%' . $text . '%')
                        ->orWhere('loans.balance', 'like', '%' . $text . '%')
                        ->orWhere('loans.created_at', 'like', '%' . $text . '%');
                })


                ->get();



        }
        $status = $request->status;
        
        return view('loan.data', compact('data','users','loan_disbursed_by','status'));
    }



    public function index2(Request $request)
    {
        if (!Sentinel::hasAccess('loans')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }

        $text = $request->text;
        $data = Loan::where('branch_id', session('branch_id'))->where('status', $request->status)->where('id', 'like', '%' . $text . '%')->orWhere('first_name', 'like', '%' . $text . '%')->orWhere('last_name', 'like', '%' . $text . '%')->orWhere('borrower_id', 'like', '%' . $text . '%')->orWhere('principal', 'like', '%' . $text . '%')->orWhere('interest_rate', 'like', '%' . $text . '%')->orWhere('loan_status', 'like', '%' . $text . '%')->orWhere('balance', 'like', '%' . $text . '%')->orWhere('created_at', 'like', '%' . $text . '%')

            ->get();


        return response()->json($data);
    }

    public function pending_approval(Request $request)
    {
        if (!Sentinel::hasAccess('loans')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        if (empty($request->status)) {
            $data = Loan::all();
        } else {
            $data = Loan::where('branch_id', session('branch_id'))->where('status', $request->status)->get();
        }

        return view('loan.data', compact('data'));
    }

    public function create(Request $request)
    {
        if (!Sentinel::hasAccess('loans.create')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }

       
        $borrowers = array();
        foreach (Borrower::all() as $key) {
            $borrowers[$key->id] = $key->first_name . ' ' . $key->last_name . '(' . $key->unique_number . ')';
        }
        $loan_products = array();
        foreach (LoanProduct::all() as $key) {
            $loan_products[$key->id] = $key->name;
        }

        $loan_disbursed_by = array();
        foreach (LoanDisbursedBy::all() as $key) {
            $loan_disbursed_by[$key->id] = $key->name;
        }
        if (isset($request->product_id)) {
            $loan_product = LoanProduct::find($request->product_id);
        } else {
            $loan_product = LoanProduct::first();
        }
        if (isset($request->borrower_id)) {
            $borrower_id = $request->borrower_id;
        } else {
            $borrower_id = '';
        }
        if (empty($loan_product)) {
            Flash::warning("No loan product set. You must first set a loan product");
            return redirect()->back();
        }
        //get custom fields
        $custom_fields = CustomField::where('category', 'loans')->get();
        $loan_fees = LoanFee::all();
        $user = null;
        if(isset($_GET['borrower_id'])){
            $user = DB::table("borrowers")->where(["id"=>$_GET['borrower_id']])->first();
        }

        $data = DB::table("borrowers")->where(["id"=>$borrower_id])->first();
        if($data){
            if($data->branch_id != session("branch_id")){
                Flash::warning("Not Your Branch");
                return redirect()->back();
            }   
        }
       
        return view('loan.create',
            compact('user','borrowers', 'loan_disbursed_by', 'loan_products', 'loan_product', 'borrower_id', 'custom_fields',
                'loan_fees'));
    }

    public function reschedule(Request $request, $id)
    {
        if (!Sentinel::hasAccess('loans.reschedule')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect()->back();
        }
        $loan = Loan::find($id);
        $loan_products = array();
        foreach (LoanProduct::all() as $key) {
            $loan_products[$key->id] = $key->name;
        }

        $loan_disbursed_by = array();
        foreach (LoanDisbursedBy::all() as $key) {
            $loan_disbursed_by[$key->id] = $key->name;
        }
        if (isset($request->product_id)) {
            $loan_product = LoanProduct::find($request->product_id);
        } else {
            if (empty($loan->product)) {
                $loan_product = LoanProduct::first();
            } else {
                $loan_product = $loan->product;
            }
        }
        if (isset($request->borrower_id)) {
            $borrower_id = $request->borrower_id;
        } else {
            $borrower_id = '';
        }
        if (empty($loan_product)) {
            Flash::warning("No loan product set. You must first set a loan product");
            return redirect()->back();
        }
        if ($request->type == 1) {
            $principal = GeneralHelper::loan_total_principal($id) + GeneralHelper::loan_total_interest($id) - GeneralHelper::loan_paid_item($id,
                    'principal') - GeneralHelper::loan_paid_item($id, 'interest');
        }
        if ($request->type == 2) {
            $principal = GeneralHelper::loan_total_principal($id) + GeneralHelper::loan_total_interest($id) + GeneralHelper::loan_total_fees($id) - GeneralHelper::loan_paid_item($id,
                    'principal') - GeneralHelper::loan_paid_item($id, 'interest') - GeneralHelper::loan_paid_item($id,
                    'fees');
        }
        if ($request->type == 3) {
            $principal = GeneralHelper::loan_total_balance($id);
        }
        //get custom fields
        $custom_fields = CustomField::where('category', 'loans')->get();
        $loan_fees = LoanFee::all();
        return view('loan.reschedule',
            compact('borrowers', 'loan_disbursed_by', 'loan_products', 'loan_product', 'borrower_id', 'custom_fields',
                'loan_fees','principal','loan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Sentinel::hasAccess('loans.create')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        if($request->principal == 0){
            Flash::warning("Amount Must Be Greater Than 0");
            return redirect()->back()->withInput();
        }
        $checkloan = DB::table("loans")->where(["borrower_id"=>$request->borrower_id,"deleted_at"=>NULL])
        ->where(function ($query) {
            $query->orWhere("status","=","pending")
            ->orWhere("status","=","disbursed")
            ->orWhere("status","=","approved");
        })->exists();
if($checkloan){
Flash::warning("This User Already Have A  Open Loan");
return redirect()->back();
}
        $loan = new Loan();
        $loan->principal = $request->principal;
        $loan->interest_method = $request->interest_method;
        $loan->interest_rate = $request->interest_rate;
        $loan->branch_id = session('branch_id');
        $loan->interest_period = $request->interest_period;
        $loan->loan_duration = $request->loan_duration;
        $loan->loan_duration_type = $request->loan_duration_type;
        $loan->repayment_cycle = $request->repayment_cycle;
        $loan->decimal_places = $request->decimal_places;
        $loan->override_interest = $request->override_interest;
        $loan->override_interest_amount = $request->override_interest_amount;
        $loan->grace_on_interest_charged = $request->grace_on_interest_charged;
        $loan->borrower_id = $request->borrower_id;
        $loan->applied_amount = $request->principal;
        $loan->user_id = Sentinel::getUser()->id;
        $loan->loan_product_id = $request->loan_product_id;
        $loan->release_date =  \Carbon\Carbon::parse($request->release_date);
        // $date = explode('-', $request->release_date);
        // var_dump($date[1]);
        $date = explode('-', $request->release_date);
        // return;
        $loan->month = $date[1];
        $loan->year = $date[0];
        if (!empty($request->first_payment_date)) {
            $loan->first_payment_date = $request->first_payment_date;
        }
        $loan->description = $request->description;
        $files = array();
        if (!empty(array_filter($request->file('files')))) {
            $count = 0;
            foreach ($request->file('files') as $key) {
                $file = array('files' => $key);
                $rules = array('files' => 'required|mimes:jpeg,jpg,bmp,png,pdf,docx,xlsx');
                $validator = Validator::make($file, $rules);
                if ($validator->fails()) {
                    Flash::warning(trans('general.validation_error'));
                    return redirect()->back()->withInput()->withErrors($validator);
                } else {
                    $files[$count] = $key->getClientOriginalName();
                    $key->move(public_path() . '/uploads',
                        $key->getClientOriginalName());
                }
                $count++;
            }
        }
        $loan->files = serialize($files);
        $loan->save();

        //save custom meta
        $custom_fields = CustomField::where('category', 'loans')->get();
        foreach ($custom_fields as $key) {
            $custom_field = new CustomFieldMeta();
            $id = $key->id;
            $custom_field->name = $request->$id;
            $custom_field->parent_id = $loan->id;
            $custom_field->custom_field_id = $key->id;
            $custom_field->category = "loans";
            $custom_field->save();
        }
        //save loan fees
        $fees_distribute = 0;
        $fees_first_payment = 0;
        $fees_last_payment = 0;
        foreach (LoanFee::all() as $key) {
            $loan_fee = new LoanFeeMeta();
            $value = 'loan_fees_amount_' . $key->id;
            $loan_fees_schedule = 'loan_fees_schedule_' . $key->id;
            $loan_fee->user_id = Sentinel::getUser()->id;
            $loan_fee->category = 'loan';
            $loan_fee->parent_id = $loan->id;
            $loan_fee->loan_fees_id = $key->id;
            $loan_fee->value = $request->$value;
            $loan_fee->loan_fees_schedule = $request->$loan_fees_schedule;
            $loan_fee->save();
            //determine amount to use
            if ($key->loan_fee_type == 'fixed') {
                if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                    $fees_distribute = $fees_distribute + $loan_fee->value;
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                    $fees_first_payment = $fees_first_payment + $loan_fee->value;
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                    $fees_last_payment = $fees_last_payment + $loan_fee->value;
                }
            } else {
                if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                    $fees_distribute = $fees_distribute + ($loan_fee->value * $loan->principal / 100);
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                    $fees_first_payment = $fees_first_payment + ($loan_fee->value * $loan->principal / 100);
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                    $fees_last_payment = $fees_last_payment + ($loan_fee->value * $loan->principal / 100);
                }
            }
        }
        //lets create schedules here
        //determine interest rate to use

        $interest_rate = GeneralHelper::determine_interest_rate($loan->id);

        $period = GeneralHelper::loan_period($loan->id);
        $loan = Loan::find($loan->id);
        if ($loan->repayment_cycle == 'daily') {
            $repayment_cycle = 'day';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' days')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'weekly') {
            $repayment_cycle = 'week';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' weeks')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'monthly') {
            $repayment_cycle = 'month';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' months')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'bi_monthly') {
            $repayment_cycle = 'month';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' months')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'quarterly') {
            $repayment_cycle = 'month';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' months')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'semi_annually') {
            $repayment_cycle = 'month';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' months')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'yearly') {
            $repayment_cycle = 'year';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' years')),
                'Y-m-d');
        }
        $loan->save();
        //generate schedules until period finished
        /*
        $next_payment = $request->first_payment_date;
        $balance = $request->principal;
        for ($i = 1; $i <= $period; $i++) {
            $fees = 0;
            if ($i == 1) {
                $fees = $fees + ($fees_first_payment);
            }
            if ($i == $period) {
                $fees = $fees + ($fees_last_payment);
            }
            $fees = $fees + ($fees_distribute / $period);
            $loan_schedule = new LoanSchedule();
            $loan_schedule->loan_id = $loan->id;
            $loan_schedule->fees = $fees;
            $loan_schedule->borrower_id = $loan->borrower_id;
            $loan_schedule->description = 'Repayment';
            $loan_schedule->due_date = $next_payment;
            $date = explode('-', $next_payment);
            $loan_schedule->month = $date[1];
            $loan_schedule->year = $date[0];
            //determine which method to use
            $due = 0;
            //reducing balance equal installments
            if ($request->interest_method == 'declining_balance_equal_installments') {
                $due = GeneralHelper::amortized_monthly_payment($loan->id, $loan->principal);
                if ($loan->decimal_places == 'round_off_to_two_decimal') {
                    //determine if we have grace period for interest

                    $interest = round(($interest_rate * $balance), 2);
                    $loan_schedule->principal = round(($due - $interest), 2);
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest, 2);
                    }
                    $loan_schedule->due = round($due, 2);
                    //determine next balance
                    $balance = round(($balance - ($due - $interest)), 2);
                    $loan_schedule->principal_balance = round($balance, 2);
                } else {
                    //determine if we have grace period for interest

                    $interest = round(($interest_rate * $balance));
                    $loan_schedule->principal = round(($due - $interest));
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest);
                    }
                    $loan_schedule->due = round($due);
                    //determine next balance
                    $balance = round(($balance - ($due - $interest)));
                    $loan_schedule->principal_balance = round($balance);
                }


            }
            //reducing balance equal principle
            if ($request->interest_method == 'declining_balance_equal_principal') {
                $principal = $loan->principal / $period;
                if ($loan->decimal_places == 'round_off_to_two_decimal') {

                    $interest = round(($interest_rate * $balance), 2);
                    $loan_schedule->principal = round($principal, 2);
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest, 2);
                    }
                    $loan_schedule->due = round(($principal + $interest), 2);
                    //determine next balance
                    $balance = round(($balance - ($principal + $interest)), 2);
                    $loan_schedule->principal_balance = round($balance, 2);
                } else {

                    $loan_schedule->principal = round(($principal));

                    $interest = round(($interest_rate * $balance));
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest);
                    }
                    $loan_schedule->due = round($principal + $interest);
                    //determine next balance
                    $balance = round(($balance - ($principal + $interest)));
                    $loan_schedule->principal_balance = round($balance);
                }

            }
            //flat  method
            if ($request->interest_method == 'flat_rate') {
                $principal = $loan->principal / $period;
                if ($loan->decimal_places == 'round_off_to_two_decimal') {
                    $interest = round(($interest_rate * $loan->principal), 2);
                    $loan_schedule->principal = round(($principal), 2);
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest, 2);
                    }
                    $loan_schedule->principal = round(($principal), 2);
                    $loan_schedule->due = round(($principal + $interest), 2);
                    //determine next balance
                    $balance = round(($balance - $principal), 2);
                    $loan_schedule->principal_balance = round($balance, 2);
                } else {
                    $interest = round(($interest_rate * $loan->principal));
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest);
                    }
                    $loan_schedule->principal = round($principal);
                    $loan_schedule->due = round($principal + $interest);
                    //determine next balance
                    $balance = round(($balance - $principal));
                    $loan_schedule->principal_balance = round($balance);
                }
            }
            //interest only method
            if ($request->interest_method == 'interest_only') {
                if ($i == $period) {
                    $principal = $loan->principal;
                } else {
                    $principal = 0;
                }
                if ($loan->decimal_places == 'round_off_to_two_decimal') {
                    $interest = round(($interest_rate * $loan->principal), 2);
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest, 2);
                    }
                    $loan_schedule->principal = round(($principal), 2);
                    $loan_schedule->due = round(($principal + $interest), 2);
                    //determine next balance
                    $balance = round(($balance - $principal), 2);
                    $loan_schedule->principal_balance = round($balance, 2);
                } else {
                    $interest = round(($interest_rate * $loan->principal));
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest);
                    }
                    $loan_schedule->principal = round($principal);
                    $loan_schedule->due = round($principal + $interest);
                    //determine next balance
                    $balance = round(($balance - $principal));
                    $loan_schedule->principal_balance = round($balance);
                }
            }
            //determine next due date
            if ($loan->repayment_cycle == 'daily') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('1 days')),
                    'Y-m-d');
                $loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'weekly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('1 weeks')),
                    'Y-m-d');
                $loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'monthly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('1 months')),
                    'Y-m-d');
                $loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'bi_monthly') {
                $next_payment = date_format(date_add(date_create($request->first_payment_date),
                    date_interval_create_from_date_string($period . ' months')),
                    'Y-m-d');
                $loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'quarterly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('2 months')),
                    'Y-m-d');
                $loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'semi_annually') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('6 months')),
                    'Y-m-d');
                $loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'yearly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('1 years')),
                    'Y-m-d');
                $loan_schedule->due_date = $next_payment;
            }
            if ($i == $period) {
                $loan_schedule->principal_balance = round($balance);
            }
            $loan_schedule->save();
        }*/
        GeneralHelper::audit_trail("Added loan with id:" . $loan->id);
        Flash::success(trans('general.successfully_saved'));

        
        return redirect("/loan/$loan->id/show");
    }


    public function show($loan)
    {
        if (!Sentinel::hasAccess('loans.view')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $loan_disbursed_by = array();
        foreach (LoanDisbursedBy::all() as $key) {
            $loan_disbursed_by[$key->id] = $key->name;
        }
        $schedules = LoanSchedule::where('loan_id', $loan->id)->orderBy('due_date', 'asc')->get();
        $payments = LoanRepayment::where('loan_id', $loan->id)->orderBy('collection_date', 'asc')->get();
        $custom_fields = CustomFieldMeta::where('category', 'loans')->where('parent_id', $loan->id)->get();
        $saving = DB::table("savings")->where(["borrower_id"=>$loan->borrower->id])->first();
        if($loan->sc != null){
            return redirect("/borrower/group/$loan->sc/loans");
        }
        return view('loan.show', compact('loan', 'schedules', 'payments', 'custom_fields', 'loan_disbursed_by','saving'));
    }

    public function approve(Request $request, $id)
    {
        if (!Sentinel::hasAccess('loans.approve')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $loan = Loan::find($id);
        $loan->status = 'approved';
        $loan->approved_date = $request->approved_date;
        $loan->approved_notes = $request->approved_notes;
        $loan->approved_by_id = Sentinel::getUser()->id;
        $loan->approved_amount = $request->approved_amount;
        $loan->principal = $request->approved_amount;
        $loan->save();
        GeneralHelper::audit_trail("Approved loan with id:" . $loan->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect('loan/' . $id . '/show');
    }
    
    public function approvebulk(Request $request){
        if (!Sentinel::hasAccess('loans.approve')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $ids = $request->ids;
        if($ids == []){
            return response()->json(["message"=>"Something Went Wrong, Or Invalid Selection!"],400);
        }

        foreach($ids as $id){

        DB::table("loans")->where(["status"=>"pending","id"=>$id])->where(["branch_id"=>session("branch_id")])->update([
            "approved_date"=>$request->approved_date,
            "approved_notes"=>$request->approved_notes,
            "approved_by_id"=>Sentinel::getUser()->id,
            "approved_amount"=>$request->approved_amount,
            "principal"=> $request->approved_amount,
            "status"=>"approved"
        ]);
            }
        return response()->json(["message"=>"Approved SuccessFully"]);
    }

    public function unapprove($id)
    {
        if (!Sentinel::hasAccess('loans.approve')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $loan = Loan::find($id);
        $loan->status = 'pending';
        $loan->save();
        GeneralHelper::audit_trail("Unapproved loan with id:" . $loan->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect('loan/' . $id . '/show');
    }

    public function disburse(Request $request, $loan)
    {




        if (!Sentinel::hasAccess('loans.disburse')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        //delete previously created schedules and payments
        LoanSchedule::where('loan_id', $loan->id)->delete();
        LoanRepayment::where('loan_id', $loan->id)->delete();
        $interest_rate = GeneralHelper::determine_interest_rate($loan->id);
        $period = GeneralHelper::loan_period($loan->id);
        $loan = Loan::find($loan->id);
        if ($loan->repayment_cycle == 'daily') {
            $repayment_cycle = '1 days';
            $repayment_type = 'days';
        }
        if ($loan->repayment_cycle == 'weekly') {
            $repayment_cycle = '1 weeks';
            $repayment_type = 'weeks';
        }
        if ($loan->repayment_cycle == 'monthly') {
            $repayment_cycle = 'month';
            $repayment_type = 'months';
        }
        if ($loan->repayment_cycle == 'bi_monthly') {
            $repayment_cycle = '2 months';
            $repayment_type = 'months';

        }
        if ($loan->repayment_cycle == 'quarterly') {
            $repayment_cycle = '4 months';
            $repayment_type = 'months';
        }
        if ($loan->repayment_cycle == 'semi_annually') {
            $repayment_cycle = '6 months';
            $repayment_type = 'months';
        }
        if ($loan->repayment_cycle == 'yearly') {
            $repayment_cycle = '1 years';
            $repayment_type = 'years';
        }
        if (empty($request->first_payment_date)) {
            $first_payment_date = date_format(date_add(date_create($request->disbursed_date),
                date_interval_create_from_date_string($repayment_cycle)),
                'Y-m-d');


        } else {
            $first_payment_date =  date('Y-m-d', strtotime($request->first_payment_date));
        }
        $loan->maturity_date = date_format(date_add(date_create($first_payment_date),
            date_interval_create_from_date_string($period . ' ' . $repayment_type)),
            'Y-m-d');
        $loan->status = 'disbursed';
        $loan->loan_disbursed_by_id = $request->loan_disbursed_by_id;
        $loan->disbursed_notes = $request->disbursed_notes;
        $loan->first_payment_date = $first_payment_date;
        $loan->disbursed_by_id = Sentinel::getUser()->id;
        $loan->disbursed_date = $request->disbursed_date;
        $loan->release_date =  \Carbon\Carbon::parse($request->disbursed_date);
        $date = explode('-', $request->disbursed_date);
           $loan->month = $date[1];
        $loan->year = $date[0];
        $loan->save();
        $fees_distribute = 0;
        $fees_first_payment = 0;
        $fees_last_payment = 0;

        foreach (LoanFee::all() as $key) {
            if (!empty(LoanFeeMeta::where('loan_fees_id', $key->id)->where('parent_id', $loan->id)->where('category',
                'loan')->first())
            ) {
                $loan_fee = LoanFeeMeta::where('loan_fees_id', $key->id)->where('parent_id',
                    $loan->id)->where('category',
                    'loan')->first();
                //determine amount to use
                if ($key->loan_fee_type == 'fixed') {
                    if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                        $fees_distribute = $fees_distribute + $loan_fee->value;
                    }
                    if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                        $fees_first_payment = $fees_first_payment + $loan_fee->value;
                    }
                    if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                        $fees_last_payment = $fees_last_payment + $loan_fee->value;
                    }
                } else {
                    if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                        $fees_distribute = $fees_distribute + ($loan_fee->value * $loan->principal / 100);
                    }
                    if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                        $fees_first_payment = $fees_first_payment + ($loan_fee->value * $loan->principal / 100);
                    }
                    if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                        $fees_last_payment = $fees_last_payment + ($loan_fee->value * $loan->principal / 100);
                    }
                }
            }

        }
        //generate schedules until period finished
        $next_payment = $first_payment_date;
        $balance = $loan->principal;
        for ($i = 1; $i <= $period; $i++) {
            $fees = 0;
            if ($i == 1) {
                $fees = $fees + ($fees_first_payment);
            }
            if ($i == $period) {
                $fees = $fees + ($fees_last_payment);
            }
            $fees = $fees + ($fees_distribute / $period);
            $loan_schedule = new LoanSchedule();
            $loan_schedule->loan_id = $loan->id;
            $loan_schedule->fees = $fees;
            $loan_schedule->branch_id = session('branch_id');
            $loan_schedule->borrower_id = $loan->borrower_id;
            $loan_schedule->description = trans_choice('general.repayment', 1);
            $loan_schedule->due_date = $next_payment;


            $loan_schedule->month = $date[1];
            $loan_schedule->year = $date[0];
            //determine which method to use
            $due = 0;
            //reducing balance equal installments
            if ($loan->interest_method == 'declining_balance_equal_installments') {
                $due = GeneralHelper::amortized_monthly_payment($loan->id, $loan->principal);

                if ($loan->decimal_places == 'round_off_to_two_decimal') {
                    //determine if we have grace period for interest

                    $interest = round(($interest_rate * $balance), 2);
                    $loan_schedule->principal = round(($due - $interest), 2);
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest, 2);
                    }
                    $loan_schedule->due = round($due, 2);
                    //determine next balance
                    $balance = round(($balance - ($due - $interest)), 2);
                    $loan_schedule->principal_balance = round($balance, 2);
                } else {
                    //determine if we have grace period for interest

                    $interest = round(($interest_rate * $balance));
                    $loan_schedule->principal = round(($due - $interest));
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest);
                    }
                    $loan_schedule->due = round($due);
                    //determine next balance
                    $balance = round(($balance - ($due - $interest)));
                    $loan_schedule->principal_balance = round($balance);
                }


            }
            //reducing balance equal principle
            if ($loan->interest_method == 'declining_balance_equal_principal') {
                $principal = $loan->principal / $period;
                if ($loan->decimal_places == 'round_off_to_two_decimal') {

                    $interest = round(($interest_rate * $balance), 2);
                    $loan_schedule->principal = round($principal, 2);
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest, 2);
                    }
                    $loan_schedule->due = round(($principal + $interest), 2);
                    //determine next balance
                    $balance = round(($balance - ($principal + $interest)), 2);
                    $loan_schedule->principal_balance = round($balance, 2);
                } else {

                    $loan_schedule->principal = round(($principal));

                    $interest = round(($interest_rate * $balance));
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest);
                    }
                    $loan_schedule->due = round($principal + $interest);
                    //determine next balance
                    $balance = round(($balance - ($principal + $interest)));
                    $loan_schedule->principal_balance = round($balance);
                }

            }
            //flat  method
            if ($loan->interest_method == 'flat_rate') {
                $principal = $loan->principal / $period;
                if ($loan->decimal_places == 'round_off_to_five_decimal') {
                    $interest = round(($interest_rate * $loan->principal), 5);
                    $loan_schedule->principal = round(($principal), 5);
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest, 5);
                    }
                    $loan_schedule->principal = round(($principal), 5);
                    $loan_schedule->due = round(($principal + $interest), 5);
                    //determine next balance
                    $balance = round(($balance - $principal), 2);
                    $loan_schedule->principal_balance = round($balance, 5);
                } else {
                    $interest = round(($interest_rate * $loan->principal));
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest);
                    }
                    $loan_schedule->principal = round($principal);
                    $loan_schedule->due = round($principal + $interest);
                    //determine next balance
                    $balance = round(($balance - $principal));
                    $loan_schedule->principal_balance = round($balance);
                }
            }
            //interest only method
            if ($loan->interest_method == 'interest_only') {
                if ($i == $period) {
                    $principal = $loan->principal;
                } else {
                    $principal = 0;
                }
                if ($loan->decimal_places == 'round_off_to_two_decimal') {
                    $interest = round(($interest_rate * $loan->principal), 2);
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest, 2);
                    }
                    $loan_schedule->principal = round(($principal), 2);
                    $loan_schedule->due = round(($principal + $interest), 2);
                    //determine next balance
                    $balance = round(($balance - $principal), 2);
                    $loan_schedule->principal_balance = round($balance, 2);
                } else {
                    $interest = round(($interest_rate * $loan->principal));
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest);
                    }
                    $loan_schedule->principal = round($principal);
                    $loan_schedule->due = round($principal + $interest);
                    //determine next balance
                    $balance = round(($balance - $principal));
                    $loan_schedule->principal_balance = round($balance);
                }
            }
            //determine next due date
            if ($loan->repayment_cycle == 'daily') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('1 days')),
                    'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'weekly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('1 weeks')),
                    'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'monthly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('1 months')),
                    'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'bi_monthly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('2 months')),
                    'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'quarterly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('4 months')),
                    'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'semi_annually') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('6 months')),
                    'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'yearly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('1 years')),
                    'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($i == $period) {
                $loan_schedule->principal_balance = round($balance);
            }
            $loan_schedule->save();
        }
        $loan = Loan::find($loan->id);
        $loan->maturity_date = $next_payment;
        $loan->save();
        GeneralHelper::audit_trail("Disbursed loan with id:" . $loan->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect('loan/' . $loan->id . '/show');
    }
    public function rescheduleStore(Request $request, $id)
    {
        if (!Sentinel::hasAccess('loans.reschedule')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $l=Loan::find($id);
        $l->status='rescheduled';
        $l->save();
        $loan = new Loan();
        $loan->principal = $request->principal;
        $loan->interest_method = $request->interest_method;
        $loan->interest_rate = $request->interest_rate;
        $loan->branch_id = session('branch_id');
        $loan->interest_period = $request->interest_period;
        $loan->loan_duration = $request->loan_duration;
        $loan->loan_duration_type = $request->loan_duration_type;
        $loan->repayment_cycle = $request->repayment_cycle;
        $loan->decimal_places = $request->decimal_places;
        $loan->override_interest = $request->override_interest;
        $loan->override_interest_amount = $request->override_interest_amount;
        $loan->grace_on_interest_charged = $request->grace_on_interest_charged;
        $loan->borrower_id = $l->borrower_id;
        $loan->applied_amount = $request->principal;
        $loan->user_id = Sentinel::getUser()->id;
        $loan->loan_product_id = $request->loan_product_id;
        $loan->release_date = $request->release_date;
        // $date = explode('-', $request->release_date);

        $loan->month = $date[1];
        $loan->year = $date[0];
        if (!empty($request->first_payment_date)) {
            $loan->first_payment_date = $request->first_payment_date;
        }
        $loan->description = $request->description;
        $files = array();
        if (!empty(array_filter($request->file('files')))) {
            $count = 0;
            foreach ($request->file('files') as $key) {
                $file = array('files' => $key);
                $rules = array('files' => 'required|mimes:jpeg,jpg,bmp,png,pdf,docx,xlsx');
                $validator = Validator::make($file, $rules);
                if ($validator->fails()) {
                    Flash::warning(trans('general.validation_error'));
                    return redirect()->back()->withInput()->withErrors($validator);
                } else {
                    $files[$count] = $key->getClientOriginalName();
                    $key->move(public_path() . '/uploads',
                        $key->getClientOriginalName());
                }
                $count++;
            }
        }
        $loan->files = serialize($files);
        $loan->save();

        //save custom meta
        $custom_fields = CustomField::where('category', 'loans')->get();
        foreach ($custom_fields as $key) {
            $custom_field = new CustomFieldMeta();
            $id = $key->id;
            $custom_field->name = $request->$id;
            $custom_field->parent_id = $loan->id;
            $custom_field->custom_field_id = $key->id;
            $custom_field->category = "loans";
            $custom_field->save();
        }
        //save loan fees
        $fees_distribute = 0;
        $fees_first_payment = 0;
        $fees_last_payment = 0;
        foreach (LoanFee::all() as $key) {
            $loan_fee = new LoanFeeMeta();
            $value = 'loan_fees_amount_' . $key->id;
            $loan_fees_schedule = 'loan_fees_schedule_' . $key->id;
            $loan_fee->user_id = Sentinel::getUser()->id;
            $loan_fee->category = 'loan';
            $loan_fee->parent_id = $loan->id;
            $loan_fee->loan_fees_id = $key->id;
            $loan_fee->value = $request->$value;
            $loan_fee->loan_fees_schedule = $request->$loan_fees_schedule;
            $loan_fee->save();
            //determine amount to use
            if ($key->loan_fee_type == 'fixed') {
                if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                    $fees_distribute = $fees_distribute + $loan_fee->value;
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                    $fees_first_payment = $fees_first_payment + $loan_fee->value;
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                    $fees_last_payment = $fees_last_payment + $loan_fee->value;
                }
            } else {
                if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                    $fees_distribute = $fees_distribute + ($loan_fee->value * $loan->principal / 100);
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                    $fees_first_payment = $fees_first_payment + ($loan_fee->value * $loan->principal / 100);
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                    $fees_last_payment = $fees_last_payment + ($loan_fee->value * $loan->principal / 100);
                }
            }
        }
        //lets create schedules here
        //determine interest rate to use

        $interest_rate = GeneralHelper::determine_interest_rate($loan->id);

        $period = GeneralHelper::loan_period($loan->id);
        $loan = Loan::find($loan->id);
        if ($loan->repayment_cycle == 'daily') {
            $repayment_cycle = 'day';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' days')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'weekly') {
            $repayment_cycle = 'week';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' weeks')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'monthly') {
            $repayment_cycle = 'month';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' months')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'bi_monthly') {
            $repayment_cycle = 'month';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' months')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'quarterly') {
            $repayment_cycle = 'month';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' months')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'semi_annually') {
            $repayment_cycle = 'month';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' months')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'yearly') {
            $repayment_cycle = 'year';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' years')),
                'Y-m-d');
        }
        $loan->save();
        //delete previously created schedules and payments
        LoanSchedule::where('loan_id', $loan->id)->delete();
        LoanRepayment::where('loan_id', $loan->id)->delete();
        $interest_rate = GeneralHelper::determine_interest_rate($loan->id);
        $period = GeneralHelper::loan_period($loan->id);
        $loan = Loan::find($loan->id);
        if ($loan->repayment_cycle == 'daily') {
            $repayment_cycle = '1 days';
            $repayment_type = 'days';
        }
        if ($loan->repayment_cycle == 'weekly') {
            $repayment_cycle = '1 weeks';
            $repayment_type = 'weeks';
        }
        if ($loan->repayment_cycle == 'monthly') {
            $repayment_cycle = 'month';
            $repayment_type = 'months';
        }
        if ($loan->repayment_cycle == 'bi_monthly') {
            $repayment_cycle = '2 months';
            $repayment_type = 'months';

        }
        if ($loan->repayment_cycle == 'quarterly') {
            $repayment_cycle = '4 months';
            $repayment_type = 'months';
        }
        if ($loan->repayment_cycle == 'semi_annually') {
            $repayment_cycle = '6 months';
            $repayment_type = 'months';
        }
        if ($loan->repayment_cycle == 'yearly') {
            $repayment_cycle = '1 years';
            $repayment_type = 'years';
        }
        if (empty($request->first_payment_date)) {
            $first_payment_date = date_format(date_add(date_create($request->disbursed_date),
                date_interval_create_from_date_string($repayment_cycle)),
                'Y-m-d');
        } else {
            $first_payment_date = $request->first_payment_date;
        }
        $loan->maturity_date = date_format(date_add(date_create($first_payment_date),
            date_interval_create_from_date_string($period . ' ' . $repayment_type)),
            'Y-m-d');
        $loan->status = 'disbursed';
        $loan->loan_disbursed_by_id = $l->loan_disbursed_by_id;
        $loan->disbursed_notes = "Loan rescheduled from :#".$l->id;
        $loan->first_payment_date = $first_payment_date;
        $loan->disbursed_by_id = Sentinel::getUser()->id;
        $loan->disbursed_date = $request->release_date;
        $loan->release_date = $request->release_date;
        // $date = explode('-', $request->release_date);
                $loan->month = $date[1];

        $loan->year = $date[0];
        $loan->save();
        $fees_distribute = 0;
        $fees_first_payment = 0;
        $fees_last_payment = 0;

        foreach (LoanFee::all() as $key) {
            if (!empty(LoanFeeMeta::where('loan_fees_id', $key->id)->where('parent_id', $loan->id)->where('category',
                'loan')->first())
            ) {
                $loan_fee = LoanFeeMeta::where('loan_fees_id', $key->id)->where('parent_id',
                    $loan->id)->where('category',
                    'loan')->first();
                //determine amount to use
                if ($key->loan_fee_type == 'fixed') {
                    if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                        $fees_distribute = $fees_distribute + $loan_fee->value;
                    }
                    if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                        $fees_first_payment = $fees_first_payment + $loan_fee->value;
                    }
                    if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                        $fees_last_payment = $fees_last_payment + $loan_fee->value;
                    }
                } else {
                    if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                        $fees_distribute = $fees_distribute + ($loan_fee->value * $loan->principal / 100);
                    }
                    if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                        $fees_first_payment = $fees_first_payment + ($loan_fee->value * $loan->principal / 100);
                    }
                    if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                        $fees_last_payment = $fees_last_payment + ($loan_fee->value * $loan->principal / 100);
                    }
                }
            }

        }
        //generate schedules until period finished
        $next_payment = $first_payment_date;
        $balance = $loan->principal;
        for ($i = 1; $i <= $period; $i++) {
            $fees = 0;
            if ($i == 1) {
                $fees = $fees + ($fees_first_payment);
            }
            if ($i == $period) {
                $fees = $fees + ($fees_last_payment);
            }
            $fees = $fees + ($fees_distribute / $period);
            $loan_schedule = new LoanSchedule();
            $loan_schedule->loan_id = $loan->id;
            $loan_schedule->fees = $fees;
            $loan_schedule->branch_id = session('branch_id');
            $loan_schedule->borrower_id = $loan->borrower_id;
            $loan_schedule->description = trans_choice('general.repayment', 1);
            $loan_schedule->due_date = $next_payment;
          
                    $loan_schedule->month = $date[1];
            $loan_schedule->year = $date[0];
            //determine which method to use
            $due = 0;
            //reducing balance equal installments
            if ($loan->interest_method == 'declining_balance_equal_installments') {
                $due = GeneralHelper::amortized_monthly_payment($loan->id, $loan->principal);

                if ($loan->decimal_places == 'round_off_to_two_decimal') {
                    //determine if we have grace period for interest

                    $interest = round(($interest_rate * $balance), 2);
                    $loan_schedule->principal = round(($due - $interest), 2);
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest, 2);
                    }
                    $loan_schedule->due = round($due, 2);
                    //determine next balance
                    $balance = round(($balance - ($due - $interest)), 2);
                    $loan_schedule->principal_balance = round($balance, 2);
                } else {
                    //determine if we have grace period for interest

                    $interest = round(($interest_rate * $balance));
                    $loan_schedule->principal = round(($due - $interest));
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest);
                    }
                    $loan_schedule->due = round($due);
                    //determine next balance
                    $balance = round(($balance - ($due - $interest)));
                    $loan_schedule->principal_balance = round($balance);
                }


            }
            //reducing balance equal principle
            if ($loan->interest_method == 'declining_balance_equal_principal') {
                $principal = $loan->principal / $period;
                if ($loan->decimal_places == 'round_off_to_two_decimal') {

                    $interest = round(($interest_rate * $balance), 2);
                    $loan_schedule->principal = round($principal, 2);
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest, 2);
                    }
                    $loan_schedule->due = round(($principal + $interest), 2);
                    //determine next balance
                    $balance = round(($balance - ($principal + $interest)), 2);
                    $loan_schedule->principal_balance = round($balance, 2);
                } else {

                    $loan_schedule->principal = round(($principal));

                    $interest = round(($interest_rate * $balance));
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest);
                    }
                    $loan_schedule->due = round($principal + $interest);
                    //determine next balance
                    $balance = round(($balance - ($principal + $interest)));
                    $loan_schedule->principal_balance = round($balance);
                }

            }
            //flat  method
            if ($loan->interest_method == 'flat_rate') {
                $principal = $loan->principal / $period;
                if ($loan->decimal_places == 'round_off_to_two_decimal') {
                    $interest = round(($interest_rate * $loan->principal), 2);
                    $loan_schedule->principal = round(($principal), 2);
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest, 2);
                    }
                    $loan_schedule->principal = round(($principal), 2);
                    $loan_schedule->due = round(($principal + $interest), 2);
                    //determine next balance
                    $balance = round(($balance - $principal), 2);
                    $loan_schedule->principal_balance = round($balance, 2);
                } else {
                    $interest = round(($interest_rate * $loan->principal));
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest);
                    }
                    $loan_schedule->principal = round($principal);
                    $loan_schedule->due = round($principal + $interest);
                    //determine next balance
                    $balance = round(($balance - $principal));
                    $loan_schedule->principal_balance = round($balance);
                }
            }
            //interest only method
            if ($loan->interest_method == 'interest_only') {
                if ($i == $period) {
                    $principal = $loan->principal;
                } else {
                    $principal = 0;
                }
                if ($loan->decimal_places == 'round_off_to_two_decimal') {
                    $interest = round(($interest_rate * $loan->principal), 2);
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest, 2);
                    }
                    $loan_schedule->principal = round(($principal), 2);
                    $loan_schedule->due = round(($principal + $interest), 2);
                    //determine next balance
                    $balance = round(($balance - $principal), 2);
                    $loan_schedule->principal_balance = round($balance, 2);
                } else {
                    $interest = round(($interest_rate * $loan->principal));
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest);
                    }
                    $loan_schedule->principal = round($principal);
                    $loan_schedule->due = round($principal + $interest);
                    //determine next balance
                    $balance = round(($balance - $principal));
                    $loan_schedule->principal_balance = round($balance);
                }
            }
            //determine next due date
            if ($loan->repayment_cycle == 'daily') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('1 days')),
                    'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'weekly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('1 weeks')),
                    'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'monthly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('1 months')),
                    'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'bi_monthly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('2 months')),
                    'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'quarterly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('4 months')),
                    'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'semi_annually') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('6 months')),
                    'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'yearly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                    date_interval_create_from_date_string('1 years')),
                    'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($i == $period) {
                $loan_schedule->principal_balance = round($balance);
            }
            $loan_schedule->save();
        }
        $loan = Loan::find($loan->id);
        $loan->maturity_date = $next_payment;
        $loan->save();
        GeneralHelper::audit_trail("Rescheduled loan with id:" . $l->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect('loan/' . $loan->id . '/show');
    }

    public function undisburse($id)
    {
        if (!Sentinel::hasAccess('loans.disburse')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        //delete previously created schedules and payments
        LoanSchedule::where('loan_id', $id)->delete();
        LoanRepayment::where('loan_id', $id)->delete();
        $loan = Loan::find($id);
        $loan->status = 'approved';
        $loan->save();
        GeneralHelper::audit_trail("Undisbursed loan with id:" . $loan->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect('loan/' . $loan->id . '/show');
    }

    public function decline(Request $request, $id)
    {
        if (!Sentinel::hasAccess('loans.approve')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $loan = Loan::find($id);
        $loan->status = 'declined';
        $loan->declined_date = $request->declined_date;
        $loan->declined_notes = $request->declined_notes;
        $loan->declined_by_id = Sentinel::getUser()->id;
        $loan->save();
        GeneralHelper::audit_trail("Declined loan with id:" . $loan->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect('loan/' . $id . '/show');
    }

    public function write_off(Request $request, $id)
    {
        if (!Sentinel::hasAccess('loans.writeoff')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $loan = Loan::find($id);
        $loan->status = 'written_off';
        $loan->written_off_date = $request->written_off_date;
        $loan->written_off_notes = $request->written_off_notes;
        $loan->written_off_by_id = Sentinel::getUser()->id;
        $loan->save();
        GeneralHelper::audit_trail("Writeoff loan with id:" . $loan->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect('loan/' . $id . '/show');
    }

    public function withdraw(Request $request, $id)
    {
        if (!Sentinel::hasAccess('loans.withdraw')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $loan = Loan::find($id);
        $loan->status = 'withdrawn';
        $loan->withdrawn_date = $request->withdrawn_date;
        $loan->withdrawn_notes = $request->withdrawn_notes;
        $loan->withdrawn_by_id = Sentinel::getUser()->id;
        $loan->save();
        GeneralHelper::audit_trail("Withdraw loan with id:" . $loan->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect('loan/' . $id . '/show');
    }

    public function unwithdraw($id)
    {
        if (!Sentinel::hasAccess('loans.withdraw')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $loan = Loan::find($id);
        $loan->status = 'disbursed';
        $loan->save();
        GeneralHelper::audit_trail("Unwithdraw loan with id:" . $loan->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect('loan/' . $id . '/show');
    }

    public function unwrite_off($id)
    {
        if (!Sentinel::hasAccess('loans.writeoff')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $loan = Loan::find($id);
        $loan->status = 'disbursed';
        $loan->save();
        GeneralHelper::audit_trail("Unwriteoff loan with id:" . $loan->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect('loan/' . $id . '/show');
    }

    public function edit($loan)
    {
        if (!Sentinel::hasAccess('loans.update')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $borrowers = array();
        foreach (Borrower::all() as $key) {
            $borrowers[$key->id] = $key->first_name . ' ' . $key->last_name . '(' . $key->unique_number . ')';
        }
        $loan_products = array();
        foreach (LoanProduct::all() as $key) {
            $loan_products[$key->id] = $key->name;
        }

        $loan_disbursed_by = array();
        foreach (LoanDisbursedBy::all() as $key) {
            $loan_disbursed_by[$key->id] = $key->name;
        }

        //get custom fields
        $custom_fields = CustomField::where('category', 'loans')->get();
        $loan_fees = LoanFee::all();
        $user = DB::table("borrowers")->where(["id"=>$loan->borrower_id])->first();

        return view('loan.edit',
            compact('loan', 'borrowers', 'loan_disbursed_by', 'loan_products', 'custom_fields', 'loan_fees','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Sentinel::hasAccess('loans.update')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $loan = Loan::find($id);
        $loan->principal = $request->principal;
        $loan->applied_amount = $request->principal;
        $loan->interest_method = $request->interest_method;
        $loan->interest_rate = $request->interest_rate;
        $loan->interest_period = $request->interest_period;
        $loan->loan_duration = $request->loan_duration;
        $loan->loan_duration_type = $request->loan_duration_type;
        $loan->repayment_cycle = $request->repayment_cycle;
        $loan->decimal_places = $request->decimal_places;
        $loan->override_interest = $request->override_interest;
        $loan->override_interest_amount = $request->override_interest_amount;
        $loan->grace_on_interest_charged = $request->grace_on_interest_charged;
        $loan->borrower_id = $request->borrower_id;
        $loan->loan_product_id = $request->loan_product_id;
        $loan->release_date =   \Carbon\Carbon::parse($request->release_date);
        // $date = explode('-', $request->release_date);
            $loan->month = $date[1];
        $loan->year = $date[0];
        if (!empty($request->first_payment_date)) {
            $loan->first_payment_date = $request->first_payment_date;
        }

        $loan->description = $request->description;
        $files = unserialize($loan->files);
        $count = count($files);
        if (!empty(array_filter($request->file('files')))) {
            foreach ($request->file('files') as $key) {
                $count++;
                $file = array('files' => $key);
                $rules = array('files' => 'required|mimes:jpeg,jpg,bmp,png,pdf,docx,xlsx');
                $validator = Validator::make($file, $rules);
                if ($validator->fails()) {
                    Flash::warning(trans('general.validation_error'));
                    return redirect()->back()->withInput()->withErrors($validator);
                } else {
                    $files[$count] = $key->getClientOriginalName();
                    $key->move(public_path() . '/uploads',
                        $key->getClientOriginalName());
                }

            }
        }
        $loan->files = serialize($files);
        $loan->save();
        $custom_fields = CustomField::where('category', 'loans')->get();
        foreach ($custom_fields as $key) {
            if (!empty(CustomFieldMeta::where('custom_field_id', $key->id)->where('parent_id', $id)->where('category',
                'loans')->first())
            ) {
                $custom_field = CustomFieldMeta::where('custom_field_id', $key->id)->where('parent_id',
                    $id)->where('category', 'loans')->first();
            } else {
                $custom_field = new CustomFieldMeta();
            }
            $kid = $key->id;
            $custom_field->name = $request->$kid;
            $custom_field->parent_id = $id;
            $custom_field->custom_field_id = $key->id;
            $custom_field->category = "loans";
            $custom_field->save();
        }
        foreach (LoanFee::all() as $key) {
            if (!empty(LoanFeeMeta::where('loan_fees_id', $key->id)->where('parent_id', $id)->where('category',
                'loan')->first())
            ) {
                $loan_fee = LoanFeeMeta::where('loan_fees_id', $key->id)->where('parent_id', $id)->where('category',
                    'loan')->first();
            } else {
                $loan_fee = new LoanFeeMeta();
            }

            $value = 'loan_fees_amount_' . $key->id;
            $loan_fees_schedule = 'loan_fees_schedule_' . $key->id;
            $loan_fee->user_id = Sentinel::getUser()->id;
            $loan_fee->category = 'loan';
            $loan_fee->parent_id = $loan->id;
            $loan_fee->loan_fees_id = $key->id;
            $loan_fee->value = $request->$value;
            $loan_fee->loan_fees_schedule = $request->$loan_fees_schedule;
            $loan_fee->save();
        }
        GeneralHelper::audit_trail("Updated loan with id:" . $loan->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect('loan/data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        if (!Sentinel::hasAccess('loans.delete')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        Loan::destroy($id);
        LoanSchedule::where('loan_id', $id)->delete();
        LoanRepayment::where('loan_id', $id)->delete();
        DB::table("loan_schedules")->where(["loan_id"=>$id])->delete();
        Guarantor::where('loan_id', $id)->delete();
        GeneralHelper::audit_trail("Deleted loan with id:" . $id);
        Flash::success(trans('general.successfully_deleted'));
        return redirect('loan/data');
    }

    public function deleteFile(Request $request, $id)
    {
        if (!Sentinel::hasAccess('loans.delete')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $loan = Loan::find($id);
        $files = unserialize($loan->files);
        @unlink(public_path() . '/uploads/' . $files[$request->id]);
        $files = array_except($files, [$request->id]);
        $loan->files = serialize($files);
        $loan->save();


    }
    
    public function apirepayment(Request $request){
        $datas  = [];
        if (!Sentinel::hasAccess('repayments')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }

        $data = LoanRepayment::where('branch_id', session('branch_id'))->orderBy("id","DESC")->get();
        if(isset($_GET['search'])){
            $text = $_GET['search'];
            $data = LoanRepayment::where('branch_id', session('branch_id'))->where(function ($query) use($text) {
                $query
                    ->orWhere('loan_repayments.loan_id', 'like', '%' . $text . '%')
                    ->orWhere('loan_repayments.sc', 'like', '%' . $text . '%')
                    ->orWhere('loan_repayments.borrower_id', 'like', '%' . $text . '%')
                    ->orWhere('loan_repayments.amount', 'like', '%' . $text . '%')
                    ->orWhere('loan_repayments.collection_date', 'like', '%' . $text . '%')
                    ->orWhere('loan_repayments.month', 'like', '%' . $text . '%')
                    ->orWhere('loan_repayments.year', 'like', '%' . $text . '%')

                    ->orWhere('loan_repayments.created_at', 'like', '%' . $text . '%');
            })->orderBy("loan_repayments.id","DESC")->get();

        }

        if(isset($_GET['date'])){
            $data = LoanRepayment::where('branch_id', session('branch_id'))->where('created_at', 'like', '%' . $_GET['date'] . '%')->orderBy("id","DESC")->get();
        }

        if(isset($_GET['loan_officer'])){
            $data = LoanRepayment::where('branch_id', session('branch_id'))
                ->join("loan_officers","loan_officers.borrower_id","=","loan_repayments.borrower_id")
                ->where(["loan_officers.officer_id"=>$_GET['loan_officer']])
                ->orderBy("loan_repayments.id","DESC")
                ->get();


        }
        if(isset($_GET['date']) && isset($_GET['loan_officer'])){
            $data = LoanRepayment::where('branch_id', session('branch_id'))
                ->join("loan_officers","loan_officers.borrower_id","=","loan_repayments.borrower_id")
                ->where(["loan_officers.officer_id"=>$_GET['loan_officer']])
                ->where('loan_repayments.created_at', 'like', '%' . $_GET['date'] . '%')
                ->orderBy("loan_repayments.id","DESC")
                ->get();
        }
        foreach($data as $key){
            $cdate = $key->collection_date;
            $borrower = "-";
            if(!empty($key->borrower)){
                if($key->loan->group_id != null){
                    $name =' <a href="'.url('borrower/'.$key->borrower_id.'/show').'">'.$key->borrower->first_name.' '.$key->borrower->last_name.'</a>'.'<a href="/borrower/group/'.$key->loan->group_id.'/vloans" target="_blank"> <span class="badge badge-danger bg-red">group loan</span> </a>';
                }else{
                    $name =' <a href="'.url('borrower/'.$key->borrower_id.'/show').'">'.$key->borrower->first_name.' '.$key->borrower->last_name.'</a>';
                }

            }else{
                $name =' <span class="label label-danger">'.trans_choice('general.broken',1).' <i
                class="fa fa-exclamation-triangle"></i> </span>';
            }
            $borrower = $name;
            $user = "";
            if(!empty($key->user)){
            $user = $key->user->first_name." ".$key->user->last_name;
            }
            if(!empty($key->loan_repayment_method)){
            $rm = $key->loan_repayment_method->name;
            }
            $amount = number_format($key->amount,2);
            $action1 = "-";
            $action2 = "-";
               if(Sentinel::hasAccess('repayments.update')){
                $action1 = '<a type="button" class="btn bg-white btn-xs text-bold"
                href="'.url('loan/'.$key->loan_id.'/repayment/'.$key->id.'/edit').'">'.trans_choice('general.edit',1).'</a>';
              }
                if(Sentinel::hasAccess('repayments.delete')){
               $action2 = ' <a type="button"
                class="btn bg-white btn-xs text-bold deletePayment"
                href="'.url('loan/'.$key->loan_id.'/repayment/'.$key->id.'/delete').'"
                >revert</a>';
              }
            $action = '<div class="btn-group-horizontal">
                    '.$action1.'
                    '.$action2.'
                </div>';

        $actions = ' <a type="button" class="btn btn-default btn-xs"
                               href="'.url('loan/'.$key->loan_id.'/repayment/'.$key->id.'/print').'"
                               target="_blank">
                                                                <span class="glyphicon glyphicon-print"
                                                                      aria-hidden="true"></span>
                            </a>
                            <a type="button" class="btn btn-default btn-xs"
                               href="'.url('loan/'.$key->loan_id.'/repayment/'.$key->id.'/pdf').'"
                               target="_blank">
                                                                <span class="glyphicon glyphicon-file"
                                                                      aria-hidden="true"></span>
                            </a>';

            array_push($datas,[$cdate,$borrower,$user,$rm,$amount,$action,$actions]);

        }

    return response()->json(["data"=>$datas]);
    }

    public function indexRepayment()
    {
        if (!Sentinel::hasAccess('repayments')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $total = 0;
        $data = LoanRepayment::where('branch_id', session('branch_id'))->orderBy("id","DESC")->get();
        if(isset($_GET['search'])){
            $text = $_GET['search'];
            $data = LoanRepayment::where('branch_id', session('branch_id'))->where(function ($query) use($text) {
                $query
                    ->orWhere('loan_repayments.sc', 'like', '%' . $text . '%')
                    ->orWhere('loan_repayments.loan_id', 'like', '%' . $text . '%')
                    ->orWhere('loan_repayments.borrower_id', 'like', '%' . $text . '%')
                    ->orWhere('loan_repayments.amount', 'like', '%' . $text . '%')
                    ->orWhere('loan_repayments.collection_date', 'like', '%' . $text . '%')
                    ->orWhere('loan_repayments.month', 'like', '%' . $text . '%')
                    ->orWhere('loan_repayments.year', 'like', '%' . $text . '%')

                    ->orWhere('loan_repayments.created_at', 'like', '%' . $text . '%');
            })->orderBy("loan_repayments.id","DESC")->get();

        }

        if(isset($_GET['date'])){
            $data = LoanRepayment::where('branch_id', session('branch_id'))->where('created_at', 'like', '%' . $_GET['date'] . '%')->orderBy("id","DESC")->get();
        }

        if(isset($_GET['loan_officer'])){
            $data = LoanRepayment::where('branch_id', session('branch_id'))
                ->join("loan_officers","loan_officers.borrower_id","=","loan_repayments.borrower_id")
                ->where(["loan_officers.officer_id"=>$_GET['loan_officer']])
                ->orderBy("loan_repayments.id","DESC")
                ->get();


        }
        if(isset($_GET['date']) && isset($_GET['loan_officer'])){
            $data = LoanRepayment::where('branch_id', session('branch_id'))
                ->join("loan_officers","loan_officers.borrower_id","=","loan_repayments.borrower_id")
                ->where(["loan_officers.officer_id"=>$_GET['loan_officer']])
                ->where('loan_repayments.created_at', 'like', '%' . $_GET['date'] . '%')
                ->orderBy("loan_repayments.id","DESC")
                ->get();
        }
          $users = DB::table("users")->where(["branch_users.branch_id"=>session("branch_id")])
            ->join("branch_users","branch_users.user_id","=","users.id")
              ->join("role_users","role_users.user_id","=","users.id")->where(["role_users.role_id"=>4])->select("users.first_name","users.last_name","users.id")->get();


        foreach($data as $d){
            $total += $d->amount;
         }
        return view('loan_repayment.data', compact('data','text','users','total'));
    }

    
    public function bulkapi(){

        $loans = array();
        $data = [];

if(!empty($_GET['q'])){
    $text = $_GET['q'];
    $query = Borrower::query();
    $query->whereRaw("concat(first_name, ' ', last_name) like '%$text%' ");
    $columns = ['mobile', 'first_name', 'last_name','created_at','email','unique_number','username','title','working_status','gender','dob','address','phone','business_name'];
    foreach($columns as $column){
        $query->orWhere($column, 'LIKE', '%' . $text . '%');
    }
    $customers = $query->where(["branch_id"=>session("branch_id")])->limit(50)->orderBy("id","DESC")->get();
    foreach($customers as $c){
        $key = Loan::where(['loans.branch_id'=>session('branch_id'),'loans.group_id'=>NULL,"borrower_id"=>$c->id])->whereIn('status', ['disbursed', 'rescheduled'])->first();
        $loan_bal = number_format(GeneralHelper::loan_total_balance($key->id),2);

        //             $loan_bal = number_format(DB::table("loan_repayments")->where(["loan_id"=>$id])->sum("amount") - $key->balance ,2) ;



        //rest of your code...
        $name = $key->borrower->first_name . ' ' . $key->borrower->last_name . '(' . trans_choice('general.loan',
                                                                                                  1) . '#' . $key->id . ',' . trans_choice('general.due',
                                                                                                                                           1) . ':' . $loan_bal. ')';
        $id = $key->id;

        array_push($loans,["name"=>$name,"id"=>$id]);
    }
}
    /*     foreach (Loan::where(['loans.branch_id'=>session('branch_id'),'loans.group_id'=>NULL])->whereIn('status', ['disbursed', 'rescheduled'])->get() as $key) {

            $loan_bal = number_format(DB::table("loan_schedules")->where(["loan_id"=>$key->id])->sum("interest") + DB::table("loan_schedules")->where(["loan_id"=>$key->id])->sum("principal"),2);

//             $loan_bal = number_format(DB::table("loan_repayments")->where(["loan_id"=>$id])->sum("amount") - $key->balance ,2) ;



            //rest of your code...
            $name = $key->borrower->first_name . ' ' . $key->borrower->last_name . '(' . trans_choice('general.loan',
                                                                                                      1) . '#' . $key->id . ',' . trans_choice('general.due',
                                                                                                                                               1) . ':' . $loan_bal. ')';
            $id = $key->id;

            array_push($loans,["name"=>$name,"id"=>$id]);


        }
 */



        //         GeneralHelper::loan_total_balance($key->id)
      /*   foreach ($data as $key) {

            $name = $key->borrower->first_name . ' ' . $key->borrower->last_name . '(' . trans_choice('general.loan',
                                                                                                      1) . '#' . $key->id . ',' . trans_choice('general.due',
                                                                                                                                               1) . ':' . $loan_bal. ')';
            $id = $key->id;

            array_push($loans,["name"=>$name,"id"=>$id]);
        }
        $loans = array_unique($loans, SORT_REGULAR); */

        return response()->json($loans);
    }
    //loan repayments
    public function createBulkRepayment()
    {

        

        if (!Sentinel::hasAccess('repayments.create')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $loans = array();
       /*  ini_set('memory_limit', '828M');
        foreach (Loan::where(['branch_id'=>session('branch_id'),'group_id'=>NULL])->limit(50)->get() as $key) {
            $loans[$key->id] = $key->borrower->first_name . ' ' . $key->borrower->last_name . '(' . trans_choice('general.loan',
                    1) . '#' . $key->id . ',' . trans_choice('general.due',
                    1) . ':' . GeneralHelper::loan_total_balance($key->id) . ')';
        } */

        $repayment_methods = array();
        foreach (LoanRepaymentMethod::all() as $key) {
            $repayment_methods[$key->id] = $key->name;
        }
        $custom_fields = CustomField::where('category', 'repayments')->get();

        return view('loan_repayment.bulk', compact('loan', 'repayment_methods', 'custom_fields', 'loans'));
    }

    public function storeBulkRepayment(Request $request)
    {
        if (!Sentinel::hasAccess('repayments.create')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        for ($i = 1; $i <= 20; $i++) {
            $amount = "repayment_amount" . $i;
            $loan_id = "loan_id" . $i;
            $repayment_method = "repayment_method_id" . $i;
            $collected_date = "repayment_collected_date" . $i;
            $repayment_description = "repayment_description" . $i;
            if (!empty($request->$amount && !empty($request->$loan_id))) {
                $loan = Loan::find($request->$loan_id);
                if ($request->$amount > round(GeneralHelper::loan_total_balance($loan->id), 2)) {
                    Flash::warning("Amount is more than the balance(" . GeneralHelper::loan_total_balance($loan->id) . ')');
                    return redirect()->back()->withInput();

                } else {
                    $repayment = new LoanRepayment();
                    $repayment->user_id = Sentinel::getUser()->id;
                    $repayment->amount = $request->$amount;
                    $repayment->loan_id = $loan->id;
                    $repayment->borrower_id = $loan->borrower_id;
                    $repayment->branch_id = session('branch_id');
                    $repayment->collection_date = \Carbon\Carbon::parse($request->$collected_date);
                    $repayment->repayment_method_id = $request->$repayment_method;
                    $repayment->notes = $request->$repayment_description;

                            $repayment->year = $date[0];
                    $repayment->month = $date[1];
                    //determine which schedule due date the payment applies too
                    $schedule = LoanSchedule::where('due_date', '>=', $request->$collected_date)->where('loan_id',
                        $loan->id)->orderBy('due_date',
                        'asc')->first();
                    if (!empty($schedule)) {
                        $repayment->due_date = $schedule->due_date;
                    } else {
                        $schedule = LoanSchedule::where('loan_id',
                            $loan->id)->orderBy('due_date',
                            'desc')->first();
                        if ($request->$collected_date > $schedule->due_date) {
                            $repayment->due_date = $schedule->due_date;
                        } else {
                            $schedule = LoanSchedule::where('due_date', '>',
                                $request->$collected_date)->where('loan_id',
                                $loan->id)->orderBy('due_date',
                                'asc')->first();
                            $repayment->due_date = $schedule->due_date;
                        }

                    }
                    $repayment->save();

                    //update loan status if need be
                    if (round(GeneralHelper::loan_total_balance($loan->id), 2) == 0) {
                        $l = Loan::find($loan->id);
                        $l->status = "closed";
                        $l->save();

                    }
                    //check if late repayment is to be applied when adding payment

                    if (!empty($loan->loan_product)) {
                        if ($loan->loan_product->enable_late_repayment_penalty == 1) {
                            $schedules = LoanSchedule::where('due_date', '<',
                                $repayment->due_date)->where('missed_penalty_applied',
                                0)->orderBy('due_date', 'asc')->get();
                            foreach ($schedules as $schedule) {
                                if (GeneralHelper::loan_total_due_period($loan->id,
                                        $schedule->due_date) > GeneralHelper::loan_total_paid_period($loan->id,
                                        $schedule->due_date)
                                ) {
                                    $sch = LoanSchedule::find($schedule->id);
                                    $sch->missed_penalty_applied = 1;
                                    //determine which amount to use
                                    if ($loan->loan_product->late_repayment_penalty_type == "fixed") {
                                        $sch->penalty = $sch->penalty + $loan->loan_product->late_repayment_penalty_amount;
                                    } else {
                                        if ($loan->loan_product->late_repayment_penalty_calculate == 'overdue_principal') {
                                            $principal = (GeneralHelper::loan_total_principal($loan->id,
                                                    $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                    'principal', $schedule->due_date));
                                            $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                        }
                                        if ($loan->loan_product->late_repayment_penalty_calculate == 'overdue_principal_interest') {
                                            $principal = (GeneralHelper::loan_total_principal($loan->id,
                                                    $schedule->due_date) + GeneralHelper::loan_total_interest($loan->id,
                                                    $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                    'principal',
                                                    $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                    'interest', $schedule->due_date));
                                            $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                        }
                                        if ($loan->loan_product->late_repayment_penalty_calculate == 'overdue_principal_interest_fees') {
                                            $principal = (GeneralHelper::loan_total_principal($loan->id,
                                                    $schedule->due_date) + GeneralHelper::loan_total_interest($loan->id,
                                                    $schedule->due_date) + GeneralHelper::loan_total_fees($loan->id,
                                                    $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                    'principal',
                                                    $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                    'interest',
                                                    $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                    'fees',
                                                    $schedule->due_date));
                                            $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                        }
                                        if ($loan->loan_product->late_repayment_penalty_calculate == 'total_overdue') {
                                            $principal = (GeneralHelper::loan_total_due_amount($loan->id,
                                                    $schedule->due_date) - GeneralHelper::loan_total_paid($loan->id,
                                                    $schedule->due_date));
                                            $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                        }
                                    }
                                    $sch->save();
                                }
                            }
                        }

                    }
                }
            }
            //notify borrower


        }
        GeneralHelper::audit_trail("Added  bulk repayment");
        Flash::success("Repayment successfully saved");
        return redirect('repayment/data');
    }

    public function createRepayment($loan)
    {
        if (!Sentinel::hasAccess('repayments.create')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        if(session("branch_id") != 1){
        if($loan->borrower->branch_id != session("branch_id")){
            Flash::warning("Not Your Branch");
            return redirect()->back();
        }
    }
        $repayment_methods = array();
        foreach (LoanRepaymentMethod::all() as $key) {
            $repayment_methods[$key->id] = $key->name;
        }
       
        $custom_fields = CustomField::where('category', 'repayments')->get();

       
        return view('loan_repayment.create', compact('loan', 'repayment_methods', 'custom_fields'));
    }

    public function storeRepayment(Request $request, $loan)
    {
        

        if (!Sentinel::hasAccess('repayments.create')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        if ($request->amount > round(GeneralHelper::loan_total_balance($loan->id), 2)) {
            Flash::warning("Amount is more than the balance(" . GeneralHelper::loan_total_balance($loan->id) . ')');
            return redirect()->back()->withInput();

        } else {

            $repayment = new LoanRepayment();
            $repayment->user_id = Sentinel::getUser()->id;
            $repayment->amount = $request->amount;
            $repayment->loan_id = $loan->id;
            $repayment->borrower_id = $loan->borrower_id;
            $repayment->branch_id = session('branch_id');
            $repayment->collection_date = \Carbon\Carbon::parse($request->collection_date);
            $repayment->repayment_method_id = $request->repayment_method_id;
            $repayment->notes = $request->notes;
            $date = explode('-', $request->collection_date);
          
            $repayment->year = (int)$date[0];
            $repayment->month = (int)$date[1];

            //determine which schedule due date the payment applies too
            $schedule = LoanSchedule::where('due_date', '>=', $request->collection_date)->where('loan_id',
                $loan->id)->orderBy('due_date',
                'asc')->first();
            if (!empty($schedule)) {
                $repayment->due_date = $schedule->due_date;
            } else {
                $schedule = LoanSchedule::where('loan_id',
                    $loan->id)->orderBy('due_date',
                    'desc')->first();
                if ($request->collection_date > $schedule->due_date) {
                    $repayment->due_date = $schedule->due_date;
                } else {
                    $schedule = LoanSchedule::where('due_date', '>', $request->collection_date)->where('loan_id',
                        $loan->id)->orderBy('due_date',
                        'asc')->first();
                    $repayment->due_date = $schedule->due_date;
                }

            }
            $repayment->save();
            //save custom meta
            $custom_fields = CustomField::where('category', 'repayments')->get();
            foreach ($custom_fields as $key) {
                $custom_field = new CustomFieldMeta();
                $id = $key->id;
                $custom_field->name = $request->$id;
                $custom_field->parent_id = $repayment->id;
                $custom_field->custom_field_id = $key->id;
                $custom_field->category = "repayments";
                $custom_field->save();
            }
            //update loan status if need be
            if (round(GeneralHelper::loan_total_balance($loan->id), 2) == 0) {
                $l = Loan::find($loan->id);
                $l->status = "closed";
                $l->save();

            }
            //check if late repayment is to be applied when adding payment
            if ($request->apply_penalty == 1) {
                if (!empty($loan->loan_product)) {
                    if ($loan->loan_product->enable_late_repayment_penalty == 1) {
                        $schedules = LoanSchedule::where('due_date', '<',
                            $repayment->due_date)->where('missed_penalty_applied',
                            0)->orderBy('due_date', 'asc')->get();
                        foreach ($schedules as $schedule) {
                            if (GeneralHelper::loan_total_due_period($loan->id,
                                    $schedule->due_date) > GeneralHelper::loan_total_paid_period($loan->id,
                                    $schedule->due_date)
                            ) {
                                $sch = LoanSchedule::find($schedule->id);
                                $sch->missed_penalty_applied = 1;
                                //determine which amount to use
                                if ($loan->loan_product->late_repayment_penalty_type == "fixed") {
                                    $sch->penalty = $sch->penalty + $loan->loan_product->late_repayment_penalty_amount;
                                } else {
                                    if ($loan->loan_product->late_repayment_penalty_calculate == 'overdue_principal') {
                                        $principal = (GeneralHelper::loan_total_principal($loan->id,
                                                $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                'principal', $schedule->due_date));
                                        $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                    }
                                    if ($loan->loan_product->late_repayment_penalty_calculate == 'overdue_principal_interest') {
                                        $principal = (GeneralHelper::loan_total_principal($loan->id,
                                                $schedule->due_date) + GeneralHelper::loan_total_interest($loan->id,
                                                $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                'principal',
                                                $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                'interest', $schedule->due_date));
                                        $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                    }
                                    if ($loan->loan_product->late_repayment_penalty_calculate == 'overdue_principal_interest_fees') {
                                        $principal = (GeneralHelper::loan_total_principal($loan->id,
                                                $schedule->due_date) + GeneralHelper::loan_total_interest($loan->id,
                                                $schedule->due_date) + GeneralHelper::loan_total_fees($loan->id,
                                                $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                'principal',
                                                $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                'interest',
                                                $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id, 'fees',
                                                $schedule->due_date));
                                        $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                    }
                                    if ($loan->loan_product->late_repayment_penalty_calculate == 'total_overdue') {
                                        $principal = (GeneralHelper::loan_total_due_amount($loan->id,
                                                $schedule->due_date) - GeneralHelper::loan_total_paid($loan->id,
                                                $schedule->due_date));
                                        $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                    }
                                }
                                $sch->save();
                            }
                        }
                    }
                }
            }
            //notify borrower
            if ($request->notify_borrower == 1) {
                if ($request->notify_method == 'both') {
                    $borrower = $loan->borrower;
                    //sent via email
                    if (!empty($borrower->email)) {
                        $body = Setting::where('setting_key',
                            'payment_received_email_template')->first()->setting_value;
                        $body = str_replace('{borrowerTitle}', $borrower->title, $body);
                        $body = str_replace('{borrowerFirstName}', $borrower->first_name, $body);
                        $body = str_replace('{borrowerLastName}', $borrower->last_name, $body);
                        $body = str_replace('{borrowerAddress}', $borrower->address, $body);
                        $body = str_replace('{borrowerUniqueNumber}', $borrower->unique_number, $body);
                        $body = str_replace('{borrowerMobile}', $borrower->mobile, $body);
                        $body = str_replace('{borrowerPhone}', $borrower->phone, $body);
                        $body = str_replace('{borrowerEmail}', $borrower->email, $body);
                        $body = str_replace('{loanNumber}', '#' . $loan->id, $body);
                        $body = str_replace('{paymentAmount}', $request->amount, $body);
                        $body = str_replace('{paymentDate}', $request->date, $body);
                        $body = str_replace('{loanAmount}', $loan->principal, $body);
                        $body = str_replace('{loanDue}',
                            round(GeneralHelper::loan_total_due_amount($loan->id), 2), $body);
                        $body = str_replace('{loanBalance}',
                            round(GeneralHelper::loan_total_due_amount($loan->id) - GeneralHelper::loan_total_paid($loan->id),
                                2), $body);
                        $body = str_replace('{loansDue}',
                            round(GeneralHelper::borrower_loans_total_due($borrower->id), 2), $body);
                        $body = str_replace('{loansBalance}',
                            round((GeneralHelper::borrower_loans_total_due($borrower->id) - GeneralHelper::borrower_loans_total_paid($borrower->id)),
                                2), $body);
                        $body = str_replace('{loansPayments}',
                            GeneralHelper::borrower_loans_total_paid($borrower->id),
                            $body);
                        PDF::AddPage();
                        PDF::writeHTML(View::make('loan_repayment.pdf', compact('loan', 'repayment'))->render());
                        PDF::SetAuthor('Tererai Mugova');
                        PDF::Output(public_path() . '/uploads/temporary/repayment_receipt' . $loan->id . ".pdf", 'F');
                        $file_name = $loan->borrower->title . ' ' . $loan->borrower->first_name . ' ' . $loan->borrower->last_name . " - Repayment Receipt.pdf";
                        Mail::raw($body, function ($message) use ($loan, $request, $borrower, $file_name) {
                            $message->from(Setting::where('setting_key', 'company_email')->first()->setting_value,
                                Setting::where('setting_key', 'company_name')->first()->setting_value);
                            $message->to($borrower->email);
                            $headers = $message->getHeaders();
                            $message->attach(public_path() . '/uploads/temporary/repayment_receipt' . $loan->id . ".pdf",
                                ["as" => $file_name]);
                            $message->setContentType('text/html');
                            $message->setSubject(Setting::where('setting_key',
                                'payment_received_email_subject')->first()->setting_value);

                        });
                        unlink(public_path() . '/uploads/temporary/repayment_receipt' . $loan->id . ".pdf");
                        $mail = new Email();
                        $mail->user_id = Sentinel::getUser()->id;
                        $mail->message = $body;
                        $mail->subject = $request->subject;
                        $mail->recipients = 1;
                        $mail->send_to = $borrower->first_name . ' ' . $borrower->last_name . '(' . $borrower->unique_number . ')';
                        $mail->save();
                    }
                    if (!empty($borrower->mobile)) {
                        if (Setting::where('setting_key', 'sms_enabled')->first()->setting_value == 1) {
                            //lets build and replace available tags
                            $body = Setting::where('setting_key',
                                'payment_received_sms_template')->first()->setting_value;
                            $body = str_replace('{borrowerTitle}', $borrower->title, $body);
                            $body = str_replace('{borrowerFirstName}', $borrower->first_name, $body);
                            $body = str_replace('{borrowerLastName}', $borrower->last_name, $body);
                            $body = str_replace('{borrowerAddress}', $borrower->address, $body);
                            $body = str_replace('{borrowerMobile}', $borrower->mobile, $body);
                            $body = str_replace('{borrowerEmail}', $borrower->email, $body);
                            $body = str_replace('{loanNumber}', '#' . $loan->id, $body);
                            $body = str_replace('{paymentAmount}', $request->amount, $body);
                            $body = str_replace('{paymentDate}', $request->date, $body);
                            $body = str_replace('{loanAmount}', $loan->principal, $body);
                            $body = str_replace('{loanTotalDue}',
                                round(GeneralHelper::loan_total_due_amount($loan->id), 2), $body);
                            $body = str_replace('{loanBalance}',
                                round(GeneralHelper::loan_total_due_amount($loan->id) - GeneralHelper::loan_total_paid($loan->id),
                                    2), $body);
                            $body = str_replace('{lLoansDue}',
                                round(GeneralHelper::borrower_loans_total_due($borrower->id), 2), $body);
                            $body = str_replace('{loansBalance}',
                                round((GeneralHelper::borrower_loans_total_due($borrower->id) - GeneralHelper::borrower_loans_total_paid($borrower->id)),
                                    2), $body);
                            $body = str_replace('{loansPayments}',
                                GeneralHelper::borrower_loans_total_paid($borrower->id),
                                $body);
                            $body = trim(strip_tags($body));
                            if (!empty($borrower->mobile)) {
                                $active_sms = Setting::where('setting_key', 'active_sms')->first()->setting_value;
                                if ($active_sms == 'twilio') {
                                    $twilio = new Twilio(Setting::where('setting_key',
                                        'twilio_sid')->first()->setting_value,
                                        Setting::where('setting_key', 'twilio_token')->first()->setting_value,
                                        Setting::where('setting_key', 'twilio_phone_number')->first()->setting_value);
                                    $twilio->message('+' . $borrower->mobile, $body);
                                }
                                if ($active_sms == 'routesms') {
                                    $host = Setting::where('setting_key', 'routesms_host')->first()->setting_value;
                                    $port = Setting::where('setting_key', 'routesms_port')->first()->setting_value;
                                    $username = Setting::where('setting_key',
                                        'routesms_username')->first()->setting_value;
                                    $password = Setting::where('setting_key',
                                        'routesms_password')->first()->setting_value;
                                    $sender = Setting::where('setting_key', 'sms_sender')->first()->setting_value;
                                    $SMSText = $body;
                                    $GSM = $borrower->mobile;
                                    $msgtype = 2;
                                    $dlr = 1;
                                    $routesms = new RouteSms($host, $port, $username, $password, $sender, $SMSText,
                                        $GSM, $msgtype,
                                        $dlr);
                                    $routesms->Submit();
                                }
                                if ($active_sms == 'clickatell') {
                                    $clickatell = new Rest(Setting::where('setting_key',
                                        'clickatell_api_id')->first()->setting_value);
                                    $response = $clickatell->sendMessage(array($borrower->mobile), $body);
                                }
                                if ($active_sms == 'infobip') {
                                    $infobip = new Infobip(Setting::where('setting_key',
                                        'sms_sender')->first()->setting_value, $body,
                                        $borrower->mobile);
                                }
                                $sms = new Sms();
                                $sms->user_id = Sentinel::getUser()->id;
                                $sms->message = $body;
                                $sms->gateway = $active_sms;
                                $sms->recipients = 1;
                                $sms->send_to = $borrower->first_name . ' ' . $borrower->last_name . '(' . $borrower->unique_number . ')';
                                $sms->save();

                            }

                        }
                    }
                    //send via sms
                }
                if ($request->notify_method == 'email') {
                    $borrower = $loan->borrower;
                    //sent via email
                    if (!empty($borrower->email)) {
                        $body = Setting::where('setting_key',
                            'payment_received_email_template')->first()->setting_value;
                        $body = str_replace('{borrowerTitle}', $borrower->title, $body);
                        $body = str_replace('{borrowerFirstName}', $borrower->first_name, $body);
                        $body = str_replace('{borrowerLastName}', $borrower->last_name, $body);
                        $body = str_replace('{borrowerAddress}', $borrower->address, $body);
                        $body = str_replace('{borrowerUniqueNumber}', $borrower->unique_number, $body);
                        $body = str_replace('{borrowerMobile}', $borrower->mobile, $body);
                        $body = str_replace('{borrowerPhone}', $borrower->phone, $body);
                        $body = str_replace('{borrowerEmail}', $borrower->email, $body);
                        $body = str_replace('{loanNumber}', '#' . $loan->id, $body);
                        $body = str_replace('{paymentAmount}', $request->amount, $body);
                        $body = str_replace('{paymentDate}', $request->date, $body);
                        $body = str_replace('{loanAmount}', $loan->principal, $body);
                        $body = str_replace('{loanDue}',
                            round(GeneralHelper::loan_total_due_amount($loan->id), 2), $body);
                        $body = str_replace('{loanBalance}',
                            round(GeneralHelper::loan_total_due_amount($loan->id) - GeneralHelper::loan_total_paid($loan->id),
                                2), $body);
                        $body = str_replace('{loansDue}',
                            round(GeneralHelper::borrower_loans_total_due($borrower->id), 2), $body);
                        $body = str_replace('{loansBalance}',
                            round((GeneralHelper::borrower_loans_total_due($borrower->id) - GeneralHelper::borrower_loans_total_paid($borrower->id)),
                                2), $body);
                        $body = str_replace('{loansPayments}',
                            GeneralHelper::borrower_loans_total_paid($borrower->id),
                            $body);
                        PDF::AddPage();
                        PDF::writeHTML(View::make('loan_repayment.pdf', compact('loan', 'repayment'))->render());
                        PDF::SetAuthor('Tererai Mugova');
                        PDF::Output(public_path() . '/uploads/temporary/repayment_receipt' . $loan->id . ".pdf", 'F');
                        $file_name = $loan->borrower->title . ' ' . $loan->borrower->first_name . ' ' . $loan->borrower->last_name . " - Repayment Receipt.pdf";
                        Mail::raw($body, function ($message) use ($loan, $request, $borrower, $file_name) {
                            $message->from(Setting::where('setting_key', 'company_email')->first()->setting_value,
                                Setting::where('setting_key', 'company_name')->first()->setting_value);
                            $message->to($borrower->email);
                            $headers = $message->getHeaders();
                            $message->attach(public_path() . '/uploads/temporary/repayment_receipt' . $loan->id . ".pdf",
                                ["as" => $file_name]);
                            $message->setContentType('text/html');
                            $message->setSubject(Setting::where('setting_key',
                                'payment_received_email_subject')->first()->setting_value);

                        });
                        unlink(public_path() . '/uploads/temporary/repayment_receipt' . $loan->id . ".pdf");
                        $mail = new Email();
                        $mail->user_id = Sentinel::getUser()->id;
                        $mail->message = $body;
                        $mail->subject = $request->subject;
                        $mail->recipients = 1;
                        $mail->send_to = $borrower->first_name . ' ' . $borrower->last_name . '(' . $borrower->unique_number . ')';
                        $mail->save();
                    }
                }
                if ($request->notify_method == 'sms') {
                    $borrower = $loan->borrower;
                    if (!empty($borrower->mobile)) {
                        if (Setting::where('setting_key', 'sms_enabled')->first()->setting_value == 1) {
                            //lets build and replace available tags
                            $body = Setting::where('setting_key',
                                'payment_received_sms_template')->first()->setting_value;
                            $body = str_replace('{borrowerTitle}', $borrower->title, $body);
                            $body = str_replace('{borrowerFirstName}', $borrower->first_name, $body);
                            $body = str_replace('{borrowerLastName}', $borrower->last_name, $body);
                            $body = str_replace('{borrowerAddress}', $borrower->address, $body);
                            $body = str_replace('{borrowerMobile}', $borrower->mobile, $body);
                            $body = str_replace('{borrowerEmail}', $borrower->email, $body);
                            $body = str_replace('{loanNumber}', '#' . $loan->id, $body);
                            $body = str_replace('{paymentAmount}', $request->amount, $body);
                            $body = str_replace('{paymentDate}', $request->date, $body);
                            $body = str_replace('{loanAmount}', $loan->principal, $body);
                            $body = str_replace('{loanTotalDue}',
                                round(GeneralHelper::loan_total_due_amount($loan->id), 2), $body);
                            $body = str_replace('{loanBalance}',
                                round(GeneralHelper::loan_total_due_amount($loan->id) - GeneralHelper::loan_total_paid($loan->id),
                                    2), $body);
                            $body = str_replace('{lLoansDue}',
                                round(GeneralHelper::borrower_loans_total_due($borrower->id), 2), $body);
                            $body = str_replace('{loansBalance}',
                                round((GeneralHelper::borrower_loans_total_due($borrower->id) - GeneralHelper::borrower_loans_total_paid($borrower->id)),
                                    2), $body);
                            $body = str_replace('{loansPayments}',
                                GeneralHelper::borrower_loans_total_paid($borrower->id),
                                $body);
                            $body = trim(strip_tags($body));
                            if (!empty($borrower->mobile)) {
                                $active_sms = Setting::where('setting_key', 'active_sms')->first()->setting_value;
                                if ($active_sms == 'twilio') {
                                    $twilio = new Twilio(Setting::where('setting_key',
                                        'twilio_sid')->first()->setting_value,
                                        Setting::where('setting_key', 'twilio_token')->first()->setting_value,
                                        Setting::where('setting_key', 'twilio_phone_number')->first()->setting_value);
                                    $twilio->message('+' . $borrower->mobile, $body);
                                }
                                if ($active_sms == 'routesms') {
                                    $host = Setting::where('setting_key', 'routesms_host')->first()->setting_value;
                                    $port = Setting::where('setting_key', 'routesms_port')->first()->setting_value;
                                    $username = Setting::where('setting_key',
                                        'routesms_username')->first()->setting_value;
                                    $password = Setting::where('setting_key',
                                        'routesms_password')->first()->setting_value;
                                    $sender = Setting::where('setting_key', 'sms_sender')->first()->setting_value;
                                    $SMSText = $body;
                                    $GSM = $borrower->mobile;
                                    $msgtype = 2;
                                    $dlr = 1;
                                    $routesms = new RouteSms($host, $port, $username, $password, $sender, $SMSText,
                                        $GSM, $msgtype,
                                        $dlr);
                                    $routesms->Submit();
                                }
                                if ($active_sms == 'clickatell') {
                                    $clickatell = new Rest(Setting::where('setting_key',
                                        'clickatell_api_id')->first()->setting_value);
                                    $response = $clickatell->sendMessage(array($borrower->mobile), $body);
                                }
                                if ($active_sms == 'infobip') {
                                    $infobip = new Infobip(Setting::where('setting_key',
                                        'sms_sender')->first()->setting_value, $body,
                                        $borrower->mobile);
                                }
                                $sms = new Sms();
                                $sms->user_id = Sentinel::getUser()->id;
                                $sms->message = $body;
                                $sms->gateway = $active_sms;
                                $sms->recipients = 1;
                                $sms->send_to = $borrower->first_name . ' ' . $borrower->last_name . '(' . $borrower->unique_number . ')';
                                $sms->save();

                            }

                        }
                    }
                }
            }

            $lbalance = DB::table("loan_repayments")->where(["loan_id"=>$loan->id])->sum("amount");
            if($lbalance == round(\App\Helpers\GeneralHelper::loan_total_due_amount($loan->id),2)){
                DB::table("loans")->where(["id"=>$loan->id])->update(["status"=>"closed"]);
            }
            GeneralHelper::audit_trail("Added repayment for loan with id:" . $loan->id);
            Flash::success("Repayment successfully saved");
            return redirect('loan/' . $loan->id . '/show');
        }
    }

    public function deleteRepayment($loan, $id)
    {
        if (!Sentinel::hasAccess('repayments.delete')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        LoanRepayment::destroy($id);
        if (GeneralHelper::loan_total_balance($loan->id) > 0 && $loan->status == "closed") {
            $l = Loan::find($loan->id);
            $l->status = "disbursed";
            $l->save();
            DB::table("grouploan")->where(["sc"=>$l->sc])->decrement("balance",$l->approved_amount);
        }
        GeneralHelper::audit_trail("Deleted repayment for loan with id:" . $loan->id);
        Flash::success("Repayment successfully deleted");
        return redirect('loan/' . $loan->id . '/show');
    }

//    print repayment
    public function pdfRepayment($loan, $repayment)
    {
        PDF::AddPage();
        PDF::writeHTML(View::make('loan_repayment.pdf', compact('loan', 'repayment'))->render());
        PDF::SetAuthor('Tererai Mugova');
        PDF::Output($loan->borrower->title . ' ' . $loan->borrower->first_name . ' ' . $loan->borrower->last_name . " - Repayment Receipt.pdf",
            'D');
    }

    public function printRepayment($loan, $repayment)
    {

        return view('loan_repayment.print', compact('loan', 'repayment'));
    }

    public function editRepayment($loan, $repayment)
    {
        

        if (!Sentinel::hasAccess('repayments.update')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $repayment_methods = array();
        foreach (LoanRepaymentMethod::all() as $key) {
            $repayment_methods[$key->id] = $key->name;
        }
        $custom_fields = CustomField::where('category', 'repayments')->get();
        return view('loan_repayment.edit', compact('loan', 'repayment_methods', 'custom_fields', 'repayment'));
    }

    public function updateRepayment(Request $request, $loan, $id)
    {
        if (!Sentinel::hasAccess('repayments.update')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        
        if ($request->amount > round(GeneralHelper::loan_total_balance($loan->id), 2)) {
            Flash::warning("Amount is more than the balance(" . GeneralHelper::loan_total_balance($loan->id) . ')');
            return redirect()->back()->withInput();

        }

        $repayment = LoanRepayment::find($id);
        DB::table("grouploan")->where(["sc"=>$loan->sc])->decrement("balance",$repayment->amount);
        DB::table("grouploan")->where(["sc"=>$loan->sc])->increment("balance",$request->amount);
        $repayment->amount = $request->amount;
        $repayment->loan_id = $loan->id;
        $repayment->collection_date = $request->collection_date;
        $repayment->repayment_method_id = $request->repayment_method_id;
        $repayment->notes = $request->notes;
        $date = explode('-', $request->collection_date);
        $repayment->year = $date[0];
        $repayment->month = $date[1];
        //determine which schedule due date the payment applies too
        $schedule = LoanSchedule::where('due_date', '>=', $request->collection_date)->where('loan_id',
            $loan->id)->orderBy('due_date',
            'asc')->first();
        if (!empty($schedule)) {
            $repayment->due_date = $schedule->due_date;
        } else {
            $schedule = LoanSchedule::where('loan_id',
                $loan->id)->orderBy('due_date',
                'desc')->first();
            if ($request->collection_date > $schedule->due_date) {
                $repayment->due_date = $schedule->due_date;
            } else {
                $schedule = LoanSchedule::where('due_date', '>', $request->collection_date)->where('loan_id',
                    $loan->id)->orderBy('due_date',
                    'asc')->first();
                $repayment->due_date = $schedule->due_date;
            }

        }
        $repayment->save();
        //save custom meta
        $custom_fields = CustomField::where('category', 'repayments')->get();
        foreach ($custom_fields as $key) {
            if (!empty(CustomFieldMeta::where('custom_field_id', $key->id)->where('parent_id',
                $id)->where('category', 'repayments')->first())
            ) {
                $custom_field = CustomFieldMeta::where('custom_field_id', $key->id)->where('parent_id',
                    $id)->where('category', 'repayments')->first();
            } else {
                $custom_field = new CustomFieldMeta();
            }
            $kid = $key->id;
            $custom_field->name = $request->$kid;
            $custom_field->parent_id = $id;
            $custom_field->custom_field_id = $key->id;
            $custom_field->category = "repayments";
            $custom_field->save();
        }
        //update loan status if need be
        if (round(GeneralHelper::loan_total_balance($loan->id), 2) == 0) {
            $l = Loan::find($loan->id);
            $l->status = "closed";
            $l->save();

        } else {
            $l = Loan::find($loan->id);
            $l->status = "disbursed";
            $l->save();
        }
        GeneralHelper::audit_trail("Updated repayment for loan with id:" . $loan->id);
        Flash::success("Repayment successfully saved");
        return redirect('loan/' . $loan->id . '/show');

    }

    //edit loan schedule
    public function editSchedule($loan)
    {
        if (!Sentinel::hasAccess('loans.update')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $rows = 0;
        $schedules = LoanSchedule::where('loan_id', $loan->id)->orderBy('due_date', 'asc')->get();
        return view('loan.edit_schedule', compact('loan', 'schedules', 'rows'));
    }

    public function updateSchedule(Request $request, $loan)
    {
        if (!Sentinel::hasAccess('loans.update')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        if ($request->submit == 'add_row') {
            $rows = $request->addrows;
            $schedules = LoanSchedule::where('loan_id', $loan->id)->orderBy('due_date', 'asc')->get();
            return view('loan.edit_schedule', compact('loan', 'schedules', 'rows'));
        }
        if ($request->submit == 'submit') {
            //lets delete existing schedules
            LoanSchedule::where('loan_id', $loan->id)->delete();
            for ($count = 0; $count < $request->count; $count++) {
                $schedule = new LoanSchedule();
                if (empty($request->due_date) && empty($request->principal) && empty($request->interest) && empty($request->fees) && empty($request->penalty)) {
                    //do nothing
                } elseif (empty($request->due_date)) {
                    //do nothing
                } else {
                    //all rosy, lets save our data here
                    $schedule->due_date = $request->due_date[$count];
                    $schedule->principal = $request->principal[$count];
                    $schedule->description = $request->description[$count];
                    $schedule->loan_id = $loan->id;
                    $schedule->borrower_id = $loan->borrower_id;
                    $schedule->interest = $request->interest[$count];
                    $schedule->fees = $request->fees[$count];
                    $schedule->penalty = $request->penalty[$count];
                    
                      $schedule->month = $date[1];
                    $schedule->year = $date[0];
                    $schedule->save();
                }
            }
            GeneralHelper::audit_trail("Updated Schedule for loan with id:" . $loan->id);
            Flash::success("Schedule successfully updated");
            return redirect('loan/' . $loan->id . '/show');
        }
    }

    public function pdfSchedule($loan)
    {

        $schedules = LoanSchedule::where('loan_id', $loan->id)->orderBy('due_date', 'asc')->get();
        PDF::AddPage();
        PDF::writeHTML(View::make('loan.pdf_schedule', compact('loan', 'schedules'))->render());
        PDF::SetAuthor('Tererai Mugova');
        PDF::Output($loan->borrower->title . ' ' . $loan->borrower->first_name . ' ' . $loan->borrower->last_name . " - Loan Repayment Schedule.pdf",
            'D');

    }

    public function printSchedule($loan)
    {
        $schedules = LoanSchedule::where('loan_id', $loan->id)->orderBy('due_date', 'asc')->get();
        return view('loan.print_schedule', compact('loan', 'schedules'));
    }

    public function pdfLoanStatement($loan)
    {
        $payments = LoanRepayment::where('loan_id', $loan->id)->orderBy('collection_date', 'asc')->get();
        PDF::AddPage();
        PDF::writeHTML(View::make('loan.pdf_loan_statement', compact('loan', 'payments'))->render());
        PDF::SetAuthor('Tererai Mugova');
        PDF::Output($loan->borrower->title . ' ' . $loan->borrower->first_name . ' ' . $loan->borrower->last_name . " - Loan Statement.pdf",
            'D');
    }

    public function printLoanStatement($loan)
    {
        $payments = LoanRepayment::where('loan_id', $loan->id)->orderBy('collection_date', 'asc')->get();
        return view('loan.print_loan_statement', compact('loan', 'payments'));
    }

    public function pdfBorrowerStatement($borrower)
    {
        $loans = Loan::where('borrower_id', $borrower->id)->orderBy('release_date', 'asc')->get();
        PDF::AddPage();
        PDF::writeHTML(View::make('loan.pdf_borrower_statement', compact('loans'))->render());
        PDF::SetAuthor('Tererai Mugova');
        PDF::Output($borrower->title . ' ' . $borrower->first_name . ' ' . $borrower->last_name . " - Client Statement.pdf",
            'D');
    }

    public function printBorrowerStatement($borrower)
    {
        $loans = Loan::where('borrower_id', $borrower->id)->orderBy('release_date', 'asc')->get();
        return view('loan.print_borrower_statement', compact('loans'));
    }

    public function override(Request $request, $loan)
    {
        if (!Sentinel::hasAccess('loans.update')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        if ($request->isMethod('post')) {
            $l = Loan::find($loan->id);
            $l->balance = $request->balance;
            if (empty($request->override)) {
                $l->override = 0;
            } else {
                $l->override = $request->override;
            }
            $l->save();
            GeneralHelper::audit_trail("Override balance for loan with id:" . $loan->id);
            Flash::success(trans('general.override_successfully_applied'));
            return redirect('loan/' . $loan->id . '/show');
        }
        return view('loan.override', compact('loan'));
    }

    public function emailBorrowerStatement($borrower)
    {
        if (!empty($borrower->email)) {
            $body = Setting::where('setting_key',
                'borrower_statement_email_template')->first()->setting_value;
            $body = str_replace('{borrowerTitle}', $borrower->title, $body);
            $body = str_replace('{borrowerFirstName}', $borrower->first_name, $body);
            $body = str_replace('{borrowerLastName}', $borrower->last_name, $body);
            $body = str_replace('{borrowerAddress}', $borrower->address, $body);
            $body = str_replace('{borrowerUniqueNumber}', $borrower->unique_number, $body);
            $body = str_replace('{borrowerMobile}', $borrower->mobile, $body);
            $body = str_replace('{borrowerPhone}', $borrower->phone, $body);
            $body = str_replace('{borrowerEmail}', $borrower->email, $body);
            $body = str_replace('{loansPayments}', GeneralHelper::borrower_loans_total_paid($borrower->id), $body);
            $body = str_replace('{loansDue}',
                round(GeneralHelper::borrower_loans_total_due($borrower->id), 2), $body);
            $body = str_replace('{loansBalance}',
                round((GeneralHelper::borrower_loans_total_due($borrower->id) - GeneralHelper::borrower_loans_total_paid($borrower->id)),
                    2), $body);
            $body = str_replace('{loanPayments}',
                GeneralHelper::borrower_loans_total_paid($borrower->id),
                $body);
            $loans = Loan::where('borrower_id', $borrower->id)->orderBy('release_date', 'asc')->get();
            PDF::AddPage();
            PDF::writeHTML(View::make('loan.pdf_borrower_statement', compact('loans'))->render());
            PDF::SetAuthor('Tererai Mugova');
            PDF::Output(public_path() . '/uploads/temporary/borrower_statement' . $borrower->id . ".pdf", 'F');
            $file_name = $borrower->title . ' ' . $borrower->first_name . ' ' . $borrower->last_name . " - Client Statement.pdf";
            Mail::raw($body, function ($message) use ($borrower, $file_name) {
                $message->from(Setting::where('setting_key', 'company_email')->first()->setting_value,
                    Setting::where('setting_key', 'company_name')->first()->setting_value);
                $message->to($borrower->email);
                $headers = $message->getHeaders();
                $message->attach(public_path() . '/uploads/temporary/borrower_statement' . $borrower->id . ".pdf",
                    ["as" => $file_name]);
                $message->setContentType('text/html');
                $message->setSubject(Setting::where('setting_key',
                    'borrower_statement_email_subject')->first()->setting_value);

            });
            unlink(public_path() . '/uploads/temporary/borrower_statement' . $borrower->id . ".pdf");
            $mail = new Email();
            $mail->user_id = Sentinel::getUser()->id;
            $mail->message = $body;
            $mail->subject = Setting::where('setting_key',
                'borrower_statement_email_subject')->first()->setting_value;
            $mail->recipients = 1;
            $mail->send_to = $borrower->first_name . ' ' . $borrower->last_name . '(' . $borrower->unique_number . ')';
            $mail->save();
            Flash::success("Statment successfully sent");
            return redirect('borrower/' . $borrower->id . '/show');
        } else {
            Flash::warning("Borrower has no email set");
            return redirect('borrower/' . $borrower->id . '/show');
        }
    }

    public function emailLoanStatement($loan)
    {
        $borrower = $loan->borrower;
        if (!empty($borrower->email)) {
            $body = Setting::where('setting_key',
                'loan_statement_email_template')->first()->setting_value;
            $body = str_replace('{borrowerTitle}', $borrower->title, $body);
            $body = str_replace('{borrowerFirstName}', $borrower->first_name, $body);
            $body = str_replace('{borrowerLastName}', $borrower->last_name, $body);
            $body = str_replace('{borrowerAddress}', $borrower->address, $body);
            $body = str_replace('{borrowerUniqueNumber}', $borrower->unique_number, $body);
            $body = str_replace('{borrowerMobile}', $borrower->mobile, $body);
            $body = str_replace('{borrowerPhone}', $borrower->phone, $body);
            $body = str_replace('{borrowerEmail}', $borrower->email, $body);
            $body = str_replace('{loanNumber}', $loan->id, $body);
            $body = str_replace('{loanPayments}', GeneralHelper::loan_total_paid($loan->id), $body);
            $body = str_replace('{loanDue}',
                round(GeneralHelper::loan_total_due_amount($loan->id), 2), $body);
            $body = str_replace('{loanBalance}',
                round((GeneralHelper::loan_total_due_amount($loan->id) - GeneralHelper::loan_total_paid($loan->id)),
                    2), $body);
            $payments = LoanRepayment::where('loan_id', $loan->id)->orderBy('collection_date', 'asc')->get();
            PDF::AddPage();
            PDF::writeHTML(View::make('loan.pdf_loan_statement', compact('loan', 'payments'))->render());
            PDF::SetAuthor('Tererai Mugova');
            PDF::Output(public_path() . '/uploads/temporary/loan_statement' . $loan->id . ".pdf", 'F');
            $file_name = $borrower->title . ' ' . $borrower->first_name . ' ' . $borrower->last_name . " - Loan Statement.pdf";
            Mail::raw($body, function ($message) use ($borrower, $loan, $file_name) {
                $message->from(Setting::where('setting_key', 'company_email')->first()->setting_value,
                    Setting::where('setting_key', 'company_name')->first()->setting_value);
                $message->to($borrower->email);
                $headers = $message->getHeaders();
                $message->attach(public_path() . '/uploads/temporary/loan_statement' . $loan->id . ".pdf",
                    ["as" => $file_name]);
                $message->setContentType('text/html');
                $message->setSubject(Setting::where('setting_key',
                    'loan_statement_email_subject')->first()->setting_value);

            });
            unlink(public_path() . '/uploads/temporary/loan_statement' . $loan->id . ".pdf");
            $mail = new Email();
            $mail->user_id = Sentinel::getUser()->id;
            $mail->message = $body;
            $mail->subject = Setting::where('setting_key',
                'loan_statement_email_subject')->first()->setting_value;
            $mail->recipients = 1;
            $mail->send_to = $borrower->first_name . ' ' . $borrower->last_name . '(' . $borrower->unique_number . ')';
            $mail->save();
            Flash::success("Loan Statement successfully sent");
            return redirect('loan/' . $loan->id . '/show');
        } else {
            Flash::warning("Borrower has no email set");
            return redirect('loan/' . $loan->id . '/show');
        }
    }

    public function emailLoanSchedule($loan)
    {
        $borrower = $loan->borrower;
        if (!empty($borrower->email)) {
            $body = Setting::where('setting_key',
                'loan_schedule_email_template')->first()->setting_value;
            $body = str_replace('{borrowerTitle}', $borrower->title, $body);
            $body = str_replace('{borrowerFirstName}', $borrower->first_name, $body);
            $body = str_replace('{borrowerLastName}', $borrower->last_name, $body);
            $body = str_replace('{borrowerAddress}', $borrower->address, $body);
            $body = str_replace('{borrowerUniqueNumber}', $borrower->unique_number, $body);
            $body = str_replace('{borrowerMobile}', $borrower->mobile, $body);
            $body = str_replace('{borrowerPhone}', $borrower->phone, $body);
            $body = str_replace('{borrowerEmail}', $borrower->email, $body);
            $body = str_replace('{loanNumber}', $loan->id, $body);
            $body = str_replace('{loanPayments}', GeneralHelper::loan_total_paid($loan->id), $body);
            $body = str_replace('{loanDue}',
                round(GeneralHelper::loan_total_due_amount($loan->id), 2), $body);
            $body = str_replace('{loanBalance}',
                round((GeneralHelper::loan_total_due_amount($loan->id) - GeneralHelper::loan_total_paid($loan->id)),
                    2), $body);
            $schedules = LoanSchedule::where('loan_id', $loan->id)->orderBy('due_date', 'asc')->get();
            PDF::AddPage();
            PDF::writeHTML(View::make('loan.pdf_schedule', compact('loan', 'schedules'))->render());
            PDF::SetAuthor('Tererai Mugova');
            PDF::Output(public_path() . '/uploads/temporary/loan_schedule' . $loan->id . ".pdf", 'F');
            $file_name = $borrower->title . ' ' . $borrower->first_name . ' ' . $borrower->last_name . " - Loan Schedule.pdf";
            Mail::raw($body, function ($message) use ($borrower, $loan, $file_name) {
                $message->from(Setting::where('setting_key', 'company_email')->first()->setting_value,
                    Setting::where('setting_key', 'company_name')->first()->setting_value);
                $message->to($borrower->email);
                $headers = $message->getHeaders();
                $message->attach(public_path() . '/uploads/temporary/loan_schedule' . $loan->id . ".pdf",
                    ["as" => $file_name]);
                $message->setContentType('text/html');
                $message->setSubject(Setting::where('setting_key',
                    'loan_schedule_email_subject')->first()->setting_value);

            });
            unlink(public_path() . '/uploads/temporary/loan_schedule' . $loan->id . ".pdf");
            $mail = new Email();
            $mail->user_id = Sentinel::getUser()->id;
            $mail->message = $body;
            $mail->subject = Setting::where('setting_key',
                'loan_statement_email_subject')->first()->setting_value;
            $mail->recipients = 1;
            $mail->send_to = $borrower->first_name . ' ' . $borrower->last_name . '(' . $borrower->unique_number . ')';
            $mail->save();
            Flash::success("Loan Statement successfully sent");
            return redirect('loan/' . $loan->id . '/show');
        } else {
            Flash::warning("Borrower has no email set");
            return redirect('loan/' . $loan->id . '/show');
        }
    }

    //loan applications
    public function indexApplication()
    {
        if (!Sentinel::hasAccess('loans')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $data = LoanApplication::all();

        return view('loan.applications', compact('data'));
    }

    public function declineApplication($id)
    {
        if (!Sentinel::hasAccess('loans')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $application = LoanApplication::find($id);
        $application->status = "declined";
        $application->save();
        GeneralHelper::audit_trail("Declined borrower  loan application with id:" . $id);
        Flash::success(trans_choice('general.successfully_saved', 1));
        return redirect('loan/loan_application/data');
    }

    public function deleteApplication($id)
    {
        if (!Sentinel::hasAccess('loans')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        LoanApplication::destroy($id);
        Guarantor::where('loan_application_id', $id)->delete();
        GeneralHelper::audit_trail("Deleted borrower  loan application with id:" . $id);
        Flash::success(trans_choice('general.successfully_deleted', 1));
        return redirect('loan/loan_application/data');
    }

    public function approveApplication($id)
    {
        if (!Sentinel::hasAccess('loans')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $loan_disbursed_by = array();
        foreach (LoanDisbursedBy::all() as $key) {
            $loan_disbursed_by[$key->id] = $key->name;
        }
        $application = LoanApplication::find($id);
        //get custom fields
        $custom_fields = CustomField::where('category', 'loans')->get();
        $loan_fees = LoanFee::all();
        $loan_product = $application->loan_product;
        if (!empty($application->loan_product)) {
            return view('loan.approve_application',
                compact('id', 'application', 'loan_product', 'loan_disbursed_by', 'custom_fields', 'loan_fees'));

        } else {
            Flash::warning(trans_choice('general.loan_application_approve_error', 1));
            return redirect('loan/loan_application/data');
        }
    }

    public function storeApproveApplication(Request $request, $id)
    {
        if (!Sentinel::hasAccess('loans')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $application = LoanApplication::find($id);
        $application->status = "approved";
        $application->save();
        //lets save the loan here
        if (!Sentinel::hasAccess('loans.create')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $loan = new Loan();
        $loan->principal = $request->principal;
        $loan->interest_method = $request->interest_method;
        $loan->interest_rate = $request->interest_rate;
        $loan->interest_period = $request->interest_period;
        $loan->loan_duration = $request->loan_duration;
        $loan->loan_duration_type = $request->loan_duration_type;
        $loan->repayment_cycle = $request->repayment_cycle;
        $loan->decimal_places = $request->decimal_places;
        $loan->override_interest = $request->override_interest;
        $loan->override_interest_amount = $request->override_interest_amount;
        $loan->grace_on_interest_charged = $request->grace_on_interest_charged;
        $loan->borrower_id = $request->borrower_id;
        $loan->applied_amount = $request->principal;
        $loan->user_id = Sentinel::getUser()->id;
        $loan->loan_product_id = $request->loan_product_id;
        $loan->release_date = $request->release_date;
        // $date = explode('-', $request->release_date);
          $loan->month = $date[1];
        $loan->year = $date[0];
        if (!empty($request->first_payment_date)) {
            $loan->first_payment_date = $request->first_payment_date;
        }
        $loan->description = $request->description;
        $files = array();
        if (!empty(array_filter($request->file('files')))) {
            $count = 0;
            foreach ($request->file('files') as $key) {
                $file = array('files' => $key);
                $rules = array('files' => 'required|mimes:jpeg,jpg,bmp,png,pdf,docx,xlsx');
                $validator = Validator::make($file, $rules);
                if ($validator->fails()) {
                    Flash::warning(trans('general.validation_error'));
                    return redirect()->back()->withInput()->withErrors($validator);
                } else {
                    $files[$count] = $key->getClientOriginalName();
                    $key->move(public_path() . '/uploads',
                        $key->getClientOriginalName());
                }
                $count++;
            }
        }
        $loan->files = serialize($files);
        $loan->save();

        //save custom meta
        $custom_fields = CustomField::where('category', 'loans')->get();
        foreach ($custom_fields as $key) {
            $custom_field = new CustomFieldMeta();
            $id = $key->id;
            $custom_field->name = $request->$id;
            $custom_field->parent_id = $loan->id;
            $custom_field->custom_field_id = $key->id;
            $custom_field->category = "loans";
            $custom_field->save();
        }
        //save loan fees
        $fees_distribute = 0;
        $fees_first_payment = 0;
        $fees_last_payment = 0;
        foreach (LoanFee::all() as $key) {
            $loan_fee = new LoanFeeMeta();
            $value = 'loan_fees_amount_' . $key->id;
            $loan_fees_schedule = 'loan_fees_schedule_' . $key->id;
            $loan_fee->user_id = Sentinel::getUser()->id;
            $loan_fee->category = 'loan';
            $loan_fee->parent_id = $loan->id;
            $loan_fee->loan_fees_id = $key->id;
            $loan_fee->value = $request->$value;
            $loan_fee->loan_fees_schedule = $request->$loan_fees_schedule;
            $loan_fee->save();
            //determine amount to use
            if ($key->loan_fee_type == 'fixed') {
                if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                    $fees_distribute = $fees_distribute + $loan_fee->value;
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                    $fees_first_payment = $fees_first_payment + $loan_fee->value;
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                    $fees_last_payment = $fees_last_payment + $loan_fee->value;
                }
            } else {
                if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                    $fees_distribute = $fees_distribute + ($loan_fee->value * $loan->principal / 100);
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                    $fees_first_payment = $fees_first_payment + ($loan_fee->value * $loan->principal / 100);
                }
                if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                    $fees_last_payment = $fees_last_payment + ($loan_fee->value * $loan->principal / 100);
                }
            }
        }
        //lets create schedules here
        //determine interest rate to use

        $interest_rate = GeneralHelper::determine_interest_rate($loan->id);

        $period = GeneralHelper::loan_period($loan->id);
        $loan = Loan::find($loan->id);
        if ($loan->repayment_cycle == 'daily') {
            $repayment_cycle = 'day';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' days')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'weekly') {
            $repayment_cycle = 'week';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' weeks')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'monthly') {
            $repayment_cycle = 'month';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' months')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'bi_monthly') {
            $repayment_cycle = 'month';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' months')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'quarterly') {
            $repayment_cycle = 'month';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' months')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'semi_annually') {
            $repayment_cycle = 'month';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' months')),
                'Y-m-d');
        }
        if ($loan->repayment_cycle == 'yearly') {
            $repayment_cycle = 'year';
            $loan->maturity_date = date_format(date_add(date_create($request->first_payment_date),
                date_interval_create_from_date_string($period . ' years')),
                'Y-m-d');
        }
        $loan->save();
        foreach (Guarantor::where('loan_application_id', $id)->get() as $key) {
            $g = Guarantor::find($key->id);
            $g->loan_id = $loan->id;
            $g->save();
        }
        //generate schedules until period finished
        $next_payment = $request->first_payment_date;
        $balance = $request->principal;
        GeneralHelper::audit_trail("Approved borrower  loan application with id:" . $id);
        Flash::success(trans_choice('general.successfully_saved', 1));
        return redirect('loan/loan_application/data');
    }

    //loan calculator
    public function createLoanCalculator(Request $request)
    {
        if (!Sentinel::hasAccess('loans.loan_calculator')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }

        $loan_fees = LoanFee::all();
        return view('loan_calculator/create',
            compact('loan_fees'));
    }

    public function showLoanCalculator(Request $request)
    {
        if (!Sentinel::hasAccess('loans.loan_calculator')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }

        return view('loan_calculator/show',
            compact('request'));
    }

    public function storeLoanCalculator(Request $request)
    {
        if (!Sentinel::hasAccess('loans.loan_calculator')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        if (!empty($request->pdf)) {
            PDF::AddPage();
            PDF::writeHTML(View::make('loan_calculator.pdf', compact('request'))->render());
            PDF::SetAuthor('Tererai Mugova');
            PDF::Output("Calculated Schedule.pdf",
                'D');
        }
        if (!empty($request->print)) {
            return view('loan_calculator.print', compact('request'));
        }

    }

    public function disbursemultiple(Request $request){
        $data = Loan::where('branch_id', session('branch_id'))->where('status', "approved")->get();
        if (!Sentinel::hasAccess('loans.disburse')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }
        $ids = $request->ids;
        if($ids == []){
            return response()->json(["message"=>"Something Went Wrong, Or Invalid Selection!"],400);
        }
        foreach($ids as $id){
            $loan = Loan::find($id);
            


if($loan != null){
        //delete previously created schedules and payments
        LoanSchedule::where('loan_id', $loan->id)->delete();
        LoanRepayment::where('loan_id', $loan->id)->delete();
        $interest_rate = GeneralHelper::determine_interest_rate($loan->id);
        $period = GeneralHelper::loan_period($loan->id);
        $loan = Loan::find($loan->id);
        if ($loan->repayment_cycle == 'daily') {
            $repayment_cycle = '1 days';
            $repayment_type = 'days';
        }
        if ($loan->repayment_cycle == 'weekly') {
            $repayment_cycle = '1 weeks';
            $repayment_type = 'weeks';
        }
        if ($loan->repayment_cycle == 'monthly') {
            $repayment_cycle = 'month';
            $repayment_type = 'months';
        }
        if ($loan->repayment_cycle == 'bi_monthly') {
            $repayment_cycle = '2 months';
            $repayment_type = 'months';

        }
        if ($loan->repayment_cycle == 'quarterly') {
            $repayment_cycle = '4 months';
            $repayment_type = 'months';
        }
        if ($loan->repayment_cycle == 'semi_annually') {
            $repayment_cycle = '6 months';
            $repayment_type = 'months';
        }
        if ($loan->repayment_cycle == 'yearly') {
            $repayment_cycle = '1 years';
            $repayment_type = 'years';
        }
        if (empty($request->first_payment_date)) {
            $first_payment_date = date_format(date_add(date_create($request->disbursed_date),
                                                       date_interval_create_from_date_string($repayment_cycle)),
                                              'Y-m-d');


        } else {
            $first_payment_date =  date('Y-m-d', strtotime($request->first_payment_date));
        }
        $loan->maturity_date = date_format(date_add(date_create($first_payment_date),
                                                    date_interval_create_from_date_string($period . ' ' . $repayment_type)),
                                           'Y-m-d');
        $loan->status = 'disbursed';
        $loan->loan_disbursed_by_id = $request->loan_disbursed_by_id;
        $loan->disbursed_notes = $request->disbursed_notes;
        $loan->first_payment_date = $first_payment_date;
        $loan->disbursed_by_id = Sentinel::getUser()->id;
        $loan->disbursed_date = $request->disbursed_date;
        $loan->release_date = date('Y-m-d', strtotime($request->disbursed_date));
        $date = explode('-', $request->disbursed_date);
        $loan->month = $date[1];
        $loan->year = $date[0];
        $loan->save();
        $fees_distribute = 0;
        $fees_first_payment = 0;
        $fees_last_payment = 0;

        foreach (LoanFee::all() as $key) {
            if (!empty(LoanFeeMeta::where('loan_fees_id', $key->id)->where('parent_id', $loan->id)->where('category',
                                                                                                          'loan')->first())
               ) {
                $loan_fee = LoanFeeMeta::where('loan_fees_id', $key->id)->where('parent_id',
                                                                                $loan->id)->where('category',
                                                                                                  'loan')->first();
                //determine amount to use
                if ($key->loan_fee_type == 'fixed') {
                    if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                        $fees_distribute = $fees_distribute + $loan_fee->value;
                    }
                    if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                        $fees_first_payment = $fees_first_payment + $loan_fee->value;
                    }
                    if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                        $fees_last_payment = $fees_last_payment + $loan_fee->value;
                    }
                } else {
                    if ($loan_fee->loan_fees_schedule == 'distribute_fees_evenly') {
                        $fees_distribute = $fees_distribute + ($loan_fee->value * $loan->principal / 100);
                    }
                    if ($loan_fee->loan_fees_schedule == 'charge_fees_on_first_payment') {
                        $fees_first_payment = $fees_first_payment + ($loan_fee->value * $loan->principal / 100);
                    }
                    if ($loan_fee->loan_fees_schedule == 'charge_fees_on_last_payment') {
                        $fees_last_payment = $fees_last_payment + ($loan_fee->value * $loan->principal / 100);
                    }
                }
            }

        }
        //generate schedules until period finished
        $next_payment = $first_payment_date;
        $balance = $loan->principal;
        for ($i = 1; $i <= $period; $i++) {
            $fees = 0;
            if ($i == 1) {
                $fees = $fees + ($fees_first_payment);
            }
            if ($i == $period) {
                $fees = $fees + ($fees_last_payment);
            }
            $fees = $fees + ($fees_distribute / $period);
            $loan_schedule = new LoanSchedule();
            $loan_schedule->loan_id = $loan->id;
            $loan_schedule->fees = $fees;
            $loan_schedule->branch_id = session('branch_id');
            $loan_schedule->borrower_id = $loan->borrower_id;
            $loan_schedule->description = trans_choice('general.repayment', 1);
            $loan_schedule->due_date = $next_payment;


            $loan_schedule->month = $date[1];
            $loan_schedule->year = $date[0];
            //determine which method to use
            $due = 0;
            //reducing balance equal installments
            if ($loan->interest_method == 'declining_balance_equal_installments') {
                $due = GeneralHelper::amortized_monthly_payment($loan->id, $loan->principal);

                if ($loan->decimal_places == 'round_off_to_two_decimal') {
                    //determine if we have grace period for interest

                    $interest = round(($interest_rate * $balance), 2);
                    $loan_schedule->principal = round(($due - $interest), 2);
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest, 2);
                    }
                    $loan_schedule->due = round($due, 2);
                    //determine next balance
                    $balance = round(($balance - ($due - $interest)), 2);
                    $loan_schedule->principal_balance = round($balance, 2);
                } else {
                    //determine if we have grace period for interest

                    $interest = round(($interest_rate * $balance));
                    $loan_schedule->principal = round(($due - $interest));
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest);
                    }
                    $loan_schedule->due = round($due);
                    //determine next balance
                    $balance = round(($balance - ($due - $interest)));
                    $loan_schedule->principal_balance = round($balance);
                }


            }
            //reducing balance equal principle
            if ($loan->interest_method == 'declining_balance_equal_principal') {
                $principal = $loan->principal / $period;
                if ($loan->decimal_places == 'round_off_to_two_decimal') {

                    $interest = round(($interest_rate * $balance), 2);
                    $loan_schedule->principal = round($principal, 2);
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest, 2);
                    }
                    $loan_schedule->due = round(($principal + $interest), 2);
                    //determine next balance
                    $balance = round(($balance - ($principal + $interest)), 2);
                    $loan_schedule->principal_balance = round($balance, 2);
                } else {

                    $loan_schedule->principal = round(($principal));

                    $interest = round(($interest_rate * $balance));
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest);
                    }
                    $loan_schedule->due = round($principal + $interest);
                    //determine next balance
                    $balance = round(($balance - ($principal + $interest)));
                    $loan_schedule->principal_balance = round($balance);
                }

            }
            //flat  method
            if ($loan->interest_method == 'flat_rate') {
                $principal = $loan->principal / $period;
                if ($loan->decimal_places == 'round_off_to_five_decimal') {
                    $interest = round(($interest_rate * $loan->principal), 5);
                    $loan_schedule->principal = round(($principal), 5);
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest, 5);
                    }
                    $loan_schedule->principal = round(($principal), 5);
                    $loan_schedule->due = round(($principal + $interest), 5);
                    //determine next balance
                    $balance = round(($balance - $principal), 2);
                    $loan_schedule->principal_balance = round($balance, 5);
                } else {
                    $interest = round(($interest_rate * $loan->principal));
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest);
                    }
                    $loan_schedule->principal = round($principal);
                    $loan_schedule->due = round($principal + $interest);
                    //determine next balance
                    $balance = round(($balance - $principal));
                    $loan_schedule->principal_balance = round($balance);
                }
            }
            //interest only method
            if ($loan->interest_method == 'interest_only') {
                if ($i == $period) {
                    $principal = $loan->principal;
                } else {
                    $principal = 0;
                }
                if ($loan->decimal_places == 'round_off_to_two_decimal') {
                    $interest = round(($interest_rate * $loan->principal), 2);
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest, 2);
                    }
                    $loan_schedule->principal = round(($principal), 2);
                    $loan_schedule->due = round(($principal + $interest), 2);
                    //determine next balance
                    $balance = round(($balance - $principal), 2);
                    $loan_schedule->principal_balance = round($balance, 2);
                } else {
                    $interest = round(($interest_rate * $loan->principal));
                    if ($loan->grace_on_interest_charged >= $i) {
                        $loan_schedule->interest = 0;
                    } else {
                        $loan_schedule->interest = round($interest);
                    }
                    $loan_schedule->principal = round($principal);
                    $loan_schedule->due = round($principal + $interest);
                    //determine next balance
                    $balance = round(($balance - $principal));
                    $loan_schedule->principal_balance = round($balance);
                }
            }
            //determine next due date
            if ($loan->repayment_cycle == 'daily') {
                $next_payment = date_format(date_add(date_create($next_payment),
                                                     date_interval_create_from_date_string('1 days')),
                                            'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'weekly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                                                     date_interval_create_from_date_string('1 weeks')),
                                            'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'monthly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                                                     date_interval_create_from_date_string('1 months')),
                                            'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'bi_monthly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                                                     date_interval_create_from_date_string('2 months')),
                                            'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'quarterly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                                                     date_interval_create_from_date_string('4 months')),
                                            'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'semi_annually') {
                $next_payment = date_format(date_add(date_create($next_payment),
                                                     date_interval_create_from_date_string('6 months')),
                                            'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($loan->repayment_cycle == 'yearly') {
                $next_payment = date_format(date_add(date_create($next_payment),
                                                     date_interval_create_from_date_string('1 years')),
                                            'Y-m-d');
                //$loan_schedule->due_date = $next_payment;
            }
            if ($i == $period) {
                $loan_schedule->principal_balance = round($balance);
            }
            $loan_schedule->save();
        }
        $loan = Loan::find($loan->id);
        $loan->maturity_date = $next_payment;
        $loan->save();
            }
            }
      return response()->json(["message"=>"SuccessFully Disbursed"]);
    }
    
    public function storeRepayment2(Request $request, $loan)
    {

        



        if (!Sentinel::hasAccess('repayments.create')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }

        $loans = DB::table("loans")->where(["sc"=>$request->sc])->get();


        $amount = 0;
        foreach($loans as $loan){
            $users = DB::table("borrower_group_members")->where(["borrower_group_id"=>$loan->group_id])->count();
            $loan = Loan::find($loan->id);

            if ($amt > round(GeneralHelper::loan_total_balance($loan->id), 2)) {
                Flash::warning("Amount is more than the balance(" . GeneralHelper::loan_total_balance($loan->id) . ')');
                return redirect()->back()->withInput();

            } else {

                $amt = $request->amount / $users;
                $amount += $amt;

                $repayment = new LoanRepayment();
                $repayment->user_id = Sentinel::getUser()->id;
                $repayment->amount = $amt;
                $repayment->sc = $loan->sc;
                $repayment->loan_id = $loan->id;
                $repayment->borrower_id = $loan->borrower_id;
                $repayment->branch_id = session('branch_id');
                $repayment->collection_date = \Carbon\Carbon::parse($request->collection_date);
                $repayment->repayment_method_id = $request->repayment_method_id;
                $repayment->notes = $request->notes;
                $date = explode('-', $request->collection_date);

                $repayment->year = (int)$date[0];
                $repayment->month = (int)$date[1];

                //determine which schedule due date the payment applies too
                $schedule = LoanSchedule::where('due_date', '>=', $request->collection_date)->where('loan_id',
                                                                                                    $loan->id)->orderBy('due_date',
                                                                                                                        'asc')->first();
                if (!empty($schedule)) {
                    $repayment->due_date = $schedule->due_date;
                } else {
                    $schedule = LoanSchedule::where('loan_id',
                                                    $loan->id)->orderBy('due_date',
                                                                        'desc')->first();
                    if ($request->collection_date > $schedule->due_date) {
                        $repayment->due_date = $schedule->due_date;
                    } else {
                        $schedule = LoanSchedule::where('due_date', '>', $request->collection_date)->where('loan_id',
                                                                                                           $loan->id)->orderBy('due_date',
                                                                                                                               'asc')->first();
                        $repayment->due_date = $schedule->due_date;
                    }

                }
                $repayment->save();
                //save custom meta
                $custom_fields = CustomField::where('category', 'repayments')->get();
                foreach ($custom_fields as $key) {
                    $custom_field = new CustomFieldMeta();
                    $id = $key->id;
                    $custom_field->name = $request->$id;
                    $custom_field->parent_id = $repayment->id;
                    $custom_field->custom_field_id = $key->id;
                    $custom_field->category = "repayments";
                    $custom_field->save();
                }
                //update loan status if need be
                if (round(GeneralHelper::loan_total_balance($loan->id), 2) == 0) {
                    $l = Loan::find($loan->id);
                    $l->status = "closed";
                    $l->save();

                }
                //check if late repayment is to be applied when adding payment
                if ($request->apply_penalty == 1) {
                    if (!empty($loan->loan_product)) {
                        if ($loan->loan_product->enable_late_repayment_penalty == 1) {
                            $schedules = LoanSchedule::where('due_date', '<',
                                                             $repayment->due_date)->where('missed_penalty_applied',
                                                                                          0)->orderBy('due_date', 'asc')->get();
                            foreach ($schedules as $schedule) {
                                if (GeneralHelper::loan_total_due_period($loan->id,
                                                                         $schedule->due_date) > GeneralHelper::loan_total_paid_period($loan->id,
                                                                                                                                      $schedule->due_date)
                                   ) {
                                    $sch = LoanSchedule::find($schedule->id);
                                    $sch->missed_penalty_applied = 1;
                                    //determine which amount to use
                                    if ($loan->loan_product->late_repayment_penalty_type == "fixed") {
                                        $sch->penalty = $sch->penalty + $loan->loan_product->late_repayment_penalty_amount;
                                    } else {
                                        if ($loan->loan_product->late_repayment_penalty_calculate == 'overdue_principal') {
                                            $principal = (GeneralHelper::loan_total_principal($loan->id,
                                                                                              $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                                                                                                                   'principal', $schedule->due_date));
                                            $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                        }
                                        if ($loan->loan_product->late_repayment_penalty_calculate == 'overdue_principal_interest') {
                                            $principal = (GeneralHelper::loan_total_principal($loan->id,
                                                                                              $schedule->due_date) + GeneralHelper::loan_total_interest($loan->id,
                                                                                                                                                    $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                'principal',
                                                $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                                                                     'interest', $schedule->due_date));
                                            $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                        }
                                        if ($loan->loan_product->late_repayment_penalty_calculate == 'overdue_principal_interest_fees') {
                                            $principal = (GeneralHelper::loan_total_principal($loan->id,
                                                                                              $schedule->due_date) + GeneralHelper::loan_total_interest($loan->id,
                                                                                                                                                    $schedule->due_date) + GeneralHelper::loan_total_fees($loan->id,
                                                $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                                                                     'principal',
                                                                                                     $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id,
                                                'interest',
                                                $schedule->due_date) - GeneralHelper::loan_paid_item($loan->id, 'fees',
                                                                                                     $schedule->due_date));
                                            $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                        }
                                        if ($loan->loan_product->late_repayment_penalty_calculate == 'total_overdue') {
                                            $principal = (GeneralHelper::loan_total_due_amount($loan->id,
                                                                                               $schedule->due_date) - GeneralHelper::loan_total_paid($loan->id,
                                                                                                                                                     $schedule->due_date));
                                            $sch->penalty = $sch->penalty + (($loan->loan_product->late_repayment_penalty_amount / 100) * $principal);
                                        }
                                    }
                                    $sch->save();
                                }
                            }
                        }
                    }
                }
                //notify borrower


                $lbalance = DB::table("loan_repayments")->where(["loan_id"=>$loan->id])->sum("amount");
                if($lbalance == round(\App\Helpers\GeneralHelper::loan_total_due_amount($loan->id),2)){
                    DB::table("loans")->where(["id"=>$loan->id])->update(["status"=>"closed"]);
                }

            }
        }
        $group = DB::table("grouploan")->where(["sc"=>$request->sc])->first();
        DB::table("grouploan")->where(["sc"=>$request->sc])->increment("balance",$amount);
        DB::table("repaymentgrouphis")->insert([
            "amount"=>$amount,
            "type"=>$request->repayment_method_id,
            "date"=>\Carbon\Carbon::parse($request->collection_date),
            "groupid"=>$group->group_id,
            "sc"=>$request->sc,
            "userid"=>Sentinel::getUser()->id
        ]);
        GeneralHelper::audit_trail("Added repayment for group loan with id:" . $request->sc);
        Flash::success("Repayment successfully saved");
        $id = $request->sc;
        return redirect("/borrower/group/$id/loans");
    }
    
    public function grouploan(){
        $loan_disbursed_by = array();
        foreach (LoanDisbursedBy::all() as $key) {
            $loan_disbursed_by[$key->id] = $key->name;
        }
        return view("loan.grouploan",compact("loan_disbursed_by"));
    }
    
    public function grouploanapi(Request $request){
   $datas = [];
        $data =  DB::table('grouploan')->get();

        if(isset($_GET['status'])){
            $data =  DB::table('grouploan')->where(["status"=>$_GET['status']])->get();
        }

        foreach($data as $loans){
            $select = "<input type='checkbox' value=".$loans->sc.">";
            $loanx = DB::table("loans")->where(["sc"=>$loans->sc])->sum("approved_amount");
            $prin = DB::table("loans")->where(["sc"=>$loans->sc])->sum("principal");
            $sc = $loans->sc;
            $ap = \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value." ".number_format($loanx,2);
            $prin = \App\Models\Setting::where('setting_key', 'currency_symbol')->first()->setting_value." ".number_format($prin,2);
            $status = $loans->status;
            $date = $loans->created_at;
            $action1 = "-";
           if(Sentinel::hasAccess('loans.delete')){
              $action1 = '  <li><a href="'. url('/borrower/group/loans/'.$loans->sc.'/delete').'"
                class="delete"><i
                class="fa fa-trash"></i>'.trans('general.delete').'</a></li>';
             }
            $action = '
              <div class="btn-group">
        <button type="button" class="btn btn-info btn-xs dropdown-toggle"
                data-toggle="dropdown" aria-expanded="false">
            '.trans('general.choose').' <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu" role="menu">
            <li><a href="'.url('/borrower/group/'.$loans->sc.'/loans').'"
                   ><i
                       class="fa fa-eye"></i> Details </a></li>
            <li><a href="/repayment/data?search='.$loans->sc.'"
                   ><i
                       class="fa fa-sticky-note-o"></i> Repayments </a></li>
          '.$action1.'
        </ul>
        </div> 
            ';
            $group = DB::table("borrower_groups")->where(["id"=>$loans->group_id])->first();
            if($group != null){
            $gname = "<a href=/borrower/group/$group->id/vloans>$group->name</a>";
                }else{
                $gname = "-";
            }
            array_push($datas,[$select,$gname,$sc,$ap,$prin,$status,$date,$action]);

        }

        return response()->json(["data"=>$datas]);
    }
    
    public function bulkdeleteloan(Request $request){
        if (!Sentinel::hasAccess('loans.delete')) {
            Flash::warning(trans('general.permission_denied'));
            return redirect('/');
        }

        $ids = $request->ids;
        if($ids == []){
            return response()->json(["message"=>"Something Went Wrong, Or Invalid Selection!"],400);
        }

        foreach($ids as $id){
            $loan = DB::table("loans")->where(["sc"=>$id])->get();
            DB::table("grouploan")->where(["sc"=>$id])->delete();
            foreach($loan as $loans){
            $id = $loans->id;
                $loan = Loan::find($id);


            Loan::destroy($id);
            LoanSchedule::where('loan_id', $id)->delete();
            LoanRepayment::where('loan_id', $id)->delete();
            Guarantor::where('loan_id', $id)->delete();
        }
            }
        
        return response()->json(["message"=>"Deleted Successfully!"]);
    }

    public function re_active(Request $request){
        DB::table("loans")->where(["id"=>$request->id,"to_pay"=>1])->update(["status"=>"disbursed","re_active"=>1,"re_active_date"=>date("Y-m-d h:i:s"),"to_pay"=>null]);
        return response()->json(["message"=>"Account Reactivated!"]);
    }
}
