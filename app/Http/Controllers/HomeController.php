<?php

namespace App\Http\Controllers;

use App\Helpers\GeneralHelper;
use App\Models\Borrower;
use App\Models\Setting;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Laracasts\Flash\Flash;
        use File;
        use DB;
        use Hash;
use DateTime;
use App\Models\Loan;
use Sentinel;
use Illuminate\Http\Request;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use App\Http\Requests;
require 'excelreader.php';
class HomeController extends Controller
{
    
 
    public function __construct()
    {
        if (Sentinel::check()) {
            if(Sentinel::getUser()->status == 1){
                Flash::warning(trans('login.failure'));
                Sentinel::logout(null, true);
                return redirect("/admin")->withErrors('User Is InActive');
            }
            return redirect('dashboard')->send();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        

        if (!Sentinel::check()) {
            if (Setting::where('setting_key', 'allow_client_login')->first()->setting_value == 1) {
                return redirect('client')->send();
            } else {
                return view("front.index");
            }
        } else {
            return redirect('dashboard');
        }
    }

    public function login()
    {
        return view('login');
    }



    public function adminLogin()
    {
        return view('admin_login');
    }

    public function logout()
    {
        GeneralHelper::audit_trail("Logged out of system");
        Sentinel::logout(null, true);
        \Artisan::call('cache:clear');

        return redirect('/admin');
    }

    public function processLogin()
    {
        $rules = array(
            'email' => 'required',
            'password' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } else {
            //process validation here
            $credentials = array(
                "email" => Input::get('email'),
                "password" => Input::get('password'),
            );
            if (!empty(Input::get('remember'))) {
                //remember me token set

                if (Sentinel::authenticateAndRemember($credentials)) {
                    
                    if(Sentinel::getUser()->status == 1){
                        Flash::warning(trans('login.failure'));
                        return redirect()->back()->withInput()->withErrors('User Is InActive');
                    }
                    GeneralHelper::audit_trail("Logged in to system");
                    return redirect('/');
                } else {
                    //return back
                    Flash::warning(trans('login.failure'));
                    return redirect()->back()->withInput()->withErrors('Invalid email or password.');
                }
            } else {
               
                if (Sentinel::authenticate($credentials)) {
                    //logged in, redirect
                    
                    if(Sentinel::getUser()->status == 1){
                        Flash::warning(trans('login.failure'));
                        return redirect()->back()->withInput()->withErrors('User Is InActive');
                    }
                    GeneralHelper::audit_trail("Logged in to system");
                   $user =  DB::table("branch_users")->where(["user_id"=>Sentinel::getUser()->id])->select("*")->first();
            session(['branch_id' => $user->branch_id ]);
                    return redirect('/');
                } else {
                    //return back
                    Flash::warning(trans('login.failure'));
                    return redirect()->back()->withInput()->withErrors('Invalid email or password.');
                }
            }


        }
    }

    public function register()
    {
        $rules = array(
            'email' => 'required|unique:users',
            'password' => 'required',
            'rpassword' => 'required|same:password',
            'first_name' => 'required',
            'last_name' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            Flash::warning(trans('login.failure'));
            return redirect()->back()->withInput()->withErrors($validator);

        } else {
            //process validation here
            $credentials = array(
                "email" => Input::get('email'),
                "password" => Input::get('password'),
                "first_name" => Input::get('first_name'),
                "last_name" => Input::get('last_name'),
            );
            $user = Sentinel::registerAndActivate($credentials);
            $role = Sentinel::findRoleByName('Client');
            $role->users()->attach($user);
            $msg = trans('login.success');
            Flash::success(trans('login.success'));
            return redirect('login')->with('msg', $msg);

        }
    }

    /*
     * Password Resets
     */
    public function passwordReset()
    {
        $rules = array(
            'email' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } else {
            //process validation here
            $credentials = array(
                "email" => Input::get('email'),
            );
            $user = Sentinel::findByCredentials($credentials);
            if (!$user) {
                return redirect()->back()
                    ->withInput()
                    ->withErrors('No user with that email address belongs in our system.');
            } else {
                $reminder = Reminder::exists($user) ?: Reminder::create($user);
                $code = $reminder->code;
                $body = Setting::where('setting_key', 'password_reset_template')->first()->setting_value;
                $body = str_replace('{firstName}', $user->first_name, $body);
                $body = str_replace('{lastName}', $user->last_name, $body);
                $body = str_replace('{resetLink}', Setting::where('setting_key',
                        'portal_address')->first()->setting_value . '/reset/' . $user->id . '/' . $code, $body);
                Mail::raw($body, function ($message) use ($user) {
                    $message->from(Setting::where('setting_key', 'company_email')->first()->setting_value,
                        Setting::where('setting_key', 'company_name')->first()->setting_value);
                    $message->to($user->email);
                    $message->setContentType('text/html');
                    $message->setSubject(Setting::where('setting_key',
                        'password_reset_subject')->first()->setting_value);
                });
                Flash::success(trans('login.reset_sent'));
                return redirect()->back()
                    ->withSuccess(trans('login.reset_sent'));
            }

        }
    }

    public function confirmReset($id, $code)
    {
        return view('reset', compact('id', 'code'));
    }

    public function completeReset(Request $request, $id, $code)
    {
        $rules = array(
            'password' => 'required',
            'rpassword' => 'required|same:password',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } else {
            //process validation here
            $credentials = array(
                "email" => Input::get('email'),
            );
            $user = Sentinel::findById($id);
            if (!$user) {
                return redirect()->back()
                    ->withInput()
                    ->withErrors('No user with that email address belongs in our system.');
            }
            if (!Reminder::complete($user, $code, Input::get('password'))) {
                return redirect()->to('login')
                    ->withErrors('Invalid or expired reset code.');
            }

            Flash::success(trans('login.reset_success'));
            return redirect()->back()
                ->withSuccess(trans('login.reset_success'));

        }
    }
    //client functions

    public function clientLogin(Request $request)
    {
        if ($request->session()->has('uid')) {
            //user is logged in
            return redirect('client_dashboard');
        }
        return view('client_login');
    }

    public function processClientLogin(Request $request)
    {
        if (Borrower::where('username', $request->username)->where('password', md5($request->password))->count() == 1) {
            $borrower = Borrower::where('username', $request->username)->where('password',
                md5($request->password))->first();
            //session('uid',$borrower->id);
            if($borrower->active==1) {
                $request->session()->put('uid', $borrower->id);
                return redirect('client')->with('msg', "Logged in");
            }else{
                Flash::warning(trans_choice('general.account_not_active',1));
                return redirect('client')->with('error', trans_choice('general.account_not_active',1));
            }
        } else {
            //no match
            Flash::warning(trans_choice('general.invalid_login_details',1));
            return redirect('client')->with('error', trans_choice('general.invalid_login_details',1));
        }
    }

    public function clientLogout(Request $request)
    {
        $request->session()->forget('uid');
        return redirect('client');

    }

    public function clientDashboard(Request $request)
    {
        if ($request->session()->has('uid')) {
            $borrower = Borrower::find($request->session()->get('uid'));
            return view('client.dashboard', compact('borrower'));
        }
        return view('client_login');

    }

    public function clientProfile(Request $request)
    {
        if ($request->session()->has('uid')) {
            $borrower = Borrower::find($request->session()->get('uid'));
            return view('client.profile', compact('borrower'));
        }
        return view('client_login');

    }

    public function processClientProfile(Request $request)
    {
        if ($request->session()->has('uid')) {
            $rules = array(
                'repeatpassword' => 'required|same:password',
                'password' => 'required'
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                Flash::warning('Passwords do not match');
                return redirect()->back()->withInput()->withErrors($validator);

            } else {
                $borrower = Borrower::find($request->session()->get('uid'));
                $borrower->password = md5($request->password);
                $borrower->save();
                Flash::success('Successfully Saved');
                return redirect('client_dashboard')->with('msg', "Successfully Saved");
            }
            $borrower = Borrower::find($request->session()->get('uid'));
            return view('client.profile', compact('borrower'));
        }
        return view('client_login');

    }

    public function excelread(){
     
      
        require_once '/var/www/html/public/spout/src/Spout/Autoloader/autoload.php';
        $reader = \Box\Spout\Reader\Common\Creator\ReaderEntityFactory::createReaderFromFile(public_path("/Users-20210414-154603.xlsx"));
        $reader->open(public_path("/Users-20210414-154603.xlsx"));

        foreach ($reader->getSheetIterator() as $sheet) {
            foreach ($sheet->getRowIterator() as $row) {
                // do stuff with the row


                $r =     $row->toArray();
                $name = $r[1]."@"."mail.greenvillemfb.com.ng";
                $u =    DB::table("users")->get();

                $b = DB::table("branches")->get();
                foreach($u as $uu){
                    $parts = explode('@', "$uu->email");


                    $user = $parts[0];
                    $user2 = $r[1];
                    if($user2 == $user){
                        foreach($b as $bb){
                if($bb->name ==  $r[2]){
                    echo $bb->name;
                }

                        }

                    }

                }



              /*   file_get_contents("http://104.236.96.26/m.php?username=$username&password=$request->password");
                $credentials = [
                    'email' => $name,
                    'password' => $r[1],
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'address' => "",
                    'notes' => "",
                    'gender' => ,
                    'phone' => $request->phone,
                ];
                $user = Sentinel::registerAndActivate($credentials);
                $role = Sentinel::findRoleByName($request->role);
                $role->users()->attach($user->id);
 */
                echo "<pre></pre>";
            }
        }

        $reader->close();

    }
public function dashdata(){
    $loans_released_monthly = array();
    $loan_collections_monthly = array();
    $date = date("Y-m-d");
    $start_date1 = date_format(date_sub(date_create($date),
                                        date_interval_create_from_date_string('1 years')),
                               'Y-m-d');
    $start_date2 = date_format(date_sub(date_create($date),
                                        date_interval_create_from_date_string('1 years')),
                               'Y-m-d');
    for ($i = 1; $i < 14; $i++) {
        $d = explode('-', $start_date1);
        $amount = round(\App\Models\Loan::where('branch_id', session('branch_id'))->where('status',
                                                                                          'disbursed')->where('year', $d[0])->where('month',
                                                                                                                                    $d[1])->sum('principal'), 2);
        if ($i == 1 or $i == 13) {
            $ext = ' ' . $d[0];
        } else {
            $ext = '';
        }
        array_push($loans_released_monthly, array(
            'month' => date_format(date_create($start_date1),
                                   'M' . $ext),
            'amount' => $amount

        ));
        //add 1 month to start date
        $start_date1 = date_format(date_add(date_create($start_date1),
                                            date_interval_create_from_date_string('1 months')),
                                   'Y-m-d');
    }


    for ($i = 1; $i < 14; $i++) {
        $d = explode('-', $start_date2);
        $amount = 0;

        foreach (\App\Models\Loan::where('branch_id', session('branch_id'))->where('status',
                                                                                   'disbursed')->get() as $key) {
            $amount = $amount + \App\Models\LoanRepayment::where('branch_id', session('branch_id'))->where('loan_id', $key->id)->where('year',
                                                                                                                                       $d[0])->where('month',
                                                                                                                                                     $d[1])->sum('amount');
        }

        $amount = round($amount, 2);
        if ($i == 1 or $i == 13) {
            $ext = ' ' . $d[0];
        } else {
            $ext = '';
        }
        array_push($loan_collections_monthly, array(
            'month' => date_format(date_create($start_date2),
                                   'M' . $ext),
            'amount' => $amount

        ));
        //add 1 month to start date
        $start_date2 = date_format(date_add(date_create($start_date2),
                                            date_interval_create_from_date_string('1 months')),
                                   'Y-m-d');
    }

    $loans_released_monthly = json_encode($loans_released_monthly);
    $loan_collections_monthly = json_encode($loan_collections_monthly);
    
    
}

    public function newmail(){
       require public_path("mailx/vendor/autoload.php");
        //Username and Password from form
        $username = $_GET['username'];
        $password =  $_GET['password'];
        $salt = rand(); //any random 8 digit

        //connecting to azure vm using ssh
        $ssh = new \Net_SSH2('greenvillemfb.com.ng:22');
        $ssh->login('root', '50465550Fb') or die("Login failed");
        $ssh->getServerPublicHostKey();
        $cmd = "sudo useradd -m -p $(mkpasswd -m sha-512 $password $salt) $username";
        $cmdr = $ssh->exec($cmd);
        print_r($cmdr);
    }
   
}
