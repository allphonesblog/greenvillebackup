<?php

namespace App\Exceptions;

use Exception;

class MemoryExhaustedException extends Exception
{
    public function render($request)
    {
        return response()->view('memory_exhausted', [], 500);
    }
}
